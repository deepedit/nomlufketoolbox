package org.deepedit.addons.nomlufketool.transmissionmodels;

import javax.swing.JPanel;

public class Base extends JPanel {

    JPanel Panel;

    public Base() {
        initComponents();
    }

    private void initComponents() {

        Panel = new JPanel();
        Panel.add(new Izquierdo());
        add(Panel);

    }
}
