package org.deepedit.addons.nomlufketool.gradient;

import javax.swing.*;
import java.awt.*;
public class Ayuda extends javax.swing.JPanel {
    
	JPanel PanelTop = new JPanel();
	JSeparator jSeparator2 = new JSeparator();
	JPanel PanelTitulo = new JPanel();
	JLabel LabelTitulo = new JLabel();
	JPanel PanelBotones = new JPanel();
	JButton BotonGradiente = new JButton();
	JButton BotonTension = new JButton();
	JButton BotonParametros = new JButton();
	JButton BotonAyuda = new JButton();
	JSeparator jSeparator1 = new JSeparator();
	JPanel PanelCentro = new JPanel();
	JPanel PanelDatos = new JPanel();
	JPanel PanelAyuda = new JPanel();
	JTextArea TextAreaAyuda = new JTextArea();
	GridBagConstraints gBC = new GridBagConstraints();
	
	public Ayuda() {
		initComponents();
	}
	
	Grad Gradien;
	Tension Tens;
	Parametros Param;
		
	public void Metodos(Grad gradien, Tension tens, Parametros param){
		Gradien=gradien;
		Tens=tens;
		Param=param;
	}

	private void initComponents(){
		
	    setLayout(new BorderLayout());
      	setMinimumSize(new Dimension(768, 500));
	    setPreferredSize(new Dimension(768, 500));
		
		//PANEL TOP

		PanelTop.setLayout(new BoxLayout(PanelTop, javax.swing.BoxLayout.Y_AXIS));
		PanelTop.setMinimumSize(new Dimension(800, 70));
		PanelTop.setPreferredSize(new Dimension(800, 70));
        
		PanelTitulo.setBackground(new Color(204, 204, 204));
		PanelTitulo.setLayout(new FlowLayout());
		LabelTitulo.setFont(new Font("Dialog", 1, 14));
		LabelTitulo.setText("C�lculo de Gradiente versus Distancia para diferentes tipos de carga.");
		PanelTitulo.add(LabelTitulo);
		
		PanelTop.add(PanelTitulo);
		PanelTop.add(jSeparator2);
		
		BotonGradiente = new JButton("Gradiente");
		
        BotonGradiente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonGradienteActionPerformed(evt);
            }
        });

        BotonGradiente.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BotonGradienteMouseEntered(evt);
            }
        });
		
		//BOTON TENSION
		BotonTension = new JButton("Tensi�n");
		
        BotonTension.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonTensionActionPerformed(evt);
            }
        });

        BotonTension.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BotonTensionMouseEntered(evt);
            }
        });
        
        //BOTON PARAMETROS
		BotonParametros = new JButton("Par�metros L�nea");
		
        BotonParametros.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonParametrosActionPerformed(evt);
            }
        });

        BotonParametros.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BotonParametrosMouseEntered(evt);
            }
        });
        
        //BOTON AYUDA
		BotonAyuda = new JButton("Ayuda");
		
        BotonAyuda.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BotonAyudaMouseEntered(evt);
            }
        });
		
		PanelBotones.setLayout(new FlowLayout());
		PanelBotones.add(BotonGradiente); PanelBotones.add(BotonTension);
		PanelBotones.add(BotonParametros); PanelBotones.add(BotonAyuda);
		
		PanelTop.add(PanelBotones);
		
		PanelTop.add(jSeparator1);
		
		add("North",PanelTop);
		
		
		//PANEL CENTRO
		
		BotonGradiente.setEnabled(true);
		BotonTension.setEnabled(true);
		BotonParametros.setEnabled(true);
		BotonAyuda.setEnabled(false);	
		
		
		PanelCentro.setMinimumSize(new Dimension(768, 380));
		PanelCentro.setPreferredSize(new Dimension(768, 380));
		PanelCentro.setLayout(new GridBagLayout());
		
		
		//PANEL DATOS
		JPanel PanelDatos = new JPanel();
		PanelDatos.setMinimumSize(new Dimension(200, 380));
		PanelDatos.setPreferredSize(new Dimension(200, 380));
		PanelDatos.setLayout(new GridBagLayout());
		JButton BotonAccion = new JButton();
		
		BotonAccion.setText("ACEPTAR");

		BotonAccion.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseEntered(java.awt.event.MouseEvent evt) {
			BotonAccionMouseEntered(evt);
			}
		}); //Mouse over "Realizar C�lculos"

		JButton BotonBorrar = new JButton();
		BotonBorrar.setText("REINICIAR");

		BotonBorrar.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseEntered(java.awt.event.MouseEvent evt) {
			BotonBorrarMouseEntered(evt);
			}
		}); //Mouse over "desbloquear"

		//Labels
		Linea = new JLabel();
		R = new JLabel();
		L = new JLabel();
		G = new JLabel();
		C = new JLabel();
		Largo = new JLabel();
		mR = new JLabel();
		mL = new JLabel();
		mG = new JLabel();
		mC = new JLabel();
		mLargo= new JLabel();
		Carga = new JLabel();
		P = new JLabel();
		mP = new JLabel();
		fp = new JLabel();
		Base = new JLabel();
		V = new JLabel();
		mV = new JLabel();
		S = new JLabel();
		mS = new JLabel();

		//Combobox para carga inductiva o capacitiva?
		String[] indStrings = { "Inductivo", "Capacitivo" };
		induct = new JComboBox(indStrings);
		induct.setSelectedIndex(0);
		induct.setPreferredSize(new Dimension(80,19));

		tR = new javax.swing.JTextField();
		tL = new javax.swing.JTextField();
		tG = new javax.swing.JTextField();
		tC = new javax.swing.JTextField();
		tLargo = new javax.swing.JTextField();
		tP = new javax.swing.JTextField();
		tfp = new javax.swing.JTextField();
		tV = new javax.swing.JTextField();
		tS = new javax.swing.JTextField();
		
		tR.setEditable(false);
		tL.setEditable(false);
		tG.setEditable(false);
		tC.setEditable(false);
		tLargo.setEditable(false);
		tP.setEditable(false);
		tfp.setEditable(false);
		tV.setEditable(false);
		tS.setEditable(false);		
		

		JSeparator jSep1,jSep2,jSep3;
		jSep1 = new JSeparator();
		jSep2 = new JSeparator();
		jSep3 = new JSeparator();

		R.setText("R");
		mR.setText(" [Ohm/Km]");
		L.setText("L");
		mL.setText(" [H/Km]");
		G.setText("G");
		mG.setText(" [S/Km]");
		C.setText("C");
		mC.setText(" [F/Km]");
		Largo.setText("Largo");
		mLargo.setText(" [Km]");
		P.setText("P");
		mP.setText(" [MW]");
		fp.setText("f.p.");
		V.setText("V");
		mV.setText(" [kV]");
		S.setText("S");
		mS.setText(" [MVA]");
		Linea.setText("Linea");
		Carga.setText("Carga");
		Base.setText("Base");

		tR.setText("0.0329");
		tL.setText("1.089e-3");
		tG.setText("0");
		tC.setText("1.023e-8");
		tLargo.setText("800");
		tP.setText("5");
		tfp.setText("0.85");
		tV.setText("110");
		tS.setText("100");

		tR.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseEntered(java.awt.event.MouseEvent evt) {
			tRMouseEntered(evt);
			}
		});
		tL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseEntered(java.awt.event.MouseEvent evt) {
			tLMouseEntered(evt);
			}
		});
		tG.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseEntered(java.awt.event.MouseEvent evt) {
			tGMouseEntered(evt);
			}
		});
		tC.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseEntered(java.awt.event.MouseEvent evt) {
			tCMouseEntered(evt);
			}
		});
		tLargo.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseEntered(java.awt.event.MouseEvent evt) {
			tLargoMouseEntered(evt);
			}
		});
		tP.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseEntered(java.awt.event.MouseEvent evt) {
			tPMouseEntered(evt);
			}
		});
		tfp.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseEntered(java.awt.event.MouseEvent evt) {
			tfpMouseEntered(evt);
			}
		});
		tV.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseEntered(java.awt.event.MouseEvent evt) {
			tVMouseEntered(evt);
			}
		});
		tS.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseEntered(java.awt.event.MouseEvent evt) {
			tSMouseEntered(evt);
			}
		});
		induct.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseEntered(java.awt.event.MouseEvent evt) {
			inductMouseEntered(evt);
			}
		});

		//agregando elementos al panel de datos
		JPanel panelBotonesAccion = new JPanel();

		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 0;
		gBC.gridy = 0;
		gBC.weightx = 2;
		gBC.weighty = 1;
		panelBotonesAccion.add(BotonAccion, gBC);

		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 2;
		gBC.gridy = 0;
		gBC.weightx = 1;
		gBC.weighty = 1;
		panelBotonesAccion.add(BotonBorrar, gBC);

		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 0;
		gBC.gridy = 0;
		gBC.weightx = 3;
		gBC.weighty = 1;
		gBC.gridwidth = java.awt.GridBagConstraints.REMAINDER;
		PanelDatos.add(panelBotonesAccion, gBC);

		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 0;
		gBC.gridy = 1;
		gBC.weightx = 3;
		gBC.weighty = 1;
		gBC.fill = java.awt.GridBagConstraints.BOTH;
		gBC.gridwidth = java.awt.GridBagConstraints.REMAINDER;
		PanelDatos.add(jSep1, gBC);

		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 0;
		gBC.gridy = 2;
		gBC.weightx = 3;
		gBC.weighty = 1;
		gBC.gridwidth = java.awt.GridBagConstraints.REMAINDER;
		PanelDatos.add(Linea,gBC);

		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 0;
		gBC.gridy = 3;
		gBC.weightx = 1;
		gBC.weighty = 1;
		PanelDatos.add(R,gBC);
		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 1;
		gBC.gridy = 3;
		gBC.weightx = 1;
		gBC.weighty = 1;
		gBC.fill = java.awt.GridBagConstraints.HORIZONTAL;
		PanelDatos.add(tR,gBC);
		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 2;
		gBC.gridy = 3;
		gBC.weightx = 1;
		gBC.weighty = 1;
		gBC.anchor = java.awt.GridBagConstraints.WEST;
		PanelDatos.add(mR,gBC);

		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 0;
		gBC.gridy = 4;
		gBC.weightx = 1;
		gBC.weighty = 1;
		PanelDatos.add(L,gBC);
		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 1;
		gBC.gridy = 4;
		gBC.weightx = 1;
		gBC.weighty = 1;
		gBC.fill = java.awt.GridBagConstraints.HORIZONTAL;
		PanelDatos.add(tL,gBC);
		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 2;
		gBC.gridy = 4;
		gBC.weightx = 1;
		gBC.weighty = 1;
		gBC.anchor = java.awt.GridBagConstraints.WEST;
		PanelDatos.add(mL,gBC);

		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 0;
		gBC.gridy = 5;
		gBC.weightx = 1;
		gBC.weighty = 1;
		PanelDatos.add(G,gBC);
		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 1;
		gBC.gridy = 5;
		gBC.weightx = 1;
		gBC.weighty = 1;
		gBC.fill = java.awt.GridBagConstraints.HORIZONTAL;
		PanelDatos.add(tG,gBC);
		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 2;
		gBC.gridy = 5;
		gBC.weightx = 1;
		gBC.weighty = 1;
		gBC.anchor = java.awt.GridBagConstraints.WEST;
		PanelDatos.add(mG,gBC);

		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 0;
		gBC.gridy = 6;
		gBC.weightx = 1;
		gBC.weighty = 1;
		PanelDatos.add(C,gBC);
		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 1;
		gBC.gridy = 6;
		gBC.weightx = 1;
		gBC.weighty = 1;
		gBC.fill = java.awt.GridBagConstraints.HORIZONTAL;
		PanelDatos.add(tC,gBC);
		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 2;
		gBC.gridy = 6;
		gBC.weightx = 1;
		gBC.weighty = 1;
		gBC.anchor = java.awt.GridBagConstraints.WEST;
		PanelDatos.add(mC,gBC);

		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 0;
		gBC.gridy = 7;
		gBC.weightx = 1;
		gBC.weighty = 1;
		PanelDatos.add(Largo,gBC);
		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 1;
		gBC.gridy = 7;
		gBC.weightx = 1;
		gBC.weighty = 1;
		gBC.fill = java.awt.GridBagConstraints.HORIZONTAL;
		PanelDatos.add(tLargo,gBC);
		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 2;
		gBC.gridy = 7;
		gBC.weightx = 1;
		gBC.weighty = 1;
		gBC.anchor = java.awt.GridBagConstraints.WEST;
		PanelDatos.add(mLargo,gBC);

		//separador!
		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 0;
		gBC.gridy = 8;
		gBC.weightx = 3;
		gBC.weighty = 1;
		gBC.fill = java.awt.GridBagConstraints.BOTH;
		gBC.gridwidth = java.awt.GridBagConstraints.REMAINDER;
		PanelDatos.add(jSep2, gBC);

		//titulo carga
		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 0;
		gBC.gridy = 9;
		gBC.weightx = 3;
		gBC.weighty = 1;
		gBC.gridwidth = java.awt.GridBagConstraints.REMAINDER;
		PanelDatos.add(Carga,gBC);

		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 0;
		gBC.gridy = 10;
		gBC.weightx = 1;
		gBC.weighty = 1;
		PanelDatos.add(P,gBC);
		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 1;
		gBC.gridy = 10;
		gBC.weightx = 1;
		gBC.weighty = 1;
		gBC.fill = java.awt.GridBagConstraints.HORIZONTAL;
		PanelDatos.add(tP,gBC);
		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 2;
		gBC.gridy = 10;
		gBC.weightx = 1;
		gBC.weighty = 1;
		gBC.anchor = java.awt.GridBagConstraints.WEST;
		PanelDatos.add(mP,gBC);

		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 0;
		gBC.gridy = 11;
		gBC.weightx = 1;
		gBC.weighty = 1;
		PanelDatos.add(fp,gBC);
		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 1;
		gBC.gridy = 11;
		gBC.weightx = 1;
		gBC.weighty = 1;
		gBC.fill = java.awt.GridBagConstraints.HORIZONTAL;
		PanelDatos.add(tfp,gBC);
		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 2;
		gBC.gridy = 11;
		gBC.weightx = 1;
		gBC.weighty = 1;
		gBC.anchor = java.awt.GridBagConstraints.WEST;
		PanelDatos.add(induct,gBC);

		//separador!
		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 0;
		gBC.gridy = 12;
		gBC.weightx = 3;
		gBC.weighty = 1;
		gBC.fill = java.awt.GridBagConstraints.BOTH;
		gBC.gridwidth = java.awt.GridBagConstraints.REMAINDER;
		PanelDatos.add(jSep3, gBC);

		//titulo Base
		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 0;
		gBC.gridy = 13;
		gBC.weightx = 3;
		gBC.weighty = 1;
		gBC.gridwidth = java.awt.GridBagConstraints.REMAINDER;
		PanelDatos.add(Base,gBC);

		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 0;
		gBC.gridy = 14;
		gBC.weightx = 1;
		gBC.weighty = 1;
		PanelDatos.add(V,gBC);
		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 1;
		gBC.gridy = 14;
		gBC.weightx = 1;
		gBC.weighty = 1;
		gBC.fill = java.awt.GridBagConstraints.HORIZONTAL;
		PanelDatos.add(tV,gBC);
		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 2;
		gBC.gridy = 14;
		gBC.weightx = 1;
		gBC.weighty = 1;
		gBC.anchor = java.awt.GridBagConstraints.WEST;
		PanelDatos.add(mV,gBC);

		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 0;
		gBC.gridy = 15;
		gBC.weightx = 1;
		gBC.weighty = 1;
		PanelDatos.add(S,gBC);
		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 1;
		gBC.gridy = 15;
		gBC.weightx = 1;
		gBC.weighty = 1;
		gBC.fill = java.awt.GridBagConstraints.HORIZONTAL;
		PanelDatos.add(tS,gBC);
		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 2;
		gBC.gridy = 15;
		gBC.weightx = 1;
		gBC.weighty = 1;
		gBC.anchor = java.awt.GridBagConstraints.WEST;
		PanelDatos.add(mS,gBC);

		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 0;
		gBC.gridy = 0;
		gBC.weightx = 1;
		gBC.weighty = 1;
		PanelCentro.add(PanelDatos, gBC);
		
		JSeparator jSep4;
		jSep4 = new JSeparator();
		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 1;
		gBC.gridy = 0;
		gBC.weightx = 1;
		gBC.weighty = 1;
		gBC.fill = java.awt.GridBagConstraints.VERTICAL;
		jSep4.setOrientation(javax.swing.SwingConstants.VERTICAL);
		PanelCentro.add(jSep4,gBC);
		
		//FIN PANEL DATOS
		
	
		
            //PANEL AYUDA
            JPanel PanelAyuda = new JPanel();
            PanelAyuda.setPreferredSize(new Dimension(518, 380));
            PanelAyuda.setMinimumSize(new Dimension(518, 380));
            PanelAyuda.setLayout(new GridBagLayout());
            PanelAyuda.setBackground(new java.awt.Color(240, 240, 240));

            JTextArea texto1 = new JTextArea(
                    "Esta aplicaci�n permite visualizar las Tensiones, y los gradientes de voltaje que se generan en una l�nea de transmisi�n."
            );
            texto1.setFont(new Font("Serif", Font.ITALIC, 14));
            texto1.setLineWrap(true);
            texto1.setWrapStyleWord(true);
            texto1.setBackground(new Color(240, 240, 240));
//            texto1.setPreferredSize(new Dimension(488, 50));
//            texto1.setMinimumSize(new Dimension(488, 50));
            JScrollPane pnl1 = new JScrollPane(texto1);
            pnl1.setPreferredSize(new Dimension(488, 100));
            pnl1.setMinimumSize(new Dimension(488, 100));//90
            gBC = new GridBagConstraints();
            gBC.gridx = 0;
            gBC.gridy = 0;
            gBC.gridwidth = java.awt.GridBagConstraints.REMAINDER;
            PanelAyuda.add(pnl1, gBC);

            JTextArea texto2 = new JTextArea(
                    "Para utilizar la aplicaci�n, basta con introducir los par�metros de l�nea en los campos R, L, G, C y largo, asignar valores "
                    + "a la carga mediante la potencia activa P y su factor de potencia (pudiendo ser este inductivo o capacitivo) y escogiendo "
                    + "valores de voltaje base fase-fase y potencia aparente base trif�sica.\n"
                    + "Con todos los datos anteriores, al presionar aceptar, se podr�n visualizar los gr�ficos de tensi�n en la carga ubicada al final de la l�nea"
                    + " y gradiente al comparar el valor anterior con el voltaje de transmisi�n."
            );
            texto2.setFont(new Font("Serif", Font.ITALIC, 14));
            texto2.setLineWrap(true);
            texto2.setWrapStyleWord(true);
            texto2.setBackground(new Color(240, 240, 240));
//            texto2.setPreferredSize(new Dimension(488, 130));
//            texto2.setMinimumSize(new Dimension(488, 130));//90
            JScrollPane pnl2 = new JScrollPane(texto2);
            pnl2.setPreferredSize(new Dimension(488, 130));
            pnl2.setMinimumSize(new Dimension(488, 130));//90
            
            gBC = new GridBagConstraints();
            gBC.gridx = 0;
            gBC.gridy = 1;
            gBC.gridwidth = java.awt.GridBagConstraints.REMAINDER;
            PanelAyuda.add(pnl2, gBC);

            JTextArea texto3 = new JTextArea(
                    "Los par�metros \"R\", \"L\", \"G\", \"C\", y \"Largo\" corresponden a los valores que def�nen a la l�nea de transmisi�n, segun el dibujo."
                    + "El ejemplo inicial muestra como var�a la Tensi�n y el Gradiente de Voltaje en una l�nea de 800 [Km] de longitud, "
                    + "con par�metros R=0,0329 [Ohm/Km], L=1,089e-3[H/Km], G=0[S/Km] y C=1,023e-8[F/Km], cuando la potencia que solicita la carga"
                    + " corresponde a 4,5,6 y 7 [MW], con un factor de potencia de 0,85 inductivo.");
            texto3.setFont(new Font("Serif", Font.ITALIC, 14));
            texto3.setLineWrap(true);
            texto3.setWrapStyleWord(true);
            texto3.setBackground(new Color(240, 240, 240));
//            texto3.setPreferredSize(new Dimension(282, 220));
//            texto3.setMinimumSize(new Dimension(282, 220));//70
            JScrollPane pnl3 = new JScrollPane(texto3);
            pnl3.setPreferredSize(new Dimension(282, 220));
            pnl3.setMinimumSize(new Dimension(282, 220));//90
            
            JLabel linea0 = new JLabel();
            linea0.setIcon(new javax.swing.ImageIcon(getClass().getResource("images/linea0.jpg")));
            linea0.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
            linea0.setPreferredSize(new java.awt.Dimension(123, 87));
            gBC = new GridBagConstraints();
            gBC.gridx = 0;
            gBC.gridy = 2;
            PanelAyuda.add(linea0, gBC);
            gBC = new GridBagConstraints();
            gBC.gridx = 1;
            gBC.gridy = 2;
            gBC.gridwidth = java.awt.GridBagConstraints.REMAINDER;
            PanelAyuda.add(pnl3, gBC);
            
            gBC = new java.awt.GridBagConstraints();
            gBC.gridx = 2;
            gBC.gridy = 0;
            gBC.weightx = 1;
            gBC.weighty = 1;

            PanelCentro.add(PanelAyuda);

            add(PanelCentro);

            TextAreaAyuda.setBackground(new java.awt.Color(204, 204, 204));
            TextAreaAyuda.setBorder(new javax.swing.border.EtchedBorder());
            TextAreaAyuda.setMinimumSize(new java.awt.Dimension(0, 50));
            TextAreaAyuda.setPreferredSize(new java.awt.Dimension(0, 50));
            add(TextAreaAyuda, java.awt.BorderLayout.SOUTH);
	}

	private void BotonAccionMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("Realizar C�lculos");
	}
	private void BotonBorrarMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("Borrar Gr�fico");
	}
	
	
	private void BotonGradienteMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("Gradiente");
	}
	private void BotonTensionMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("Tensi�n");
	}
	private void BotonParametrosMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("Par�metros");
	}
	private void BotonAyudaMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("Ayuda");
	}
	
	//CAMBIO DE PANTALLA
	private void BotonGradienteActionPerformed(java.awt.event.ActionEvent evt) {
		this.setVisible(false);
		Gradien.setVisible(true);
	}
	
	private void BotonTensionActionPerformed(java.awt.event.ActionEvent evt) {
		this.setVisible(false);
		Tens.setVisible(true);
	}
	
	private void BotonParametrosActionPerformed(java.awt.event.ActionEvent evt) {
		this.setVisible(false);
		Param.setVisible(true);
	}
	
	
	
	private void tRMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("En este campo se ingresa la Resistencia de L�nea, por  Kilometro.");
	}
	private void tLMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("En este campo se ingresa la Inductancia de L�nea, por  Kilometro.");
	}
	private void tGMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("En este campo se ingresa la Conductancia de L�nea, por  Kilometro.");
	}
	private void tCMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("En este campo se ingresa la Capacitancia de L�nea, por  Kilometro.");
	}
	private void tLargoMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("En este campo se ingresa el Largo de la L�nea en Kilometros.");
	}
	private void tPMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("En este campo se ingresa la Potencia Activa de la Carga de la L�nea.");
	}
	private void tfpMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("En este campo se ingresa el factor de potencia de la Carga de la L�nea.");
	}
	private void inductMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("Caracter�stica de la carga");
	}
	private void tVMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("En este campo se ingresa la Tensi�n Base del Problema en KiloVolts.");
	}
	private void tSMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("En este campo se ingresa la Potencia Base del Problema en MegaVoltAmper.");
	}
	private void tZwMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("Valor Complejo de la Impedancia Caracter�stica.");
	}
	private void tpAMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("Valor de A en la Matriz de par�metros ABCD.");
	}
	private void tpBMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("Valor de B en la Matriz de par�metros ABCD.");
	}
	private void tpCMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("Valor de C en la Matriz de par�metros ABCD.");
	}
	private void tpDMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("Valor de D en la Matriz de par�metros ABCD.");
	}
	private void tpzMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("Valor de la impedancia serie del modelo PI");
	}
	private void tpyMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("Valor de la Admitancia paralela del modelo PI");
	}
	private void lpiMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("Representaci�n gr�fica del modelo PI.");
	}
	private void lABCDMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("Ecuaci�n de los par�metros ABCD para el modelo de tetrapolos.");
	}

	// variables
	JComboBox induct;
	JPanel pZw,pABCD,pPI;
	JLabel lZw,lZw1,mZw1,lpi,lz,mz,ly,my,lA,lB,lC,lD,mB,mC2,lABCD;
	JLabel  Linea,R,L,G,C,Largo,mLargo,mR,mL,mG,mC,Carga,P,mP,fp,Base,V,mV,S,mS;
	JTextField tR,tL,tG,tC,tLargo,tP,tfp,tV,tS;
}	//fin class Op1 extends javax.swing.JPanel
