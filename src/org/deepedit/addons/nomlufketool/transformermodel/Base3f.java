package org.deepedit.addons.nomlufketool.transformermodel;

/** Base trifasica.
 * Voltajes fase-fase, Corrientes de linea, Potencia trifasica.
 */
public class Base3f{
	private double S, V;

	public Base3f(double S, double V){
		this.S = S;
		this.V = V;
	}

	public double getS(){ return this.S; }

	public double getV(){ return this.V; }

	public double getZ(){ return this.V*this.V/this.S; }

	public double getI(){ return this.S/(Math.sqrt(3)*this.V); }

	public void setS(double S){ this.S = S; }

	public void setV(double V){ this.V = V; }

	public Object clone(){
		return new Base3f(S,V);
	}
}