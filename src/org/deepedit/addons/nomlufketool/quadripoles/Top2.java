package org.deepedit.addons.nomlufketool.quadripoles;

import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;

public class Top2 extends JPanel {

	JPanel PanelTop;
	JPanel PanelTitulo;
	JLabel LabelTitulo;
	JSeparator jSeparator1,jSeparator2;
	JPanel PanelBotones;
	JButton BotonAplicacion,BotonAyuda;
	TetrapolosCalc TC;
	Ayuda33 Ayuda;

//	public Top2(int i){
//		initComponents(i);
//	}
        
        public Top2(TetrapolosCalc tc,Ayuda33 ayuda, int i) {
            initComponents(i);
            Metodos(tc,ayuda);
        }
        

	public void Metodos(TetrapolosCalc tc,Ayuda33 ayuda){
		TC=tc;
		Ayuda=ayuda;
	}

	private void initComponents(int i){
	
		PanelTop=new JPanel();
		PanelTitulo=new JPanel();
		LabelTitulo=new JLabel();
		jSeparator1=new JSeparator();
		jSeparator2=new JSeparator();
		PanelBotones=new JPanel();
		BotonAplicacion=new JButton();
		BotonAyuda=new JButton();
	
		setLayout(new BorderLayout());
	
		PanelTop.setLayout(new javax.swing.BoxLayout(PanelTop, javax.swing.BoxLayout.Y_AXIS));
		PanelTop.setMinimumSize(new java.awt.Dimension(768, 70));
		PanelTop.setPreferredSize(new java.awt.Dimension(768, 70));
		PanelTitulo.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER));
		LabelTitulo.setFont(new java.awt.Font("Dialog", 1, 14));
	    PanelTitulo.setBackground(new java.awt.Color(204,204,204));
		if(i==4) LabelTitulo.setText("Ayuda");
	
		PanelTitulo.add(LabelTitulo);
		PanelTop.add(PanelTitulo);
		PanelTop.add(jSeparator2);
	    PanelBotones.setLayout(new java.awt.GridLayout(1,2,0,5));
	
		BotonAplicacion.setText("Aplicación");
		PanelBotones.add(BotonAplicacion);
	
		BotonAplicacion.addActionListener(new java.awt.event.ActionListener() {
	       public void actionPerformed(java.awt.event.ActionEvent evt) {
	       BotonAplicacionActionPerformed(evt);}});
	
		BotonAyuda.setText("Ayuda");
		if(i==4)BotonAyuda.setEnabled(false);
		PanelBotones.add(BotonAyuda);
		PanelTop.add(PanelBotones);
		PanelTop.add(jSeparator1);
		add(PanelTop, java.awt.BorderLayout.NORTH);
	}//initComponents

	private void BotonAplicacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonOp2ActionPerformed
//		System.out.println("HOLAAAAAA!!!!");
		Ayuda.setVisible(false);
		TC.setVisible(true);
		TC.repaint();
	}//GEN-LAST:event_BotonOp2ActionPerformed
}