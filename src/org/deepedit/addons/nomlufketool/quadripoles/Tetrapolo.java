package org.deepedit.addons.nomlufketool.quadripoles;

//package Tetrapolos;

import org.deepedit.addons.nomlufketool.utils.Complex;

/*
 * Tetrapolo.java
 *
 * @author AtalayA
 */
public class Tetrapolo {
    
    public Complex A;
    public Complex B;
    public Complex C;
    public Complex D;
    
    /** Creates a new instance of Tetrapolo */
    public Tetrapolo() {
        A = new Complex();
        B = new Complex();
        C = new Complex();
        D = new Complex();
    }

    public Tetrapolo(Complex a, Complex b, Complex c, Complex d) {
        A = a;
        B = b;
        C = c;
        D = d;
    }
 
    public Tetrapolo(double a, double b, double c, double d) {
        A = new Complex(a, 0);
        B = new Complex(b, 0);
        C = new Complex(c, 0);
        D = new Complex(d, 0);
    }
 
// 3 Formas de Obtener V1.
    public Complex getV1_cV2I2(Complex v2, Complex i2){
       Complex v1;
       Complex temp1, temp2;
       temp1 = Complex.multiply(A, v2);
       temp2 = Complex.multiply(B, i2);
       v1 = Complex.add(temp1, temp2);
       return v1;        
    }
  
    public Complex getV1_cV2I1(Complex v2, Complex i1){
       Complex v1;
       Complex temp1, temp2;
       temp1 = Complex.multiply(B,i1);
       temp2 = Complex.add(v2,temp1);
       v1 = Complex.divide(temp2, D);
       return v1;        
    }

    public Complex getV1_cI1I2(Complex i1, Complex i2){
       Complex v1;
       Complex temp1, temp2;
       temp1 = Complex.multiply(A, i1);
       temp2 = Complex.subtract(temp1, i2);
       v1 = Complex.divide(temp2, C);
       return v1;        
    }
    
// 3 Formas de Obtener I1.      
    public Complex getI1_cV2I2(Complex v2, Complex i2){
       Complex i1;
       Complex temp1, temp2;
       temp1 = Complex.multiply(C, v2);
       temp2 = Complex.multiply(D, i2);
       i1 = Complex.add(temp1, temp2);
       return i1;        
    }
    
    public Complex getI1_cV1V2(Complex v1, Complex v2){
       Complex i1;
       Complex temp1, temp2;
       temp1 = Complex.multiply(D, v1);
       temp2 = Complex.subtract(temp1, v2);
       i1 = Complex.divide(temp2, B);
       return i1;        
    }

    public Complex getI1_cV1I2(Complex v1, Complex i2){
       Complex i1;
       Complex temp1, temp2;
       temp1 = Complex.multiply(C,v1);
       temp2 = Complex.add(temp1,i2);
       i1 = Complex.divide(temp2, A);
       return i1;        
    }
    
// 3 Formas de Obtener V2.      
    public Complex getV2_cV1I2(Complex v1, Complex i2){
       Complex v2;
       Complex temp1, temp2;
       temp1 = Complex.multiply(B, i2);
       temp2 = Complex.subtract(v1, temp1);
       v2 = Complex.divide(temp2, A);
       return v2;        
    }
  
    public Complex getV2_cI1I2(Complex i1, Complex i2){
       Complex v2;
       Complex temp1, temp2;
       temp1 = Complex.multiply(D, i2);
       temp2 = Complex.subtract(i1, temp1);
       v2 = Complex.divide(temp2, C);
       return v2;        
    }

    public Complex getV2_cV1I1(Complex v1, Complex i1){
       Complex v2;
       Complex temp1, temp2;
       temp1 = Complex.multiply(D, v1);
       temp2 = Complex.multiply(B, i1);
       v2 = Complex.subtract(temp1, temp2);
       return v2;        
    }
    
// 3 Formas de Obtener I2.      
    public Complex getI2_cV1V2(Complex v1, Complex v2){
       Complex i2;
       Complex temp1, temp2;
       temp1 = Complex.multiply(A, v2);
       temp2 = Complex.subtract(v1, temp1);
       i2 = Complex.divide(temp2, B);
       return i2;        
    }
  
    public Complex getI2_cV2I1(Complex v2, Complex i1){
       Complex i2;
       Complex temp1, temp2;
       temp1 = Complex.multiply(C, v2);
       temp2 = Complex.subtract(i1, temp1);
       i2 = Complex.divide(temp2, D);
       return i2;        
    }

    public Complex getI2_cV1I1(Complex v1, Complex i1){
       Complex i2;
       Complex temp1, temp2;
       temp1 = Complex.multiply(A, i1);
       temp2 = Complex.multiply(C, v1);
       i2 = Complex.subtract(temp1, temp2);
       return i2;        
    }  
    
 // Tetrapolos en Serie o Cascada:
    public static Tetrapolo Serie(Tetrapolo x, Tetrapolo y){
        Tetrapolo z = new Tetrapolo();
        Complex temp1,temp2;
        temp1 = Complex.multiply(x.A, y.A);
        temp2 = Complex.multiply(x.B, y.C);
        z.A = Complex.add(temp1, temp2);
        temp1 = Complex.multiply(x.A, y.B);
        temp2 = Complex.multiply(x.B, y.D);
        z.B = Complex.add(temp1, temp2);
        temp1 = Complex.multiply(x.C, y.A);
        temp2 = Complex.multiply(x.D, y.C);
        z.C = Complex.add(temp1, temp2);
        temp1 = Complex.multiply(x.D, y.D);
        temp2 = Complex.multiply(x.C, y.B);
        z.D = Complex.add(temp1, temp2);       
        return z;
    }

// Tetrapolo en Paralelo:
    public static Tetrapolo Paralelo(Tetrapolo x, Tetrapolo y){
        Tetrapolo z = new Tetrapolo();
        Complex temp1,temp2;
        temp1 = Complex.multiply(x.A, y.B);
        temp2 = Complex.multiply(x.B, y.A);
        temp1 = Complex.add(temp1, temp2);
        temp2 = Complex.add(x.B, y.B);
        z.A = Complex.divide(temp1, temp2);
        temp1 = Complex.multiply(x.B, y.B);
        temp2 = Complex.add(x.B, y.B);
        z.B = Complex.divide(temp1, temp2);
        temp1 = Complex.subtract(x.D, y.D);
        temp2 = Complex.subtract(y.A, x.A);
        temp1 = Complex.multiply(temp1, temp2);
        temp2 = Complex.add(x.B, y.B);
        temp2 = Complex.divide(temp1, temp2);
        temp1 = Complex.add(y.C,temp2);
        z.C = Complex.add(x.C, temp1);
        temp1 = Complex.multiply(x.D, y.B);
        temp2 = Complex.multiply(x.B, y.D);
        temp1 = Complex.add(temp1, temp2);
        temp2 = Complex.add(x.B, y.B);
        z.D = Complex.divide(temp1, temp2);
        return z;
    }
    
//Calculo de Potencias Complejas.    
    public Complex getS1(Complex v1, Complex i1){
        Complex S1, temp1;
        temp1 = Complex.conjugate(i1); 
        S1 = Complex.multiply(v1,temp1);
        return S1;
    }
    
    public Complex getS2(Complex v2, Complex i2){
        Complex S2, temp1;
        temp1 = Complex.conjugate(i2); 
        S2 = Complex.multiply(v2,temp1);
        return S2;
    }
    
//Calculo de Tetrapolos:
    
    static public Tetrapolo ImpedanciaZ(Complex z){
        Tetrapolo x;
        Complex temp1, temp0;
        temp1 = new Complex(1, 0);
        temp0 = new Complex(0, 0);
        x = new Tetrapolo(temp1, z, temp0, temp1);
        return x;
    }    
    
    static public Tetrapolo ImpedanciaY(Complex y){
        Tetrapolo x;
        Complex temp1, temp0;
        temp1 = new Complex(1, 0);
        temp0 = new Complex(0, 0);
        x = new Tetrapolo(temp1, temp0, y, temp1);
        return x;
    }    
    
    static public Tetrapolo CircuitoG(Complex z, Complex y){
        Tetrapolo x, t1, t2;        
        t1 = Tetrapolo.ImpedanciaY(y);
        t2 = Tetrapolo.ImpedanciaZ(z);
        x = Tetrapolo.Serie(t1, t2);
        return x;        
    }   
    
    static public Tetrapolo Circuito7(Complex z, Complex y){
        Tetrapolo x, t1, t2;        
        t1 = Tetrapolo.ImpedanciaY(z);
        t2 = Tetrapolo.ImpedanciaZ(y);
        x = Tetrapolo.Serie(t1, t2);
        return x;        
    }
    
    static public Tetrapolo CircuitoPi(Complex z, Complex y1, Complex y2){
        Tetrapolo x, t1, t2;        
        t1 = Tetrapolo.CircuitoG(z, y1);
        t2 = Tetrapolo.ImpedanciaY(y2);
        x = Tetrapolo.Serie(t1, t2);
        return x;        
    }
    
    static public Tetrapolo CircuitoT(Complex z1, Complex z2, Complex y){
        Tetrapolo x, t1, t2;        
        t1 = Tetrapolo.ImpedanciaZ(z1);
        t2 = Tetrapolo.CircuitoG(z2, y);
        x = Tetrapolo.Serie(t1, t2);
        return x;        
    }

    static public Tetrapolo CircuitoSteinmetz(Complex z1, Complex z2, Complex y1, Complex y2){
        Tetrapolo x, t1, t2;        
        t1 = Tetrapolo.CircuitoG(z1, y1);
        t2 = Tetrapolo.CircuitoPi(z2, y2, y1);
        x = Tetrapolo.Serie(t1, t2);
        return x;        
    }
}