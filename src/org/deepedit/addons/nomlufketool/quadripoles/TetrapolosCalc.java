package org.deepedit.addons.nomlufketool.quadripoles;

import javax.swing.*;
import org.deepedit.addons.nomlufketool.utils.Complex;
public class TetrapolosCalc extends javax.swing.JPanel {

    Tetrapolo ABCD;
    Complex Z1, Z2, Y1, Y2;
    Complex V1, V2, I1, I2;
    Complex S1, S2;
    double P1, P2, Q1, Q2, Phi1, Phi2;
    boolean Comb_V1, Comb_V2, Comb_I1, Comb_I2;
    boolean ABCD_Modif;
    boolean FormaRealImag, FormaAbsAng;
    int NumeroDecimales, NumeroDecimalesSpinner;

    /** Creates new form TetrapolosCalc */
    public TetrapolosCalc() {
        NumeroDecimales = 10;
        NumeroDecimalesSpinner = 4;
        initComponents();
    }
    
    Ayuda33 Ayuda;
	public void Metodos(Ayuda33 ayuda){
    	Ayuda = ayuda;
    }

    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanelTop = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jPanelBottom = new javax.swing.JPanel();
        jTextAreaAyuda = new javax.swing.JTextArea();
        jPanelCenter = new javax.swing.JPanel();
        jPanelIzq = new javax.swing.JPanel();
        jPanelModeloTop = new javax.swing.JPanel();
        jLabelModelo = new javax.swing.JLabel();
        jComboBoxModelo = new javax.swing.JComboBox();
        jPanelModeloCenter = new javax.swing.JPanel();
        jMonoModelo = new javax.swing.JLabel();
        jPanelModelobottom = new javax.swing.JPanel();
        jLabelZ1 = new javax.swing.JLabel();
        jTextFieldY1Real = new javax.swing.JTextField();
        jLabelY1 = new javax.swing.JLabel();
        jTextFieldY2Imag = new javax.swing.JTextField();
        jLabelZ2 = new javax.swing.JLabel();
        jTextFieldZ1Real = new javax.swing.JTextField();
        jTextFieldZ2Imag = new javax.swing.JTextField();
        jButtonActualizarModelo = new javax.swing.JButton();
        jTextFieldY1Imag = new javax.swing.JTextField();
        jTextFieldZ1Imag = new javax.swing.JTextField();
        jTextFieldZ2Real = new javax.swing.JTextField();
        jTextFieldY2Real = new javax.swing.JTextField();
        jLabelY1j = new javax.swing.JLabel();
        jLabelY2 = new javax.swing.JLabel();
        jLabelZ2j = new javax.swing.JLabel();
        jLabelY2j = new javax.swing.JLabel();
        jLabelZ1j = new javax.swing.JLabel();
        LabelZ1Unit = new javax.swing.JLabel();
        LabelY1Unit = new javax.swing.JLabel();
        LabelZ2Unit = new javax.swing.JLabel();
        LabelY2Unit = new javax.swing.JLabel();
        jSeparator11 = new javax.swing.JSeparator();
        jPanelDer = new javax.swing.JPanel();
        jPanelForma = new javax.swing.JPanel();
        jLabelForma = new javax.swing.JLabel();
        jRadioButtonRealImag = new javax.swing.JRadioButton();
        jRadioButtonAbsAng = new javax.swing.JRadioButton();
        jPanelValores = new javax.swing.JPanel();
        jLabelV1 = new javax.swing.JLabel();
        CheckBoxV1 = new javax.swing.JCheckBox();
        jLabelI2 = new javax.swing.JLabel();
        jLabelV2 = new javax.swing.JLabel();
        CheckBoxV2 = new javax.swing.JCheckBox();
        jLabelI1 = new javax.swing.JLabel();
        CheckBoxI2 = new javax.swing.JCheckBox();
        SpinnerV1Real = new javax.swing.JSpinner();
        SpinnerI1Imag = new javax.swing.JSpinner();
        SpinnerI1Real = new javax.swing.JSpinner();
        CheckBoxI1 = new javax.swing.JCheckBox();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        TextFieldP1 = new javax.swing.JTextField();
        TextFieldQ1 = new javax.swing.JTextField();
        TextFieldPhi1 = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        TextFieldP2 = new javax.swing.JTextField();
        TextFieldQ2 = new javax.swing.JTextField();
        TextFieldPhi2 = new javax.swing.JTextField();
        jLabelV1j = new javax.swing.JLabel();
        SpinnerV2Imag = new javax.swing.JSpinner();
        jLabelReal1 = new javax.swing.JLabel();
        jLabelImag1 = new javax.swing.JLabel();
        SpinnerV1Imag = new javax.swing.JSpinner();
        jLabelV2j = new javax.swing.JLabel();
        SpinnerI2Imag = new javax.swing.JSpinner();
        SpinnerI2Real = new javax.swing.JSpinner();
        jLabelI1j = new javax.swing.JLabel();
        jLabelI2j = new javax.swing.JLabel();
        jLabelReal2 = new javax.swing.JLabel();
        jLabelImag2 = new javax.swing.JLabel();
        jSeparator8 = new javax.swing.JSeparator();
        jSeparator9 = new javax.swing.JSeparator();
        jSeparator12 = new javax.swing.JSeparator();
        jButtonActualizarValores = new javax.swing.JButton();
        SpinnerV2Real = new javax.swing.JSpinner();
        LabelQ1Unit = new javax.swing.JLabel();
        LabelP2Unit = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        LabelQ2Unit = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        LabelP1Unit = new javax.swing.JLabel();
        jPanelABCD = new javax.swing.JPanel();
        jPanelTetrapoloImag = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jPanelTetrapoloParam = new javax.swing.JPanel();
        jCheckBoxModificarABCD = new javax.swing.JCheckBox();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jSeparator10 = new javax.swing.JSeparator();
        jLabel16 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        jTextFieldCReal = new javax.swing.JTextField();
        jLabelCj = new javax.swing.JLabel();
        jTextFieldDReal = new javax.swing.JTextField();
        jTextFieldDImag = new javax.swing.JTextField();
        jLabelDj = new javax.swing.JLabel();
        jTextFieldAImag = new javax.swing.JTextField();
        jTextFieldAReal = new javax.swing.JTextField();
        jLabelAj = new javax.swing.JLabel();
        jTextFieldBImag = new javax.swing.JTextField();
        jTextFieldCImag = new javax.swing.JTextField();
        jLabelBj = new javax.swing.JLabel();
        jTextFieldBReal = new javax.swing.JTextField();
        jButtonActualizarABCD = new javax.swing.JButton();
        
        javax.swing.JPanel PanelBotones = new javax.swing.JPanel();
        javax.swing.JButton BotonAyuda  = new javax.swing.JButton();
        PanelBotones.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT));
        PanelBotones.setBackground(new java.awt.Color(204, 204, 204));

        setLayout(new java.awt.BorderLayout());

        jPanelTop.setBackground(new java.awt.Color(204, 204, 204));
        jPanelTop.setMaximumSize(new java.awt.Dimension(768, 40));
        jPanelTop.setMinimumSize(new java.awt.Dimension(768, 40));
        jPanelTop.setPreferredSize(new java.awt.Dimension(768, 40));
        
        jPanelTop.setLayout(new java.awt.FlowLayout());
        
        BotonAyuda.setText("Ayuda");
        BotonAyuda.setMaximumSize(new java.awt.Dimension(158, 28));
        BotonAyuda.setMinimumSize(new java.awt.Dimension(158, 28));
        BotonAyuda.setPreferredSize(new java.awt.Dimension(158, 28));
        BotonAyuda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonAyudaActionPerformed(evt);
            }
        });

        BotonAyuda.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BotonAyudaMouseEntered(evt);
            }
        });

        PanelBotones.add(BotonAyuda);
        
        jLabel1.setBackground(new java.awt.Color(204, 204, 204));
        jLabel1.setFont(new java.awt.Font("Dialog", 1, 16));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel1.setText("An\u00e1lisis de distintos tipos de Tetrapolos");
        jLabel1.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        jLabel1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel1.setMaximumSize(new java.awt.Dimension(500, 22));
        jLabel1.setMinimumSize(new java.awt.Dimension(500, 22));
        jLabel1.setPreferredSize(new java.awt.Dimension(500, 22));
        jPanelTop.add(jLabel1);
        
        jPanelTop.add(PanelBotones);

        jSeparator1.setPreferredSize(new java.awt.Dimension(768, 2));

        add(jPanelTop, java.awt.BorderLayout.NORTH);

        jPanelBottom.setLayout(new java.awt.BorderLayout());

        jPanelBottom.setBackground(new java.awt.Color(204, 204, 204));
        jPanelBottom.setMaximumSize(new java.awt.Dimension(768, 40));
        jPanelBottom.setMinimumSize(new java.awt.Dimension(768, 40));
        jPanelBottom.setPreferredSize(new java.awt.Dimension(768, 40));
        jTextAreaAyuda.setBackground(new java.awt.Color(204, 204, 204));
        jTextAreaAyuda.setEditable(false);
        jTextAreaAyuda.setMaximumSize(new java.awt.Dimension(768, 40));
        jTextAreaAyuda.setMinimumSize(new java.awt.Dimension(400, 40));
        jTextAreaAyuda.setPreferredSize(new java.awt.Dimension(400, 40));
        jTextAreaAyuda.setFont(new java.awt.Font("Verdana",1,11));
        
        jPanelBottom.add(jTextAreaAyuda, java.awt.BorderLayout.WEST);

        add(jPanelBottom, java.awt.BorderLayout.SOUTH);

        jPanelCenter.setLayout(new java.awt.BorderLayout());

        jPanelCenter.setMaximumSize(new java.awt.Dimension(768, 450));
        jPanelCenter.setMinimumSize(new java.awt.Dimension(768, 450));
        jPanelCenter.setPreferredSize(new java.awt.Dimension(768, 450));
        jPanelIzq.setLayout(new java.awt.BorderLayout());

        jPanelIzq.setMaximumSize(new java.awt.Dimension(250, 450));
        jPanelModeloTop.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 5, 15));

        jPanelModeloTop.setBorder(new javax.swing.border.EtchedBorder());
        jPanelModeloTop.setMaximumSize(new java.awt.Dimension(250, 50));
        jPanelModeloTop.setMinimumSize(new java.awt.Dimension(250, 50));
        jPanelModeloTop.setPreferredSize(new java.awt.Dimension(250, 50));
        jLabelModelo.setText("Modelo: ");
        jPanelModeloTop.add(jLabelModelo);

        jComboBoxModelo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Impedancia Z", "Impedancia Y", "Circuito Gamma", "Circuito 7", "Circuito PI", "Circuito T", "Circuito Steinmetz" }));
        jComboBoxModelo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxModeloActionPerformed(evt);
            }
        });

        jPanelModeloTop.add(jComboBoxModelo);

        jPanelIzq.add(jPanelModeloTop, java.awt.BorderLayout.NORTH);

        jPanelModeloCenter.setLayout(new java.awt.BorderLayout());

        jPanelModeloCenter.setBorder(new javax.swing.border.EtchedBorder());
        jPanelModeloCenter.setMaximumSize(new java.awt.Dimension(250, 250));
        jPanelModeloCenter.setMinimumSize(new java.awt.Dimension(250, 250));
        jPanelModeloCenter.setPreferredSize(new java.awt.Dimension(250, 250));
        jMonoModelo.setMinimumSize(new java.awt.Dimension(250, 145));
        jMonoModelo.setPreferredSize(new java.awt.Dimension(250, 145));
        jPanelModeloCenter.add(jMonoModelo, java.awt.BorderLayout.CENTER);

        jPanelIzq.add(jPanelModeloCenter, java.awt.BorderLayout.CENTER);

        jPanelModelobottom.setLayout(new java.awt.GridBagLayout());

        jPanelModelobottom.setBorder(new javax.swing.border.EtchedBorder());
        jPanelModelobottom.setMaximumSize(new java.awt.Dimension(250, 150));
        jPanelModelobottom.setMinimumSize(new java.awt.Dimension(250, 150));
        jPanelModelobottom.setOpaque(false);
        jPanelModelobottom.setPreferredSize(new java.awt.Dimension(250, 150));
        jLabelZ1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabelZ1.setText("Z1 :");
        jLabelZ1.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 2);
        jPanelModelobottom.add(jLabelZ1, gridBagConstraints);

        jTextFieldY1Real.setMaximumSize(new java.awt.Dimension(68, 20));
        jTextFieldY1Real.setMinimumSize(new java.awt.Dimension(68, 20));
        jTextFieldY1Real.setPreferredSize(new java.awt.Dimension(68, 20));
        jTextFieldY1Real.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldY1RealKeyTyped(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        jPanelModelobottom.add(jTextFieldY1Real, gridBagConstraints);

        jLabelY1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabelY1.setText("Y1 :");
        jLabelY1.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 2);
        jPanelModelobottom.add(jLabelY1, gridBagConstraints);

        jTextFieldY2Imag.setMaximumSize(new java.awt.Dimension(68, 20));
        jTextFieldY2Imag.setMinimumSize(new java.awt.Dimension(68, 20));
        jTextFieldY2Imag.setPreferredSize(new java.awt.Dimension(68, 20));
        jTextFieldY2Imag.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldY2ImagKeyTyped(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 3;
        jPanelModelobottom.add(jTextFieldY2Imag, gridBagConstraints);

        jLabelZ2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabelZ2.setText("Z2 :");
        jLabelZ2.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 2);
        jPanelModelobottom.add(jLabelZ2, gridBagConstraints);

        jTextFieldZ1Real.setMaximumSize(new java.awt.Dimension(68, 20));
        jTextFieldZ1Real.setMinimumSize(new java.awt.Dimension(68, 20));
        jTextFieldZ1Real.setPreferredSize(new java.awt.Dimension(68, 20));
        jTextFieldZ1Real.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldZ1RealKeyTyped(evt);
            }
        });

        jPanelModelobottom.add(jTextFieldZ1Real, new java.awt.GridBagConstraints());

        jTextFieldZ2Imag.setMaximumSize(new java.awt.Dimension(68, 20));
        jTextFieldZ2Imag.setMinimumSize(new java.awt.Dimension(68, 20));
        jTextFieldZ2Imag.setPreferredSize(new java.awt.Dimension(68, 20));
        jTextFieldZ2Imag.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldZ2ImagKeyTyped(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 2;
        jPanelModelobottom.add(jTextFieldZ2Imag, gridBagConstraints);

        jButtonActualizarModelo.setText("Actualizar");
        jButtonActualizarModelo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonActualizarModeloActionPerformed(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 5;
        jPanelModelobottom.add(jButtonActualizarModelo, gridBagConstraints);

        jTextFieldY1Imag.setMaximumSize(new java.awt.Dimension(68, 20));
        jTextFieldY1Imag.setMinimumSize(new java.awt.Dimension(68, 20));
        jTextFieldY1Imag.setPreferredSize(new java.awt.Dimension(68, 20));
        jTextFieldY1Imag.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldY1ImagKeyTyped(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        jPanelModelobottom.add(jTextFieldY1Imag, gridBagConstraints);

        jTextFieldZ1Imag.setMaximumSize(new java.awt.Dimension(68, 20));
        jTextFieldZ1Imag.setMinimumSize(new java.awt.Dimension(68, 20));
        jTextFieldZ1Imag.setPreferredSize(new java.awt.Dimension(68, 20));
        jTextFieldZ1Imag.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldZ1ImagKeyTyped(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        jPanelModelobottom.add(jTextFieldZ1Imag, gridBagConstraints);

        jTextFieldZ2Real.setMaximumSize(new java.awt.Dimension(68, 20));
        jTextFieldZ2Real.setMinimumSize(new java.awt.Dimension(68, 20));
        jTextFieldZ2Real.setPreferredSize(new java.awt.Dimension(68, 20));
        jTextFieldZ2Real.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldZ2RealKeyTyped(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        jPanelModelobottom.add(jTextFieldZ2Real, gridBagConstraints);

        jTextFieldY2Real.setMaximumSize(new java.awt.Dimension(68, 20));
        jTextFieldY2Real.setMinimumSize(new java.awt.Dimension(68, 20));
        jTextFieldY2Real.setPreferredSize(new java.awt.Dimension(68, 20));
        jTextFieldY2Real.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldY2RealKeyTyped(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        jPanelModelobottom.add(jTextFieldY2Real, gridBagConstraints);

        jLabelY1j.setText("   +   j");
        jLabelY1j.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        jPanelModelobottom.add(jLabelY1j, gridBagConstraints);

        jLabelY2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabelY2.setText("Y2 :");
        jLabelY2.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabelY2.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 2);
        jPanelModelobottom.add(jLabelY2, gridBagConstraints);

        jLabelZ2j.setText("   +   j");
        jLabelZ2j.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        jPanelModelobottom.add(jLabelZ2j, gridBagConstraints);

        jLabelY2j.setText("   +   j");
        jLabelY2j.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 3;
        jPanelModelobottom.add(jLabelY2j, gridBagConstraints);

        jLabelZ1j.setText("   +   j");
        jLabelZ1j.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanelModelobottom.add(jLabelZ1j, gridBagConstraints);

        LabelZ1Unit.setText("[\u03a9]");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        jPanelModelobottom.add(LabelZ1Unit, gridBagConstraints);

        LabelY1Unit.setText("[S]");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 1;
        jPanelModelobottom.add(LabelY1Unit, gridBagConstraints);

        LabelZ2Unit.setText("[\u03a9]");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 2;
        jPanelModelobottom.add(LabelZ2Unit, gridBagConstraints);

        LabelY2Unit.setText("[S]");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 3;
        jPanelModelobottom.add(LabelY2Unit, gridBagConstraints);

        jSeparator11.setBackground(new java.awt.Color(204, 204, 204));
        jSeparator11.setForeground(new java.awt.Color(204, 204, 204));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(13, 0, 11, 0);
        jPanelModelobottom.add(jSeparator11, gridBagConstraints);

        jPanelIzq.add(jPanelModelobottom, java.awt.BorderLayout.SOUTH);

        jPanelCenter.add(jPanelIzq, java.awt.BorderLayout.WEST);

        jPanelDer.setLayout(new java.awt.BorderLayout());

        jPanelDer.setMaximumSize(new java.awt.Dimension(400, 450));
        jPanelForma.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 5, 15));

        jPanelForma.setBorder(new javax.swing.border.EtchedBorder());
        jPanelForma.setMaximumSize(new java.awt.Dimension(400, 50));
        jPanelForma.setMinimumSize(new java.awt.Dimension(400, 50));
        jPanelForma.setPreferredSize(new java.awt.Dimension(400, 50));
        jLabelForma.setText("Forma: ");
        jPanelForma.add(jLabelForma);

        jRadioButtonRealImag.setText("Real + j Imag ");
        jRadioButtonRealImag.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonRealImagActionPerformed(evt);
            }
        });

        jPanelForma.add(jRadioButtonRealImag);

        jRadioButtonAbsAng.setText("Abs < \u03c6\u00ba");
        jRadioButtonAbsAng.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonAbsAngActionPerformed(evt);
            }
        });

        jPanelForma.add(jRadioButtonAbsAng);

        jPanelDer.add(jPanelForma, java.awt.BorderLayout.NORTH);

        jPanelValores.setLayout(new java.awt.GridBagLayout());

        jPanelValores.setBorder(new javax.swing.border.EtchedBorder());
        jPanelValores.setMaximumSize(new java.awt.Dimension(400, 250));
        jPanelValores.setMinimumSize(new java.awt.Dimension(400, 250));
        jPanelValores.setPreferredSize(new java.awt.Dimension(400, 250));
        jLabelV1.setText("V1 :");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        jPanelValores.add(jLabelV1, gridBagConstraints);

        CheckBoxV1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CheckBoxV1ActionPerformed(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 1;
        jPanelValores.add(CheckBoxV1, gridBagConstraints);

        jLabelI2.setText("I2 : ");
        jLabelI2.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 2;
        jPanelValores.add(jLabelI2, gridBagConstraints);

        jLabelV2.setText("V2 :");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        jPanelValores.add(jLabelV2, gridBagConstraints);

        CheckBoxV2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CheckBoxV2ActionPerformed(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 2;
        jPanelValores.add(CheckBoxV2, gridBagConstraints);

        jLabelI1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelI1.setText("I1 :");
        jLabelI1.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 1;
        jPanelValores.add(jLabelI1, gridBagConstraints);

        CheckBoxI2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CheckBoxI2ActionPerformed(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 2;
        jPanelValores.add(CheckBoxI2, gridBagConstraints);

        SpinnerV1Real.setMaximumSize(new java.awt.Dimension(60, 18));
        SpinnerV1Real.setMinimumSize(new java.awt.Dimension(60, 18));
        SpinnerV1Real.setPreferredSize(new java.awt.Dimension(60, 18));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 2);
        jPanelValores.add(SpinnerV1Real, gridBagConstraints);

        SpinnerI1Imag.setMaximumSize(new java.awt.Dimension(60, 18));
        SpinnerI1Imag.setMinimumSize(new java.awt.Dimension(60, 18));
        SpinnerI1Imag.setPreferredSize(new java.awt.Dimension(60, 18));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 9;
        gridBagConstraints.gridy = 1;
        jPanelValores.add(SpinnerI1Imag, gridBagConstraints);

        SpinnerI1Real.setMaximumSize(new java.awt.Dimension(60, 18));
        SpinnerI1Real.setMinimumSize(new java.awt.Dimension(60, 18));
        SpinnerI1Real.setPreferredSize(new java.awt.Dimension(60, 18));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 7;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 2);
        jPanelValores.add(SpinnerI1Real, gridBagConstraints);

        CheckBoxI1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CheckBoxI1ActionPerformed(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 1;
        jPanelValores.add(CheckBoxI1, gridBagConstraints);

        jLabel7.setText("P1 :");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        jPanelValores.add(jLabel7, gridBagConstraints);

        jLabel8.setText("Q1 :");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        jPanelValores.add(jLabel8, gridBagConstraints);

        jLabel9.setText("\u03c61 :");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        jPanelValores.add(jLabel9, gridBagConstraints);

        TextFieldP1.setEditable(false);
        TextFieldP1.setMaximumSize(new java.awt.Dimension(130, 20));
        TextFieldP1.setMinimumSize(new java.awt.Dimension(130, 20));
        TextFieldP1.setPreferredSize(new java.awt.Dimension(130, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanelValores.add(TextFieldP1, gridBagConstraints);

        TextFieldQ1.setEditable(false);
        TextFieldQ1.setMaximumSize(new java.awt.Dimension(130, 20));
        TextFieldQ1.setMinimumSize(new java.awt.Dimension(130, 20));
        TextFieldQ1.setPreferredSize(new java.awt.Dimension(130, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanelValores.add(TextFieldQ1, gridBagConstraints);

        TextFieldPhi1.setEditable(false);
        TextFieldPhi1.setMaximumSize(new java.awt.Dimension(130, 20));
        TextFieldPhi1.setMinimumSize(new java.awt.Dimension(130, 20));
        TextFieldPhi1.setPreferredSize(new java.awt.Dimension(130, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanelValores.add(TextFieldPhi1, gridBagConstraints);

        jLabel10.setText("P2 :");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        jPanelValores.add(jLabel10, gridBagConstraints);

        jLabel11.setText("Q2 :");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        jPanelValores.add(jLabel11, gridBagConstraints);

        jLabel12.setText("\u03c62 :");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        jPanelValores.add(jLabel12, gridBagConstraints);

        TextFieldP2.setEditable(false);
        TextFieldP2.setMaximumSize(new java.awt.Dimension(130, 20));
        TextFieldP2.setMinimumSize(new java.awt.Dimension(130, 20));
        TextFieldP2.setPreferredSize(new java.awt.Dimension(130, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 7;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanelValores.add(TextFieldP2, gridBagConstraints);

        TextFieldQ2.setEditable(false);
        TextFieldQ2.setMaximumSize(new java.awt.Dimension(130, 20));
        TextFieldQ2.setMinimumSize(new java.awt.Dimension(130, 20));
        TextFieldQ2.setPreferredSize(new java.awt.Dimension(130, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 7;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanelValores.add(TextFieldQ2, gridBagConstraints);

        TextFieldPhi2.setEditable(false);
        TextFieldPhi2.setMaximumSize(new java.awt.Dimension(130, 20));
        TextFieldPhi2.setMinimumSize(new java.awt.Dimension(130, 20));
        TextFieldPhi2.setPreferredSize(new java.awt.Dimension(130, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 7;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanelValores.add(TextFieldPhi2, gridBagConstraints);

        jLabelV1j.setText(" + j");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(0, 1, 0, 1);
        jPanelValores.add(jLabelV1j, gridBagConstraints);

        SpinnerV2Imag.setMaximumSize(new java.awt.Dimension(60, 18));
        SpinnerV2Imag.setMinimumSize(new java.awt.Dimension(60, 18));
        SpinnerV2Imag.setOpaque(false);
        SpinnerV2Imag.setPreferredSize(new java.awt.Dimension(60, 18));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 2;
        jPanelValores.add(SpinnerV2Imag, gridBagConstraints);

        jLabelReal1.setText("Real");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        jPanelValores.add(jLabelReal1, gridBagConstraints);

        jLabelImag1.setText("Imag");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        jPanelValores.add(jLabelImag1, gridBagConstraints);

        SpinnerV1Imag.setMaximumSize(new java.awt.Dimension(60, 18));
        SpinnerV1Imag.setMinimumSize(new java.awt.Dimension(60, 18));
        SpinnerV1Imag.setPreferredSize(new java.awt.Dimension(60, 18));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        jPanelValores.add(SpinnerV1Imag, gridBagConstraints);

        jLabelV2j.setText(" + j");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.insets = new java.awt.Insets(0, 1, 0, 1);
        jPanelValores.add(jLabelV2j, gridBagConstraints);

        SpinnerI2Imag.setMaximumSize(new java.awt.Dimension(60, 18));
        SpinnerI2Imag.setMinimumSize(new java.awt.Dimension(60, 18));
        SpinnerI2Imag.setOpaque(false);
        SpinnerI2Imag.setPreferredSize(new java.awt.Dimension(60, 18));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 9;
        gridBagConstraints.gridy = 2;
        jPanelValores.add(SpinnerI2Imag, gridBagConstraints);

        SpinnerI2Real.setMaximumSize(new java.awt.Dimension(60, 18));
        SpinnerI2Real.setMinimumSize(new java.awt.Dimension(60, 18));
        SpinnerI2Real.setPreferredSize(new java.awt.Dimension(60, 18));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 7;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 2);
        jPanelValores.add(SpinnerI2Real, gridBagConstraints);

        jLabelI1j.setText(" + j");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(0, 1, 0, 1);
        jPanelValores.add(jLabelI1j, gridBagConstraints);

        jLabelI2j.setText(" + j");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.insets = new java.awt.Insets(0, 1, 0, 1);
        jPanelValores.add(jLabelI2j, gridBagConstraints);

        jLabelReal2.setText("Real");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 7;
        gridBagConstraints.gridy = 0;
        jPanelValores.add(jLabelReal2, gridBagConstraints);

        jLabelImag2.setText("Imag");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 9;
        gridBagConstraints.gridy = 0;
        jPanelValores.add(jLabelImag2, gridBagConstraints);

        jSeparator8.setBackground(new java.awt.Color(204, 204, 204));
        jSeparator8.setForeground(new java.awt.Color(204, 204, 204));
        jSeparator8.setOrientation(javax.swing.SwingConstants.VERTICAL);
        jSeparator8.setMaximumSize(new java.awt.Dimension(0, 0));
        jSeparator8.setPreferredSize(new java.awt.Dimension(10, 0));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 7;
        jPanelValores.add(jSeparator8, gridBagConstraints);

        jSeparator9.setBackground(new java.awt.Color(204, 204, 204));
        jSeparator9.setForeground(new java.awt.Color(204, 204, 204));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(11, 0, 11, 0);
        jPanelValores.add(jSeparator9, gridBagConstraints);

        jSeparator12.setBackground(new java.awt.Color(204, 204, 204));
        jSeparator12.setForeground(new java.awt.Color(204, 204, 204));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(7, 0, 7, 0);
        jPanelValores.add(jSeparator12, gridBagConstraints);

        jButtonActualizarValores.setText("Actualizar");
        jButtonActualizarValores.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonActualizarValoresActionPerformed(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 8;
        gridBagConstraints.gridy = 9;
        gridBagConstraints.gridwidth = 2;
        jPanelValores.add(jButtonActualizarValores, gridBagConstraints);

        SpinnerV2Real.setMaximumSize(new java.awt.Dimension(60, 18));
        SpinnerV2Real.setMinimumSize(new java.awt.Dimension(60, 18));
        SpinnerV2Real.setPreferredSize(new java.awt.Dimension(60, 18));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 2);
        jPanelValores.add(SpinnerV2Real, gridBagConstraints);

        LabelQ1Unit.setText("[VAr]");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanelValores.add(LabelQ1Unit, gridBagConstraints);

        LabelP2Unit.setText("[W]");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanelValores.add(LabelP2Unit, gridBagConstraints);

        jLabel15.setText("[\u00ba]");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanelValores.add(jLabel15, gridBagConstraints);

        LabelQ2Unit.setText("[VAr]");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanelValores.add(LabelQ2Unit, gridBagConstraints);

        jLabel28.setText("[\u00ba]");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 10;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanelValores.add(jLabel28, gridBagConstraints);

        LabelP1Unit.setText("[W]");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanelValores.add(LabelP1Unit, gridBagConstraints);

        jPanelDer.add(jPanelValores, java.awt.BorderLayout.CENTER);

        jPanelABCD.setLayout(new java.awt.BorderLayout());

        jPanelABCD.setBorder(new javax.swing.border.EtchedBorder());
        jPanelABCD.setMaximumSize(new java.awt.Dimension(400, 150));
        jPanelABCD.setMinimumSize(new java.awt.Dimension(400, 150));
        jPanelABCD.setPreferredSize(new java.awt.Dimension(400, 150));
        jPanelTetrapoloImag.setLayout(new java.awt.BorderLayout());

        jPanelTetrapoloImag.setMaximumSize(new java.awt.Dimension(180, 150));
        jPanelTetrapoloImag.setMinimumSize(new java.awt.Dimension(180, 150));
        jPanelTetrapoloImag.setPreferredSize(new java.awt.Dimension(180, 150));
        jPanel3.setLayout(new java.awt.BorderLayout());

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("images/ABCD.gif")));
        jLabel2.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel2.setAlignmentY(0.0F);
        jLabel2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel2.setMaximumSize(new java.awt.Dimension(180, 120));
        jLabel2.setMinimumSize(new java.awt.Dimension(180, 120));
        jLabel2.setPreferredSize(new java.awt.Dimension(180, 120));
        jLabel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel2MouseEntered(evt);
            }
        });

        jPanel3.add(jLabel2, java.awt.BorderLayout.CENTER);

        jPanelTetrapoloImag.add(jPanel3, java.awt.BorderLayout.SOUTH);

        jPanelABCD.add(jPanelTetrapoloImag, java.awt.BorderLayout.WEST);

        jPanelTetrapoloParam.setLayout(new java.awt.GridBagLayout());

        jPanelTetrapoloParam.setMaximumSize(new java.awt.Dimension(250, 150));
        jPanelTetrapoloParam.setMinimumSize(new java.awt.Dimension(250, 150));
        jPanelTetrapoloParam.setPreferredSize(new java.awt.Dimension(250, 150));
        jCheckBoxModificarABCD.setText("Modificable");
        jCheckBoxModificarABCD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxModificarABCDActionPerformed(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        jPanelTetrapoloParam.add(jCheckBoxModificarABCD, gridBagConstraints);

        jLabel13.setText("A :");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        jPanelTetrapoloParam.add(jLabel13, gridBagConstraints);

        jLabel14.setText("B :");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        jPanelTetrapoloParam.add(jLabel14, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 0, 2, 0);
        jPanelTetrapoloParam.add(jSeparator10, gridBagConstraints);

        jLabel16.setText("C :");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        jPanelTetrapoloParam.add(jLabel16, gridBagConstraints);

        jLabel34.setText("D :");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        jPanelTetrapoloParam.add(jLabel34, gridBagConstraints);

        jTextFieldCReal.setEditable(false);
        jTextFieldCReal.setMaximumSize(new java.awt.Dimension(90, 20));
        jTextFieldCReal.setMinimumSize(new java.awt.Dimension(90, 20));
        jTextFieldCReal.setPreferredSize(new java.awt.Dimension(90, 20));
        jTextFieldCReal.setRequestFocusEnabled(false);
        jTextFieldCReal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldCRealKeyTyped(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        jPanelTetrapoloParam.add(jTextFieldCReal, gridBagConstraints);

        jLabelCj.setText("   +   j");
        jLabelCj.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 2);
        jPanelTetrapoloParam.add(jLabelCj, gridBagConstraints);

        jTextFieldDReal.setEditable(false);
        jTextFieldDReal.setMaximumSize(new java.awt.Dimension(90, 20));
        jTextFieldDReal.setMinimumSize(new java.awt.Dimension(90, 20));
        jTextFieldDReal.setPreferredSize(new java.awt.Dimension(90, 20));
        jTextFieldDReal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldDRealKeyTyped(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        jPanelTetrapoloParam.add(jTextFieldDReal, gridBagConstraints);

        jTextFieldDImag.setEditable(false);
        jTextFieldDImag.setMaximumSize(new java.awt.Dimension(90, 20));
        jTextFieldDImag.setMinimumSize(new java.awt.Dimension(90, 20));
        jTextFieldDImag.setPreferredSize(new java.awt.Dimension(90, 20));
        jTextFieldDImag.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldDImagKeyTyped(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 5;
        jPanelTetrapoloParam.add(jTextFieldDImag, gridBagConstraints);

        jLabelDj.setText("   +   j");
        jLabelDj.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 2);
        jPanelTetrapoloParam.add(jLabelDj, gridBagConstraints);

        jTextFieldAImag.setEditable(false);
        jTextFieldAImag.setMaximumSize(new java.awt.Dimension(90, 20));
        jTextFieldAImag.setMinimumSize(new java.awt.Dimension(90, 20));
        jTextFieldAImag.setPreferredSize(new java.awt.Dimension(90, 20));
        jTextFieldAImag.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldAImagKeyTyped(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 2;
        jPanelTetrapoloParam.add(jTextFieldAImag, gridBagConstraints);

        jTextFieldAReal.setEditable(false);
        jTextFieldAReal.setMaximumSize(new java.awt.Dimension(90, 20));
        jTextFieldAReal.setMinimumSize(new java.awt.Dimension(90, 20));
        jTextFieldAReal.setPreferredSize(new java.awt.Dimension(90, 20));
        jTextFieldAReal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldARealKeyTyped(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        jPanelTetrapoloParam.add(jTextFieldAReal, gridBagConstraints);

        jLabelAj.setText("   +   j");
        jLabelAj.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 2);
        jPanelTetrapoloParam.add(jLabelAj, gridBagConstraints);

        jTextFieldBImag.setEditable(false);
        jTextFieldBImag.setMaximumSize(new java.awt.Dimension(90, 20));
        jTextFieldBImag.setMinimumSize(new java.awt.Dimension(90, 20));
        jTextFieldBImag.setPreferredSize(new java.awt.Dimension(90, 20));
        jTextFieldBImag.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldBImagKeyTyped(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 3;
        jPanelTetrapoloParam.add(jTextFieldBImag, gridBagConstraints);

        jTextFieldCImag.setEditable(false);
        jTextFieldCImag.setMaximumSize(new java.awt.Dimension(90, 20));
        jTextFieldCImag.setMinimumSize(new java.awt.Dimension(90, 20));
        jTextFieldCImag.setPreferredSize(new java.awt.Dimension(90, 20));
        jTextFieldCImag.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldCImagKeyTyped(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 4;
        jPanelTetrapoloParam.add(jTextFieldCImag, gridBagConstraints);

        jLabelBj.setText("   +   j");
        jLabelBj.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 2);
        jPanelTetrapoloParam.add(jLabelBj, gridBagConstraints);

        jTextFieldBReal.setEditable(false);
        jTextFieldBReal.setMaximumSize(new java.awt.Dimension(90, 20));
        jTextFieldBReal.setMinimumSize(new java.awt.Dimension(90, 20));
        jTextFieldBReal.setPreferredSize(new java.awt.Dimension(90, 20));
        jTextFieldBReal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldBRealKeyTyped(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        jPanelTetrapoloParam.add(jTextFieldBReal, gridBagConstraints);

        jButtonActualizarABCD.setText("Actualizar");
        jButtonActualizarABCD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonActualizarABCDActionPerformed(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        jPanelTetrapoloParam.add(jButtonActualizarABCD, gridBagConstraints);

        jPanelABCD.add(jPanelTetrapoloParam, java.awt.BorderLayout.EAST);

        jPanelDer.add(jPanelABCD, java.awt.BorderLayout.SOUTH);

        jPanelCenter.add(jPanelDer, java.awt.BorderLayout.CENTER);

        add(jPanelCenter, java.awt.BorderLayout.CENTER);

    }
    // </editor-fold>//GEN-END:initComponents

    private void jLabel2MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel2MouseEntered
        //jTextPaneAyuda.setText("Ecuaci�n de los Par�metros ABCD");
    }//GEN-LAST:event_jLabel2MouseEntered
    
    private void jRadioButtonAbsAngActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonAbsAngActionPerformed
        ChangeForma(1);
    }//GEN-LAST:event_jRadioButtonAbsAngActionPerformed

    private void jRadioButtonRealImagActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonRealImagActionPerformed
        ChangeForma(0);
    }//GEN-LAST:event_jRadioButtonRealImagActionPerformed

    private void jButtonActualizarValoresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonActualizarValoresActionPerformed
        CalcularValores();
        FillValores();
    }//GEN-LAST:event_jButtonActualizarValoresActionPerformed

    private void jCheckBoxModificarABCDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxModificarABCDActionPerformed
        CheckModificar();
    }//GEN-LAST:event_jCheckBoxModificarABCDActionPerformed
    
    private void jButtonActualizarABCDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonActualizarABCDActionPerformed
        try{
            if(FormaRealImag){
                ABCD.A = new Complex(Double.parseDouble(jTextFieldAReal.getText()),Double.parseDouble(jTextFieldAImag.getText()));
                ABCD.B = new Complex(Double.parseDouble(jTextFieldBReal.getText()),Double.parseDouble(jTextFieldBImag.getText()));
                ABCD.C = new Complex(Double.parseDouble(jTextFieldCReal.getText()),Double.parseDouble(jTextFieldCImag.getText()));
                ABCD.D = new Complex(Double.parseDouble(jTextFieldDReal.getText()),Double.parseDouble(jTextFieldDImag.getText()));
            }else{
                ABCD.A = new Complex(Double.parseDouble(jTextFieldAReal.getText()),Double.parseDouble(jTextFieldAImag.getText()),1);
                ABCD.B = new Complex(Double.parseDouble(jTextFieldBReal.getText()),Double.parseDouble(jTextFieldBImag.getText()),1);
                ABCD.C = new Complex(Double.parseDouble(jTextFieldCReal.getText()),Double.parseDouble(jTextFieldCImag.getText()),1);
                ABCD.D = new Complex(Double.parseDouble(jTextFieldDReal.getText()),Double.parseDouble(jTextFieldDImag.getText()),1);
            }
        }catch (Exception e){
                JOptionPane error=new JOptionPane();
		error.showMessageDialog(null, "Algunos de los datos introducidos son invalidos", "Error en los datos", JOptionPane.ERROR_MESSAGE);
		return;
        }
        
        jButtonActualizarValores.setEnabled(true);
    }//GEN-LAST:event_jButtonActualizarABCDActionPerformed

    private void jTextFieldARealKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldARealKeyTyped
        if (ABCD_Modif)
            jButtonActualizarValores.setEnabled(false);
    }//GEN-LAST:event_jTextFieldARealKeyTyped

    private void jTextFieldBRealKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldBRealKeyTyped
        if (ABCD_Modif)
            jButtonActualizarValores.setEnabled(false);
    }//GEN-LAST:event_jTextFieldBRealKeyTyped

    private void jTextFieldCRealKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldCRealKeyTyped
        if (ABCD_Modif)
            jButtonActualizarValores.setEnabled(false);
    }//GEN-LAST:event_jTextFieldCRealKeyTyped

    private void jTextFieldDRealKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldDRealKeyTyped
        if (ABCD_Modif)
            jButtonActualizarValores.setEnabled(false);
    }//GEN-LAST:event_jTextFieldDRealKeyTyped

    private void jTextFieldAImagKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldAImagKeyTyped
        if (ABCD_Modif)
            jButtonActualizarValores.setEnabled(false);
    }//GEN-LAST:event_jTextFieldAImagKeyTyped

    private void jTextFieldBImagKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldBImagKeyTyped
        if (ABCD_Modif)
            jButtonActualizarValores.setEnabled(false);
    }//GEN-LAST:event_jTextFieldBImagKeyTyped

    private void jTextFieldCImagKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldCImagKeyTyped
        if (ABCD_Modif)
            jButtonActualizarValores.setEnabled(false);
    }//GEN-LAST:event_jTextFieldCImagKeyTyped

    private void jTextFieldDImagKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldDImagKeyTyped
        if (ABCD_Modif)
            jButtonActualizarValores.setEnabled(false);
    }//GEN-LAST:event_jTextFieldDImagKeyTyped

    private void CheckBoxV1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CheckBoxV1ActionPerformed
        if(CheckBoxVI()<=2){
             Comb_V1 = CheckBoxV1.isSelected();
        }else{
            CheckBoxV1.setSelected(false);
            Comb_V1 = false;
        }
        CheckBoxsVI();
    }//GEN-LAST:event_CheckBoxV1ActionPerformed

    private void CheckBoxV2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CheckBoxV2ActionPerformed
        if(CheckBoxVI()<=2){
            Comb_V2 = CheckBoxV2.isSelected();
        }else{
            CheckBoxV2.setSelected(false);
            Comb_V2 = false;
        }
        CheckBoxsVI();
    }//GEN-LAST:event_CheckBoxV2ActionPerformed

    private void CheckBoxI1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CheckBoxI1ActionPerformed
        if(CheckBoxVI()<=2){
            Comb_I1 = CheckBoxI1.isSelected();
        }else{
            CheckBoxI1.setSelected(false);
            Comb_I1 = false;
        }
        CheckBoxsVI();
    }//GEN-LAST:event_CheckBoxI1ActionPerformed

    private void CheckBoxI2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CheckBoxI2ActionPerformed
        if(CheckBoxVI()<=2){
            Comb_I2 = CheckBoxI2.isSelected();
        }else{
            CheckBoxI2.setSelected(false);
            Comb_I2 = false;
        }
        CheckBoxsVI();
    }//GEN-LAST:event_CheckBoxI2ActionPerformed

    private void jTextFieldZ1RealKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldZ1RealKeyTyped
        if (!ABCD_Modif)
            jButtonActualizarValores.setEnabled(false);
    }//GEN-LAST:event_jTextFieldZ1RealKeyTyped

    private void jTextFieldY1RealKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldY1RealKeyTyped
        if (!ABCD_Modif)
            jButtonActualizarValores.setEnabled(false);
    }//GEN-LAST:event_jTextFieldY1RealKeyTyped

    private void jTextFieldZ2RealKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldZ2RealKeyTyped
        if (!ABCD_Modif)
            jButtonActualizarValores.setEnabled(false);
    }//GEN-LAST:event_jTextFieldZ2RealKeyTyped

    private void jTextFieldY2RealKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldY2RealKeyTyped
        if (!ABCD_Modif)
            jButtonActualizarValores.setEnabled(false);
    }//GEN-LAST:event_jTextFieldY2RealKeyTyped

    private void jTextFieldZ1ImagKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldZ1ImagKeyTyped
        if (!ABCD_Modif)
            jButtonActualizarValores.setEnabled(false);
    }//GEN-LAST:event_jTextFieldZ1ImagKeyTyped

    private void jTextFieldY1ImagKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldY1ImagKeyTyped
        if (!ABCD_Modif)
            jButtonActualizarValores.setEnabled(false);
    }//GEN-LAST:event_jTextFieldY1ImagKeyTyped

    private void jTextFieldZ2ImagKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldZ2ImagKeyTyped
        if (!ABCD_Modif)
            jButtonActualizarValores.setEnabled(false);
    }//GEN-LAST:event_jTextFieldZ2ImagKeyTyped

    private void jTextFieldY2ImagKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldY2ImagKeyTyped
        if (!ABCD_Modif)
            jButtonActualizarValores.setEnabled(false);
    }//GEN-LAST:event_jTextFieldY2ImagKeyTyped
    
	private void BotonAyudaMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BotonAyudaMouseEntered
		jTextAreaAyuda.setText("Elija esta opci�n si necesita Ayuda.");
	}//GEN-LAST:event_BotonAyudaMouseEntered
	
	private void BotonAyudaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonAyudaActionPerformed
		setVisible(false);
		Ayuda.setVisible(true);
		Ayuda.repaint();
	}//GEN-LAST:event_BotonAyudaActionPerformed
	

    private void jButtonActualizarModeloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonActualizarModeloActionPerformed
        switch(jComboBoxModelo.getSelectedIndex()){
            case 0:
                ModeloZ(); break;
            case 1:
                ModeloY(); break;
            case 2:
                ModeloG(); break;
            case 3:
                Modelo7(); break;
            case 4:
                ModeloPi(); break;
            case 5:
                ModeloT(); break;
            case 6:
                ModeloSteinmetz(); break;
        }
        if (!ABCD_Modif)
            FillABCD();
        jButtonActualizarValores.setEnabled(true);
    }//GEN-LAST:event_jButtonActualizarModeloActionPerformed

    private void jComboBoxModeloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxModeloActionPerformed
        switch(jComboBoxModelo.getSelectedIndex()){
            case 0:
                ModeloZ(); break;
            case 1:
                ModeloY(); break;
            case 2:
                ModeloG(); break;
            case 3:
                Modelo7(); break;
            case 4:
                ModeloPi(); break;
            case 5:
                ModeloT(); break;
            case 6:
                ModeloSteinmetz(); break;
        }
    }//GEN-LAST:event_jComboBoxModeloActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox CheckBoxI1;
    private javax.swing.JCheckBox CheckBoxI2;
    private javax.swing.JCheckBox CheckBoxV1;
    private javax.swing.JCheckBox CheckBoxV2;
    private javax.swing.JLabel LabelP1Unit;
    private javax.swing.JLabel LabelP2Unit;
    private javax.swing.JLabel LabelQ1Unit;
    private javax.swing.JLabel LabelQ2Unit;
    private javax.swing.JLabel LabelY1Unit;
    private javax.swing.JLabel LabelY2Unit;
    private javax.swing.JLabel LabelZ1Unit;
    private javax.swing.JLabel LabelZ2Unit;
    private javax.swing.JSpinner SpinnerI1Imag;
    private javax.swing.JSpinner SpinnerI1Real;
    private javax.swing.JSpinner SpinnerI2Imag;
    private javax.swing.JSpinner SpinnerI2Real;
    private javax.swing.JSpinner SpinnerV1Imag;
    private javax.swing.JSpinner SpinnerV1Real;
    private javax.swing.JSpinner SpinnerV2Imag;
    private javax.swing.JSpinner SpinnerV2Real;
    private javax.swing.JTextField TextFieldP1;
    private javax.swing.JTextField TextFieldP2;
    private javax.swing.JTextField TextFieldPhi1;
    private javax.swing.JTextField TextFieldPhi2;
    private javax.swing.JTextField TextFieldQ1;
    private javax.swing.JTextField TextFieldQ2;
    private javax.swing.JButton jButtonActualizarABCD;
    private javax.swing.JButton jButtonActualizarModelo;
    private javax.swing.JButton jButtonActualizarValores;
    private javax.swing.JCheckBox jCheckBoxModificarABCD;
    private javax.swing.JComboBox jComboBoxModelo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabelAj;
    private javax.swing.JLabel jLabelBj;
    private javax.swing.JLabel jLabelCj;
    private javax.swing.JLabel jLabelDj;
    private javax.swing.JLabel jLabelForma;
    private javax.swing.JLabel jLabelI1;
    private javax.swing.JLabel jLabelI1j;
    private javax.swing.JLabel jLabelI2;
    private javax.swing.JLabel jLabelI2j;
    private javax.swing.JLabel jLabelImag1;
    private javax.swing.JLabel jLabelImag2;
    private javax.swing.JLabel jLabelModelo;
    private javax.swing.JLabel jLabelReal1;
    private javax.swing.JLabel jLabelReal2;
    private javax.swing.JLabel jLabelV1;
    private javax.swing.JLabel jLabelV1j;
    private javax.swing.JLabel jLabelV2;
    private javax.swing.JLabel jLabelV2j;
    private javax.swing.JLabel jLabelY1;
    private javax.swing.JLabel jLabelY1j;
    private javax.swing.JLabel jLabelY2;
    private javax.swing.JLabel jLabelY2j;
    private javax.swing.JLabel jLabelZ1;
    private javax.swing.JLabel jLabelZ1j;
    private javax.swing.JLabel jLabelZ2;
    private javax.swing.JLabel jLabelZ2j;
    private javax.swing.JLabel jMonoModelo;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanelABCD;
    private javax.swing.JPanel jPanelBottom;
    private javax.swing.JPanel jPanelCenter;
    private javax.swing.JPanel jPanelDer;
    private javax.swing.JPanel jPanelForma;
    private javax.swing.JPanel jPanelIzq;
    private javax.swing.JPanel jPanelModeloCenter;
    private javax.swing.JPanel jPanelModeloTop;
    private javax.swing.JPanel jPanelModelobottom;
    private javax.swing.JPanel jPanelTetrapoloImag;
    private javax.swing.JPanel jPanelTetrapoloParam;
    private javax.swing.JPanel jPanelTop;
    private javax.swing.JPanel jPanelValores;
    private javax.swing.JRadioButton jRadioButtonAbsAng;
    private javax.swing.JRadioButton jRadioButtonRealImag;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator10;
    private javax.swing.JSeparator jSeparator11;
    private javax.swing.JSeparator jSeparator12;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JSeparator jSeparator9;
    private javax.swing.JTextField jTextFieldAImag;
    private javax.swing.JTextField jTextFieldAReal;
    private javax.swing.JTextField jTextFieldBImag;
    private javax.swing.JTextField jTextFieldBReal;
    private javax.swing.JTextField jTextFieldCImag;
    private javax.swing.JTextField jTextFieldCReal;
    private javax.swing.JTextField jTextFieldDImag;
    private javax.swing.JTextField jTextFieldDReal;
    private javax.swing.JTextField jTextFieldY1Imag;
    private javax.swing.JTextField jTextFieldY1Real;
    private javax.swing.JTextField jTextFieldY2Imag;
    private javax.swing.JTextField jTextFieldY2Real;
    private javax.swing.JTextField jTextFieldZ1Imag;
    private javax.swing.JTextField jTextFieldZ1Real;
    private javax.swing.JTextField jTextFieldZ2Imag;
    private javax.swing.JTextField jTextFieldZ2Real;
    private javax.swing.JTextArea jTextAreaAyuda;
    // End of variables declaration//GEN-END:variables

    public void ModeloZ(){
        jMonoModelo.setIcon(new javax.swing.ImageIcon(getClass().getResource("images/Z.jpg")));
        jLabelZ1.setVisible(true);
        jLabelZ1j.setVisible(true);
        jTextFieldZ1Real.setVisible(true);
        jTextFieldZ1Imag.setVisible(true);
        jLabelZ1.setText("Z: ");
        LabelZ1Unit.setVisible(true);
        jLabelY1.setVisible(false);
        jLabelY1j.setVisible(false);
        jTextFieldY1Real.setVisible(false);
        jTextFieldY1Imag.setVisible(false);
        LabelY1Unit.setVisible(false);
        jLabelZ2.setVisible(false);
        jLabelZ2j.setVisible(false);
        jTextFieldZ2Real.setVisible(false);
        jTextFieldZ2Imag.setVisible(false);
        LabelZ2Unit.setVisible(false);
        jLabelY2.setVisible(false);
        jLabelY2j.setVisible(false);
        jTextFieldY2Real.setVisible(false);
        jTextFieldY2Imag.setVisible(false);
        LabelY2Unit.setVisible(false);
        try{
            if(FormaRealImag){
                Z1 = new Complex(Double.parseDouble(jTextFieldZ1Real.getText()),Double.parseDouble(jTextFieldZ1Imag.getText()));
            }else{
                Z1 = new Complex(Double.parseDouble(jTextFieldZ1Real.getText()),Double.parseDouble(jTextFieldZ1Imag.getText()),1);
            }
        }catch (Exception e){
            JOptionPane error=new JOptionPane();
            error.showMessageDialog(null, "Algunos de los datos introducidos son invalidos", "Error en los datos", JOptionPane.ERROR_MESSAGE);
            return;
        }
        ABCD = Tetrapolo.ImpedanciaZ(Z1);

    }
    
    public void ModeloY(){
        jMonoModelo.setIcon(new javax.swing.ImageIcon(getClass().getResource("images/Y.jpg")));
        jLabelZ1.setVisible(false);
        jLabelZ1j.setVisible(false);
        jTextFieldZ1Real.setVisible(false);
        jTextFieldZ1Imag.setVisible(false);
        LabelZ1Unit.setVisible(false);
        jLabelY1.setVisible(true);
        jLabelY1j.setVisible(true);
        jTextFieldY1Real.setVisible(true);
        jTextFieldY1Imag.setVisible(true);
        LabelY1Unit.setVisible(true);
        jLabelY1.setText("Y: ");
        jLabelZ2.setVisible(false);
        jLabelZ2j.setVisible(false);
        jTextFieldZ2Real.setVisible(false);
        jTextFieldZ2Imag.setVisible(false);
        LabelZ2Unit.setVisible(false);
        jLabelY2.setVisible(false);
        jLabelY2j.setVisible(false);
        jTextFieldY2Real.setVisible(false);
        jTextFieldY2Imag.setVisible(false);
        LabelY2Unit.setVisible(false);
        try{
            if(FormaRealImag){
                Y1 = new Complex(Double.parseDouble(jTextFieldY1Real.getText()),Double.parseDouble(jTextFieldY1Imag.getText()));
            }else{
                Y1 = new Complex(Double.parseDouble(jTextFieldY1Real.getText()),Double.parseDouble(jTextFieldY1Imag.getText()),1);
            }
        }catch (NumberFormatException e){
            JOptionPane.showMessageDialog(this, "Algunos de los datos introducidos son invalidos", "Error en los datos", JOptionPane.ERROR_MESSAGE);
            return;
        }
        ABCD = Tetrapolo.ImpedanciaY(Y1);

    }
    
    public void ModeloG(){
        jMonoModelo.setIcon(new javax.swing.ImageIcon(getClass().getResource("images/G.jpg")));
        jLabelZ1.setVisible(true);
        jLabelZ1j.setVisible(true);
        jTextFieldZ1Real.setVisible(true);
        jTextFieldZ1Imag.setVisible(true);
        jLabelZ1.setText("Z: ");
        LabelZ1Unit.setVisible(true);
        jLabelY1.setVisible(true);
        jLabelY1j.setVisible(true);
        jTextFieldY1Real.setVisible(true);
        jTextFieldY1Imag.setVisible(true);
        LabelY1Unit.setVisible(true);
        jLabelY1.setText("Y: ");
        jLabelZ2.setVisible(false);
        jLabelZ2j.setVisible(false);
        jTextFieldZ2Real.setVisible(false);
        jTextFieldZ2Imag.setVisible(false);
        LabelZ2Unit.setVisible(false);
        jLabelY2.setVisible(false);
        jLabelY2j.setVisible(false);
        jTextFieldY2Real.setVisible(false);
        jTextFieldY2Imag.setVisible(false);
        LabelY2Unit.setVisible(false);
        try{
            if(FormaRealImag){
                Z1 = new Complex(Double.parseDouble(jTextFieldZ1Real.getText()),Double.parseDouble(jTextFieldZ1Imag.getText()));
                Y1 = new Complex(Double.parseDouble(jTextFieldY1Real.getText()),Double.parseDouble(jTextFieldY1Imag.getText()));
            }else{
                Z1 = new Complex(Double.parseDouble(jTextFieldZ1Real.getText()),Double.parseDouble(jTextFieldZ1Imag.getText()),1);
                Y1 = new Complex(Double.parseDouble(jTextFieldY1Real.getText()),Double.parseDouble(jTextFieldY1Imag.getText()),1);
            }
        }catch (Exception e){
            JOptionPane error=new JOptionPane();
            error.showMessageDialog(null, "Algunos de los datos introducidos son invalidos", "Error en los datos", JOptionPane.ERROR_MESSAGE);
            return;
        }            
        ABCD = Tetrapolo.CircuitoG(Z1,Y1);
    }
    
    public void Modelo7(){
        jMonoModelo.setIcon(new javax.swing.ImageIcon(getClass().getResource("images/7.jpg")));
        jLabelZ1.setVisible(true);
        jLabelZ1j.setVisible(true);
        jTextFieldZ1Real.setVisible(true);
        jTextFieldZ1Imag.setVisible(true);
        jLabelZ1.setText("Z: ");
        LabelZ1Unit.setVisible(true);
        jLabelY1.setVisible(true);
        jLabelY1j.setVisible(true);
        jTextFieldY1Real.setVisible(true);
        jTextFieldY1Imag.setVisible(true);
        LabelY1Unit.setVisible(true);
        jLabelY1.setText("Y: ");
        jLabelZ2.setVisible(false);
        jLabelZ2j.setVisible(false);
        jTextFieldZ2Real.setVisible(false);
        jTextFieldZ2Imag.setVisible(false);
        LabelZ2Unit.setVisible(false);
        jLabelY2.setVisible(false);
        jLabelY2j.setVisible(false);
        jTextFieldY2Real.setVisible(false);
        jTextFieldY2Imag.setVisible(false);
        LabelY2Unit.setVisible(false);
        try{
            if(FormaRealImag){
                Z1 = new Complex(Double.parseDouble(jTextFieldZ1Real.getText()),Double.parseDouble(jTextFieldZ1Imag.getText()));
                Y1 = new Complex(Double.parseDouble(jTextFieldY1Real.getText()),Double.parseDouble(jTextFieldY1Imag.getText()));
            }else{
                Z1 = new Complex(Double.parseDouble(jTextFieldZ1Real.getText()),Double.parseDouble(jTextFieldZ1Imag.getText()),1);
                Y1 = new Complex(Double.parseDouble(jTextFieldY1Real.getText()),Double.parseDouble(jTextFieldY1Imag.getText()),1);
            }
        }catch (Exception e){
                JOptionPane error=new JOptionPane();
		error.showMessageDialog(null, "Algunos de los datos introducidos son invalidos", "Error en los datos", JOptionPane.ERROR_MESSAGE);
		return;
        }            
        ABCD = Tetrapolo.Circuito7(Z1,Y1);
    }
    
    public void ModeloPi(){
        jMonoModelo.setIcon(new javax.swing.ImageIcon(getClass().getResource("images/Pi.jpg")));
        jLabelZ1.setVisible(true);
        jLabelZ1j.setVisible(true);
        jTextFieldZ1Real.setVisible(true);
        jTextFieldZ1Imag.setVisible(true);
        jLabelZ1.setText("Z: ");
        LabelZ1Unit.setVisible(true);
        jLabelY1.setVisible(true);
        jLabelY1j.setVisible(true);
        jTextFieldY1Real.setVisible(true);
        jTextFieldY1Imag.setVisible(true);
        LabelY1Unit.setVisible(true);
        jLabelY1.setText("Y1: ");
        jLabelZ2.setVisible(false);
        jLabelZ2j.setVisible(false);
        jTextFieldZ2Real.setVisible(false);
        jTextFieldZ2Imag.setVisible(false);
        LabelZ2Unit.setVisible(false);
        jLabelY2.setVisible(true);
        jLabelY2j.setVisible(true);
        jTextFieldY2Real.setVisible(true);
        jTextFieldY2Imag.setVisible(true);
        LabelY2Unit.setVisible(true);
        jLabelY2.setText("Y2: ");
        try{
            if(FormaRealImag){
                Z1 = new Complex(Double.parseDouble(jTextFieldZ1Real.getText()),Double.parseDouble(jTextFieldZ1Imag.getText()));
                Y1 = new Complex(Double.parseDouble(jTextFieldY1Real.getText()),Double.parseDouble(jTextFieldY1Imag.getText()));
                Y2 = new Complex(Double.parseDouble(jTextFieldY2Real.getText()),Double.parseDouble(jTextFieldY2Imag.getText()));
            }else{
                Z1 = new Complex(Double.parseDouble(jTextFieldZ1Real.getText()),Double.parseDouble(jTextFieldZ1Imag.getText()),1);
                Y1 = new Complex(Double.parseDouble(jTextFieldY1Real.getText()),Double.parseDouble(jTextFieldY1Imag.getText()),1);
                Y2 = new Complex(Double.parseDouble(jTextFieldY2Real.getText()),Double.parseDouble(jTextFieldY2Imag.getText()),1);
            }
        }catch (Exception e){
                JOptionPane error=new JOptionPane();
		error.showMessageDialog(null, "Algunos de los datos introducidos son invalidos", "Error en los datos", JOptionPane.ERROR_MESSAGE);
		return;
        }            
        ABCD = Tetrapolo.CircuitoPi(Z1,Y1,Y2);
    }
    
    public void ModeloT(){
        jMonoModelo.setIcon(new javax.swing.ImageIcon(getClass().getResource("images/T.jpg")));
        jLabelZ1.setVisible(true);
        jLabelZ1j.setVisible(true);
        jTextFieldZ1Real.setVisible(true);
        jTextFieldZ1Imag.setVisible(true);
        jLabelZ1.setText("Z1: ");
        LabelZ1Unit.setVisible(true);
        jLabelY1.setVisible(true);
        jLabelY1j.setVisible(true);
        jTextFieldY1Real.setVisible(true);
        jTextFieldY1Imag.setVisible(true);
        LabelY1Unit.setVisible(true);
        jLabelY1.setText("Y: ");
        jLabelZ2.setVisible(true);
        jLabelZ2j.setVisible(true);
        jTextFieldZ2Real.setVisible(true);
        jTextFieldZ2Imag.setVisible(true);
        LabelZ2Unit.setVisible(true);
        jLabelZ2.setText("Z2: ");
        jLabelY2.setVisible(false);
        jLabelY2j.setVisible(false);
        jTextFieldY2Real.setVisible(false);
        jTextFieldY2Imag.setVisible(false);
        LabelY2Unit.setVisible(false);
        try{
            if(FormaRealImag){
                Z1 = new Complex(Double.parseDouble(jTextFieldZ1Real.getText()),Double.parseDouble(jTextFieldZ1Imag.getText()));
                Y1 = new Complex(Double.parseDouble(jTextFieldY1Real.getText()),Double.parseDouble(jTextFieldY1Imag.getText()));
                Z2 = new Complex(Double.parseDouble(jTextFieldZ2Real.getText()),Double.parseDouble(jTextFieldZ2Imag.getText()));
            }else{
                Z1 = new Complex(Double.parseDouble(jTextFieldZ1Real.getText()),Double.parseDouble(jTextFieldZ1Imag.getText()),1);
                Y1 = new Complex(Double.parseDouble(jTextFieldY1Real.getText()),Double.parseDouble(jTextFieldY1Imag.getText()),1);
                Z2 = new Complex(Double.parseDouble(jTextFieldZ2Real.getText()),Double.parseDouble(jTextFieldZ2Imag.getText()),1);
            }
        }catch (Exception e){
                JOptionPane error=new JOptionPane();
		error.showMessageDialog(null, "Algunos de los datos introducidos son invalidos", "Error en los datos", JOptionPane.ERROR_MESSAGE);
		return;
        }            
        ABCD = Tetrapolo.CircuitoPi(Z1,Z2,Y1);
    }
    
    public void ModeloSteinmetz(){
        jMonoModelo.setIcon(new javax.swing.ImageIcon(getClass().getResource("images/Steinmetz.jpg")));
        jLabelZ1.setVisible(true);
        jLabelZ1j.setVisible(true);
        jTextFieldZ1Real.setVisible(true);
        jTextFieldZ1Imag.setVisible(true);
        jLabelZ1.setText("Z1: ");
        LabelZ1Unit.setVisible(true);
        jLabelY1.setVisible(true);
        jLabelY1j.setVisible(true);
        jTextFieldY1Real.setVisible(true);
        jTextFieldY1Imag.setVisible(true);
        LabelY1Unit.setVisible(true);
        jLabelY1.setText("Y1: ");
        jLabelZ2.setVisible(true);
        jLabelZ2j.setVisible(true);
        jTextFieldZ2Real.setVisible(true);
        jTextFieldZ2Imag.setVisible(true);
        LabelZ2Unit.setVisible(true);
        jLabelZ2.setText("Z2: ");
        jLabelY2.setVisible(true);
        jLabelY2j.setVisible(true);
        jTextFieldY2Real.setVisible(true);
        jTextFieldY2Imag.setVisible(true);
        LabelY2Unit.setVisible(true);
        jLabelY2.setText("Y2: ");
        try{
            if(FormaRealImag){
                Z1 = new Complex(Double.parseDouble(jTextFieldZ1Real.getText()),Double.parseDouble(jTextFieldZ1Imag.getText()));
                Y1 = new Complex(Double.parseDouble(jTextFieldY1Real.getText()),Double.parseDouble(jTextFieldY1Imag.getText()));
                Z2 = new Complex(Double.parseDouble(jTextFieldZ2Real.getText()),Double.parseDouble(jTextFieldZ2Imag.getText()));
                Y2 = new Complex(Double.parseDouble(jTextFieldY2Real.getText()),Double.parseDouble(jTextFieldY2Imag.getText()));
            }else{
                Z1 = new Complex(Double.parseDouble(jTextFieldZ1Real.getText()),Double.parseDouble(jTextFieldZ1Imag.getText()),1);
                Y1 = new Complex(Double.parseDouble(jTextFieldY1Real.getText()),Double.parseDouble(jTextFieldY1Imag.getText()),1);
                Z2 = new Complex(Double.parseDouble(jTextFieldZ2Real.getText()),Double.parseDouble(jTextFieldZ2Imag.getText()),1);
                Y2 = new Complex(Double.parseDouble(jTextFieldY2Real.getText()),Double.parseDouble(jTextFieldY2Imag.getText()),1);
            }
        }catch (Exception e){
                JOptionPane error=new JOptionPane();
		error.showMessageDialog(null, "Algunos de los datos introducidos son invalidos", "Error en los datos", JOptionPane.ERROR_MESSAGE);
		return;
        }            
        ABCD = Tetrapolo.CircuitoSteinmetz(Z1,Z2,Y1,Y2);
    }


    public void ModeloPorDefecto(){
        jComboBoxModelo.setSelectedIndex(4); // Modelo PI;
    }

    public void ValoresPorDefecto(){
        FormaRealImag = true;
        FormaAbsAng = false;
        jRadioButtonRealImag.setSelected(FormaRealImag);
        Comb_V1 = false;
        Comb_I1 = false;
        Comb_V2 = true;
        Comb_I2 = true;
        Z1 = new Complex(5.4187, 31.2407);
        Z2 = new Complex(5.4187, 31.2407);
        Y1 = new Complex(1.2374E-8, 1.17123E-4);
        Y2 = new Complex(1.2374E-8, 1.17123E-4);

        V2 = new Complex(13800,0);
        I2 = new Complex(7246.38,0); //Suponiendo Carga Nominal Resistiva a 100[MVA].

        // Llenando Forma de Mostrar Datos
        jRadioButtonRealImag.setSelected(FormaRealImag);
        jRadioButtonAbsAng.setSelected(FormaAbsAng);

        // Llenando Cuadros Parametros.
        FillParams();

        // Llenando Cuadro Voltajes y Corrientes.
        FillComplexBoxs(SpinnerV2Real, SpinnerV2Imag, V2);
        FillComplexBoxs(SpinnerI2Real, SpinnerI2Imag, I2);
        CheckBoxV1.setSelected(Comb_V1);
        CheckBoxV2.setSelected(Comb_V2);
        CheckBoxI1.setSelected(Comb_I1);
        CheckBoxI2.setSelected(Comb_I2);
        CheckBoxsVI();
        jButtonActualizarValores.setEnabled(false);
    }

    public void FillComplexBoxs(javax.swing.JSpinner real, javax.swing.JSpinner imag, Complex complejo){
        real.setValue(new Double(Truncar(complejo.re, NumeroDecimalesSpinner)));
        imag.setValue(new Double(Truncar(complejo.im, NumeroDecimalesSpinner)));
    }

    public void FillComplexBoxsAng(javax.swing.JSpinner mod, javax.swing.JSpinner ang, Complex complejo){
        mod.setValue(new Double(Truncar(Complex.abs(complejo), NumeroDecimalesSpinner)));
        ang.setValue(new Double(Truncar(Complex.argument_degree(complejo), NumeroDecimalesSpinner)));
    }

    public void FillComplexBoxs(javax.swing.JTextField real, javax.swing.JTextField imag, Complex complejo){
        real.setText(Double.toString(Truncar(complejo.re, NumeroDecimales)));
        imag.setText(Double.toString(Truncar(complejo.im, NumeroDecimales)));
    }

    public void FillComplexBoxsAng(javax.swing.JTextField mod, javax.swing.JTextField ang, Complex complejo){
        mod.setText(Double.toString(Truncar(Complex.abs(complejo), NumeroDecimales)));
        ang.setText(Double.toString(Truncar(Complex.argument_degree(complejo), NumeroDecimales)));
    }

    public void FillComplexBoxs(javax.swing.JTextField box, double n){
        box.setText(Double.toString(Truncar(n, NumeroDecimales)));
    }

    public void FillABCD(){
        if(FormaRealImag){
            FillComplexBoxs(jTextFieldAReal,jTextFieldAImag, ABCD.A);
            FillComplexBoxs(jTextFieldBReal,jTextFieldBImag, ABCD.B);
            FillComplexBoxs(jTextFieldCReal,jTextFieldCImag, ABCD.C);
            FillComplexBoxs(jTextFieldDReal,jTextFieldDImag, ABCD.D);
        }else{
            FillComplexBoxsAng(jTextFieldAReal,jTextFieldAImag, ABCD.A);
            FillComplexBoxsAng(jTextFieldBReal,jTextFieldBImag, ABCD.B);
            FillComplexBoxsAng(jTextFieldCReal,jTextFieldCImag, ABCD.C);
            FillComplexBoxsAng(jTextFieldDReal,jTextFieldDImag, ABCD.D);
         }
    }

    public void CalcularValores(){
       
        if(FormaRealImag){
           if(Comb_V1 && Comb_V2){
               try{
                   V1 = new Complex(((Number)SpinnerV1Real.getValue()).doubleValue(),((Number)SpinnerV1Imag.getValue()).doubleValue());
                   V2 = new Complex(((Number)SpinnerV2Real.getValue()).doubleValue(),((Number)SpinnerV2Imag.getValue()).doubleValue());
               }catch (Exception e){
                    JOptionPane error=new JOptionPane();
                    error.showMessageDialog(null, "Algunos de los datos introducidos son invalidos", "Error en los datos", JOptionPane.ERROR_MESSAGE);
                    return;
               }                   
               I1 = ABCD.getI1_cV1V2(V1,V2);
               I2 = ABCD.getI2_cV1V2(V1,V2);
           } else
           if (Comb_V1 && Comb_I1){
               try{
                   V1 = new Complex(((Number)SpinnerV1Real.getValue()).doubleValue(),((Number)SpinnerV1Imag.getValue()).doubleValue());
                   I1 = new Complex(((Number)SpinnerI1Real.getValue()).doubleValue(),((Number)SpinnerI1Imag.getValue()).doubleValue());
               }catch (Exception e){
                    JOptionPane error=new JOptionPane();
                    error.showMessageDialog(null, "Algunos de los datos introducidos son invalidos", "Error en los datos", JOptionPane.ERROR_MESSAGE);
                    return;
               }                   
               V2 = ABCD.getV2_cV1I1(V1, I1);
               I2 = ABCD.getI2_cV1I1(V1, I1);
           } else
           if (Comb_V1 && Comb_I2){
               try{
                   V1 = new Complex(((Number)SpinnerV1Real.getValue()).doubleValue(),((Number)SpinnerV1Imag.getValue()).doubleValue());
                   I2 = new Complex(((Number)SpinnerI2Real.getValue()).doubleValue(),((Number)SpinnerI2Imag.getValue()).doubleValue());
               }catch (Exception e){
                    JOptionPane error=new JOptionPane();
                    error.showMessageDialog(null, "Algunos de los datos introducidos son invalidos", "Error en los datos", JOptionPane.ERROR_MESSAGE);
                    return;
               }                   
               V2 = ABCD.getV2_cV1I2(V1, I2);
               I1 = ABCD.getI1_cV1I2(V1, I2);
           } else
           if (Comb_V2 && Comb_I1){
               try{
                   V2 = new Complex(((Number)SpinnerV2Real.getValue()).doubleValue(),((Number)SpinnerV2Imag.getValue()).doubleValue());
                   I1 = new Complex(((Number)SpinnerI1Real.getValue()).doubleValue(),((Number)SpinnerI1Imag.getValue()).doubleValue());
               }catch (Exception e){
                    JOptionPane error=new JOptionPane();
                    error.showMessageDialog(null, "Algunos de los datos introducidos son invalidos", "Error en los datos", JOptionPane.ERROR_MESSAGE);
                    return;
               }                   
               V1 = ABCD.getV1_cV2I1(V2, I1);
               I2 = ABCD.getI2_cV2I1(V2, I1);
           } else
           if (Comb_V2 && Comb_I2){
               try{
                   V2 = new Complex(((Number)SpinnerV2Real.getValue()).doubleValue(),((Number)SpinnerV2Imag.getValue()).doubleValue());
                   I2 = new Complex(((Number)SpinnerI2Real.getValue()).doubleValue(),((Number)SpinnerI2Imag.getValue()).doubleValue());
               }catch (Exception e){
                    JOptionPane error=new JOptionPane();
                    error.showMessageDialog(null, "Algunos de los datos introducidos son invalidos", "Error en los datos", JOptionPane.ERROR_MESSAGE);
                    return;
               }                   
               V1 = ABCD.getV1_cV2I2(V2, I2);
               I1 = ABCD.getI1_cV2I2(V2, I2);
           } else
           if (Comb_I1 && Comb_I2){
               try{
                   I1 = new Complex(((Number)SpinnerI1Real.getValue()).doubleValue(),((Number)SpinnerI1Imag.getValue()).doubleValue());
                   I2 = new Complex(((Number)SpinnerI2Real.getValue()).doubleValue(),((Number)SpinnerI2Imag.getValue()).doubleValue());
               }catch (Exception e){
                    JOptionPane error=new JOptionPane();
                    error.showMessageDialog(null, "Algunos de los datos introducidos son invalidos", "Error en los datos", JOptionPane.ERROR_MESSAGE);
                    return;
               }                   
               V1 = ABCD.getV1_cI1I2(I1, I2);
               V2 = ABCD.getV2_cI1I2(I1, I2);
           }
       }else{
           if(Comb_V1 && Comb_V2){
               try{
                   V1 = new Complex(((Number)SpinnerV1Real.getValue()).doubleValue(),((Number)SpinnerV1Imag.getValue()).doubleValue(),1);
                   V2 = new Complex(((Number)SpinnerV2Real.getValue()).doubleValue(),((Number)SpinnerV2Imag.getValue()).doubleValue(),1);
               }catch (Exception e){
                    JOptionPane error=new JOptionPane();
                    error.showMessageDialog(null, "Algunos de los datos introducidos son invalidos", "Error en los datos", JOptionPane.ERROR_MESSAGE);
                    return;
               }                   
               I1 = ABCD.getI1_cV1V2(V1,V2);
               I2 = ABCD.getI2_cV1V2(V1,V2);
           } else
           if (Comb_V1 && Comb_I1){
               try{
                   V1 = new Complex(((Number)SpinnerV1Real.getValue()).doubleValue(),((Number)SpinnerV1Imag.getValue()).doubleValue(),1);
                   I1 = new Complex(((Number)SpinnerI1Real.getValue()).doubleValue(),((Number)SpinnerI1Imag.getValue()).doubleValue(),1);
               }catch (Exception e){
                    JOptionPane error=new JOptionPane();
                    error.showMessageDialog(null, "Algunos de los datos introducidos son invalidos", "Error en los datos", JOptionPane.ERROR_MESSAGE);
                    return;
               }                   
               V2 = ABCD.getV2_cV1I1(V1, I1);
               I2 = ABCD.getI2_cV1I1(V1, I1);
           } else
           if (Comb_V1 && Comb_I2){
               try{
                   V1 = new Complex(((Number)SpinnerV1Real.getValue()).doubleValue(),((Number)SpinnerV1Imag.getValue()).doubleValue(),1);
                   I2 = new Complex(((Number)SpinnerI2Real.getValue()).doubleValue(),((Number)SpinnerI2Imag.getValue()).doubleValue(),1);
               }catch (Exception e){
                    JOptionPane error=new JOptionPane();
                    error.showMessageDialog(null, "Algunos de los datos introducidos son invalidos", "Error en los datos", JOptionPane.ERROR_MESSAGE);
                    return;
               }                   
               V2 = ABCD.getV2_cV1I2(V1, I2);
               I1 = ABCD.getI1_cV1I2(V1, I2);
           } else
           if (Comb_V2 && Comb_I1){
               try{
                   V2 = new Complex(((Number)SpinnerV2Real.getValue()).doubleValue(),((Number)SpinnerV2Imag.getValue()).doubleValue(),1);
                   I1 = new Complex(((Number)SpinnerI1Real.getValue()).doubleValue(),((Number)SpinnerI1Imag.getValue()).doubleValue(),1);
               }catch (Exception e){
                    JOptionPane error=new JOptionPane();
                    error.showMessageDialog(null, "Algunos de los datos introducidos son invalidos", "Error en los datos", JOptionPane.ERROR_MESSAGE);
                    return;
               }                   
               V1 = ABCD.getV1_cV2I1(V2, I1);
               I2 = ABCD.getI2_cV2I1(V2, I1);
           } else
           if (Comb_V2 && Comb_I2){
               try{
                   V2 = new Complex(((Number)SpinnerV2Real.getValue()).doubleValue(),((Number)SpinnerV2Imag.getValue()).doubleValue(),1);
                   I2 = new Complex(((Number)SpinnerI2Real.getValue()).doubleValue(),((Number)SpinnerI2Imag.getValue()).doubleValue(),1);
               }catch (Exception e){
                    JOptionPane error=new JOptionPane();
                    error.showMessageDialog(null, "Algunos de los datos introducidos son invalidos", "Error en los datos", JOptionPane.ERROR_MESSAGE);
                    return;
               }                   
               V1 = ABCD.getV1_cV2I2(V2, I2);
               I1 = ABCD.getI1_cV2I2(V2, I2);
           } else
           if (Comb_I1 && Comb_I2){
               try{
                   I1 = new Complex(((Number)SpinnerI1Real.getValue()).doubleValue(),((Number)SpinnerI1Imag.getValue()).doubleValue(),1);
                   I2 = new Complex(((Number)SpinnerI2Real.getValue()).doubleValue(),((Number)SpinnerI2Imag.getValue()).doubleValue(),1);
               }catch (Exception e){
                    JOptionPane error=new JOptionPane();
                    error.showMessageDialog(null, "Algunos de los datos introducidos son invalidos", "Error en los datos", JOptionPane.ERROR_MESSAGE);
                    return;
               }                   
               V1 = ABCD.getV1_cI1I2(I1, I2);
               V2 = ABCD.getV2_cI1I2(I1, I2);
           }
       }

       S1 = ABCD.getS1(V1,I1);
       S2 = ABCD.getS2(V2,I2);
       P1 = S1.re;
       P2 = S2.re;
       Q1 = S1.im;
       Q2 = S2.im;
       Phi1 = Complex.argument_degree(S1);
       Phi2 = Complex.argument_degree(S2);

    }

    public void FillValores(){
        if(FormaRealImag){
                FillComplexBoxs(SpinnerV1Real, SpinnerV1Imag, V1);
                FillComplexBoxs(SpinnerI1Real, SpinnerI1Imag, I1);
                FillComplexBoxs(SpinnerV2Real, SpinnerV2Imag, V2);
                FillComplexBoxs(SpinnerI2Real, SpinnerI2Imag, I2);
        }else{
                FillComplexBoxsAng(SpinnerV1Real, SpinnerV1Imag, V1);
                FillComplexBoxsAng(SpinnerI1Real, SpinnerI1Imag, I1);
                FillComplexBoxsAng(SpinnerV2Real, SpinnerV2Imag, V2);
                FillComplexBoxsAng(SpinnerI2Real, SpinnerI2Imag, I2);
        }

        FillComplexBoxs(TextFieldP1, TextFieldQ1, S1);
        FillComplexBoxs(TextFieldP2, TextFieldQ2, S2);
        FillComplexBoxs(TextFieldPhi1, Phi1);
        FillComplexBoxs(TextFieldPhi2, Phi2);
    }


    private double Truncar(double x,int k){
		return Math.round( Math.pow(10,k) * x )/Math.pow(10,k);
    }

    public void CheckModificar(){
        if(jCheckBoxModificarABCD.isSelected()){
            ABCD_Modif = true;
            jButtonActualizarModelo.setEnabled(false);
            jTextFieldAReal.setEditable(true);
            jTextFieldBReal.setEditable(true);
            jTextFieldCReal.setEditable(true);
            jTextFieldDReal.setEditable(true);
            jTextFieldAImag.setEditable(true);
            jTextFieldBImag.setEditable(true);
            jTextFieldCImag.setEditable(true);
            jTextFieldDImag.setEditable(true);
            jTextFieldZ1Real.setEnabled(false);
            jTextFieldZ1Imag.setEnabled(false);
            jTextFieldY1Real.setEnabled(false);
            jTextFieldY1Imag.setEnabled(false);
            jTextFieldZ2Real.setEnabled(false);
            jTextFieldZ2Imag.setEnabled(false);
            jTextFieldY2Real.setEnabled(false);
            jTextFieldY2Imag.setEnabled(false);
        }else{
            ABCD_Modif = false;
            jButtonActualizarModelo.setEnabled(true);
            jTextFieldAReal.setEditable(false);
            jTextFieldBReal.setEditable(false);
            jTextFieldCReal.setEditable(false);
            jTextFieldDReal.setEditable(false);
            jTextFieldAImag.setEditable(false);
            jTextFieldBImag.setEditable(false);
            jTextFieldCImag.setEditable(false);
            jTextFieldDImag.setEditable(false);
            jTextFieldZ1Real.setEnabled(true);
            jTextFieldZ1Imag.setEnabled(true);
            jTextFieldY1Real.setEnabled(true);
            jTextFieldY1Imag.setEnabled(true);
            jTextFieldZ2Real.setEnabled(true);
            jTextFieldZ2Imag.setEnabled(true);
            jTextFieldY2Real.setEnabled(true);
            jTextFieldY2Imag.setEnabled(true);
        }
    }

    public int CheckBoxVI(){
        int count = 0;
        if(CheckBoxV1.isSelected())
            count++;
        if(CheckBoxV2.isSelected())
            count++;
        if(CheckBoxI1.isSelected())
            count++;
        if(CheckBoxI2.isSelected())
            count++;
        if(count < 2)
            jButtonActualizarValores.setEnabled(false);
        else
            jButtonActualizarValores.setEnabled(true);
        return count;
    }

    public void CheckBoxsVI(){
        SpinnerV1Real.setEnabled(Comb_V1);
        SpinnerV1Imag.setEnabled(Comb_V1);
        SpinnerV2Real.setEnabled(Comb_V2);
        SpinnerV2Imag.setEnabled(Comb_V2);
        SpinnerI1Real.setEnabled(Comb_I1);
        SpinnerI1Imag.setEnabled(Comb_I1);
        SpinnerI2Real.setEnabled(Comb_I2);
        SpinnerI2Imag.setEnabled(Comb_I2);
    }

    public void ChangeForma(int forma){
        if(forma == 0){
            if(FormaRealImag){
                jRadioButtonRealImag.setSelected(FormaRealImag);
                return;
            }
            FormaRealImag = true;
            FormaAbsAng = false;
            jRadioButtonAbsAng.setSelected(FormaAbsAng);
            ChangeToRealImag();
        }
        if(forma == 1){
            if(FormaAbsAng){
                jRadioButtonAbsAng.setSelected(FormaAbsAng);
                return;
            }
            FormaRealImag = false;
            FormaAbsAng = true;
            jRadioButtonRealImag.setSelected(FormaRealImag);
            ChangeToAbsAng();
        }
        FillAll();
    }

    private void ChangeToRealImag(){
        jLabelReal1.setText("Real");
        jLabelReal2.setText("Real");
        jLabelImag1.setText("Imag");
        jLabelImag2.setText("Imag");
        jLabelV1j.setText(" + j");
        jLabelV2j.setText(" + j");
        jLabelI1j.setText(" + j");
        jLabelI2j.setText(" + j");
        jLabelZ1j.setText("   +   j");
        jLabelZ2j.setText("   +   j");
        jLabelY1j.setText("   +   j");
        jLabelY2j.setText("   +   j");
        jLabelAj.setText("   +   j");
        jLabelBj.setText("   +   j");
        jLabelCj.setText("   +   j");
        jLabelDj.setText("   +   j");
    }

    private void ChangeToAbsAng(){
        jLabelReal1.setText("Mod");
        jLabelReal2.setText("Mod");
        jLabelImag1.setText("Ang");
        jLabelImag2.setText("Ang");
        jLabelV1j.setText("  < ");
        jLabelV2j.setText("  < ");
        jLabelI1j.setText("  < ");
        jLabelI2j.setText("  < ");
        jLabelZ1j.setText("     <  ");
        jLabelZ2j.setText("     <  ");
        jLabelY1j.setText("     <  ");
        jLabelY2j.setText("     <  ");
        jLabelAj.setText("     <  ");
        jLabelBj.setText("     <  ");
        jLabelCj.setText("     <  ");
        jLabelDj.setText("     <  ");
    }

    private void FillParams(){
        if(FormaRealImag){
            FillComplexBoxs(jTextFieldZ1Real, jTextFieldZ1Imag, Z1);
            FillComplexBoxs(jTextFieldY1Real, jTextFieldY1Imag, Y1);
            FillComplexBoxs(jTextFieldZ2Real, jTextFieldZ2Imag, Z2);
            FillComplexBoxs(jTextFieldY2Real, jTextFieldY2Imag, Y2);
        }else{
            FillComplexBoxsAng(jTextFieldZ1Real, jTextFieldZ1Imag, Z1);
            FillComplexBoxsAng(jTextFieldY1Real, jTextFieldY1Imag, Y1);
            FillComplexBoxsAng(jTextFieldZ2Real, jTextFieldZ2Imag, Z2);
            FillComplexBoxsAng(jTextFieldY2Real, jTextFieldY2Imag, Y2);
        }
    }

    private void FillAll(){
        FillParams();
        FillABCD();
        FillValores();

    }
}