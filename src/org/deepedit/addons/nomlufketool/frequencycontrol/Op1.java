package org.deepedit.addons.nomlufketool.frequencycontrol;

import javax.swing.*;
import java.io.*;
import java.net.*;
import java.awt.*;
import java.awt.event.*;

public class Op1 extends javax.swing.JPanel {

    Op2 op2;
    private final Font txtFont = new Font(Font.DIALOG_INPUT, Font.PLAIN, 9);

    /**
     * Creates new form Op1
     */
    public Op1() {
        jPanel11 = new javax.swing.JPanel();
        jPanel11.setBackground(new java.awt.Color(255, 255, 255));
        initComponents();
    }

    public void Metodos(Op2 o) {
        op2 = o;
    }

    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel2 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        h = new javax.swing.JTextField();
        r = new javax.swing.JTextField();
        sb = new javax.swing.JTextField();
        fs = new javax.swing.JTextField();
        mh = new javax.swing.JButton();
        mr = new javax.swing.JButton();
        msb = new javax.swing.JButton();
        mfs = new javax.swing.JButton();
        FS = new javax.swing.JLabel();
        SB = new javax.swing.JLabel();
        R = new javax.swing.JLabel();
        H = new javax.swing.JLabel();
        D = new javax.swing.JLabel();
        md = new javax.swing.JButton();
        d = new javax.swing.JTextField();
        jPanel6 = new javax.swing.JPanel();
        //ojo
        fotos = new javax.swing.JLabel();
        //
        jLabel4 = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        help = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jPanel10 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();

        jPanel1 = new javax.swing.JPanel();
        TITULO = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        CALCULOPU = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        CALCULOPU.setEnabled(false);
        jPanel12 = new javax.swing.JPanel();
        BotonADE = new javax.swing.JButton();
        setLayout(new java.awt.GridBagLayout());

        setMinimumSize(new java.awt.Dimension(650, 508));
        setPreferredSize(new java.awt.Dimension(650, 508));
        jPanel2.setLayout(new java.awt.GridBagLayout());

        jPanel2.setBackground(new java.awt.Color(20, 20, 167));

        jPanel2.setDoubleBuffered(false);
        jPanel2.setMinimumSize(new java.awt.Dimension(250, 350));
        jPanel2.setPreferredSize(new java.awt.Dimension(250, 300));
        jPanel5.setLayout(new java.awt.GridBagLayout());

        jPanel5.setBorder(new javax.swing.border.TitledBorder("Par�metros de Inter�s"));
        jPanel5.setMinimumSize(new java.awt.Dimension(250, 170));
        jPanel5.setPreferredSize(new java.awt.Dimension(250, 180));
        h.setText("\n");
        h.setFont(txtFont);
        h.setMinimumSize(new java.awt.Dimension(65, 19));
        h.setPreferredSize(new java.awt.Dimension(65, 19));
        h.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                hMouseExited(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        jPanel5.add(h, gridBagConstraints);
        r.setFont(txtFont);
        r.setMinimumSize(new java.awt.Dimension(65, 19));
        r.setPreferredSize(new java.awt.Dimension(65, 19));
        r.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                rMouseExited(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        jPanel5.add(r, gridBagConstraints);
        sb.setFont(txtFont);
        sb.setMinimumSize(new java.awt.Dimension(65, 19));
        sb.setPreferredSize(new java.awt.Dimension(65, 19));
        sb.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                sbMouseExited(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        jPanel5.add(sb, gridBagConstraints);
        fs.setFont(txtFont);
        fs.setMinimumSize(new java.awt.Dimension(65, 19));
        fs.setPreferredSize(new java.awt.Dimension(65, 19));
        fs.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                fsMouseExited(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        jPanel5.add(fs, gridBagConstraints);

        mh.setText("Calcular\n");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        jPanel5.add(mh, gridBagConstraints);

        mr.setText("Calcular");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        jPanel5.add(mr, gridBagConstraints);

        msb.setText("Calcular");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        jPanel5.add(msb, gridBagConstraints);

        mfs.setText("Calcular\n");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 3;
        jPanel5.add(mfs, gridBagConstraints);

        FS.setText("F. base  :fs");
        FS.setMaximumSize(new java.awt.Dimension(80, 15));
        FS.setMinimumSize(new java.awt.Dimension(80, 15));
        FS.setPreferredSize(new java.awt.Dimension(80, 15));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        jPanel5.add(FS, gridBagConstraints);

        SB.setText("P. base  :Sb");
        SB.setMaximumSize(new java.awt.Dimension(80, 15));
        SB.setMinimumSize(new java.awt.Dimension(80, 15));
        SB.setPreferredSize(new java.awt.Dimension(80, 15));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        jPanel5.add(SB, gridBagConstraints);

        R.setText("Estat.     :R");
        R.setMaximumSize(new java.awt.Dimension(80, 15));
        R.setMinimumSize(new java.awt.Dimension(80, 15));
        R.setPreferredSize(new java.awt.Dimension(80, 15));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        jPanel5.add(R, gridBagConstraints);

        H.setText("Inercia   :H");
        H.setMaximumSize(new java.awt.Dimension(80, 15));
        H.setMinimumSize(new java.awt.Dimension(80, 15));
        H.setPreferredSize(new java.awt.Dimension(80, 15));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel5.add(H, gridBagConstraints);

        D.setText("Sens.     :D");
        D.setMaximumSize(new java.awt.Dimension(80, 15));
        D.setMinimumSize(new java.awt.Dimension(80, 15));
        D.setPreferredSize(new java.awt.Dimension(80, 15));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        jPanel5.add(D, gridBagConstraints);

        md.setText("Calcular\n");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        jPanel5.add(md, gridBagConstraints);

        d.setText("\n");
        d.setFont(txtFont);
        d.setMinimumSize(new java.awt.Dimension(65, 19));
        d.setPreferredSize(new java.awt.Dimension(65, 19));
        d.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                dMouseExited(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        jPanel5.add(d, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.gridheight = 4;
        jPanel2.add(jPanel5, gridBagConstraints);

        jPanel6.setLayout(new java.awt.GridBagLayout());

        jPanel6.setBorder(new javax.swing.border.TitledBorder("Ecuaci\u00f3n del Generador:"));
        jPanel6.setMinimumSize(new java.awt.Dimension(250, 90));
        jPanel6.setPreferredSize(new java.awt.Dimension(244, 90));
        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("images/foto1.jpg")));
        jLabel4.setText("\n");
        //ojo
        fotos.setIcon(new javax.swing.ImageIcon(getClass().getResource("images/blanco.jpg")));
        fotos.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        fotos.setVerticalTextPosition(javax.swing.SwingConstants.NORTH);
        fotos.setMaximumSize(new java.awt.Dimension(500, 180));
        fotos.setMinimumSize(new java.awt.Dimension(320, 370));
        fotos.setPreferredSize(new java.awt.Dimension(360, 220));
        fotos.setText("\n");

        jLabel4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel4.setMaximumSize(new java.awt.Dimension(240, 60));
        jLabel4.setMinimumSize(new java.awt.Dimension(235, 60));
        jLabel4.setPreferredSize(new java.awt.Dimension(235, 60));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridheight = 3;
        jPanel6.add(jLabel4, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTH;
        jPanel2.add(jPanel6, gridBagConstraints);

        jPanel8.setBorder(new javax.swing.border.TitledBorder("Teorema del valor final"));
        jPanel8.setMinimumSize(new java.awt.Dimension(250, 90));
        jPanel8.setPreferredSize(new java.awt.Dimension(250, 90));
        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("images/foto2.jpg")));
        jLabel2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel2.setMaximumSize(new java.awt.Dimension(240, 60));
        jLabel2.setMinimumSize(new java.awt.Dimension(240, 60));
        jLabel2.setPreferredSize(new java.awt.Dimension(240, 60));
        jPanel8.add(jLabel2);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        jPanel2.add(jPanel8, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.gridheight = 4;
        add(jPanel2, gridBagConstraints);

        jPanel3.setBorder(new javax.swing.border.TitledBorder("Ayuda"));
        jPanel3.setMinimumSize(new java.awt.Dimension(650, 50));
        jPanel3.setPreferredSize(new java.awt.Dimension(650, 50));
        help.setText("\n");
        jPanel3.add(help);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.gridwidth = 10;
        add(jPanel3, gridBagConstraints);

        jPanel4.setLayout(new java.awt.GridBagLayout());

        jPanel4.setBorder(new javax.swing.border.TitledBorder("Calculo de los parametros (fisicos) en por unidad, paso a paso"));
        jPanel4.setMinimumSize(new java.awt.Dimension(400, 350));
        jPanel4.setPreferredSize(new java.awt.Dimension(450, 300));
        jPanel10.setBorder(new javax.swing.border.TitledBorder("Resultado Num\u00e9rico"));
        jPanel10.setMinimumSize(new java.awt.Dimension(380, 65));
        jPanel10.setOpaque(false);
        jPanel10.setPreferredSize(new java.awt.Dimension(380, 86));
        jLabel1.setText("");
        jLabel1.setMaximumSize(new java.awt.Dimension(370, 20));
        jLabel1.setMinimumSize(new java.awt.Dimension(370, 20));
        jLabel1.setPreferredSize(new java.awt.Dimension(370, 20));
        jPanel10.add(jLabel1);

        jPanel4.add(jPanel10, new java.awt.GridBagConstraints());

        jPanel11.setBorder(new javax.swing.border.TitledBorder("Paso a Paso"));
        jPanel11.setMinimumSize(new java.awt.Dimension(380, 250));
        jPanel11.setPreferredSize(new java.awt.Dimension(380, 250));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        //ojo
        jPanel11.add(fotos);
        //
        jPanel4.add(jPanel11, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 6;
        gridBagConstraints.gridheight = 4;
        add(jPanel4, gridBagConstraints);

        jPanel1.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        TITULO.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        TITULO.setText("CONTROL DE FRECUENCIA CARGA");
        jPanel1.add(TITULO);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        add(jPanel1, gridBagConstraints);

        jPanel7.setLayout(new java.awt.GridBagLayout());

        CALCULOPU.setText("Calculo [0/1]");
        CALCULOPU.setMaximumSize(new java.awt.Dimension(115, 25));
        CALCULOPU.setMinimumSize(new java.awt.Dimension(115, 25));
        CALCULOPU.setPreferredSize(new java.awt.Dimension(115, 25));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanel7.add(CALCULOPU, gridBagConstraints);

        jButton1.setText("LFC");
        jButton1.setMaximumSize(new java.awt.Dimension(115, 25));
        jButton1.setMinimumSize(new java.awt.Dimension(115, 25));
        jButton1.setPreferredSize(new java.awt.Dimension(115, 25));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButton1MouseEntered(evt);
            }
        });

        jPanel7.add(jButton1, new java.awt.GridBagConstraints());

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        add(jPanel7, gridBagConstraints);

        jPanel12.setLayout(new java.awt.GridBagLayout());

        BotonADE.setText("Ayuda");
        BotonADE.setMaximumSize(new java.awt.Dimension(115, 25));
        BotonADE.setMinimumSize(new java.awt.Dimension(115, 25));
        BotonADE.setPreferredSize(new java.awt.Dimension(115, 25));
        jPanel12.add(BotonADE, new java.awt.GridBagConstraints());

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 9;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        add(jPanel12, gridBagConstraints);

        BotonADE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonADEActionPerformed(evt);
            }
        });
        BotonADE.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BotonADEMouseEntered(evt);
            }
        });
//***** lo que se debe copiar *//
        mh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mhActionPerformed(evt);
            }
        });
        mh.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                mhMouseEntered(evt);
            }
        });

        mr.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mrActionPerformed(evt);
            }
        });
        mr.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                mrMouseEntered(evt);
            }
        });

        msb.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                msbActionPerformed(evt);
            }
        });
        msb.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                msbMouseEntered(evt);
            }
        });

        mfs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mfsActionPerformed(evt);
            }
        });
        mfs.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                mfsMouseEntered(evt);
            }
        });

        md.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mdActionPerformed(evt);
            }
        });
        md.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                mdMouseEntered(evt);
            }
        });

    }
    // </editor-fold>

    private void dMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dMouseExited
        help.setText("En este campo usted debe ingresar la sensibilidad de la frecuaencia ante variaciones en la carga D en [MW/Hz]");
        h.setEditable(true);
        r.setEditable(true);
        sb.setEditable(true);
        fs.setEditable(true);
        d.setEditable(true);

    }//GEN-LAST:event_dMouseExited

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        //op1.setVisible(false);
        fotos.setIcon(new javax.swing.ImageIcon(getClass().getResource("images/blanco.jpg")));
        op2.setVisible(true);
        setVisible(false);
        op2.repaint();
    }//GEN-LAST:event_jButton1ActionPerformed

    //private void jButton1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_fsMouseExited
    //fotos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/fotos/blanco.jpg")));
    //}//GEN-LAST:event_fsMouseExited
    private void fsMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_fsMouseExited
        help.setText("En este cuadro usted debe ingresar la frecuancia base fs en [Hz]");
        h.setEditable(true);
        r.setEditable(true);
        sb.setEditable(true);
        fs.setEditable(true);
        d.setEditable(true);
    }//GEN-LAST:event_fsMouseExited

    private void sbMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sbMouseExited
        help.setText("En este campo usted debe ingresar el valor de la potencia base Sb en [MVA]");
        h.setEditable(true);
        r.setEditable(true);
        sb.setEditable(true);
        fs.setEditable(true);
        d.setEditable(true);
    }//GEN-LAST:event_sbMouseExited

    private void rMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rMouseExited
        h.setEditable(true);
        r.setEditable(true);
        sb.setEditable(true);
        fs.setEditable(true);
        d.setEditable(true);

        help.setText("En este campo usted debe ingresar el estatismo R en [Hz/MW]");
    }//GEN-LAST:event_rMouseExited

    private void BotonADEMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BotonADEMouseEntered
        help.setText("Elija esta opci�n si necesita Ayuda");
    }//GEN-LAST:event_BotonADEMouseEntered

    private void hMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_hMouseExited
        h.setEditable(true);
        r.setEditable(true);
        sb.setEditable(true);
        fs.setEditable(true);
        d.setEditable(true);

        help.setText("En este campo usted debe ingresar la inercia H en [MW/HZ]");

    }//GEN-LAST:event_hMouseExited

    private void BotonADEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonADEActionPerformed
        showHTML("Ayuda", "help.html");
    }//GEN-LAST:event_BotonADEActionPerformed

    /**
     * ** lo que se debe copiar
     */
    private void mhMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BotonADEMouseEntered
        help.setText("Ingrese los parametros H y Sb");
    }//GEN-LAST:event_BotonADEMouseEntered

    private void mrMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BotonADEMouseEntered
        help.setText("Ingrese los parametros R, fs y Sb");
    }//GEN-LAST:event_BotonADEMouseEntered

    private void msbMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BotonADEMouseEntered
        help.setText("Ingrese el parametro Sb");
    }//GEN-LAST:event_BotonADEMouseEntered

    private void mfsMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BotonADEMouseEntered
        help.setText("Ingrese el parametro fs");
    }//GEN-LAST:event_BotonADEMouseEntered

    private void mdMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BotonADEMouseEntered
        help.setText("Ingrese los parametros D,fs y Sb");
    }//GEN-LAST:event_BotonADEMouseEntered

    private void jButton1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BotonLFCMouseEntered
        help.setText("Control de Frecuencia y Carga");
    }//GEN-LAST:event_BotonLFCMouseEntered

    /**
     * ************************************************************
     */
    private void mhActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonCalcularActionPerformed

        r.setEditable(false);
        fs.setEditable(false);
        d.setEditable(false);
        String base, fisico;

        try {
            calcpu p = new calcpu();
            base = sb.getText();
            fisico = h.getText();
            double resultado = p.h(base, fisico);
            jLabel1.setText(resultado + "");
            fotos.setIcon(new javax.swing.ImageIcon(getClass().getResource("images/h.jpg")));
            fotos.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
            fotos.setMaximumSize(new java.awt.Dimension(500, 100));
            fotos.setMinimumSize(new java.awt.Dimension(350, 370));
            //fotos.setPreferredSize(new java.awt.Dimension(360, 240));
            fotos.setText("\n");
        } catch (java.lang.NumberFormatException e) {
            JOptionPane.showMessageDialog(this, "Missing required values: " + e.getMessage(), "Error parsing input", JOptionPane.ERROR_MESSAGE);
        }

    }//GEN-LAST:event_BotonCalcularActionPerformed

    private void mrActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonCalcularActionPerformed

        h.setEditable(false);
        d.setEditable(false);
        String base, fisico, otro;

        try {
            calcpu p = new calcpu();
            base = sb.getText();
            fisico = r.getText();
            otro = fs.getText();
            double resultado = p.b(fisico, base, otro);
            jLabel1.setText(resultado + "");
            fotos.setIcon(new javax.swing.ImageIcon(getClass().getResource("images/r.jpg")));
            fotos.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
            fotos.setMaximumSize(new java.awt.Dimension(500, 100));
            fotos.setMinimumSize(new java.awt.Dimension(350, 370));
            //fotos.setPreferredSize(new java.awt.Dimension(370, 260));
            fotos.setText("\n");
        } catch (java.lang.NumberFormatException e) {
            JOptionPane.showMessageDialog(this, "Missing required values: " + e.getMessage(), "Error parsing input", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_BotonCalcularActionPerformed

    private void msbActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonCalcularActionPerformed

        r.setEditable(false);
        fs.setEditable(false);
        d.setEditable(false);
        h.setEditable(false);
        String base, fisico;

        try {
            calcpu p = new calcpu();
            base = sb.getText();
            fisico = sb.getText();
            double resultado = p.h(fisico, base);
            jLabel1.setText(resultado + "");
            fotos.setIcon(new javax.swing.ImageIcon(getClass().getResource("images/sb.jpg")));
            fotos.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
            fotos.setMaximumSize(new java.awt.Dimension(500, 100));
            fotos.setMinimumSize(new java.awt.Dimension(350, 370));
            //fotos.setPreferredSize(new java.awt.Dimension(370, 260));
            fotos.setText("\n");
        } catch (java.lang.NumberFormatException e) {
            JOptionPane.showMessageDialog(this, "Missing required values: " + e.getMessage(), "Error parsing input", JOptionPane.ERROR_MESSAGE);
        }

    }//GEN-LAST:event_BotonCalcularActionPerformed

    private void mfsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonCalcularActionPerformed

        r.setEditable(false);
        sb.setEditable(false);
        d.setEditable(false);
        h.setEditable(false);
        String base, fisico;

        try {
            calcpu p = new calcpu();
            base = fs.getText();
            fisico = fs.getText();
            double resultado = p.h(fisico, base);
            jLabel1.setText(resultado + "");
            fotos.setIcon(new javax.swing.ImageIcon(getClass().getResource("images/fs.jpg")));
            fotos.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
            fotos.setMaximumSize(new java.awt.Dimension(500, 100));
            fotos.setMinimumSize(new java.awt.Dimension(350, 370));
            //fotos.setPreferredSize(new java.awt.Dimension(370, 260));
            fotos.setText("\n");
        } catch (java.lang.NumberFormatException e) {
            JOptionPane.showMessageDialog(this, "Missing required values: " + e.getMessage(), "Error parsing input", JOptionPane.ERROR_MESSAGE);
        }

    }//GEN-LAST:event_BotonCalcularActionPerformed

    private void mdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonCalcularActionPerformed

        r.setEditable(false);
        h.setEditable(false);

        String base, fisico, otro;

        try {
            calcpu p = new calcpu();
            base = d.getText();
            fisico = fs.getText();
            otro = sb.getText();
            double resultado = p.b(fisico, base, otro);
            jLabel1.setText(resultado + "");
            fotos.setIcon(new javax.swing.ImageIcon(getClass().getResource("images/d.jpg")));
            fotos.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
            fotos.setMaximumSize(new java.awt.Dimension(500, 100));
            fotos.setMinimumSize(new java.awt.Dimension(350, 370));
            //fotos.setPreferredSize(new java.awt.Dimension(370, 260));
            fotos.setText("\n");
        } catch (java.lang.NumberFormatException e) {
            JOptionPane.showMessageDialog(this, "Missing required values: " + e.getMessage(), "Error parsing input", JOptionPane.ERROR_MESSAGE);
        }

    }//GEN-LAST:event_BotonCalcularActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BotonADE;
    private javax.swing.JButton CALCULOPU;
    private javax.swing.JLabel D;
    private javax.swing.JLabel FS;
    private javax.swing.JLabel H;
    private javax.swing.JLabel R;
    private javax.swing.JLabel SB;
    private javax.swing.JLabel TITULO;
    private javax.swing.JTextField d;
    private javax.swing.JTextField fs;
    private javax.swing.JTextField h;
    private javax.swing.JLabel help;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JButton md;
    private javax.swing.JButton mfs;
    private javax.swing.JButton mh;
    private javax.swing.JButton mr;
    private javax.swing.JButton msb;
    private javax.swing.JTextField r;
    private javax.swing.JTextField sb;
    //
    private javax.swing.JLabel fotos;
    // End of variables declaration//GEN-END:variables

    /**
     * Displays a window with the provided html content
     * <br><b>WARNING</b>: The html must reside in the same package as this
     * class!
     *
     * @param title pop up window title
     * @param archivo name of html file. File must reside in the same package as
     * this class
     */
    protected void showHTML(String title, String archivo) {

        URL paginita;
        try {
            paginita = getClass().getResource(archivo);
//            InputStream s_in = paginita.openStream();
//            BufferedReader in = new BufferedReader(new InputStreamReader(s_in));

//            String lineas = "";
//            while (true) {
//                String linea = in.readLine();
//                if (linea == null) {
//                    break;
//                }
//                lineas += linea;
//            }
            final JDialog dialog = new JDialog((JFrame) null, title);
            dialog.setModal(true);
            JEditorPane label = new JEditorPane(paginita);
            label.setEditable(false);
            JScrollPane scrollPane = new JScrollPane(label);
            scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
            JButton closeButton = new JButton("Cerrar");
            closeButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    dialog.setVisible(false);
                    dialog.dispose();
//                    dispose();
                }
            });
            JPanel closePanel = new JPanel();
            closePanel.setLayout(new BoxLayout(closePanel,
                    BoxLayout.LINE_AXIS));
            closePanel.add(Box.createHorizontalGlue());
            closePanel.add(closeButton);
            closePanel.setBorder(BorderFactory.
                    createEmptyBorder(0, 0, 5, 5));

            JPanel contentPane = new JPanel(new BorderLayout());
            contentPane.add(scrollPane, BorderLayout.CENTER);
            contentPane.add(closePanel, BorderLayout.PAGE_END);
            contentPane.setOpaque(true);
            dialog.setContentPane(contentPane);

            //Show it.
            dialog.setSize(new Dimension(750, 500));
            dialog.setLocationRelativeTo(this);
            dialog.setVisible(true);

        } catch (IOException q) {
            JOptionPane.showInternalInputDialog(this, "Couldn't find the html file " + archivo, "Error displaying html file", JOptionPane.ERROR_MESSAGE);
            q.printStackTrace(System.err);
        }
    }

}
