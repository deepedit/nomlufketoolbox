package org.deepedit.addons.nomlufketool.gauss_seidel;

public class calculo extends complejo {
 	
 	double [][]V;
 	double [][]delta;
 	double [][]P;
 	double [][]Q;
 	
	public calculo(double [][]V,double [][]delta,double [][]P,double [][]Q){
	
		this.V=V;
		this.delta=delta;
		this.P=P;
		this.Q=Q;
	}
	
	public calculo (int Niter){
					
		this.V=new double[4][Niter];
		this.delta=new double[4][Niter];
		this.P=new double[4][Niter];
		this.Q=new double[4][Niter];
	}
	public calculo (){
		
	}
	
//******************************************************************************************************************
//******************************************************************************************************************	
	public calculo Calcular_Gauss_Seidel(complejo V1,complejo V2,complejo V3,complejo V4, 
					   					 double PC2,double PG3,double PC4, double QC2,double QC4,
					 				     complejo y11,complejo y12,complejo y13,complejo y14,
					                     complejo y22,complejo y23,complejo y24,complejo y33,
					 					 complejo y34,complejo y44, int Niter){
					 					 	
		//Pi=PGi-PCi
		double P2=-PC2;
		double P3=PG3;
		double P4=-PC4;
		double Q2=-QC2;
		double Q4=-QC4;
	
					 					 	
		double [][]V_por_Iteracion = new double [4][Niter];
		double [][]P_por_Iteracion = new double [4][Niter];
		double [][]Q_por_Iteracion = new double [4][Niter];
		double [][]Delta_por_Iteracion = new double [4][Niter]; 
		
		int iter=0;
		complejo uno = new complejo(1,0);
		double modV3=modulo(V3);
		
		while(iter < Niter){
		
			//Barra 2 => PQ
			complejo S2=new complejo(P2,Q2);
			complejo I2=conjugado(div2(S2,V2));
			complejo V2i= mult2(div2(uno,y22),resta2(I2,(suma3(mult2(y12,V1),mult2(y23,V3),mult2(y24,V4)))));	
			
			//Barra 3 => PV
			
			double Q3i=mult2(V3,conjugado(suma4(mult2(y13,V1),mult2(y23,V2),mult2(y33,V3),mult2(y34,V4)))).im;
			complejo S3i=new complejo(P3,Q3i);
			complejo I3=conjugado(div2(S3i,V3));
			complejo V3i=mult2(div2(uno,y33),resta2(I3,suma3(mult2(y13,V1),mult2(y23,V2),mult2(y34,V4))));	
		    //solo debe considerarse el angulo de V3i
		    V3i=toma_angulo(modV3,angulo_rad(V3i));
		    				
			//Barra 4 => PQ
			
			complejo S4= new complejo(P4,Q4);
			complejo I4=conjugado(div2(S4,V4));
			complejo V4i=mult2(div2(uno,y44),resta2(I4,suma3(mult2(y14,V1),mult2(y24,V2),mult2(y34,V3))));	
		
			//Barra 1 => Slack
						
			complejo S1i=mult2(V1,conjugado(suma4(mult2(y11,V1),mult2(y12,V2i),mult2(y13,V3i),mult2(y14,V4i))));
			
			double P1i=S1i.re;
			double Q1i=S1i.im;
		
			V_por_Iteracion[0][iter]=modulo(V1);
			V_por_Iteracion[1][iter]=modulo(V2i);
			V_por_Iteracion[2][iter]=modulo(V3i);
			V_por_Iteracion[3][iter]=modulo(V4i);
			
			Delta_por_Iteracion[0][iter]=angulo_grados(V1);
			Delta_por_Iteracion[1][iter]=angulo_grados(V2i);
			Delta_por_Iteracion[2][iter]=angulo_grados(V3i);
			Delta_por_Iteracion[3][iter]=angulo_grados(V4i);
		
			Q_por_Iteracion[0][iter]=Q1i;
			Q_por_Iteracion[1][iter]=Q2;
			Q_por_Iteracion[2][iter]=Q3i;
			Q_por_Iteracion[3][iter]=Q4;
		
			P_por_Iteracion[0][iter]=P1i;
			P_por_Iteracion[1][iter]=P2;
			P_por_Iteracion[2][iter]=P3;
			P_por_Iteracion[3][iter]=P4;
			
			//se actualizan las variables
			
			V2=V2i;
			V4=V4i;
			V3=V3i;
			iter++;
		}				 					 	
		calculo res=new calculo(V_por_Iteracion, Delta_por_Iteracion, P_por_Iteracion, Q_por_Iteracion);			 					 	
		return res;		 					 	
	}
//******************************************************************************************************************
//******************************************************************************************************************
 	public calculo Calcular_Gauss_Seidel_Aceleracion(complejo V1,complejo V2,complejo V3,complejo V4, 
					   					 double PC2,double PG3,double PC4, double QC2,double QC4,
					 				     complejo y11,complejo y12,complejo y13,complejo y14,
					                     complejo y22,complejo y23,complejo y24,complejo y33,
					 					 complejo y34,complejo y44, int Niter, double alfa){
					 					 	
		//Pi=PGi-PCi
		double P2=-PC2;
		double P3=PG3;
		double P4=-PC4;
		double Q2=-QC2;
		double Q4=-QC4;
					 					 	
		double [][]V_por_Iteracion = new double [4][Niter];
		double [][]P_por_Iteracion = new double [4][Niter];
		double [][]Q_por_Iteracion = new double [4][Niter];
		double [][]Delta_por_Iteracion = new double [4][Niter]; 
		
		int iter=0;
		complejo uno = new complejo(1,0);
		double modV3=modulo(V3);
		
				
		while(iter < Niter){
		
			//Barra 2 => PQ
			complejo S2=new complejo(P2,Q2);
			complejo I2=conjugado(div2(S2,V2));
			complejo V2i_cal= mult2(div2(uno,y22),resta2(I2,(suma3(mult2(y12,V1),mult2(y23,V3),mult2(y24,V4)))));
			complejo V2i_ace=suma2(V2,mult_escalar_complejo(alfa,resta2(V2i_cal,V2)));	
			
			//Barra 3 => PV
			
			double Q3i=mult2(V3,conjugado(suma4(mult2(y13,V1),mult2(y23,V2),mult2(y33,V3),mult2(y34,V4)))).im;
			complejo S3i=new complejo(P3,Q3i);
			complejo I3=conjugado(div2(S3i,V3));
			complejo V3i_cal=mult2(div2(uno,y33),resta2(I3,suma3(mult2(y13,V1),mult2(y23,V2),mult2(y34,V4))));	
		    //solo debe considerarse el angulo de V3i
		    V3i_cal=toma_angulo(modV3,angulo_rad(V3i_cal));
		    complejo V3i_ace=suma2(V3,mult_escalar_complejo(alfa,resta2(V3i_cal,V3)));
			V3i_ace=toma_angulo(modV3,angulo_rad(V3i_ace));	
			
			//Barra 4 => PQ
			
			complejo S4= new complejo(P4,Q4);
			complejo I4=conjugado(div2(S4,V4));
			complejo V4i_cal=mult2(div2(uno,y44),resta2(I4,suma3(mult2(y14,V1),mult2(y24,V2),mult2(y34,V3))));	
	        complejo V4i_ace=suma2(V4,mult_escalar_complejo(alfa,resta2(V4i_cal,V4)));
			
			//Barra 1 => Slack
						
			complejo S1i=mult2(V1,conjugado(suma4(mult2(y11,V1),mult2(y12,V2i_ace),mult2(y13,V3i_ace),mult2(y14,V4i_ace))));
			
			double P1i=S1i.re;
			double Q1i=S1i.im;
		
			V_por_Iteracion[0][iter]=modulo(V1);
			V_por_Iteracion[1][iter]=modulo(V2i_ace);
			V_por_Iteracion[2][iter]=modulo(V3i_ace);
			V_por_Iteracion[3][iter]=modulo(V4i_ace);
			
			Delta_por_Iteracion[0][iter]=angulo_grados(V1);
			Delta_por_Iteracion[1][iter]=angulo_grados(V2i_ace);
			Delta_por_Iteracion[2][iter]=angulo_grados(V3i_ace);
			Delta_por_Iteracion[3][iter]=angulo_grados(V4i_ace);
		
			Q_por_Iteracion[0][iter]=Q1i;
			Q_por_Iteracion[1][iter]=Q2;
			Q_por_Iteracion[2][iter]=Q3i;
			Q_por_Iteracion[3][iter]=Q4;
		
			P_por_Iteracion[0][iter]=P1i;
			P_por_Iteracion[1][iter]=P2;
			P_por_Iteracion[2][iter]=P3;
			P_por_Iteracion[3][iter]=P4;
			
			//se actualizan las variables
			
			V2=V2i_ace;
			V3=V3i_ace;
			V4=V4i_ace;
			
			iter++;
		}				 					 	
		calculo res=new calculo(V_por_Iteracion, Delta_por_Iteracion, P_por_Iteracion, Q_por_Iteracion);			 					 	
		return res;		 					 	
	}

//******************************************************************************************************************	
//******************************************************************************************************************	
	public calculo Calcular_Gauss_Seidel_Actualizacion(complejo V1,complejo V2,complejo V3,complejo V4, 
					   					 			   double PC2,double PG3,double PC4, double QC2,double QC4,
					 				     			   complejo y11,complejo y12,complejo y13,complejo y14,
					                     			   complejo y22,complejo y23,complejo y24,complejo y33,
					 					 			   complejo y34,complejo y44, int Niter, double orden){
					 					 	
					 					 	
		//Pi=PGi-PCi
		double P2=-PC2;
		double P3=PG3;
		double P4=-PC4;
		double Q2=-QC2;
		double Q4=-QC4;
		
		double [][]V_por_Iteracion = new double [4][Niter];
		double [][]P_por_Iteracion = new double [4][Niter];
		double [][]Q_por_Iteracion = new double [4][Niter];
		double [][]Delta_por_Iteracion = new double [4][Niter]; 
		
		int iter=0;
		complejo uno = new complejo(1,0);
		double modV3=modulo(V3);		
		
		while(iter < Niter){
		
			if(orden == 234){
		
				//Barra 2 => PQ
				complejo S2=new complejo(P2,Q2);
				complejo I2=conjugado(div2(S2,V2));
				complejo V2i= mult2(div2(uno,y22),resta2(I2,(suma3(mult2(y12,V1),mult2(y23,V3),mult2(y24,V4)))));	
			
				//Barra 3 => PV
			
				double Q3i=mult2(V3,conjugado(suma4(mult2(y13,V1),mult2(y23,V2),mult2(y33,V3),mult2(y34,V4)))).im;
				complejo S3i=new complejo(P3,Q3i);
				complejo I3=conjugado(div2(S3i,V3));
				complejo V3i=mult2(div2(uno,y33),resta2(I3,suma3(mult2(y13,V1),mult2(y23,V2i),mult2(y34,V4))));	
		 	    //solo debe considerarse el angulo de V3i
		  		V3i=toma_angulo(modV3,angulo_rad(V3i));
		    				
				//Barra 4 => PQ
			
				complejo S4= new complejo(P4,Q4);
				complejo I4=conjugado(div2(S4,V4));
				complejo V4i=mult2(div2(uno,y44),resta2(I4,suma3(mult2(y14,V1),mult2(y24,V2i),mult2(y34,V3i))));	
		
				//Barra 1 => Slack
						
				complejo S1i=mult2(V1,conjugado(suma4(mult2(y11,V1),mult2(y12,V2i),mult2(y13,V3i),mult2(y14,V4i))));
			
				double P1i=S1i.re;
				double Q1i=S1i.im;
		
				V_por_Iteracion[0][iter]=modulo(V1);
				V_por_Iteracion[1][iter]=modulo(V2i);
				V_por_Iteracion[2][iter]=modulo(V3i);
				V_por_Iteracion[3][iter]=modulo(V4i);
			
				Delta_por_Iteracion[0][iter]=angulo_grados(V1);
				Delta_por_Iteracion[1][iter]=angulo_grados(V2i);
				Delta_por_Iteracion[2][iter]=angulo_grados(V3i);
				Delta_por_Iteracion[3][iter]=angulo_grados(V4i);
		
				Q_por_Iteracion[0][iter]=Q1i;
				Q_por_Iteracion[1][iter]=Q2;
				Q_por_Iteracion[2][iter]=Q3i;
				Q_por_Iteracion[3][iter]=Q4;
		
				P_por_Iteracion[0][iter]=P1i;
				P_por_Iteracion[1][iter]=P2;
				P_por_Iteracion[2][iter]=P3;
				P_por_Iteracion[3][iter]=P4;
			
				//se actualizan las variables
			
				V2=V2i;
				V4=V4i;
				V3=V3i;
				iter++;
			}
			
			if(orden == 243){
		
				//Barra 2 => PQ
				complejo S2=new complejo(P2,Q2);
				complejo I2=conjugado(div2(S2,V2));
				complejo V2i= mult2(div2(uno,y22),resta2(I2,(suma3(mult2(y12,V1),mult2(y23,V3),mult2(y24,V4)))));	
			
				//Barra 4 => PQ
			
				complejo S4= new complejo(P4,Q4);
				complejo I4=conjugado(div2(S4,V4));
				complejo V4i=mult2(div2(uno,y44),resta2(I4,suma3(mult2(y14,V1),mult2(y24,V2i),mult2(y34,V3))));	
		
				//Barra 3 => PV
			
				double Q3i=mult2(V3,conjugado(suma4(mult2(y13,V1),mult2(y23,V2),mult2(y33,V3),mult2(y34,V4)))).im;
				complejo S3i=new complejo(P3,Q3i);
				complejo I3=conjugado(div2(S3i,V3));
				complejo V3i=mult2(div2(uno,y33),resta2(I3,suma3(mult2(y13,V1),mult2(y23,V2i),mult2(y34,V4i))));	
		 	    //solo debe considerarse el angulo de V3i
		  		V3i=toma_angulo(modV3,angulo_rad(V3i));
		    				
			
				//Barra 1 => Slack
						
				complejo S1i=mult2(V1,conjugado(suma4(mult2(y11,V1),mult2(y12,V2i),mult2(y13,V3i),mult2(y14,V4i))));
			
				double P1i=S1i.re;
				double Q1i=S1i.im;
		
				V_por_Iteracion[0][iter]=modulo(V1);
				V_por_Iteracion[1][iter]=modulo(V2i);
				V_por_Iteracion[2][iter]=modulo(V3i);
				V_por_Iteracion[3][iter]=modulo(V4i);
			
				Delta_por_Iteracion[0][iter]=angulo_grados(V1);
				Delta_por_Iteracion[1][iter]=angulo_grados(V2i);
				Delta_por_Iteracion[2][iter]=angulo_grados(V3i);
				Delta_por_Iteracion[3][iter]=angulo_grados(V4i);
		
				Q_por_Iteracion[0][iter]=Q1i;
				Q_por_Iteracion[1][iter]=Q2;
				Q_por_Iteracion[2][iter]=Q3i;
				Q_por_Iteracion[3][iter]=Q4;
		
				P_por_Iteracion[0][iter]=P1i;
				P_por_Iteracion[1][iter]=P2;
				P_por_Iteracion[2][iter]=P3;
				P_por_Iteracion[3][iter]=P4;
			
				//se actualizan las variables
			
				V2=V2i;
				V4=V4i;
				V3=V3i;
				iter++;
			}
			
			if(orden == 324){
		
				//Barra 3 => PV
			
				double Q3i=mult2(V3,conjugado(suma4(mult2(y13,V1),mult2(y23,V2),mult2(y33,V3),mult2(y34,V4)))).im;
				complejo S3i=new complejo(P3,Q3i);
				complejo I3=conjugado(div2(S3i,V3));
				complejo V3i=mult2(div2(uno,y33),resta2(I3,suma3(mult2(y13,V1),mult2(y23,V2),mult2(y34,V4))));	
		 	    //solo debe considerarse el angulo de V3i
		  		V3i=toma_angulo(modV3,angulo_rad(V3i));
		    				
				
				//Barra 2 => PQ
				complejo S2=new complejo(P2,Q2);
				complejo I2=conjugado(div2(S2,V2));
				complejo V2i= mult2(div2(uno,y22),resta2(I2,(suma3(mult2(y12,V1),mult2(y23,V3i),mult2(y24,V4)))));	
			
				//Barra 4 => PQ
			
				complejo S4= new complejo(P4,Q4);
				complejo I4=conjugado(div2(S4,V4));
				complejo V4i=mult2(div2(uno,y44),resta2(I4,suma3(mult2(y14,V1),mult2(y24,V2i),mult2(y34,V3i))));	
		
						
				//Barra 1 => Slack
						
				complejo S1i=mult2(V1,conjugado(suma4(mult2(y11,V1),mult2(y12,V2i),mult2(y13,V3i),mult2(y14,V4i))));
			
				double P1i=S1i.re;
				double Q1i=S1i.im;
		
				V_por_Iteracion[0][iter]=modulo(V1);
				V_por_Iteracion[1][iter]=modulo(V2i);
				V_por_Iteracion[2][iter]=modulo(V3i);
				V_por_Iteracion[3][iter]=modulo(V4i);
			
				Delta_por_Iteracion[0][iter]=angulo_grados(V1);
				Delta_por_Iteracion[1][iter]=angulo_grados(V2i);
				Delta_por_Iteracion[2][iter]=angulo_grados(V3i);
				Delta_por_Iteracion[3][iter]=angulo_grados(V4i);
		
				Q_por_Iteracion[0][iter]=Q1i;
				Q_por_Iteracion[1][iter]=Q2;
				Q_por_Iteracion[2][iter]=Q3i;
				Q_por_Iteracion[3][iter]=Q4;
		
				P_por_Iteracion[0][iter]=P1i;
				P_por_Iteracion[1][iter]=P2;
				P_por_Iteracion[2][iter]=P3;
				P_por_Iteracion[3][iter]=P4;
			
				//se actualizan las variables
			
				V2=V2i;
				V4=V4i;
				V3=V3i;
				iter++;
			}
			if(orden == 342){
		
				//Barra 3 => PV
			
				double Q3i=mult2(V3,conjugado(suma4(mult2(y13,V1),mult2(y23,V2),mult2(y33,V3),mult2(y34,V4)))).im;
				complejo S3i=new complejo(P3,Q3i);
				complejo I3=conjugado(div2(S3i,V3));
				complejo V3i=mult2(div2(uno,y33),resta2(I3,suma3(mult2(y13,V1),mult2(y23,V2),mult2(y34,V4))));	
		 	    //solo debe considerarse el angulo de V3i
		  		V3i=toma_angulo(modV3,angulo_rad(V3i));
		    				
				//Barra 4 => PQ
			
				complejo S4= new complejo(P4,Q4);
				complejo I4=conjugado(div2(S4,V4));
				complejo V4i=mult2(div2(uno,y44),resta2(I4,suma3(mult2(y14,V1),mult2(y24,V2),mult2(y34,V3i))));	
		
				//Barra 2 => PQ
				complejo S2=new complejo(P2,Q2);
				complejo I2=conjugado(div2(S2,V2));
				complejo V2i= mult2(div2(uno,y22),resta2(I2,(suma3(mult2(y12,V1),mult2(y23,V3i),mult2(y24,V4i)))));	
			
				//Barra 1 => Slack
						
				complejo S1i=mult2(V1,conjugado(suma4(mult2(y11,V1),mult2(y12,V2i),mult2(y13,V3i),mult2(y14,V4i))));
			
				double P1i=S1i.re;
				double Q1i=S1i.im;
		
				V_por_Iteracion[0][iter]=modulo(V1);
				V_por_Iteracion[1][iter]=modulo(V2i);
				V_por_Iteracion[2][iter]=modulo(V3i);
				V_por_Iteracion[3][iter]=modulo(V4i);
			
				Delta_por_Iteracion[0][iter]=angulo_grados(V1);
				Delta_por_Iteracion[1][iter]=angulo_grados(V2i);
				Delta_por_Iteracion[2][iter]=angulo_grados(V3i);
				Delta_por_Iteracion[3][iter]=angulo_grados(V4i);
		
				Q_por_Iteracion[0][iter]=Q1i;
				Q_por_Iteracion[1][iter]=Q2;
				Q_por_Iteracion[2][iter]=Q3i;
				Q_por_Iteracion[3][iter]=Q4;
		
				P_por_Iteracion[0][iter]=P1i;
				P_por_Iteracion[1][iter]=P2;
				P_por_Iteracion[2][iter]=P3;
				P_por_Iteracion[3][iter]=P4;
			
				//se actualizan las variables
			
				V2=V2i;
				V4=V4i;
				V3=V3i;
				iter++;
			}
			
			if(orden == 423){
		
				//Barra 4 => PQ
			
				complejo S4= new complejo(P4,Q4);
				complejo I4=conjugado(div2(S4,V4));
				complejo V4i=mult2(div2(uno,y44),resta2(I4,suma3(mult2(y14,V1),mult2(y24,V2),mult2(y34,V3))));	
				
				//Barra 2 => PQ
				complejo S2=new complejo(P2,Q2);
				complejo I2=conjugado(div2(S2,V2));
				complejo V2i= mult2(div2(uno,y22),resta2(I2,(suma3(mult2(y12,V1),mult2(y23,V3),mult2(y24,V4i)))));	
			
				//Barra 3 => PV
			
				double Q3i=mult2(V3,conjugado(suma4(mult2(y13,V1),mult2(y23,V2i),mult2(y33,V3),mult2(y34,V4i)))).im;
				complejo S3i=new complejo(P3,Q3i);
				complejo I3=conjugado(div2(S3i,V3));
				complejo V3i=mult2(div2(uno,y33),resta2(I3,suma3(mult2(y13,V1),mult2(y23,V2),mult2(y34,V4))));	
		 	    //solo debe considerarse el angulo de V3i
		  		V3i=toma_angulo(modV3,angulo_rad(V3i));
		    				
			
				//Barra 1 => Slack
						
				complejo S1i=mult2(V1,conjugado(suma4(mult2(y11,V1),mult2(y12,V2i),mult2(y13,V3i),mult2(y14,V4i))));
			
				double P1i=S1i.re;
				double Q1i=S1i.im;
		
				V_por_Iteracion[0][iter]=modulo(V1);
				V_por_Iteracion[1][iter]=modulo(V2i);
				V_por_Iteracion[2][iter]=modulo(V3i);
				V_por_Iteracion[3][iter]=modulo(V4i);
			
				Delta_por_Iteracion[0][iter]=angulo_grados(V1);
				Delta_por_Iteracion[1][iter]=angulo_grados(V2i);
				Delta_por_Iteracion[2][iter]=angulo_grados(V3i);
				Delta_por_Iteracion[3][iter]=angulo_grados(V4i);
		
				Q_por_Iteracion[0][iter]=Q1i;
				Q_por_Iteracion[1][iter]=Q2;
				Q_por_Iteracion[2][iter]=Q3i;
				Q_por_Iteracion[3][iter]=Q4;
		
				P_por_Iteracion[0][iter]=P1i;
				P_por_Iteracion[1][iter]=P2;
				P_por_Iteracion[2][iter]=P3;
				P_por_Iteracion[3][iter]=P4;
			
				//se actualizan las variables
			
				V2=V2i;
				V4=V4i;
				V3=V3i;
				iter++;
			}
			
			if(orden == 432){
		
				//Barra 4 => PQ
			
				complejo S4= new complejo(P4,Q4);
				complejo I4=conjugado(div2(S4,V4));
				complejo V4i=mult2(div2(uno,y44),resta2(I4,suma3(mult2(y14,V1),mult2(y24,V2),mult2(y34,V3))));	
		
				//Barra 3 => PV
			
				double Q3i=mult2(V3,conjugado(suma4(mult2(y13,V1),mult2(y23,V2),mult2(y33,V3),mult2(y34,V4)))).im;
				complejo S3i=new complejo(P3,Q3i);
				complejo I3=conjugado(div2(S3i,V3));
				complejo V3i=mult2(div2(uno,y33),resta2(I3,suma3(mult2(y13,V1),mult2(y23,V2),mult2(y34,V4i))));	
		 	    //solo debe considerarse el angulo de V3i
		  		V3i=toma_angulo(modV3,angulo_rad(V3i));
		    				
				//Barra 2 => PQ
				
				complejo S2=new complejo(P2,Q2);
				complejo I2=conjugado(div2(S2,V2));
				complejo V2i= mult2(div2(uno,y22),resta2(I2,(suma3(mult2(y12,V1),mult2(y23,V3i),mult2(y24,V4i)))));
				
				//Barra 1 => Slack
						
				complejo S1i=mult2(V1,conjugado(suma4(mult2(y11,V1),mult2(y12,V2i),mult2(y13,V3i),mult2(y14,V4i))));
			
				double P1i=S1i.re;
				double Q1i=S1i.im;
		
				V_por_Iteracion[0][iter]=modulo(V1);
				V_por_Iteracion[1][iter]=modulo(V2i);
				V_por_Iteracion[2][iter]=modulo(V3i);
				V_por_Iteracion[3][iter]=modulo(V4i);
			
				Delta_por_Iteracion[0][iter]=angulo_grados(V1);
				Delta_por_Iteracion[1][iter]=angulo_grados(V2i);
				Delta_por_Iteracion[2][iter]=angulo_grados(V3i);
				Delta_por_Iteracion[3][iter]=angulo_grados(V4i);
		
				Q_por_Iteracion[0][iter]=Q1i;
				Q_por_Iteracion[1][iter]=Q2;
				Q_por_Iteracion[2][iter]=Q3i;
				Q_por_Iteracion[3][iter]=Q4;
		
				P_por_Iteracion[0][iter]=P1i;
				P_por_Iteracion[1][iter]=P2;
				P_por_Iteracion[2][iter]=P3;
				P_por_Iteracion[3][iter]=P4;
			
				//se actualizan las variables
			
				V2=V2i;
				V4=V4i;
				V3=V3i;
				iter++;
			}
		}	
		
		calculo res=new calculo(V_por_Iteracion, Delta_por_Iteracion, P_por_Iteracion, Q_por_Iteracion);			 					 	
		return res;		 					 	
	}

//******************************************************************************************************************	
//******************************************************************************************************************
	public calculo Calcular_Gauss_Seidel_Actualizacion_Aceleracion(complejo V1,complejo V2,complejo V3,complejo V4, 
					   					 						   double PC2,double PG3,double PC4, double QC2,double QC4,
					 				     						   complejo y11,complejo y12,complejo y13,complejo y14,
					                     						   complejo y22,complejo y23,complejo y24,complejo y33,
					 					 						   complejo y34,complejo y44, int Niter, 
					 					 						   double alfa, double orden){
					 					 	
					 					 	
		//Pi=PGi-PCi
		double P2=-PC2;
		double P3=PG3;
		double P4=-PC4;
		double Q2=-QC2;
		double Q4=-QC4;
		
		double [][]V_por_Iteracion = new double [4][Niter];
		double [][]P_por_Iteracion = new double [4][Niter];
		double [][]Q_por_Iteracion = new double [4][Niter];
		double [][]Delta_por_Iteracion = new double [4][Niter]; 
		
		int iter=0;
		complejo uno = new complejo(1,0);
		double modV3=modulo(V3);		
		
		while(iter < Niter){
		
			if(orden == 234){
				
				//Barra 2 => PQ
				
				complejo S2=new complejo(P2,Q2);
				complejo I2=conjugado(div2(S2,V2));
				complejo V2i_cal= mult2(div2(uno,y22),resta2(I2,(suma3(mult2(y12,V1),mult2(y23,V3),mult2(y24,V4)))));
				complejo V2i_ace=suma2(V2,mult_escalar_complejo(alfa,resta2(V2i_cal,V2)));	
			
				//Barra 3 => PV
			
				double Q3i=mult2(V3,conjugado(suma4(mult2(y13,V1),mult2(y23,V2),mult2(y33,V3),mult2(y34,V4)))).im;
				complejo S3i=new complejo(P3,Q3i);
				complejo I3=conjugado(div2(S3i,V3));
				complejo V3i_cal=mult2(div2(uno,y33),resta2(I3,suma3(mult2(y13,V1),mult2(y23,V2i_ace),mult2(y34,V4))));	
		    	//solo debe considerarse el angulo de V3i
		    	V3i_cal=toma_angulo(modV3,angulo_rad(V3i_cal));
		    	complejo V3i_ace=suma2(V3,mult_escalar_complejo(alfa,resta2(V3i_cal,V3)));
				V3i_ace=toma_angulo(modV3,angulo_rad(V3i_ace));	
			
				//Barra 4 => PQ
			
				complejo S4= new complejo(P4,Q4);
				complejo I4=conjugado(div2(S4,V4));
				complejo V4i_cal=mult2(div2(uno,y44),resta2(I4,suma3(mult2(y14,V1),mult2(y24,V2i_ace),mult2(y34,V3i_ace))));	
	        	complejo V4i_ace=suma2(V4,mult_escalar_complejo(alfa,resta2(V4i_cal,V4)));
			
				//Barra 1 => Slack
						
				complejo S1i=mult2(V1,conjugado(suma4(mult2(y11,V1),mult2(y12,V2i_ace),mult2(y13,V3i_ace),mult2(y14,V4i_ace))));
			
				double P1i=S1i.re;
				double Q1i=S1i.im;
		
				V_por_Iteracion[0][iter]=modulo(V1);
				V_por_Iteracion[1][iter]=modulo(V2i_ace);
				V_por_Iteracion[2][iter]=modulo(V3i_ace);
				V_por_Iteracion[3][iter]=modulo(V4i_ace);
			
				Delta_por_Iteracion[0][iter]=angulo_grados(V1);
				Delta_por_Iteracion[1][iter]=angulo_grados(V2i_ace);
				Delta_por_Iteracion[2][iter]=angulo_grados(V3i_ace);
				Delta_por_Iteracion[3][iter]=angulo_grados(V4i_ace);
		
				Q_por_Iteracion[0][iter]=Q1i;
				Q_por_Iteracion[1][iter]=Q2;
				Q_por_Iteracion[2][iter]=Q3i;
				Q_por_Iteracion[3][iter]=Q4;
		
				P_por_Iteracion[0][iter]=P1i;
				P_por_Iteracion[1][iter]=P2;
				P_por_Iteracion[2][iter]=P3;
				P_por_Iteracion[3][iter]=P4;
			
				//se actualizan las variables
			
				V2=V2i_ace;
				V3=V3i_ace;
				V4=V4i_ace;
			
				iter++;
			}
			
			if(orden == 243){
			
				//Barra 2 => PQ
				
				complejo S2=new complejo(P2,Q2);
				complejo I2=conjugado(div2(S2,V2));
				complejo V2i_cal= mult2(div2(uno,y22),resta2(I2,(suma3(mult2(y12,V1),mult2(y23,V3),mult2(y24,V4)))));
				complejo V2i_ace=suma2(V2,mult_escalar_complejo(alfa,resta2(V2i_cal,V2)));	
			
				//Barra 4 => PQ
			
				complejo S4= new complejo(P4,Q4);
				complejo I4=conjugado(div2(S4,V4));
				complejo V4i_cal=mult2(div2(uno,y44),resta2(I4,suma3(mult2(y14,V1),mult2(y24,V2i_ace),mult2(y34,V3))));	
	        	complejo V4i_ace=suma2(V4,mult_escalar_complejo(alfa,resta2(V4i_cal,V4)));
			
				//Barra 3 => PV
			
				double Q3i=mult2(V3,conjugado(suma4(mult2(y13,V1),mult2(y23,V2),mult2(y33,V3),mult2(y34,V4)))).im;
				complejo S3i=new complejo(P3,Q3i);
				complejo I3=conjugado(div2(S3i,V3));
				complejo V3i_cal=mult2(div2(uno,y33),resta2(I3,suma3(mult2(y13,V1),mult2(y23,V2i_ace),mult2(y34,V4i_ace))));	
		    	//solo debe considerarse el angulo de V3i
		    	V3i_cal=toma_angulo(modV3,angulo_rad(V3i_cal));
		    	complejo V3i_ace=suma2(V3,mult_escalar_complejo(alfa,resta2(V3i_cal,V3)));
				V3i_ace=toma_angulo(modV3,angulo_rad(V3i_ace));	
			
				//Barra 1 => Slack
						
				complejo S1i=mult2(V1,conjugado(suma4(mult2(y11,V1),mult2(y12,V2i_ace),mult2(y13,V3i_ace),mult2(y14,V4i_ace))));
			
				double P1i=S1i.re;
				double Q1i=S1i.im;
		
				V_por_Iteracion[0][iter]=modulo(V1);
				V_por_Iteracion[1][iter]=modulo(V2i_ace);
				V_por_Iteracion[2][iter]=modulo(V3i_ace);
				V_por_Iteracion[3][iter]=modulo(V4i_ace);
			
				Delta_por_Iteracion[0][iter]=angulo_grados(V1);
				Delta_por_Iteracion[1][iter]=angulo_grados(V2i_ace);
				Delta_por_Iteracion[2][iter]=angulo_grados(V3i_ace);
				Delta_por_Iteracion[3][iter]=angulo_grados(V4i_ace);
		
				Q_por_Iteracion[0][iter]=Q1i;
				Q_por_Iteracion[1][iter]=Q2;
				Q_por_Iteracion[2][iter]=Q3i;
				Q_por_Iteracion[3][iter]=Q4;
		
				P_por_Iteracion[0][iter]=P1i;
				P_por_Iteracion[1][iter]=P2;
				P_por_Iteracion[2][iter]=P3;
				P_por_Iteracion[3][iter]=P4;
			
				//se actualizan las variables
			
				V2=V2i_ace;
				V3=V3i_ace;
				V4=V4i_ace;
			
				iter++;
			}
			
			if(orden == 324){
			
				//Barra 3 => PV
			
				double Q3i=mult2(V3,conjugado(suma4(mult2(y13,V1),mult2(y23,V2),mult2(y33,V3),mult2(y34,V4)))).im;
				complejo S3i=new complejo(P3,Q3i);
				complejo I3=conjugado(div2(S3i,V3));
				complejo V3i_cal=mult2(div2(uno,y33),resta2(I3,suma3(mult2(y13,V1),mult2(y23,V2),mult2(y34,V4))));	
		    	//solo debe considerarse el angulo de V3i
		    	V3i_cal=toma_angulo(modV3,angulo_rad(V3i_cal));
		    	complejo V3i_ace=suma2(V3,mult_escalar_complejo(alfa,resta2(V3i_cal,V3)));
				V3i_ace=toma_angulo(modV3,angulo_rad(V3i_ace));	
			
				//Barra 2 => PQ
				
				complejo S2=new complejo(P2,Q2);
				complejo I2=conjugado(div2(S2,V2));
				complejo V2i_cal= mult2(div2(uno,y22),resta2(I2,(suma3(mult2(y12,V1),mult2(y23,V3i_ace),mult2(y24,V4)))));
				complejo V2i_ace=suma2(V2,mult_escalar_complejo(alfa,resta2(V2i_cal,V2)));	
				
				//Barra 4 => PQ
			
				complejo S4= new complejo(P4,Q4);
				complejo I4=conjugado(div2(S4,V4));
				complejo V4i_cal=mult2(div2(uno,y44),resta2(I4,suma3(mult2(y14,V1),mult2(y24,V2i_ace),mult2(y34,V3i_ace))));	
	        	complejo V4i_ace=suma2(V4,mult_escalar_complejo(alfa,resta2(V4i_cal,V4)));
			
				//Barra 1 => Slack
						
				complejo S1i=mult2(V1,conjugado(suma4(mult2(y11,V1),mult2(y12,V2i_ace),mult2(y13,V3i_ace),mult2(y14,V4i_ace))));
			
				double P1i=S1i.re;
				double Q1i=S1i.im;
		
				V_por_Iteracion[0][iter]=modulo(V1);
				V_por_Iteracion[1][iter]=modulo(V2i_ace);
				V_por_Iteracion[2][iter]=modulo(V3i_ace);
				V_por_Iteracion[3][iter]=modulo(V4i_ace);
			
				Delta_por_Iteracion[0][iter]=angulo_grados(V1);
				Delta_por_Iteracion[1][iter]=angulo_grados(V2i_ace);
				Delta_por_Iteracion[2][iter]=angulo_grados(V3i_ace);
				Delta_por_Iteracion[3][iter]=angulo_grados(V4i_ace);
		
				Q_por_Iteracion[0][iter]=Q1i;
				Q_por_Iteracion[1][iter]=Q2;
				Q_por_Iteracion[2][iter]=Q3i;
				Q_por_Iteracion[3][iter]=Q4;
		
				P_por_Iteracion[0][iter]=P1i;
				P_por_Iteracion[1][iter]=P2;
				P_por_Iteracion[2][iter]=P3;
				P_por_Iteracion[3][iter]=P4;
			
				//se actualizan las variables
			
				V2=V2i_ace;
				V3=V3i_ace;
				V4=V4i_ace;
			
				iter++;
			
			}
			
			if(orden == 342){
							
				//Barra 3 => PV
			
				double Q3i=mult2(V3,conjugado(suma4(mult2(y13,V1),mult2(y23,V2),mult2(y33,V3),mult2(y34,V4)))).im;
				complejo S3i=new complejo(P3,Q3i);
				complejo I3=conjugado(div2(S3i,V3));
				complejo V3i_cal=mult2(div2(uno,y33),resta2(I3,suma3(mult2(y13,V1),mult2(y23,V2),mult2(y34,V4))));	
		    	//solo debe considerarse el angulo de V3i
		    	V3i_cal=toma_angulo(modV3,angulo_rad(V3i_cal));
		    	complejo V3i_ace=suma2(V3,mult_escalar_complejo(alfa,resta2(V3i_cal,V3)));
				V3i_ace=toma_angulo(modV3,angulo_rad(V3i_ace));	
				
				//Barra 4 => PQ
			
				complejo S4= new complejo(P4,Q4);
				complejo I4=conjugado(div2(S4,V4));
				complejo V4i_cal=mult2(div2(uno,y44),resta2(I4,suma3(mult2(y14,V1),mult2(y24,V2),mult2(y34,V3i_ace))));	
	        	complejo V4i_ace=suma2(V4,mult_escalar_complejo(alfa,resta2(V4i_cal,V4)));
				
				//Barra 2 => PQ
				
				complejo S2=new complejo(P2,Q2);
				complejo I2=conjugado(div2(S2,V2));
				complejo V2i_cal= mult2(div2(uno,y22),resta2(I2,(suma3(mult2(y12,V1),mult2(y23,V3i_ace),mult2(y24,V4i_ace)))));
				complejo V2i_ace=suma2(V2,mult_escalar_complejo(alfa,resta2(V2i_cal,V2)));	
				
				//Barra 1 => Slack
						
				complejo S1i=mult2(V1,conjugado(suma4(mult2(y11,V1),mult2(y12,V2i_ace),mult2(y13,V3i_ace),mult2(y14,V4i_ace))));
			
				double P1i=S1i.re;
				double Q1i=S1i.im;
		
				V_por_Iteracion[0][iter]=modulo(V1);
				V_por_Iteracion[1][iter]=modulo(V2i_ace);
				V_por_Iteracion[2][iter]=modulo(V3i_ace);
				V_por_Iteracion[3][iter]=modulo(V4i_ace);
			
				Delta_por_Iteracion[0][iter]=angulo_grados(V1);
				Delta_por_Iteracion[1][iter]=angulo_grados(V2i_ace);
				Delta_por_Iteracion[2][iter]=angulo_grados(V3i_ace);
				Delta_por_Iteracion[3][iter]=angulo_grados(V4i_ace);
		
				Q_por_Iteracion[0][iter]=Q1i;
				Q_por_Iteracion[1][iter]=Q2;
				Q_por_Iteracion[2][iter]=Q3i;
				Q_por_Iteracion[3][iter]=Q4;
		
				P_por_Iteracion[0][iter]=P1i;
				P_por_Iteracion[1][iter]=P2;
				P_por_Iteracion[2][iter]=P3;
				P_por_Iteracion[3][iter]=P4;
			
				//se actualizan las variables
			
				V2=V2i_ace;
				V3=V3i_ace;
				V4=V4i_ace;
			
				iter++;
			
			}
			
			if(orden == 423){
		
				//Barra 4 => PQ
			
				complejo S4= new complejo(P4,Q4);
				complejo I4=conjugado(div2(S4,V4));
				complejo V4i_cal=mult2(div2(uno,y44),resta2(I4,suma3(mult2(y14,V1),mult2(y24,V2),mult2(y34,V3))));	
	        	complejo V4i_ace=suma2(V4,mult_escalar_complejo(alfa,resta2(V4i_cal,V4)));
				
				//Barra 2 => PQ
				
				complejo S2=new complejo(P2,Q2);
				complejo I2=conjugado(div2(S2,V2));
				complejo V2i_cal= mult2(div2(uno,y22),resta2(I2,(suma3(mult2(y12,V1),mult2(y23,V3),mult2(y24,V4i_ace)))));
				complejo V2i_ace=suma2(V2,mult_escalar_complejo(alfa,resta2(V2i_cal,V2)));	
			
				//Barra 3 => PV
			
				double Q3i=mult2(V3,conjugado(suma4(mult2(y13,V1),mult2(y23,V2),mult2(y33,V3),mult2(y34,V4)))).im;
				complejo S3i=new complejo(P3,Q3i);
				complejo I3=conjugado(div2(S3i,V3));
				complejo V3i_cal=mult2(div2(uno,y33),resta2(I3,suma3(mult2(y13,V1),mult2(y23,V2i_ace),mult2(y34,V4i_ace))));	
		    	//solo debe considerarse el angulo de V3i
		    	V3i_cal=toma_angulo(modV3,angulo_rad(V3i_cal));
		    	complejo V3i_ace=suma2(V3,mult_escalar_complejo(alfa,resta2(V3i_cal,V3)));
				V3i_ace=toma_angulo(modV3,angulo_rad(V3i_ace));	
			
				//Barra 1 => Slack
						
				complejo S1i=mult2(V1,conjugado(suma4(mult2(y11,V1),mult2(y12,V2i_ace),mult2(y13,V3i_ace),mult2(y14,V4i_ace))));
			
				double P1i=S1i.re;
				double Q1i=S1i.im;
		
				V_por_Iteracion[0][iter]=modulo(V1);
				V_por_Iteracion[1][iter]=modulo(V2i_ace);
				V_por_Iteracion[2][iter]=modulo(V3i_ace);
				V_por_Iteracion[3][iter]=modulo(V4i_ace);
			
				Delta_por_Iteracion[0][iter]=angulo_grados(V1);
				Delta_por_Iteracion[1][iter]=angulo_grados(V2i_ace);
				Delta_por_Iteracion[2][iter]=angulo_grados(V3i_ace);
				Delta_por_Iteracion[3][iter]=angulo_grados(V4i_ace);
		
				Q_por_Iteracion[0][iter]=Q1i;
				Q_por_Iteracion[1][iter]=Q2;
				Q_por_Iteracion[2][iter]=Q3i;
				Q_por_Iteracion[3][iter]=Q4;
		
				P_por_Iteracion[0][iter]=P1i;
				P_por_Iteracion[1][iter]=P2;
				P_por_Iteracion[2][iter]=P3;
				P_por_Iteracion[3][iter]=P4;
			
				//se actualizan las variables
			
				V2=V2i_ace;
				V3=V3i_ace;
				V4=V4i_ace;
			
				iter++;
			
			}	
			
			if(orden == 432){
			
				//Barra 4 => PQ
			
				complejo S4= new complejo(P4,Q4);
				complejo I4=conjugado(div2(S4,V4));
				complejo V4i_cal=mult2(div2(uno,y44),resta2(I4,suma3(mult2(y14,V1),mult2(y24,V2),mult2(y34,V3))));	
	        	complejo V4i_ace=suma2(V4,mult_escalar_complejo(alfa,resta2(V4i_cal,V4)));
			
				//Barra 3 => PV
			
				double Q3i=mult2(V3,conjugado(suma4(mult2(y13,V1),mult2(y23,V2),mult2(y33,V3),mult2(y34,V4)))).im;
				complejo S3i=new complejo(P3,Q3i);
				complejo I3=conjugado(div2(S3i,V3));
				complejo V3i_cal=mult2(div2(uno,y33),resta2(I3,suma3(mult2(y13,V1),mult2(y23,V2),mult2(y34,V4i_ace))));	
		    	//solo debe considerarse el angulo de V3i
		    	V3i_cal=toma_angulo(modV3,angulo_rad(V3i_cal));
		    	complejo V3i_ace=suma2(V3,mult_escalar_complejo(alfa,resta2(V3i_cal,V3)));
				V3i_ace=toma_angulo(modV3,angulo_rad(V3i_ace));	
			
				//Barra 2 => PQ
				
				complejo S2=new complejo(P2,Q2);
				complejo I2=conjugado(div2(S2,V2));
				complejo V2i_cal= mult2(div2(uno,y22),resta2(I2,(suma3(mult2(y12,V1),mult2(y23,V3i_ace),mult2(y24,V4i_ace)))));
				complejo V2i_ace=suma2(V2,mult_escalar_complejo(alfa,resta2(V2i_cal,V2)));	
				
				//Barra 1 => Slack
						
				complejo S1i=mult2(V1,conjugado(suma4(mult2(y11,V1),mult2(y12,V2i_ace),mult2(y13,V3i_ace),mult2(y14,V4i_ace))));
			
				double P1i=S1i.re;
				double Q1i=S1i.im;
		
				V_por_Iteracion[0][iter]=modulo(V1);
				V_por_Iteracion[1][iter]=modulo(V2i_ace);
				V_por_Iteracion[2][iter]=modulo(V3i_ace);
				V_por_Iteracion[3][iter]=modulo(V4i_ace);
			
				Delta_por_Iteracion[0][iter]=angulo_grados(V1);
				Delta_por_Iteracion[1][iter]=angulo_grados(V2i_ace);
				Delta_por_Iteracion[2][iter]=angulo_grados(V3i_ace);
				Delta_por_Iteracion[3][iter]=angulo_grados(V4i_ace);
		
				Q_por_Iteracion[0][iter]=Q1i;
				Q_por_Iteracion[1][iter]=Q2;
				Q_por_Iteracion[2][iter]=Q3i;
				Q_por_Iteracion[3][iter]=Q4;
		
				P_por_Iteracion[0][iter]=P1i;
				P_por_Iteracion[1][iter]=P2;
				P_por_Iteracion[2][iter]=P3;
				P_por_Iteracion[3][iter]=P4;
			
				//se actualizan las variables
			
				V2=V2i_ace;
				V3=V3i_ace;
				V4=V4i_ace;
			
				iter++;
						
			}
		}	
		
		calculo res=new calculo(V_por_Iteracion, Delta_por_Iteracion, P_por_Iteracion, Q_por_Iteracion);			 					 	
		return res;		 					 	
	}
	
//****************************************************************************************************************
//****************************************************************************************************************
}




