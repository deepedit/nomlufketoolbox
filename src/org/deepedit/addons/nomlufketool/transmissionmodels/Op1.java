package org.deepedit.addons.nomlufketool.transmissionmodels;

import javax.swing.*;
/*
 * Op1.java
 *
 * Created on 14 de junio de 2003, 19:18
 */

/**
 *
 * @author  Carolina
 */
public class Op1 extends javax.swing.JPanel {


	    // Variables declaration - do not modify//GEN-BEGIN:variables
	    private javax.swing.JPanel PanelPi;
	    private javax.swing.JTextField jTextFieldYp;
	    private javax.swing.JSeparator jSeparator3;
	    private javax.swing.JSeparator jSeparator2;
	    private javax.swing.JSeparator jSeparator1;
	    private javax.swing.JPanel PanelCentro;
	    private javax.swing.JLabel jLabel21;
	    private javax.swing.JPanel PanelResultados;
	    private javax.swing.JLabel jLabel61;
	    private javax.swing.ButtonGroup ButtonGroupPerdidas;
	    private javax.swing.JPanel PanelOpciones;
	    private javax.swing.ButtonGroup ButtonGroupLargoLinea;
	    private javax.swing.JButton BotonAyuda;
	    private javax.swing.JTextArea TextAreaResena;
	    private javax.swing.JPanel PanelTop;
	    private javax.swing.JLabel jLabel16;
	    private javax.swing.JLabel jLabel9;
	    private javax.swing.JLabel jLabel15;
	    private javax.swing.JLabel jLabel8;
	    private javax.swing.JLabel jLabel14;
	    private javax.swing.JLabel jLabel7;
	    private javax.swing.JLabel jLabel13;
	    private javax.swing.JLabel jLabel6;
	    private javax.swing.JLabel jLabel12;
	    private javax.swing.JButton BotonOp2;
	    private javax.swing.JLabel jLabel5;
	    private javax.swing.JButton BotonOp1;
		private javax.swing.JButton BotonOp3;
	    private javax.swing.JLabel jLabel11;
	    private javax.swing.JLabel jLabel4;
	    private javax.swing.JLabel jLabel10;
	    private javax.swing.JLabel jLabel3;
	    private javax.swing.JLabel jLabel2;
	    private javax.swing.JLabel jLabel1;
	    private javax.swing.JLabel jLabel51;
	    private javax.swing.JTextField TextFieldR;
	    private javax.swing.JLabel jLabel91;
	    private javax.swing.JPanel PanelZw;
	    private javax.swing.JTextField TextFieldL;
	    private javax.swing.JPanel PanelTitulo;
	    private javax.swing.JTextField TextFieldG;
	    private javax.swing.JPanel PanelDatos;
	    private javax.swing.JTextField TextFieldC;
	    private javax.swing.JLabel LabelZw;
	    private javax.swing.JLabel LabelMonoPi;
	    private javax.swing.JLabel LabelTitulo;
	    private javax.swing.JRadioButton RadioButtonLL;
	    private javax.swing.JLabel jLabel41;
	    private javax.swing.JPanel PanelABCD;
	    private javax.swing.JRadioButton RadioButtonLC;
	    private javax.swing.JLabel jLabel81;
	    private javax.swing.JTextField jTextFieldD;
	    private javax.swing.JTextArea TextAreaAyuda;
	    private javax.swing.JTextField jTextFieldZw;
	    private javax.swing.JTextField jTextFieldC;
	    private javax.swing.JTextField jTextFieldB;
	    private javax.swing.JTextField jTextFieldA;
	    private javax.swing.JTextField jTextFieldZs;
	    private javax.swing.JRadioButton RadioButtonCP;
	    private javax.swing.JLabel LabelMonoABCD;
	    private javax.swing.JTextField TextFieldLargo;
	    private javax.swing.JButton BotonDatos;
	    private javax.swing.JPanel PanelBotones;
	    private javax.swing.JRadioButton RadioButtonSP;
	    private javax.swing.JLabel jLabel31;
	    private javax.swing.JLabel jLabel101;
	    private javax.swing.JLabel jLabel71;




//	Portada portada;
	Op2 op2;
	Op3 op3;
	Ayuda33 ayuda;

	/** Creates new form Op1 */
    public Op1() {
        initComponents();
    }

    public void Metodos(Op2 o, Op3 o3, Ayuda33 a)
    {
    	op2=o;
    	op3=o3;
		ayuda=a;
    }

    private void initComponents() {//GEN-BEGIN:initComponents
        java.awt.GridBagConstraints gridBagConstraints;

        ButtonGroupLargoLinea = new javax.swing.ButtonGroup();
        ButtonGroupPerdidas = new javax.swing.ButtonGroup();
        PanelTop = new javax.swing.JPanel();
        PanelTitulo = new javax.swing.JPanel();
        LabelTitulo = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        PanelBotones = new javax.swing.JPanel();
        BotonOp1 = new javax.swing.JButton();
        BotonOp3 = new javax.swing.JButton();
        BotonOp2 = new javax.swing.JButton();
        BotonAyuda = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        PanelCentro = new javax.swing.JPanel();
        PanelOpciones = new javax.swing.JPanel();
        RadioButtonLC = new javax.swing.JRadioButton();
        RadioButtonCP = new javax.swing.JRadioButton();
        RadioButtonLL = new javax.swing.JRadioButton();
        RadioButtonSP = new javax.swing.JRadioButton();
        TextAreaResena = new javax.swing.JTextArea();
        PanelDatos = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        TextFieldR = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        TextFieldL = new javax.swing.JTextField();
        jLabel41 = new javax.swing.JLabel();
        jLabel51 = new javax.swing.JLabel();
        TextFieldG = new javax.swing.JTextField();
        jLabel61 = new javax.swing.JLabel();
        jLabel71 = new javax.swing.JLabel();
        TextFieldC = new javax.swing.JTextField();
        jLabel81 = new javax.swing.JLabel();
        jLabel91 = new javax.swing.JLabel();
        TextFieldLargo = new javax.swing.JTextField();
        jLabel101 = new javax.swing.JLabel();
        BotonDatos = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        PanelResultados = new javax.swing.JPanel();
        LabelMonoPi = new javax.swing.JLabel();
        LabelMonoABCD = new javax.swing.JLabel();
        PanelPi = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jTextFieldZs = new javax.swing.JTextField();
        jTextFieldYp = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        PanelABCD = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jTextFieldA = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jTextFieldB = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jTextFieldC = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jTextFieldD = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        PanelZw = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        jTextFieldZw = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        LabelZw = new javax.swing.JLabel();
        jSeparator3 = new javax.swing.JSeparator();
        TextAreaAyuda = new javax.swing.JTextArea();

        setLayout(new java.awt.BorderLayout());

        setMinimumSize(new java.awt.Dimension(768, 500));
        setPreferredSize(new java.awt.Dimension(768, 500));
        PanelTop.setLayout(new javax.swing.BoxLayout(PanelTop, javax.swing.BoxLayout.Y_AXIS));

        PanelTop.setMinimumSize(new java.awt.Dimension(768, 70));
        PanelTop.setPreferredSize(new java.awt.Dimension(768, 70));
        PanelTitulo.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER));

        LabelTitulo.setFont(new java.awt.Font("Dialog", 1, 14));
        LabelTitulo.setText("C\u00e1lculo de Par\u00e1metros para Modelos Circuitales de L\u00edneas de Transmisi\u00f3n");
        PanelTitulo.setBackground(new java.awt.Color(204,204,204));
        PanelTitulo.add(LabelTitulo);

        PanelTop.add(PanelTitulo);

        PanelTop.add(jSeparator2);

        PanelBotones.setLayout(new java.awt.GridLayout(1,4,0,5));//FlowLayout(java.awt.FlowLayout.RIGHT));

        BotonOp1.setText("C\u00e1lculo de Par\u00e1metros");
        BotonOp1.setEnabled(false);
        PanelBotones.add(BotonOp1);

        BotonOp2.setText("Comparaci\u00f3n de Modelos");
        BotonOp2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonOp2ActionPerformed(evt);
            }
        });

        BotonOp2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BotonOp2MouseEntered(evt);
            }
        });

        PanelBotones.add(BotonOp2);

		BotonOp3.setText("Diagrama de Circulo");
        BotonOp3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonOp3ActionPerformed(evt);
            }
        });

        BotonOp3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BotonOp3MouseEntered(evt);
            }
        });

        PanelBotones.add(BotonOp3);


        BotonAyuda.setText("Ayuda");
        BotonAyuda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonAyudaActionPerformed(evt);
            }
        });

        BotonAyuda.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BotonAyudaMouseEntered(evt);
            }
        });

        PanelBotones.add(BotonAyuda);

        PanelTop.add(PanelBotones);

        PanelTop.add(jSeparator1);

        add(PanelTop, java.awt.BorderLayout.NORTH);

        PanelCentro.setLayout(new java.awt.GridBagLayout());

        PanelCentro.setMinimumSize(new java.awt.Dimension(768, 380));
        PanelCentro.setPreferredSize(new java.awt.Dimension(768, 380));
        PanelOpciones.setLayout(new java.awt.GridLayout(2, 2));

        PanelOpciones.setMaximumSize(new java.awt.Dimension(200, 30));
        PanelOpciones.setMinimumSize(new java.awt.Dimension(200, 30));
        PanelOpciones.setPreferredSize(new java.awt.Dimension(200, 30));
        RadioButtonLC.setText("L\u00ednea Corta");
        ButtonGroupLargoLinea.add(RadioButtonLC);
        RadioButtonLC.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                RadioButtonLCMouseEntered(evt);
            }
        });

        PanelOpciones.add(RadioButtonLC);

        RadioButtonCP.setSelected(true);
        RadioButtonCP.setText("Con P\u00e9rdidas");
        ButtonGroupPerdidas.add(RadioButtonCP);
        RadioButtonCP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RadioButtonCPActionPerformed(evt);
            }
        });

        RadioButtonCP.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                RadioButtonCPMouseEntered(evt);
            }
        });

        PanelOpciones.add(RadioButtonCP);

        RadioButtonLL.setSelected(true);
        RadioButtonLL.setText("L\u00ednea Larga");
        ButtonGroupLargoLinea.add(RadioButtonLL);
        RadioButtonLL.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                RadioButtonLLMouseEntered(evt);
            }
        });

        PanelOpciones.add(RadioButtonLL);

        RadioButtonSP.setText("Sin P\u00e9rdidas");
        ButtonGroupPerdidas.add(RadioButtonSP);
        RadioButtonSP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RadioButtonSPActionPerformed(evt);
            }
        });

        RadioButtonSP.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                RadioButtonSPMouseEntered(evt);
            }
        });

        PanelOpciones.add(RadioButtonSP);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        PanelCentro.add(PanelOpciones, gridBagConstraints);

        TextAreaResena.setBackground(new java.awt.Color(204, 204, 204));
        TextAreaResena.setEditable(false);
        TextAreaResena.setMinimumSize(new java.awt.Dimension(370, 40));
        TextAreaResena.setPreferredSize(new java.awt.Dimension(370, 40));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        PanelCentro.add(TextAreaResena, gridBagConstraints);

        PanelDatos.setLayout(new java.awt.GridBagLayout());

        PanelDatos.setMinimumSize(new java.awt.Dimension(200, 300));
        PanelDatos.setPreferredSize(new java.awt.Dimension(200, 300));
        jLabel11.setText("R");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        PanelDatos.add(jLabel11, gridBagConstraints);

        TextFieldR.setText("0.0329");
        TextFieldR.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                TextFieldRMouseEntered(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        PanelDatos.add(TextFieldR, gridBagConstraints);

        jLabel21.setText(" [Ohm/Km]");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        PanelDatos.add(jLabel21, gridBagConstraints);

        jLabel31.setText("L");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        PanelDatos.add(jLabel31, gridBagConstraints);

        TextFieldL.setText("1.089e-3");
        TextFieldL.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                TextFieldLMouseEntered(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        PanelDatos.add(TextFieldL, gridBagConstraints);

        jLabel41.setText(" [H/Km]");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        PanelDatos.add(jLabel41, gridBagConstraints);

        jLabel51.setText("G");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        PanelDatos.add(jLabel51, gridBagConstraints);

        TextFieldG.setText("0");
        TextFieldG.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                TextFieldGMouseEntered(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        PanelDatos.add(TextFieldG, gridBagConstraints);

        jLabel61.setText(" [S/Km]");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        PanelDatos.add(jLabel61, gridBagConstraints);

        jLabel71.setText("C");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        PanelDatos.add(jLabel71, gridBagConstraints);

        TextFieldC.setText("1.023e-8");
        TextFieldC.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                TextFieldCMouseEntered(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        PanelDatos.add(TextFieldC, gridBagConstraints);

        jLabel81.setText(" [F/Km]");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        PanelDatos.add(jLabel81, gridBagConstraints);

        jLabel91.setText("Largo ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.ipadx = 2;
        PanelDatos.add(jLabel91, gridBagConstraints);

        TextFieldLargo.setText("800");
        TextFieldLargo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                TextFieldLargoMouseEntered(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 2;
        PanelDatos.add(TextFieldLargo, gridBagConstraints);

        jLabel101.setText(" [Km]");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        PanelDatos.add(jLabel101, gridBagConstraints);

        BotonDatos.setText("Aceptar");
        BotonDatos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonDatosActionPerformed(evt);
            }
        });

        BotonDatos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BotonDatosMouseEntered(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 5;
        PanelDatos.add(BotonDatos, gridBagConstraints);

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("images/linea0.jpg")));
        jLabel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel1MouseEntered(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.ipady = 20;
        PanelDatos.add(jLabel1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        PanelCentro.add(PanelDatos, gridBagConstraints);

        PanelResultados.setLayout(new java.awt.GridBagLayout());

        PanelResultados.setMinimumSize(new java.awt.Dimension(390, 320));
        PanelResultados.setPreferredSize(new java.awt.Dimension(390, 330));
        LabelMonoPi.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelMonoPi.setIcon(new javax.swing.ImageIcon(getClass().getResource("images/linea1.jpg")));
        LabelMonoPi.setPreferredSize(new java.awt.Dimension(200, 150));
        LabelMonoPi.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                LabelMonoPiMouseEntered(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        PanelResultados.add(LabelMonoPi, gridBagConstraints);

        LabelMonoABCD.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        LabelMonoABCD.setIcon(new javax.swing.ImageIcon(getClass().getResource("images/ABCD2.jpg")));
        LabelMonoABCD.setPreferredSize(new java.awt.Dimension(200, 150));
        LabelMonoABCD.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                LabelMonoABCDMouseEntered(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        PanelResultados.add(LabelMonoABCD, gridBagConstraints);

        PanelPi.setLayout(new java.awt.GridBagLayout());

        PanelPi.setMinimumSize(new java.awt.Dimension(200, 150));
        PanelPi.setPreferredSize(new java.awt.Dimension(200, 150));
        jLabel2.setText("Zs = ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        PanelPi.add(jLabel2, gridBagConstraints);

        jLabel3.setText("Yp = ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        PanelPi.add(jLabel3, gridBagConstraints);

        jTextFieldZs.setEditable(false);
        jTextFieldZs.setMinimumSize(new java.awt.Dimension(100, 19));
        jTextFieldZs.setPreferredSize(new java.awt.Dimension(100, 19));
        jTextFieldZs.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jTextFieldZsMouseEntered(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        PanelPi.add(jTextFieldZs, gridBagConstraints);

        jTextFieldYp.setEditable(false);
        jTextFieldYp.setMinimumSize(new java.awt.Dimension(100, 19));
        jTextFieldYp.setPreferredSize(new java.awt.Dimension(100, 19));
        jTextFieldYp.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jTextFieldYpMouseEntered(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        PanelPi.add(jTextFieldYp, gridBagConstraints);

        jLabel4.setText(" [Ohm]");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        PanelPi.add(jLabel4, gridBagConstraints);

        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel5.setText(" [S]");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        PanelPi.add(jLabel5, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        PanelResultados.add(PanelPi, gridBagConstraints);

        PanelABCD.setLayout(new java.awt.GridBagLayout());

        PanelABCD.setMinimumSize(new java.awt.Dimension(200, 150));
        PanelABCD.setPreferredSize(new java.awt.Dimension(200, 150));
        jLabel6.setText("A = ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        PanelABCD.add(jLabel6, gridBagConstraints);

        jTextFieldA.setEditable(false);
        jTextFieldA.setMinimumSize(new java.awt.Dimension(100, 19));
        jTextFieldA.setPreferredSize(new java.awt.Dimension(100, 19));
        jTextFieldA.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jTextFieldAMouseEntered(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        PanelABCD.add(jTextFieldA, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        PanelABCD.add(jLabel7, gridBagConstraints);

        jLabel8.setText("B = ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        PanelABCD.add(jLabel8, gridBagConstraints);

        jTextFieldB.setEditable(false);
        jTextFieldB.setMinimumSize(new java.awt.Dimension(100, 19));
        jTextFieldB.setPreferredSize(new java.awt.Dimension(100, 19));
        jTextFieldB.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jTextFieldBMouseEntered(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        PanelABCD.add(jTextFieldB, gridBagConstraints);

        jLabel9.setText(" [Ohm]");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        PanelABCD.add(jLabel9, gridBagConstraints);

        jLabel10.setText("C = ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        PanelABCD.add(jLabel10, gridBagConstraints);

        jTextFieldC.setEditable(false);
        jTextFieldC.setMinimumSize(new java.awt.Dimension(100, 19));
        jTextFieldC.setPreferredSize(new java.awt.Dimension(100, 19));
        jTextFieldC.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jTextFieldCMouseEntered(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        PanelABCD.add(jTextFieldC, gridBagConstraints);

        jLabel12.setText(" [S]");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        PanelABCD.add(jLabel12, gridBagConstraints);

        jLabel13.setText("D = ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        PanelABCD.add(jLabel13, gridBagConstraints);

        jTextFieldD.setEditable(false);
        jTextFieldD.setMinimumSize(new java.awt.Dimension(100, 19));
        jTextFieldD.setPreferredSize(new java.awt.Dimension(100, 19));
        jTextFieldD.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jTextFieldDMouseEntered(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        PanelABCD.add(jTextFieldD, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 3;
        PanelABCD.add(jLabel14, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        PanelResultados.add(PanelABCD, gridBagConstraints);

        PanelZw.setLayout(new java.awt.GridBagLayout());

        PanelZw.setMinimumSize(new java.awt.Dimension(200, 20));
        PanelZw.setPreferredSize(new java.awt.Dimension(200, 20));
        jLabel15.setText("Zw = ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        PanelZw.add(jLabel15, gridBagConstraints);

        jTextFieldZw.setEditable(false);
        jTextFieldZw.setMinimumSize(new java.awt.Dimension(100, 19));
        jTextFieldZw.setPreferredSize(new java.awt.Dimension(100, 19));
        jTextFieldZw.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jTextFieldZwMouseEntered(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        PanelZw.add(jTextFieldZw, gridBagConstraints);

        jLabel16.setText(" [Ohm]");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        PanelZw.add(jLabel16, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        PanelResultados.add(PanelZw, gridBagConstraints);

        LabelZw.setText("Impedancia Caracter\u00edstica");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        PanelResultados.add(LabelZw, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        PanelCentro.add(PanelResultados, gridBagConstraints);

        jSeparator3.setOrientation(javax.swing.SwingConstants.VERTICAL);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        PanelCentro.add(jSeparator3, gridBagConstraints);

        add(PanelCentro, java.awt.BorderLayout.CENTER);

        TextAreaAyuda.setBackground(new java.awt.Color(204, 204, 204));
        TextAreaAyuda.setBorder(new javax.swing.border.EtchedBorder());
        TextAreaAyuda.setMinimumSize(new java.awt.Dimension(0, 50));
        TextAreaAyuda.setPreferredSize(new java.awt.Dimension(0, 50));
        add(TextAreaAyuda, java.awt.BorderLayout.SOUTH);

    }//GEN-END:initComponents

	private void jTextFieldZsMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextFieldZsMouseEntered
		TextAreaAyuda.setText("Valor de la impedancia serie del modelo PI.");
	}//GEN-LAST:event_jTextFieldZsMouseEntered

	private void jLabel1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MouseEntered
		TextAreaAyuda.setText("Representaci�n circuital de la l�nea de transmisi�n, por unidad de longitud.");
	}//GEN-LAST:event_jLabel1MouseEntered

	private void LabelMonoABCDMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LabelMonoABCDMouseEntered
		TextAreaAyuda.setText("Ecuaci�n de los par�metros ABCD para el modelo de tetrapolos.");
	}//GEN-LAST:event_LabelMonoABCDMouseEntered

	private void LabelMonoPiMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_LabelMonoPiMouseEntered
		TextAreaAyuda.setText("Representaci�n gr�fica del modelo PI.");
	}//GEN-LAST:event_LabelMonoPiMouseEntered

	private void jTextFieldDMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextFieldDMouseEntered
		TextAreaAyuda.setText("Valor de D en la matriz de par�metros ABCD.");
	}//GEN-LAST:event_jTextFieldDMouseEntered

	private void jTextFieldCMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextFieldCMouseEntered
		TextAreaAyuda.setText("Valor de C en la matriz de par�metros ABCD.");
	}//GEN-LAST:event_jTextFieldCMouseEntered

	private void jTextFieldBMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextFieldBMouseEntered
		TextAreaAyuda.setText("Valor de B en la matriz de par�metros ABCD.");
	}//GEN-LAST:event_jTextFieldBMouseEntered

	private void jTextFieldAMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextFieldAMouseEntered
		TextAreaAyuda.setText("Valor de A en la matriz de par�metros ABCD.");
	}//GEN-LAST:event_jTextFieldAMouseEntered

	private void jTextFieldYpMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextFieldYpMouseEntered
		TextAreaAyuda.setText("Valor de la admitancia paralela del modelo PI.");
	}//GEN-LAST:event_jTextFieldYpMouseEntered

	private void jTextFieldZwMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextFieldZwMouseEntered
		TextAreaAyuda.setText("Valor complejo para la impedancia caracter�stica.");
	}//GEN-LAST:event_jTextFieldZwMouseEntered

	private void BotonDatosMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BotonDatosMouseEntered
		TextAreaAyuda.setText("Presione este bot�n para realizar el c�lculo de par�metros.");		// Add your handling code here:
	}//GEN-LAST:event_BotonDatosMouseEntered

	private void TextFieldLargoMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TextFieldLargoMouseEntered
		TextAreaAyuda.setText("En este campo puede ingresar el valor de la longitud de la l�nea.");
	}//GEN-LAST:event_TextFieldLargoMouseEntered

	private void TextFieldCMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TextFieldCMouseEntered
		TextAreaAyuda.setText("En este campo puede ingresar el valor de la capacitancia de la l�nea, por unidad de longitud.");
	}//GEN-LAST:event_TextFieldCMouseEntered

	private void TextFieldGMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TextFieldGMouseEntered
		TextAreaAyuda.setText("En este campo puede ingresar el valor de la conductancia de la l�nea, por unidad de longitud.");
	}//GEN-LAST:event_TextFieldGMouseEntered

	private void TextFieldLMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TextFieldLMouseEntered
		TextAreaAyuda.setText("En este campo puede ingresar el valor de la inductancia de la l�nea, por unidad de longitud.");
	}//GEN-LAST:event_TextFieldLMouseEntered

	private void TextFieldRMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TextFieldRMouseEntered
		TextAreaAyuda.setText("En este campo puede ingresar el valor de la resistencia de la l�nea, por unidad de longitud.");
	}//GEN-LAST:event_TextFieldRMouseEntered

	private void BotonAyudaMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BotonAyudaMouseEntered
		TextAreaAyuda.setText("Esta opci�n despliega una ventana que contiene ayuda sobre la interfaz y ayuda tem�tica.");
	}//GEN-LAST:event_BotonAyudaMouseEntered

	private void BotonOp2MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BotonOp2MouseEntered
		TextAreaAyuda.setText("Esta opci�n permite comparar los distintos modelos circuitales respecto a un caso base.");
	}//GEN-LAST:event_BotonOp2MouseEntered

	private void BotonOp3MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BotonOp2MouseEntered
		TextAreaAyuda.setText("Esta opci�n permite dibujar el Diagrama de Circulo de la Linea de Transmision.");
	}//GEN-LAST:event_BotonOp2MouseEntered

	private void BotonAyudaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonAyudaActionPerformed
        setVisible(false);
		ayuda.setVisible(true);
	ayuda.repaint();
	}//GEN-LAST:event_BotonAyudaActionPerformed

	private void RadioButtonLLMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_RadioButtonLLMouseEntered
		TextAreaAyuda.setText("Permite seleccionar un modelo de l�nea larga.\nV�lido para l�neas a�reas mayores a 250[Km] y cables de poder mayores a 30[Km].");
	}//GEN-LAST:event_RadioButtonLLMouseEntered

	private void RadioButtonLCMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_RadioButtonLCMouseEntered
		TextAreaAyuda.setText("Permite seleccionar un modelo de l�nea corta.\nV�lido para l�neas a�reas menores a 250[Km] y cables de poder menores a 30[Km].");
	}//GEN-LAST:event_RadioButtonLCMouseEntered

	private void RadioButtonSPMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_RadioButtonSPMouseEntered
		TextAreaAyuda.setText("Permite seleccionar un modelo sin considerar p�rdidas �hmicas.\nV�lido para l�neas con p�rdidas resistivas despreciables.");
	}//GEN-LAST:event_RadioButtonSPMouseEntered

	private void RadioButtonCPMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_RadioButtonCPMouseEntered
		TextAreaAyuda.setText("Permite seleccionar un modelo considerando p�rdidas �hmicas.");
	}//GEN-LAST:event_RadioButtonCPMouseEntered

    private void BotonDatosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonDatosActionPerformed
		double R,L,G,C,Largo;
		ParamsData x=new ParamsData();

		if ( !( (RadioButtonCP.isSelected() || RadioButtonSP.isSelected()) &&
			 (RadioButtonLC.isSelected() || RadioButtonLL.isSelected()) ) )
		{
			JOptionPane error=new JOptionPane();
			error.showMessageDialog(null, "No ha seleccionado las opciones necesarias.", "Error en las opciones", JOptionPane.ERROR_MESSAGE);
			return;
		}

		if ( RadioButtonCP.isSelected() )
		{
			try
			{
				R=Double.parseDouble(TextFieldR.getText());
				L=Double.parseDouble(TextFieldL.getText());
				G=Double.parseDouble(TextFieldG.getText());
				C=Double.parseDouble(TextFieldC.getText());
				Largo=Double.parseDouble(TextFieldLargo.getText());
			} catch (Exception e) {
				JOptionPane error=new JOptionPane();
				error.showMessageDialog(null, "Algunos de los datos introducidos son invalidos", "Error en los datos", JOptionPane.ERROR_MESSAGE);
				return;
			}

			if ( RadioButtonLL.isSelected() )
				x=CalcParams.LLCP(R,L,G,C,Largo);
			else if ( RadioButtonLC.isSelected() )
				x=CalcParams.LCCP(R,L,G,C,Largo);
		}

		if ( RadioButtonSP.isSelected() )
		{
			R=0;
			G=0;
			try
			{
				L=Double.parseDouble(TextFieldL.getText());
				C=Double.parseDouble(TextFieldC.getText());
				Largo=Double.parseDouble(TextFieldLargo.getText());
			} catch (Exception e) {
				JOptionPane error=new JOptionPane();
				error.showMessageDialog(null, "Algunos de los datos introducidos son invalidos", "Error en los datos", JOptionPane.ERROR_MESSAGE);
				return;
			}

			if ( RadioButtonLL.isSelected() )
				x=CalcParams.LLSP(L,C,Largo);
			else if ( RadioButtonLC.isSelected() )
				x=CalcParams.LCSP(L,C,Largo);
		}

		jTextFieldZw.setText(x.Zw.toString());
		jTextFieldZs.setText(x.Pi[0].toString());
		jTextFieldYp.setText(x.Pi[1].toString());
		jTextFieldA.setText(x.ABCD[0][0].toString());
		jTextFieldB.setText(x.ABCD[0][1].toString());
		jTextFieldC.setText(x.ABCD[1][0].toString());
		jTextFieldD.setText(x.ABCD[1][1].toString());
		int aux;
		String saux, saux2;
		aux=jTextFieldA.getText().indexOf("j");
		saux=jTextFieldA.getText().substring(0,aux-1);
		saux2=jTextFieldA.getText().substring(aux+1);
		op3.izquierdo.TextParam[0][0].setText(saux);
		op3.izquierdo.TextParam[0][1].setText(saux2);

		aux=jTextFieldB.getText().indexOf("j");
		saux=jTextFieldB.getText().substring(0,aux-1);
		saux2=jTextFieldB.getText().substring(aux+1);
		op3.izquierdo.TextParam[1][0].setText(saux);
		op3.izquierdo.TextParam[1][1].setText(saux2);

		aux=jTextFieldC.getText().indexOf("j");
		saux=jTextFieldC.getText().substring(0,aux-1);
		saux2=jTextFieldC.getText().substring(aux+1);
		op3.izquierdo.TextParam[2][0].setText(saux);
		op3.izquierdo.TextParam[2][1].setText(saux2);

		aux=jTextFieldD.getText().indexOf("j");
		saux=jTextFieldD.getText().substring(0,aux-1);
		saux2=jTextFieldD.getText().substring(aux+1);
		op3.izquierdo.TextParam[3][0].setText(saux);
		op3.izquierdo.TextParam[3][1].setText(saux2);
		op3.izquierdo.RadioButtonRectangular.setSelected(true);

		//op3.repaint();

		//AQUI SE BAN AL OTRO PANEL
    }//GEN-LAST:event_BotonDatosActionPerformed

    private void RadioButtonCPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RadioButtonCPActionPerformed
        TextFieldR.setEditable(true);
        TextFieldG.setEditable(true);
    }//GEN-LAST:event_RadioButtonCPActionPerformed

    private void RadioButtonSPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RadioButtonSPActionPerformed
        TextFieldR.setEditable(false);
        TextFieldG.setEditable(false);
    }//GEN-LAST:event_RadioButtonSPActionPerformed

    private void BotonOp2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonOp2ActionPerformed
        op2.setVisible(true);
        setVisible(false);
        op2.repaint();
    }//GEN-LAST:event_BotonOp2ActionPerformed

    private void BotonOp3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonOp2ActionPerformed
        op3.setVisible(true);
        setVisible(false);
        op3.repaint();
    }//GEN-LAST:event_BotonOp2ActionPerformed



}
