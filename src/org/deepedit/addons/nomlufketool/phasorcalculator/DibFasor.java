package org.deepedit.addons.nomlufketool.phasorcalculator;

/* * @Version 1.0 * @Autor Nolberto Emilio Oyarce Seguin */
import java.awt.*;
import java.awt.geom.*;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class DibFasor extends JFrame  {
	final static int maxCharHeight = 15;
	final static int minFontSize = 6;
	JPanel p0;
	//Colores
	Color   WhiteSmoke =     new Color(245,245,245);
	Color   Lavender =       new Color(230,230,250);
	final static Color bg = new Color(0,0,0);
	final static Color fg = Color.black;
	final static Color red = Color.red;
	final static Color white = Color.white;
	final static BasicStroke stroke = new BasicStroke(2.0f);
	final static BasicStroke wideStroke = new BasicStroke(8.0f);

	final static float dash1[] =  {
		10.0f
	};

	final static BasicStroke dashed = new BasicStroke(1.0f, 
	BasicStroke.CAP_BUTT,  BasicStroke.JOIN_MITER,  10.0f, dash1, 0.0f);
	Dimension totalSize;
	FontMetrics fontMetrics;
	LineBorder myLineBorder = new LineBorder(red, 10);
	/* *Seteo de los complejos en (0.0, 0.0) */

	double[] re;
	double[] im;
	char[] le;
	int n;

	double max_global;
	double precision = 0.0000001;
	
	protected Font TextDialogFont;   
	protected FontMetrics TextDialogFontMetrics;
	public double  tolerancia = (double)0; 

	
	////////////////Funci�n llamada desde fuera
	
	
	public DibFasor(int xn, double[] xre, double[] xim, char letras[], String titulo) {
		super(""+titulo);  //Poner t�tulo
		int i;

		n=xn;

		re=new double[xn];
		im=new double[xn];
		le=new char[xn];


		for (i=0; i<n; i++)
		{
			re[i]=xre[i];
			im[i]=xim[i];
			le[i]=letras[i];
		}
		
		setBackground(WhiteSmoke);
		Font TextDialogFont = new Font("TimesRoman",Font.PLAIN, 12);
		TextDialogFontMetrics = getFontMetrics(TextDialogFont);
		setFont(TextDialogFont);   
		/* *Creaci�n de los Paneles */
		BorderLayout borderl = new BorderLayout();
		getContentPane().setLayout(borderl);
		p0 = panel0();
		getContentPane().add("Center",p0); 
		pack();
		setVisible(true);
		return;
	}


	public JPanel panel0() {
		JPanel p = new JPanel();
		p.setSize(new Dimension(550,550));
		setBackground(WhiteSmoke);
		setForeground(WhiteSmoke);
		return p;
	}


	FontMetrics pickFont(Graphics2D g2, String longString, int xSpace)  {
		boolean fontFits = false;
		Font font = g2.getFont();
		FontMetrics fontMetrics = g2.getFontMetrics();
		int size = font.getSize();
		String name = font.getName();
		int style = font.getStyle();
		setSize(500,530);

		while ( !fontFits )  {

			if ( (fontMetrics.getHeight() <= maxCharHeight) && (fontMetrics.stringWidth(longString) <= xSpace) )  {
				fontFits = true;
			}


			else  {

				if ( size <= minFontSize )  {
					fontFits = true;
				}


				else  {
					g2.setFont(font = new Font(name, style, --size));
					fontMetrics = g2.getFontMetrics();
				}

			}

		}

		return fontMetrics;
	}


	Color genera(int i)
	{
		switch(i)
		{
			case 0:
				return Color.black;
			case 1:
				return Color.red;
			case 2:
				return Color.green;
			case 3:
				return Color.blue;
			case 4:
				return Color.gray;
			default:
				int r,g,b; //colores aleatorios
				r=((12-i)*10)%256;
				g=((i+63)*15)%256;
				b=((i*i)*19)%256;
				return new Color(r,g,b);
		}
	}




	/* *Funcion que pinta todo. */
	public void paint(Graphics g)  {
		setLocation(358,0);
		Graphics2D g2 = (Graphics2D) g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		//Dimension d = getSize();
		Dimension d = new Dimension(550,500);
		int gridWidth = d.width ;
		int gridHeight = d.height;
		fontMetrics = pickFont(g2, "Filled and Stroked GeneralPath", gridWidth);
		Color fg3D = Lavender;
		/* *Asignacion de los Complejos con las variables de entrada */

		
		/* *Seteo de los bordes de la zona de gr�fico */
		g2.setPaint(fg3D);
		g2.draw3DRect(0, 0, d.width - 1, d.height - 1, true);
		g2.draw3DRect(3, 3, d.width - 7, d.height - 7, true);
		g2.setPaint(fg);
		int x=5;
		int y=7;
		int rectWidth = gridWidth - 2*x;
		int stringY = gridHeight - 3 - fontMetrics.getDescent();
		int rectHeight = stringY - fontMetrics.getMaxAscent() - y - 2;
		/* * Centro de los Fasores */
		double x00_ab = gridWidth/2;
		double y00_ab = gridHeight/2;
		double x00_c = gridWidth/2;
		double y00_c = gridHeight/2;
		//funcion que calcula los maximos de los fasores
		max_global=0;

		int i;


		for(i=0; i<n; i++)
		{
			max_global=Math.max(max_global, Math.abs(re[i]));
			max_global=Math.max(max_global, Math.abs(im[i]));
		}
		
		/* * Escalamiento de los fasores a el pixelado de la pantalla */
		double escala = 200/max_global;


		double[] Re=new double[n];
		double[] Im=new double[n];

		for (i=0;i<n ;i++ )
		{
			Re[i]=escala*re[i];
			Im[i]=escala*im[i];
		}


		String[] labelre=new String[n];
		String[] labelim=new String[n];

		
		if (max_global<=precision)
		{
			g2.setColor(Color.black);
			g2.draw(new Line2D.Double(-200+x00_ab, y00_ab,200+x00_ab, y00_ab));
			g2.draw(new Line2D.Double(x00_ab, -200+y00_ab,x00_ab,200+ y00_ab));
			/* *Salida de los complejos en forma num�rica en pantalla */
			/* *Salida A */

			for(i=0; i<n; i++)
			{
				g2.setColor(genera(i));
				labelre[i]=String.valueOf(Math.round(10000*re[i])/10000.0);
				labelim[i]=String.valueOf(Math.round(10000*im[i])/10000.0);
				g2.drawString(le[i]+" = "+labelre[i]+ "+j"+labelim[i],10,500-20*n+20*i);
			}
			g2.drawString(" ",10,480-20*n+20*(i+1));
		}


		else  {
			myLineBorder = null;
			myLineBorder = new LineBorder(red, 10);

			for(i=0; i<n; i++)
			{
				g2.setColor(genera(i));
				g2.draw(new Line2D.Double(x00_ab,y00_ab,Re[i]+x00_ab,-Im[i]+y00_ab));
			}

			
			//myLineBorder.paintBorder(myLine2D, g2, (int)x00_ab,(int)y00_ab, 10, 10);
			/* *Punta de flecha de los fasores. */


			double L = 10;
			double grados = 30;
			double alpha = grados*Math.PI/180;
			double beta;
			double flecha_x1;
			double flecha_y1;
			double flecha_x2;
			double flecha_y2;
			
			for (i=0;i<n;i++)
			{
				beta=Math.atan2(im[i],re[i]);
				flecha_x1 = L*(Math.sin(Math.PI/2-beta-alpha));
				flecha_y1 = L*(Math.cos(Math.PI/2-beta-alpha));
				flecha_x2 = L*(Math.cos(beta-alpha));
				flecha_y2 = L*(Math.sin(beta-alpha));
				//Dibujo
				g2.setColor(genera(i));
				g2.draw(new Line2D.Double(Re[i]+x00_ab,-Im[i]+y00_ab,-flecha_x1+x00_ab+Re[i],flecha_y1+y00_ab-Im[i]));
				g2.draw(new Line2D.Double(Re[i]+x00_ab,-Im[i]+y00_ab,-flecha_x2+x00_ab+Re[i],flecha_y2+y00_ab-Im[i]));
			}
			
			//Asignaci�n de Nombres

			for (i=0;i<n;i++)
			{
				g2.setColor(genera(i));
				g2.drawString(""+le[i],(int)(Re[i]+x00_ab),(int)(-Im[i]+y00_ab));
			}							

			g2.setColor(Color.black);
			/* *Creaci�n de los ejes */
			g2.draw(new Line2D.Double(-200+x00_ab, y00_ab,200+x00_ab, y00_ab));
			g2.draw(new Line2D.Double(x00_ab, -200+y00_ab,x00_ab,200+ y00_ab));

			/* *FOR para la creaci�n de la rejilla */
			if (escala<= tolerancia) {
				return;
			}


			else {

				for (i=-10;i<=10; i++) {
					double paso =  max_global*i/10;
					double inc = escala*paso;
					g2.draw(new Line2D.Double(-5+x00_ab, y00_ab+ inc,5+x00_ab, y00_ab+ inc ));
					g2.draw(new Line2D.Double(inc + x00_ab,-5+y00_ab,inc + x00_ab,5+ y00_ab));
				}

			}

			/* *Label de los ejes */
			g2.setColor(red);

			for (int j = -5; j<=5; j++) {
				double paso_par =  max_global*2*j/10;
				double inc = escala*paso_par;
				double y_label =Math.round(1000*paso_par);
				double Y_label = y_label/1000;
				String label = String.valueOf(Y_label);
				int label_x_y = (int)(-5+x00_ab);
				int label_y_y = (int)(y00_ab-inc);
				g2.setColor(Color.blue);
				g2.drawString(label,label_x_y+7,label_y_y);
				int label_x_x = (int)(x00_ab+inc);
				int label_y_x= (int)(y00_ab-5);
				g2.drawString(label,label_x_x,label_y_x+17);
			}

			// Salida de los complejos en forma num�rica en pantalla //

			for(i=0; i<n; i++)
			{
				g2.setColor(genera(i));
				labelre[i]=String.valueOf(Math.round(10000*re[i])/10000.0);
				labelim[i]=String.valueOf(Math.round(10000*im[i])/10000.0);
				g2.drawString(le[i]+" = "+labelre[i]+ "+j"+labelim[i],10,500-20*n+20*i);
			}
			g2.drawString(" ",10,480-20*n+20*(i+1));

		}

	}

}

