/*
 *  *****************************************************************************
 *  ---- version information ----
 *  
 *  File Name:         ProtecccionesFrame.java
 *  Version:           (Check for latest version in SVN repository)
 *  Written by:        Frank Leanez
 *  URL:               www.centroenergia.cl 
 *  Initial release:   August 2018
 *  
 *  
 *  ---- Description ---- 
 *   Display the Nom Lufke Relays Protection toolbox
 *   
 *   Copyright 1997-2019
 *   
 *   Rodrigo Palma Behnke 
 *   Universidad de Chile 
 *   Last Modified: (Check for latest version in SVN repository)
 * 
 *   ****************************************************************************
*/
package org.deepedit.addons.nomlufketool.relays;

/**
 * Display the Nom Lufke Relays Protection toolbox
 *
 * @author Frank Leanez
 */
public class RelaysFrame extends javax.swing.JDialog {
    
    
        /**
     * Displays the Parameters tab when creating this frame. This is the default
     */
    public static final int DISPLAY_EXAMPLE1 = 0;
    /**
     * Displays the Methods tab  when creating this frame
     */
    public static final int DISPLAY_EXAMPLE2 = 1;
    
    private int initialPanel = 0;
       
    /**
     * Creates a new Parameter Line's frame app
     * @param display Use constants to set initially activated tab when this.
     * Use this class constants to set supported values this parameter
     * application is first run
     * @param parent Owner of this dialog. Use of null is accepted
     * @param modal window modal flag
     */
    public RelaysFrame(int display, javax.swing.JDialog parent, boolean modal) {
        super(parent, modal);
        if (display >= 0 && display <= 2) {
            initialPanel = display;
        }
        initComponent();
    }
    
    private void initComponent() {

        javax.swing.JApplet ap1 = new Protecciones_JApplet();
        javax.swing.JApplet ap2 = new Protecciones_JApplet2();
        
        switch (initialPanel) {
            case RelaysFrame.DISPLAY_EXAMPLE1:
                getContentPane().add(ap1.getContentPane());
                setJMenuBar(ap1.getJMenuBar());
                break;
            case RelaysFrame.DISPLAY_EXAMPLE2:
                getContentPane().add(ap2.getContentPane());
                setJMenuBar(ap2.getJMenuBar());
                break;
            default:
                getContentPane().add(ap1.getContentPane());
                setJMenuBar(ap1.getJMenuBar());
                break;
        }
        
//        getContentPane().setLayout(new java.awt.FlowLayout());
        pack();
        
    }
    
}
