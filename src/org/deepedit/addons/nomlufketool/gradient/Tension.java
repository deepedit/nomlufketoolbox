package org.deepedit.addons.nomlufketool.gradient;

import javax.swing.*;
import java.awt.*;
public class Tension extends javax.swing.JPanel {

	
	JPanel PanelTop = new JPanel();
	JSeparator jSeparator2 = new JSeparator();
	JPanel PanelTitulo = new JPanel();
	JLabel LabelTitulo = new JLabel();
	JPanel PanelBotones = new JPanel();
	JButton BotonGradiente = new JButton();
	JButton BotonTension = new JButton();
	JButton BotonParametros = new JButton();
	JButton BotonAyuda = new JButton();
	JSeparator jSeparator1 = new JSeparator();
	
	
	JPanel PanelCentro = new JPanel();
	
	JPanel PanelDatos = new JPanel();
	
	JPanel PanelAyuda = new JPanel();
	JTextArea TextAreaAyuda = new JTextArea();	
	
	GridBagConstraints gBC = new GridBagConstraints();
	
	public Tension() {
		initComponents();
	}
	
	Grad Gradien;
	Parametros Param;
	Ayuda Ayu;	
	
	public void Metodos(Grad gradien, Parametros param, Ayuda ayu){
		Gradien=gradien;
		Param=param;
		Ayu=ayu;		
	}

	private void initComponents(){
		
	    setLayout(new BorderLayout());
      	setMinimumSize(new Dimension(768, 500));
	    setPreferredSize(new Dimension(768, 500));
		
		//PANEL TOP

		PanelTop.setLayout(new BoxLayout(PanelTop, javax.swing.BoxLayout.Y_AXIS));
		PanelTop.setMinimumSize(new Dimension(768, 70));
		PanelTop.setPreferredSize(new Dimension(768, 70));
        
		PanelTitulo.setBackground(new Color(204, 204, 204));
		PanelTitulo.setLayout(new FlowLayout());
		LabelTitulo.setFont(new Font("Dialog", 1, 14));
		LabelTitulo.setText("C�lculo de Gradiente versus Distancia para diferentes tipos de carga.");
		PanelTitulo.add(LabelTitulo);
		
		PanelTop.add(PanelTitulo);
		PanelTop.add(jSeparator2);

		//BOTON GRADIENTE		
		BotonGradiente = new JButton("Gradiente");
	
        BotonGradiente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonGradienteActionPerformed(evt);
            }
        });

        BotonGradiente.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BotonGradienteMouseEntered(evt);
            }
        });
		
		//BOTON TENSION
		BotonTension = new JButton("Tensi�n");
		
        BotonTension.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BotonTensionMouseEntered(evt);
            }
        });
        
        //BOTON PARAMETROS
		BotonParametros = new JButton("Par�metros L�nea");
		
        BotonParametros.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonParametrosActionPerformed(evt);
            }
        });

        BotonParametros.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BotonParametrosMouseEntered(evt);
            }
        });
        
        //BOTON AYUDA
		BotonAyuda = new JButton("Ayuda");
		
        BotonAyuda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonAyudaActionPerformed(evt);
            }
        });

        BotonAyuda.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BotonAyudaMouseEntered(evt);
            }
        });
		
		PanelBotones.setLayout(new FlowLayout());
		PanelBotones.add(BotonGradiente); PanelBotones.add(BotonTension);
		PanelBotones.add(BotonParametros); PanelBotones.add(BotonAyuda);
		
		PanelTop.add(PanelBotones);
		
		PanelTop.add(jSeparator1);
		
		add("North",PanelTop);
		
		//PANEL CENTRO
		
		BotonGradiente.setEnabled(true);
		BotonTension.setEnabled(false);
		BotonParametros.setEnabled(true);
		BotonAyuda.setEnabled(true);	
		
		PanelCentro.setMinimumSize(new java.awt.Dimension(768, 380));
		PanelCentro.setPreferredSize(new java.awt.Dimension(768, 380));
		PanelCentro.setLayout(new java.awt.GridBagLayout());
		
		//PANEL DATOS
		JPanel PanelDatos = new JPanel();
		PanelDatos.setMinimumSize(new java.awt.Dimension(200, 380));
		PanelDatos.setPreferredSize(new java.awt.Dimension(200, 380));
		PanelDatos.setLayout(new java.awt.GridBagLayout());
		JButton BotonAccion = new JButton();
		
		BotonAccion.setText("ACEPTAR");

		BotonAccion.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
			BotonAccionActionPerformed(evt);
			}
		}); 

		BotonAccion.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseEntered(java.awt.event.MouseEvent evt) {
			BotonAccionMouseEntered(evt);
			}
		}); 

		JButton BotonBorrar = new JButton();
		BotonBorrar.setText("REINICIAR");

		BotonBorrar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
			BotonBorrarActionPerformed(evt);
			}
		}); 

		BotonBorrar.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseEntered(java.awt.event.MouseEvent evt) {
			BotonBorrarMouseEntered(evt);
			}
		}); //Mouse over "desbloquear"

		//Labels
		Linea = new JLabel();
		R = new JLabel();
		L = new JLabel();
		G = new JLabel();
		C = new JLabel();
		Largo = new JLabel();
		mR = new JLabel();
		mL = new JLabel();
		mG = new JLabel();
		mC = new JLabel();
		mLargo= new JLabel();
		Carga = new JLabel();
		P = new JLabel();
		mP = new JLabel();
		fp = new JLabel();
		Base = new JLabel();
		V = new JLabel();
		mV = new JLabel();
		S = new JLabel();
		mS = new JLabel();

		//Combobox para carga inductiva o capacitiva?
		String[] indStrings = { "Inductivo", "Capacitivo" };
		induct = new JComboBox(indStrings);
		induct.setSelectedIndex(0);
		induct.setPreferredSize(new Dimension(80,19));

		tR = new javax.swing.JTextField();
		tL = new javax.swing.JTextField();
		tG = new javax.swing.JTextField();
		tC = new javax.swing.JTextField();
		tLargo = new javax.swing.JTextField();
		tP = new javax.swing.JTextField();
		tfp = new javax.swing.JTextField();
		tV = new javax.swing.JTextField();
		tS = new javax.swing.JTextField();

		JSeparator jSep1,jSep2,jSep3;
		jSep1 = new JSeparator();
		jSep2 = new JSeparator();
		jSep3 = new JSeparator();

		R.setText("R");
		mR.setText(" [Ohm/Km]");
		L.setText("L");
		mL.setText(" [H/Km]");
		G.setText("G");
		mG.setText(" [S/Km]");
		C.setText("C");
		mC.setText(" [F/Km]");
		Largo.setText("Largo");
		mLargo.setText(" [Km]");
		P.setText("P");
		mP.setText(" [MW]");
		fp.setText("f.p.");
		V.setText("V");
		mV.setText(" [kV]");
		S.setText("S");
		mS.setText(" [MVA]");
		Linea.setText("Linea");
		Carga.setText("Carga");
		Base.setText("Base");

		tR.setText("0.0329");
		tL.setText("1.089e-3");
		tG.setText("0");
		tC.setText("1.023e-8");
		tLargo.setText("800");
		tP.setText("5");
		tfp.setText("0.85");
		tV.setText("110");
		tS.setText("100");

		tR.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseEntered(java.awt.event.MouseEvent evt) {
			tRMouseEntered(evt);
			}
		});
		tL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseEntered(java.awt.event.MouseEvent evt) {
			tLMouseEntered(evt);
			}
		});
		tG.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseEntered(java.awt.event.MouseEvent evt) {
			tGMouseEntered(evt);
			}
		});
		tC.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseEntered(java.awt.event.MouseEvent evt) {
			tCMouseEntered(evt);
			}
		});
		tLargo.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseEntered(java.awt.event.MouseEvent evt) {
			tLargoMouseEntered(evt);
			}
		});
		tP.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseEntered(java.awt.event.MouseEvent evt) {
			tPMouseEntered(evt);
			}
		});
		tfp.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseEntered(java.awt.event.MouseEvent evt) {
			tfpMouseEntered(evt);
			}
		});
		tV.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseEntered(java.awt.event.MouseEvent evt) {
			tVMouseEntered(evt);
			}
		});
		tS.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseEntered(java.awt.event.MouseEvent evt) {
			tSMouseEntered(evt);
			}
		});
		induct.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseEntered(java.awt.event.MouseEvent evt) {
			inductMouseEntered(evt);
			}
		});

		//agregando elementos al panel de datos
		JPanel panelBotonesAccion = new JPanel();

		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 0;
		gBC.gridy = 0;
		gBC.weightx = 2;
		gBC.weighty = 1;
		panelBotonesAccion.add(BotonAccion, gBC);

		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 2;
		gBC.gridy = 0;
		gBC.weightx = 1;
		gBC.weighty = 1;
		panelBotonesAccion.add(BotonBorrar, gBC);

		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 0;
		gBC.gridy = 0;
		gBC.weightx = 3;
		gBC.weighty = 1;
		gBC.gridwidth = java.awt.GridBagConstraints.REMAINDER;
		PanelDatos.add(panelBotonesAccion, gBC);

		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 0;
		gBC.gridy = 1;
		gBC.weightx = 3;
		gBC.weighty = 1;
		gBC.fill = java.awt.GridBagConstraints.BOTH;
		gBC.gridwidth = java.awt.GridBagConstraints.REMAINDER;
		PanelDatos.add(jSep1, gBC);

		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 0;
		gBC.gridy = 2;
		gBC.weightx = 3;
		gBC.weighty = 1;
		gBC.gridwidth = java.awt.GridBagConstraints.REMAINDER;
		PanelDatos.add(Linea,gBC);

		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 0;
		gBC.gridy = 3;
		gBC.weightx = 1;
		gBC.weighty = 1;
		PanelDatos.add(R,gBC);
		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 1;
		gBC.gridy = 3;
		gBC.weightx = 1;
		gBC.weighty = 1;
		gBC.fill = java.awt.GridBagConstraints.HORIZONTAL;
		PanelDatos.add(tR,gBC);
		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 2;
		gBC.gridy = 3;
		gBC.weightx = 1;
		gBC.weighty = 1;
		gBC.anchor = java.awt.GridBagConstraints.WEST;
		PanelDatos.add(mR,gBC);

		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 0;
		gBC.gridy = 4;
		gBC.weightx = 1;
		gBC.weighty = 1;
		PanelDatos.add(L,gBC);
		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 1;
		gBC.gridy = 4;
		gBC.weightx = 1;
		gBC.weighty = 1;
		gBC.fill = java.awt.GridBagConstraints.HORIZONTAL;
		PanelDatos.add(tL,gBC);
		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 2;
		gBC.gridy = 4;
		gBC.weightx = 1;
		gBC.weighty = 1;
		gBC.anchor = java.awt.GridBagConstraints.WEST;
		PanelDatos.add(mL,gBC);

		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 0;
		gBC.gridy = 5;
		gBC.weightx = 1;
		gBC.weighty = 1;
		PanelDatos.add(G,gBC);
		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 1;
		gBC.gridy = 5;
		gBC.weightx = 1;
		gBC.weighty = 1;
		gBC.fill = java.awt.GridBagConstraints.HORIZONTAL;
		PanelDatos.add(tG,gBC);
		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 2;
		gBC.gridy = 5;
		gBC.weightx = 1;
		gBC.weighty = 1;
		gBC.anchor = java.awt.GridBagConstraints.WEST;
		PanelDatos.add(mG,gBC);

		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 0;
		gBC.gridy = 6;
		gBC.weightx = 1;
		gBC.weighty = 1;
		PanelDatos.add(C,gBC);
		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 1;
		gBC.gridy = 6;
		gBC.weightx = 1;
		gBC.weighty = 1;
		gBC.fill = java.awt.GridBagConstraints.HORIZONTAL;
		PanelDatos.add(tC,gBC);
		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 2;
		gBC.gridy = 6;
		gBC.weightx = 1;
		gBC.weighty = 1;
		gBC.anchor = java.awt.GridBagConstraints.WEST;
		PanelDatos.add(mC,gBC);

		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 0;
		gBC.gridy = 7;
		gBC.weightx = 1;
		gBC.weighty = 1;
		PanelDatos.add(Largo,gBC);
		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 1;
		gBC.gridy = 7;
		gBC.weightx = 1;
		gBC.weighty = 1;
		gBC.fill = java.awt.GridBagConstraints.HORIZONTAL;
		PanelDatos.add(tLargo,gBC);
		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 2;
		gBC.gridy = 7;
		gBC.weightx = 1;
		gBC.weighty = 1;
		gBC.anchor = java.awt.GridBagConstraints.WEST;
		PanelDatos.add(mLargo,gBC);

		//separador!
		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 0;
		gBC.gridy = 8;
		gBC.weightx = 3;
		gBC.weighty = 1;
		gBC.fill = java.awt.GridBagConstraints.BOTH;
		gBC.gridwidth = java.awt.GridBagConstraints.REMAINDER;
		PanelDatos.add(jSep2, gBC);

		//titulo carga
		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 0;
		gBC.gridy = 9;
		gBC.weightx = 3;
		gBC.weighty = 1;
		gBC.gridwidth = java.awt.GridBagConstraints.REMAINDER;
		PanelDatos.add(Carga,gBC);

		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 0;
		gBC.gridy = 10;
		gBC.weightx = 1;
		gBC.weighty = 1;
		PanelDatos.add(P,gBC);
		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 1;
		gBC.gridy = 10;
		gBC.weightx = 1;
		gBC.weighty = 1;
		gBC.fill = java.awt.GridBagConstraints.HORIZONTAL;
		PanelDatos.add(tP,gBC);
		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 2;
		gBC.gridy = 10;
		gBC.weightx = 1;
		gBC.weighty = 1;
		gBC.anchor = java.awt.GridBagConstraints.WEST;
		PanelDatos.add(mP,gBC);

		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 0;
		gBC.gridy = 11;
		gBC.weightx = 1;
		gBC.weighty = 1;
		PanelDatos.add(fp,gBC);
		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 1;
		gBC.gridy = 11;
		gBC.weightx = 1;
		gBC.weighty = 1;
		gBC.fill = java.awt.GridBagConstraints.HORIZONTAL;
		PanelDatos.add(tfp,gBC);
		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 2;
		gBC.gridy = 11;
		gBC.weightx = 1;
		gBC.weighty = 1;
		gBC.anchor = java.awt.GridBagConstraints.WEST;
		PanelDatos.add(induct,gBC);

		//separador!
		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 0;
		gBC.gridy = 12;
		gBC.weightx = 3;
		gBC.weighty = 1;
		gBC.fill = java.awt.GridBagConstraints.BOTH;
		gBC.gridwidth = java.awt.GridBagConstraints.REMAINDER;
		PanelDatos.add(jSep3, gBC);

		//titulo Base
		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 0;
		gBC.gridy = 13;
		gBC.weightx = 3;
		gBC.weighty = 1;
		gBC.gridwidth = java.awt.GridBagConstraints.REMAINDER;
		PanelDatos.add(Base,gBC);

		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 0;
		gBC.gridy = 14;
		gBC.weightx = 1;
		gBC.weighty = 1;
		PanelDatos.add(V,gBC);
		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 1;
		gBC.gridy = 14;
		gBC.weightx = 1;
		gBC.weighty = 1;
		gBC.fill = java.awt.GridBagConstraints.HORIZONTAL;
		PanelDatos.add(tV,gBC);
		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 2;
		gBC.gridy = 14;
		gBC.weightx = 1;
		gBC.weighty = 1;
		gBC.anchor = java.awt.GridBagConstraints.WEST;
		PanelDatos.add(mV,gBC);

		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 0;
		gBC.gridy = 15;
		gBC.weightx = 1;
		gBC.weighty = 1;
		PanelDatos.add(S,gBC);
		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 1;
		gBC.gridy = 15;
		gBC.weightx = 1;
		gBC.weighty = 1;
		gBC.fill = java.awt.GridBagConstraints.HORIZONTAL;
		PanelDatos.add(tS,gBC);
		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 2;
		gBC.gridy = 15;
		gBC.weightx = 1;
		gBC.weighty = 1;
		gBC.anchor = java.awt.GridBagConstraints.WEST;
		PanelDatos.add(mS,gBC);

		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 0;
		gBC.gridy = 0;
		gBC.weightx = 1;
		gBC.weighty = 1;
		PanelCentro.add(PanelDatos, gBC);
		
		JSeparator jSep4;
		jSep4 = new JSeparator();
		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 1;
		gBC.gridy = 0;
		gBC.weightx = 1;
		gBC.weighty = 1;
		gBC.fill = java.awt.GridBagConstraints.VERTICAL;
		jSep4.setOrientation(javax.swing.SwingConstants.VERTICAL);
		PanelCentro.add(jSep4,gBC);
		
		//FIN PANEL DATOS
		
		
		add(PanelCentro);
		
		
		//JTabbedPane tabedPane;
		//java.awt.Canvas cv;
		//JPanel result;
		//ImageIcon icon;
//		JTabbedPane tabbedPane = new JTabbedPane();
//		tabbedPane.setMinimumSize(new java.awt.Dimension(600,380));
//		tabbedPane.setPreferredSize(new java.awt.Dimension(600,380));
		java.awt.Canvas cv = new java.awt.Canvas();
		cv.setSize (508, 360);

		graf= new Grafico(cv);

		JPanel result = new JPanel();
		result.setLayout(new GridBagLayout());
		//resultados a mostrar.
//desde aca

		tZw = new JTextField();
		tpA = new JTextField();
		tpB = new JTextField();
		tpC = new JTextField();
		tpD = new JTextField();
		tpz = new JTextField();
		tpy = new JTextField();

		tZw.setEditable(false);
		tpA.setEditable(false);
		tpB.setEditable(false);
		tpC.setEditable(false);
		tpD.setEditable(false);
		tpz.setEditable(false);
		tpy.setEditable(false);


		tZw.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseEntered(java.awt.event.MouseEvent evt) {
			tZwMouseEntered(evt);
			}
		});
		tpA.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseEntered(java.awt.event.MouseEvent evt) {
			tpAMouseEntered(evt);
			}
		});
		tpB.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseEntered(java.awt.event.MouseEvent evt) {
			tpBMouseEntered(evt);
			}
		});
		tpC.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseEntered(java.awt.event.MouseEvent evt) {
			tpCMouseEntered(evt);
			}
		});
		tpD.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseEntered(java.awt.event.MouseEvent evt) {
			tpDMouseEntered(evt);
			}
		});
		tpz.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseEntered(java.awt.event.MouseEvent evt) {
			tpzMouseEntered(evt);
			}
		});
		tpy.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseEntered(java.awt.event.MouseEvent evt) {
			tpyMouseEntered(evt);
			}
		});


		lZw = new JLabel();
		lZw1 = new JLabel();
		mZw1 = new JLabel();
		lpi = new JLabel();
		lz = new JLabel();
		mz = new JLabel();
		ly = new JLabel();
		my = new JLabel();
		lA = new JLabel();
		lB = new JLabel();
		lC = new JLabel();
		lD = new JLabel();
		mB = new JLabel();
		mC2 = new JLabel();
		lABCD = new JLabel();


		lZw.setText("Impedancia Caracter�stica");
		lZw1.setText("Zw =");
		mZw1.setText(" [Ohm]");
		lpi.setIcon(new javax.swing.ImageIcon(getClass().getResource("images/linea1.jpg")));
		lpi.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		lpi.setPreferredSize(new java.awt.Dimension(200, 150));
		lz.setText("Zs =");
		mz.setText(" [Ohm]");
		ly.setText("Yp =");
		my.setText(" [S]");
		lA.setText("A =");
		lB.setText("B =");
		lC.setText("C =");
		lD.setText("D =");
		mB.setText(" [Ohm]");
		mC2.setText(" [S]");
		lABCD.setIcon(new javax.swing.ImageIcon(getClass().getResource("images/ABCD2.jpg")));
		lABCD.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		lABCD.setPreferredSize(new java.awt.Dimension(200, 150));


		lpi.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseEntered(java.awt.event.MouseEvent evt) {
			lpiMouseEntered(evt);
			}
		});
		lABCD.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseEntered(java.awt.event.MouseEvent evt) {
			lABCDMouseEntered(evt);
			}
		});

		tpA.setMinimumSize(new java.awt.Dimension(200, 19));
		tpA.setPreferredSize(new java.awt.Dimension(200, 19));
		tpB.setMinimumSize(new java.awt.Dimension(200, 19));
		tpB.setPreferredSize(new java.awt.Dimension(200, 19));
		tpC.setMinimumSize(new java.awt.Dimension(200, 19));
		tpC.setPreferredSize(new java.awt.Dimension(200, 19));
		tpD.setMinimumSize(new java.awt.Dimension(200, 19));
		tpD.setPreferredSize(new java.awt.Dimension(200, 19));
		tZw.setMinimumSize(new java.awt.Dimension(200, 19));
		tZw.setPreferredSize(new java.awt.Dimension(200, 19));
		tpz.setMinimumSize(new java.awt.Dimension(200, 19));
		tpz.setPreferredSize(new java.awt.Dimension(200, 19));
		tpy.setMinimumSize(new java.awt.Dimension(200, 19));
		tpy.setPreferredSize(new java.awt.Dimension(200, 19));

		pZw = new JPanel();
		pABCD = new JPanel();
		pPI = new JPanel();

		//panel derecho de Zw
		pZw.setLayout(new GridBagLayout());
		gBC = new GridBagConstraints();
		gBC.gridx = 0;
		gBC.gridy = 0;
		gBC.weightx = 0;
		gBC.weighty = 1;
		pZw.add(lZw1,gBC);
		gBC = new GridBagConstraints();
		gBC.gridx = 2;
		gBC.gridy = 0;
		gBC.weightx = 1;
		gBC.weighty = 1;
		pZw.add(tZw,gBC);
		gBC.gridx = 3;
		gBC.gridy = 0;
		gBC.weightx = 0;
		gBC.weighty = 1;
		pZw.add(mZw1,gBC);

		//panel derecho de modelo PI
		pPI.setLayout(new GridBagLayout());
		gBC = new GridBagConstraints();
		gBC.gridx = 0;
		gBC.gridy = 0;
		gBC.weightx = 0;
		gBC.weighty = 1;
		pPI.add(lz,gBC);
		gBC = new GridBagConstraints();
		gBC.gridx = 1;
		gBC.gridy = 0;
		gBC.weightx = 1;
		gBC.weighty = 1;
		pPI.add(tpz,gBC);
		gBC = new GridBagConstraints();
		gBC.gridx = 2;
		gBC.gridy = 0;
		gBC.weightx = 0;
		gBC.weighty = 1;
		pPI.add(mz,gBC);
		gBC = new GridBagConstraints();
		gBC.gridx = 0;
		gBC.gridy = 1;
		gBC.weightx = 1;
		gBC.weighty = 1;
		pPI.add(ly,gBC);
		gBC = new GridBagConstraints();
		gBC.gridx = 1;
		gBC.gridy = 1;
		gBC.weightx = 1;
		gBC.weighty = 1;
		pPI.add(tpy,gBC);
		gBC = new GridBagConstraints();
		gBC.gridx = 2;
		gBC.gridy = 1;
		gBC.weightx = 1;
		gBC.weighty = 1;
		pPI.add(my,gBC);

		//panel derecho de modelo ABCD
		pABCD.setLayout(new GridBagLayout());
		gBC = new GridBagConstraints();
		gBC.gridx = 0;
		gBC.gridy = 0;
		gBC.weightx = 0;
		gBC.weighty = 1;
		pABCD.add(lA,gBC);
		gBC = new GridBagConstraints();
		gBC.gridx = 1;
		gBC.gridy = 0;
		gBC.weightx = 1;
		gBC.weighty = 1;
		pABCD.add(tpA,gBC);
/**		gBC = new GridBagConstraints();
		gBC.gridx = 2;
		gBC.gridy = 0;
		gBC.weightx = 0;
		gBC.weighty = 1;
		pABCD.add(,gBC);
*/		gBC = new GridBagConstraints();
		gBC.gridx = 0;
		gBC.gridy = 1;
		gBC.weightx = 0;
		gBC.weighty = 1;
		pABCD.add(lB,gBC);
		gBC = new GridBagConstraints();
		gBC.gridx = 1;
		gBC.gridy = 1;
		gBC.weightx = 1;
		gBC.weighty = 1;
		pABCD.add(tpB,gBC);
		gBC = new GridBagConstraints();
		gBC.gridx = 2;
		gBC.gridy = 1;
		gBC.weightx = 0;
		gBC.weighty = 1;
		pABCD.add(mB,gBC);
		gBC = new GridBagConstraints();
		gBC.gridx = 0;
		gBC.gridy = 2;
		gBC.weightx = 0;
		gBC.weighty = 1;
		pABCD.add(lC,gBC);
		gBC = new GridBagConstraints();
		gBC.gridx = 1;
		gBC.gridy = 2;
		gBC.weightx = 1;
		gBC.weighty = 1;
		pABCD.add(tpC,gBC);
		gBC = new GridBagConstraints();
		gBC.gridx = 2;
		gBC.gridy = 2;
		gBC.weightx = 0;
		gBC.weighty = 1;
		pABCD.add(mC2,gBC);
		gBC = new GridBagConstraints();
		gBC.gridx = 0;
		gBC.gridy = 3;
		gBC.weightx = 0;
		gBC.weighty = 1;
		pABCD.add(lD,gBC);
		gBC = new GridBagConstraints();
		gBC.gridx = 1;
		gBC.gridy = 3;
		gBC.weightx = 1;
		gBC.weighty = 1;
		pABCD.add(tpD,gBC);
/**		gBC = new GridBagConstraints();
		gBC.gridx = 2;
		gBC.gridy = 3;
		gBC.weightx = 0;
		gBC.weighty = 1;
		pABCD.add(,gBC);
*/

		gBC = new GridBagConstraints();
		gBC.gridx = 0;
		gBC.gridy = 0;
		gBC.weightx = 1;
		gBC.weighty = 1;
		result.add(lZw,gBC);
		gBC = new GridBagConstraints();
		gBC.gridx = 1;
		gBC.gridy = 0;
		gBC.weightx = 1;
		gBC.weighty = 1;
		result.add(pZw,gBC);
		gBC = new GridBagConstraints();
		gBC.gridx = 0;
		gBC.gridy = 1;
		gBC.weightx = 1;
		gBC.weighty = 1;
		result.add(lpi,gBC);
		gBC = new GridBagConstraints();
		gBC.gridx = 1;
		gBC.gridy = 1;
		gBC.weightx = 1;
		gBC.weighty = 1;
		result.add(pPI,gBC);
		gBC = new GridBagConstraints();
		gBC.gridx = 0;
		gBC.gridy = 2;
		gBC.weightx = 1;
		gBC.weighty = 1;
		result.add(lABCD,gBC);
		gBC = new GridBagConstraints();
		gBC.gridx = 1;
		gBC.gridy = 2;
		gBC.weightx = 1;
		gBC.weighty = 1;
		result.add(pABCD,gBC);


//hasta ac�

/*
		ImageIcon rana = new ImageIcon(getClass().getResource("monos/rana.gif"));
		tabbedPane.addTab("Gradiente", rana,graf,"Gr�fico G v/s l");
		graf2 = new Grafico(cv);
		tabbedPane.addTab("Tensi�n",rana,graf2,"Gr�fico V v/s l");
		tabbedPane.addTab("Par�metros L�nea",rana,result,"C�lculos de par�metros de la L�nea");
		tabbedPane.addTab("Ayuda",rana,PanelAyuda,"Ayuda");


		gBC = new java.awt.GridBagConstraints();
		gBC.gridx = 2;
		gBC.gridy = 0;
		gBC.weightx = 1;
		gBC.weighty = 1;
		PanelCentro.add(tabbedPane,gBC);


		add(PanelCentro, java.awt.BorderLayout.CENTER);


		TextAreaAyuda.setBackground(new java.awt.Color(204, 204, 204));
		TextAreaAyuda.setBorder(new javax.swing.border.EtchedBorder());
		TextAreaAyuda.setMinimumSize(new java.awt.Dimension(0, 50));
		TextAreaAyuda.setPreferredSize(new java.awt.Dimension(0, 50));
		add(TextAreaAyuda, java.awt.BorderLayout.SOUTH);*/
	}

	//M�todos
	//evento de apretar un boton
	private void BotonAccionActionPerformed(java.awt.event.ActionEvent evt){
		accion();

	}
	//evento de mouseover
	private void BotonAccionMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("Realizar C�lculos");
	}
	private void acercaDeActionPerformed(java.awt.event.ActionEvent evt){
		JOptionPane acercade=new JOptionPane();
		acercade.showMessageDialog(null, "Aplicaci�n realizada por: \nSergio Lillo M. y \nAndr�s L�pez L. \nen el marco del ramo \"Sistemas El�ctricos de Potencia\", \ndictado por el profesor Rodrigo Palma B. en el \nDeparamento de Ingenier�a El�ctrica de la \nFacultad de Ciencias F�sicas y Matem�ticas de la \nUniversidad de Chile.", "Acerca de:", JOptionPane.INFORMATION_MESSAGE);

	}
	private void acercaDeMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("Acerca De");
	}
	private void BotonBorrarActionPerformed(java.awt.event.ActionEvent evt){
		borrar();
	}

	private void BotonGradienteMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("Gradiente");
	}
	private void BotonTensionMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("Tensi�n");
	}
	private void BotonParametrosMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("Par�metros");
	}
	private void BotonAyudaMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("Ayuda");
	}
	
	
	private void BotonGradienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonAyudaActionPerformed
		this.setVisible(false);
		Gradien.setVisible(true);
	}//GEN-LAST:event_BotonAyudaActionPerformed
	
	private void BotonParametrosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonAyudaActionPerformed
		this.setVisible(false);
		Param.setVisible(true);
	}//GEN-LAST:event_BotonAyudaActionPerformed
	
	private void BotonAyudaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonAyudaActionPerformed
		this.setVisible(false);
		Ayu.setVisible(true);
	}//GEN-LAST:event_BotonAyudaActionPerformed
	
	
	
	
	
	
	
	
	
	
	
	private void BotonBorrarMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("Borrar Gr�fico");
	}
	private void tRMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("En este campo se ingresa la Resistencia de L�nea, por  Kilometro.");
	}
	private void tLMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("En este campo se ingresa la Inductancia de L�nea, por  Kilometro.");
	}
	private void tGMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("En este campo se ingresa la Conductancia de L�nea, por  Kilometro.");
	}
	private void tCMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("En este campo se ingresa la Capacitancia de L�nea, por  Kilometro.");
	}
	private void tLargoMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("En este campo se ingresa el Largo de la L�nea en Kilometros.");
	}
	private void tPMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("En este campo se ingresa la Potencia Activa de la Carga de la L�nea.");
	}
	private void tfpMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("En este campo se ingresa el factor de potencia de la Carga de la L�nea.");
	}
	private void inductMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("Caracter�stica de la carga");
	}
	private void tVMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("En este campo se ingresa la Tensi�n Base del Problema en KiloVolts.");
	}
	private void tSMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("En este campo se ingresa la Potencia Base del Problema en MegaVoltAmper.");
	}
	private void tZwMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("Valor Complejo de la Impedancia Caracter�stica.");
	}
	private void tpAMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("Valor de A en la Matriz de par�metros ABCD.");
	}
	private void tpBMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("Valor de B en la Matriz de par�metros ABCD.");
	}
	private void tpCMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("Valor de C en la Matriz de par�metros ABCD.");
	}
	private void tpDMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("Valor de D en la Matriz de par�metros ABCD.");
	}
	private void tpzMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("Valor de la impedancia serie del modelo PI");
	}
	private void tpyMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("Valor de la Admitancia paralela del modelo PI");
	}
	private void lpiMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("Representaci�n gr�fica del modelo PI.");
	}
	private void lABCDMouseEntered(java.awt.event.MouseEvent evt){
		TextAreaAyuda.setText("Ecuaci�n de los par�metros ABCD para el modelo de tetrapolos.");
	}


	public void accion(){
		double nR,nC,nL,nG,nLargo,nP,nfp,nV,nS;
		int indu;
		ParamsData x=new ParamsData();
		boolean bolinduct;

		try
			{
				nR=Double.parseDouble(tR.getText());
				nL=Double.parseDouble(tL.getText());
				nG=Double.parseDouble(tG.getText());
				nC=Double.parseDouble(tC.getText());
				nLargo=Double.parseDouble(tLargo.getText());
				nP=Double.parseDouble(tP.getText());
				nfp=Double.parseDouble(tfp.getText());
				nV=Double.parseDouble(tV.getText());
				nS=Double.parseDouble(tS.getText());
				indu=induct.getSelectedIndex();

			} catch (Exception e) {
				JOptionPane error=new JOptionPane();
				error.showMessageDialog(null, "Algunos de los datos introducidos son invalidos", "Error en los datos", JOptionPane.ERROR_MESSAGE);
				return;
			}
		if(indu==0){bolinduct=true;}
		else {bolinduct=false;}

		x=CalcParams.LLCP(nR,nL,nG,nC,nLargo);
		tZw.setText(x.Zw.toString());
		tpz.setText(x.Pi[0].toString());
		tpy.setText(x.Pi[1].toString());
		tpA.setText(x.ABCD[0][0].toString());
		tpB.setText(x.ABCD[0][1].toString());
		tpC.setText(x.ABCD[1][0].toString());
		tpD.setText(x.ABCD[1][1].toString());

		graf.getParam(nLargo,nV,nS,nP,nfp,nR,nL,nG,nC,bolinduct,true);
		graf.realizarCalculos();
		graf.repaint();
		graf2.getParam(nLargo,nV,nS,nP,nfp,nR,nL,nG,nC,bolinduct,false);
		graf2.realizarCalculos();
		graf2.repaint();

		//bloquear campos de la l�nea
		tR.setEditable(false);
		tL.setEditable(false);
		tG.setEditable(false);
		tC.setEditable(false);
		tLargo.setEditable(false);
		tS.setEditable(false);


	}

	public void primeraAccion(){

		double nR,nC,nL,nG,nLargo,nP,nfp,nV,nS;
		int indu;
		ParamsData x=new ParamsData();
		boolean bolinduct;

		try
			{
				nR=Double.parseDouble(tR.getText());
				nL=Double.parseDouble(tL.getText());
				nG=Double.parseDouble(tG.getText());
				nC=Double.parseDouble(tC.getText());
				nLargo=Double.parseDouble(tLargo.getText());
				nP=Double.parseDouble(tP.getText());
				nfp=Double.parseDouble(tfp.getText());
				nV=Double.parseDouble(tV.getText());
				nS=Double.parseDouble(tS.getText());
				indu=induct.getSelectedIndex();

			} catch (Exception e) {
				JOptionPane error=new JOptionPane();
				error.showMessageDialog(null, "Algunos de los datos introducidos son invalidos", "Error en los datos", JOptionPane.ERROR_MESSAGE);
				return;
			}
		if(indu==0){bolinduct=true;}
		else {bolinduct=false;}

		x=CalcParams.LLCP(nR,nL,nG,nC,nLargo);
		tZw.setText(x.Zw.toString());
		tpz.setText(x.Pi[0].toString());
		tpy.setText(x.Pi[1].toString());
		tpA.setText(x.ABCD[0][0].toString());
		tpB.setText(x.ABCD[0][1].toString());
		tpC.setText(x.ABCD[1][0].toString());
		tpD.setText(x.ABCD[1][1].toString());

		int[] p = new int[4];
		p[0]=4;
		p[1]=5;
		p[2]=6;
		p[3]=7;
		for(int i=0;i<4;++i){
			graf.getParam(nLargo,nV,nS,p[i],nfp,nR,nL,nG,nC,bolinduct,true);
			graf.realizarCalculos();
			graf.repaint();
			graf2.getParam(nLargo,nV,nS,p[i],nfp,nR,nL,nG,nC,bolinduct,false);
			graf2.realizarCalculos();
			graf2.repaint();
		}

		//bloquear campos de la l�nea
		tR.setEditable(false);
		tL.setEditable(false);
		tG.setEditable(false);
		tC.setEditable(false);
		tLargo.setEditable(false);
		tS.setEditable(false);



	}


	public void borrar(){
		//desbloquear campos de la linea
		//bloquear campos de la l�nea
		tR.setEditable(true);
		tL.setEditable(true);
		tG.setEditable(true);
		tC.setEditable(true);
		tLargo.setEditable(true);
		tS.setEditable(true);

//		tZw.setText("");
//		tpz.setText("");
//		tpy.setText("");
//		tpA.setText("");
//		tpB.setText("");
//		tpC.setText("");
//		tpD.setText("");

		graf.blanquear();
		graf2.blanquear();

	}


	// variables
	JComboBox induct;
    Grafico graf,graf2;
	JPanel pZw,pABCD,pPI;
	JTextField tpA,tpB,tpC,tpD,tZw,tpz,tpy;
	JLabel lZw,lZw1,mZw1,lpi,lz,mz,ly,my,lA,lB,lC,lD,mB,mC2,lABCD;
	JLabel  Linea,R,L,G,C,Largo,mLargo,mR,mL,mG,mC,Carga,P,mP,fp,Base,V,mV,S,mS;
	JTextField tR,tL,tG,tC,tLargo,tP,tfp,tV,tS;
}	//fin class Op1 extends javax.swing.JPanel
