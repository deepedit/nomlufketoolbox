package org.deepedit.addons.nomlufketool.frequencycontrol;

import org.deepedit.addons.nomlufketool.utils.Complex;

public class ParamsData {

    Complex Pi[];
    Complex ABCD[][];
    Complex Zw;
    Complex Gamma;

    public ParamsData() {
        Pi = new Complex[2];            // (Z,Y)
        ABCD = new Complex[2][2];       //[fila][columna]
    }
    
}
