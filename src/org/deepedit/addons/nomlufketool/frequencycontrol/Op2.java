package org.deepedit.addons.nomlufketool.frequencycontrol;;


public class Op2 extends javax.swing.JPanel {

    Op1 op1;

    public void Metodos(Op1 o){
    	op1=o;
    }

    /** Creates new form Op2 */
    public Op2() {
        initComponents();
    }

    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        PANEL_TITULO = new javax.swing.JPanel();
        TITULO = new javax.swing.JLabel();
        PANEL_OP = new javax.swing.JPanel();
        BotonPU = new javax.swing.JButton();
        BotonLFC = new javax.swing.JButton();
        BotonLFC.setEnabled(false);
        PANEL_ADE = new javax.swing.JPanel();
        BotonADE = new javax.swing.JButton();
        PANEL_DATOS = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        TextFieldTt = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        TextFieldH = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        TextFieldTg = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        TextFieldR = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        TextFieldD = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        TextFieldPl = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jPanel14 = new javax.swing.JPanel();
        BotonCalcular = new javax.swing.JButton();
        BotonReset = new javax.swing.JButton();
        PANEL_GRAFICO = new javax.swing.JPanel();
        PANEL_AYUDA = new javax.swing.JPanel();
        AYUDA = new javax.swing.JLabel();
        graf = new MCanvas();

        setLayout(new java.awt.GridBagLayout());

        setMinimumSize(new java.awt.Dimension(700, 500));
        setPreferredSize(new java.awt.Dimension(700, 500));
        setRequestFocusEnabled(false);
        PANEL_TITULO.setLayout(new java.awt.BorderLayout());

        TITULO.setFont(new java.awt.Font("Dialog", 1, 14));
        TITULO.setText("Control de la Frecuencia y de la Potencia Activa para Distintas Condiciones");
        PANEL_TITULO.add(TITULO, java.awt.BorderLayout.CENTER);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        add(PANEL_TITULO, gridBagConstraints);

        PANEL_OP.setLayout(new java.awt.GridBagLayout());

        BotonPU.setText("Calculo [0/1]");
        BotonPU.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonPUActionPerformed(evt);
            }
        });
        BotonPU.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BotonPUMouseEntered(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        PANEL_OP.add(BotonPU, gridBagConstraints);

        BotonLFC.setText("LFC");
        BotonLFC.setMaximumSize(new java.awt.Dimension(93, 25));
        BotonLFC.setMinimumSize(new java.awt.Dimension(93, 25));
        BotonLFC.setPreferredSize(new java.awt.Dimension(93, 25));
        BotonLFC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonLFCActionPerformed(evt);
            }
        });
 /*       BotonLFC.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BotonLFCMouseEntered(evt);
            }
        });*/

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        PANEL_OP.add(BotonLFC, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        add(PANEL_OP, gridBagConstraints);

        BotonADE.setText("Ayuda");
        BotonADE.setMaximumSize(new java.awt.Dimension(93, 25));
        BotonADE.setMinimumSize(new java.awt.Dimension(93, 25));
        BotonADE.setPreferredSize(new java.awt.Dimension(93, 25));
        BotonADE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonADEActionPerformed(evt);
            }
        });
        BotonADE.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BotonADEMouseEntered(evt);
            }
        });

        PANEL_ADE.add(BotonADE);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        add(PANEL_ADE, gridBagConstraints);

        PANEL_DATOS.setLayout(new java.awt.GridBagLayout());

        PANEL_DATOS.setDebugGraphicsOptions(javax.swing.DebugGraphics.NONE_OPTION);
        PANEL_DATOS.setMinimumSize(new java.awt.Dimension(422, 90));
        PANEL_DATOS.setPreferredSize(new java.awt.Dimension(411, 115));
        jPanel3.setLayout(new java.awt.GridBagLayout());

        jPanel3.setBorder(new javax.swing.border.TitledBorder("Turbina, Generador"));
        jPanel3.setPreferredSize(new java.awt.Dimension(130, 53));
        jLabel2.setText("Tt");
        jLabel2.setMaximumSize(new java.awt.Dimension(100, 15));
        jLabel2.setMinimumSize(new java.awt.Dimension(20, 15));
        jLabel2.setPreferredSize(new java.awt.Dimension(20, 15));
        jPanel3.add(jLabel2, new java.awt.GridBagConstraints());

        TextFieldTt.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldTt.setText("0.5");
        TextFieldTt.setMinimumSize(new java.awt.Dimension(40, 19));
        TextFieldTt.setPreferredSize(new java.awt.Dimension(40, 19));
        TextFieldTt.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                TextFieldTtMouseEntered(evt);
            }
        });

        jPanel3.add(TextFieldTt, new java.awt.GridBagConstraints());

        jLabel3.setText("[seg]");
        jPanel3.add(jLabel3, new java.awt.GridBagConstraints());

        jLabel6.setText("H");
        jLabel6.setMaximumSize(new java.awt.Dimension(50, 15));
        jLabel6.setMinimumSize(new java.awt.Dimension(20, 15));
        jLabel6.setPreferredSize(new java.awt.Dimension(20, 15));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        jPanel3.add(jLabel6, gridBagConstraints);

        TextFieldH.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldH.setText("5");
        TextFieldH.setMinimumSize(new java.awt.Dimension(70, 19));
        TextFieldH.setPreferredSize(new java.awt.Dimension(40, 19));
        TextFieldH.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                TextFieldHMouseEntered(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        jPanel3.add(TextFieldH, gridBagConstraints);

        jLabel7.setText("[seg]");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        jPanel3.add(jLabel7, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 16;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        PANEL_DATOS.add(jPanel3, gridBagConstraints);

        jPanel5.setLayout(new java.awt.GridBagLayout());

        jPanel5.setBorder(new javax.swing.border.TitledBorder("Regulador"));
        jPanel5.setPreferredSize(new java.awt.Dimension(130, 120));
        jLabel4.setText("Tg");
        jLabel4.setMaximumSize(new java.awt.Dimension(50, 15));
        jLabel4.setMinimumSize(new java.awt.Dimension(20, 15));
        jLabel4.setPreferredSize(new java.awt.Dimension(20, 15));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 2);
        jPanel5.add(jLabel4, gridBagConstraints);

        TextFieldTg.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldTg.setText("0.2");
        TextFieldTg.setMinimumSize(new java.awt.Dimension(40, 19));
        TextFieldTg.setPreferredSize(new java.awt.Dimension(40, 19));
        TextFieldTg.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                TextFieldTgMouseEntered(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 2);
        jPanel5.add(TextFieldTg, gridBagConstraints);

        jLabel5.setText("[seg]");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 0);
        jPanel5.add(jLabel5, gridBagConstraints);

        jLabel9.setText("R");
        jLabel9.setMaximumSize(new java.awt.Dimension(50, 15));
        jLabel9.setMinimumSize(new java.awt.Dimension(20, 15));
        jLabel9.setPreferredSize(new java.awt.Dimension(20, 15));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 2);
        jPanel5.add(jLabel9, gridBagConstraints);

        TextFieldR.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldR.setText("0.05");
        TextFieldR.setMinimumSize(new java.awt.Dimension(40, 19));
        TextFieldR.setPreferredSize(new java.awt.Dimension(40, 19));
        TextFieldR.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                TextFieldRMouseEntered(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 2);
        jPanel5.add(TextFieldR, gridBagConstraints);

        jLabel8.setText("pu");
        jLabel8.setMaximumSize(new java.awt.Dimension(30, 15));
        jLabel8.setMinimumSize(new java.awt.Dimension(24, 15));
        jLabel8.setPreferredSize(new java.awt.Dimension(24, 15));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 0);
        jPanel5.add(jLabel8, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        PANEL_DATOS.add(jPanel5, gridBagConstraints);

        jPanel6.setLayout(new java.awt.GridBagLayout());

        jPanel6.setBorder(new javax.swing.border.TitledBorder("Carga"));
        jPanel6.setPreferredSize(new java.awt.Dimension(130, 120));
        jLabel11.setText("D");
        jLabel11.setMaximumSize(new java.awt.Dimension(50, 15));
        jLabel11.setMinimumSize(new java.awt.Dimension(20, 15));
        jLabel11.setPreferredSize(new java.awt.Dimension(20, 15));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 2);
        jPanel6.add(jLabel11, gridBagConstraints);

        TextFieldD.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldD.setText("0.8");
        TextFieldD.setMinimumSize(new java.awt.Dimension(40, 19));
        TextFieldD.setPreferredSize(new java.awt.Dimension(40, 19));
        TextFieldD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TextFieldDActionPerformed(evt);
            }
        });
        TextFieldD.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                TextFieldDMouseEntered(evt);
            }
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 2);
        jPanel6.add(TextFieldD, gridBagConstraints);

        jLabel12.setText("pu");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 0);
        jPanel6.add(jLabel12, gridBagConstraints);

        jLabel13.setText("PL");
        jLabel13.setMaximumSize(new java.awt.Dimension(50, 15));
        jLabel13.setMinimumSize(new java.awt.Dimension(20, 15));
        jLabel13.setPreferredSize(new java.awt.Dimension(20, 15));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 2);
        jPanel6.add(jLabel13, gridBagConstraints);

        TextFieldPl.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        TextFieldPl.setText("0.2");
        TextFieldPl.setMinimumSize(new java.awt.Dimension(40, 19));
        TextFieldPl.setPreferredSize(new java.awt.Dimension(40, 19));

		TextFieldPl.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseEntered(java.awt.event.MouseEvent evt) {
				TextFieldPlMouseEntered(evt);
			}
        });

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 2);
        jPanel6.add(TextFieldPl, gridBagConstraints);

        jLabel14.setText("pu");
        jLabel14.setMaximumSize(new java.awt.Dimension(30, 15));
        jLabel14.setMinimumSize(new java.awt.Dimension(24, 15));
        jLabel14.setPreferredSize(new java.awt.Dimension(24, 15));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 0);
        jPanel6.add(jLabel14, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 6;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        PANEL_DATOS.add(jPanel6, gridBagConstraints);

        BotonCalcular.setText("Calcular");
        BotonCalcular.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        BotonCalcular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonCalcularActionPerformed(evt);
            }
        });
        BotonCalcular.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BotonCalcularMouseEntered(evt);
            }
        });

        jPanel14.add(BotonCalcular);

        BotonReset.setText("Reset");
        BotonReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonResetActionPerformed(evt);
            }
        });
        BotonReset.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                BotonResetMouseEntered(evt);
            }
        });

        jPanel14.add(BotonReset);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 7;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        PANEL_DATOS.add(jPanel14, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        add(PANEL_DATOS, gridBagConstraints);

        PANEL_GRAFICO.setLayout(new java.awt.BorderLayout());

        PANEL_GRAFICO.setBorder(new javax.swing.border.TitledBorder("Curva de Variacion de Frecuencia v/s Tiempo"));
        PANEL_GRAFICO.setPreferredSize(new java.awt.Dimension(600, 270));
        PANEL_GRAFICO.add(graf, java.awt.BorderLayout.CENTER);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 14;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.gridheight = 7;
        add(PANEL_GRAFICO, gridBagConstraints);

        PANEL_AYUDA.setLayout(new java.awt.BorderLayout());

        PANEL_AYUDA.setBorder(new javax.swing.border.TitledBorder("Ayuda"));
        AYUDA.setText(" ");
        AYUDA.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        PANEL_AYUDA.add(AYUDA, java.awt.BorderLayout.CENTER);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 24;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        add(PANEL_AYUDA, gridBagConstraints);
    }
    // </editor-fold>//GEN-END:initComponents

    private void TextFieldPlMouseEntered(java.awt.event.MouseEvent evt) {
		AYUDA.setText("Ingresar Variacion de la Carga");
    }

    private void BotonResetMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BotonResetMouseEntered
		AYUDA.setText("Borra el Grafico");
    }//GEN-LAST:event_BotonResetMouseEntered

    private void BotonCalcularMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BotonCalcularMouseEntered
		AYUDA.setText("Grafica la Variacion de Frecuencia en Funcion del Tiempo");
    }//GEN-LAST:event_BotonCalcularMouseEntered

    private void TextFieldHMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TextFieldHMouseEntered
		AYUDA.setText("Ingresar la Constante de Inercia del Generador");
    }//GEN-LAST:event_TextFieldHMouseEntered

    private void TextFieldTtMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TextFieldTtMouseEntered
		AYUDA.setText("Ingersar la Comstante de Tiempo de la Turbina");
    }//GEN-LAST:event_TextFieldTtMouseEntered

    private void TextFieldDMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TextFieldDMouseEntered
		AYUDA.setText("Ingresar Sensibilidad Frecuencia-Carga");
    }//GEN-LAST:event_TextFieldDMouseEntered

    private void TextFieldRMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TextFieldRMouseEntered
		AYUDA.setText("Ingresar Estatismo");
    }//GEN-LAST:event_TextFieldRMouseEntered

    private void TextFieldTgMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TextFieldTgMouseEntered
		AYUDA.setText("Ingresar Constante de Tiempo del Regulador");
    }//GEN-LAST:event_TextFieldTgMouseEntered

    private void BotonADEMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BotonADEMouseEntered
		AYUDA.setText("Elija esta opci�n si necesita Ayuda");
    }//GEN-LAST:event_BotonADEMouseEntered

/*    private void BotonLFCMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BotonLFCMouseEntered
		AYUDA.setText("Control de Frecuencia y Carga");
    }//GEN-LAST:event_BotonLFCMouseEntered*/

    private void BotonPUMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_BotonPUMouseEntered
		AYUDA.setText("Cambia a la Pantalla Calculo [0/1]");
    }//GEN-LAST:event_BotonPUMouseEntered

    private void TextFieldDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TextFieldDActionPerformed
		AYUDA.setText("Sensibilidad de la Frecuencia ante Variaciones de Carga");
    }//GEN-LAST:event_TextFieldDActionPerformed

	private void BotonADEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonADEActionPerformed
		op1.showHTML("Ayuda","help.html");
    }//GEN-LAST:event_BotonADEActionPerformed

    private void BotonLFCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonLFCActionPerformed
// TODO add your handling code here:

    }//GEN-LAST:event_BotonLFCActionPerformed

    private void BotonPUActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonPUActionPerformed
		op1.setVisible(true);
		setVisible(false);
        op1.repaint();
    }//GEN-LAST:event_BotonPUActionPerformed

    private void BotonResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonResetActionPerformed

		TextFieldD.setEditable(true);
		TextFieldH.setEditable(true);
		TextFieldPl.setEditable(true);
		TextFieldTg.setEditable(true);
		TextFieldTt.setEditable(true);

		graf.blanquear();
    }//GEN-LAST:event_BotonResetActionPerformed

    private void BotonCalcularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonCalcularActionPerformed

		TextFieldD.setEditable(false);
		TextFieldH.setEditable(false);
		TextFieldPl.setEditable(false);
		TextFieldTg.setEditable(false);
		TextFieldTt.setEditable(false);

		/*FUNCION DE TRANSFERENCIA*/
		double Tt,Tg,H,R,Pl,D;

		Tt 	= Double.parseDouble(TextFieldTt.getText());
		Tg	= Double.parseDouble(TextFieldTg.getText());
		H	= Double.parseDouble(TextFieldH.getText());
		R	= Double.parseDouble(TextFieldR.getText());
		Pl	= Double.parseDouble(TextFieldPl.getText());
		D	= Double.parseDouble(TextFieldD.getText());

		boolean StepR = true;
		int Nnum = 1;
		int Nden = 4;
		double[] num = new double [Nnum];
		double[] den = new double [Nden];

		num[0] = 1;

		den[3] = 2*H*Tg*Tt;
		den[2] = D*Tg*Tt+2*H*(Tg+Tt);
		den[1] = D*(Tg+Tt)+2*H;
		den[0] = D+1/R;

		graf.Calcular(num,den,Nnum,Nden,Pl,20);
		graf.repaint();

    }//GEN-LAST:event_BotonCalcularActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel AYUDA;
    private javax.swing.JButton BotonADE;
    private javax.swing.JButton BotonCalcular;
    private javax.swing.JButton BotonLFC;
    private javax.swing.JButton BotonPU;
    private javax.swing.JButton BotonReset;
    private javax.swing.JPanel PANEL_ADE;
    private javax.swing.JPanel PANEL_AYUDA;
    private javax.swing.JPanel PANEL_DATOS;
    private javax.swing.JPanel PANEL_GRAFICO;
    private javax.swing.JPanel PANEL_OP;
    private javax.swing.JPanel PANEL_TITULO;
    private javax.swing.JLabel TITULO;
    private javax.swing.JTextField TextFieldD;
    private javax.swing.JTextField TextFieldH;
    private javax.swing.JTextField TextFieldPl;
    private javax.swing.JTextField TextFieldR;
    private javax.swing.JTextField TextFieldTg;
    private javax.swing.JTextField TextFieldTt;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    // End of variables declaration//GEN-END:variables
    private MCanvas graf;
    
}

