package org.deepedit.addons.nomlufketool.unbalancedcomponents;

/*
	* @Version 1.0
	* @Autor Nolberto Emilio Oyarce Seguin
 */

import java.awt.*;
import java.awt.geom.*;
import javax.swing.*;
import org.deepedit.addons.nomlufketool.utils.Complex;

public class Fasor extends JDialog {

    final static int maxCharHeight = 15;
    final static int minFontSize = 6;
    LAFPanel pLAF;
    JPanel p0;
    final static Color bg = Color.white;
    final static Color fg = Color.black;
    final static Color red = Color.red;
    final static Color white = Color.white;
    Color WhiteSmoke = new Color(240, 240, 240);
    final static BasicStroke stroke = new BasicStroke(2.0f);
    final static BasicStroke wideStroke = new BasicStroke(8.0f);

    final static float dash1[] = {10.0f};
    final static BasicStroke dashed = new BasicStroke(1.0f,
            BasicStroke.CAP_BUTT,
            BasicStroke.JOIN_MITER,
            10.0f, dash1, 0.0f);
    Dimension totalSize;
    FontMetrics fontMetrics;

    /*
	*Seteo de los complejos en (0.0, 0.0)
     */
    Complex A = new Complex((double) 0, (double) 0);
    Complex B = new Complex((double) 0, (double) 0);
    Complex C = new Complex((double) 0, (double) 0);
    Complex a = new Complex((double) 0, (double) 0);
    Complex aa = new Complex((double) 0, (double) 0);
    Complex a_B = new Complex((double) 0, (double) 0);
    Complex aa_B = new Complex((double) 0, (double) 0);
    Complex a_C = new Complex((double) 0, (double) 0);
    Complex aa_C = new Complex((double) 0, (double) 0);

    double presicion = 0.0000001;

    public double r, pi, alpha, beta, x11_a, y11_a, x11_b, y11_b, x11_c, y11_c, max_a, max_b, max_c, max_ab, max_global;
    protected Font TextDialogFont;
    protected FontMetrics TextDialogFontMetrics;
    public double tolerancia = (double) 0;

    public Fasor(JFrame parent, int numero, double re_a, double im_a, double re_b, double im_b, double re_c, double im_c) {
        super(parent, true);
        setTitle("Representaci�n Gr�fica de los Fasores");

        x11_a = re_a;
        y11_a = im_a;
        x11_b = re_b;
        y11_b = im_b;
        x11_c = re_c;
        y11_c = im_c;

        setBackground(Color.lightGray);
        Font TextDialogFont = new Font("TimesRoman", Font.PLAIN, 12);
        TextDialogFontMetrics = getFontMetrics(TextDialogFont);
        setFont(TextDialogFont);

        /*
		*Creaci�n de los Paneles
         */
        BorderLayout borderl = new BorderLayout();
        getContentPane().setLayout(borderl);
        p0 = panel0();
        getContentPane().add("Center", p0);

        pack();
        setVisible(true);
        return;
    }

    public JPanel panel0() {
        JPanel p = new JPanel();
        p.setSize(new Dimension(750, 600));
        setBackground(WhiteSmoke);
        setForeground(WhiteSmoke);
        return p;
    }

    FontMetrics pickFont(Graphics2D g2,
            String longString,
            int xSpace) {
        boolean fontFits = false;
        Font font = g2.getFont();
        FontMetrics fontMetrics = g2.getFontMetrics();
        int size = font.getSize();
        String name = font.getName();
        int style = font.getStyle();
        setSize(750, 600);
        setLocation(150, 100);

        while (!fontFits) {
            if ((fontMetrics.getHeight() <= maxCharHeight)
                    && (fontMetrics.stringWidth(longString) <= xSpace)) {
                fontFits = true;
            } else {
                if (size <= minFontSize) {
                    fontFits = true;
                } else {
                    g2.setFont(font = new Font(name,
                            style,
                            --size));
                    fontMetrics = g2.getFontMetrics();
                }
            }
        }

        return fontMetrics;
    }

    /*
	*Funcion que pinta todo.
     */
    public void paint(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        //Dimension d = getSize();
        Dimension d = new Dimension(750, 600);
        int gridWidth = d.width;
        int gridHeight = d.height;

        fontMetrics = pickFont(g2, "Filled and Stroked GeneralPath",
                gridWidth);

        Color fg3D = Color.blue;

        /*
	*Asignacion de los Complejos con las variables de entrada
         */
        Complex A = new Complex(x11_a, y11_a);
        Complex B = new Complex(x11_b, y11_b);
        Complex C = new Complex(x11_c, y11_c);

        double a_im = (Math.sqrt(3)) / 2;
        a = null;
        a = new Complex((double) (-0.5), a_im);

        aa = null;
        aa = new Complex((double) (-0.5), -a_im);
        a_B = null;
        a_B = Complex.multiply(a, B);
        aa_B = null;
        aa_B = Complex.multiply(aa, B);
        a_C = null;
        a_C = Complex.multiply(a, C);
        aa_C = null;
        aa_C = Complex.multiply(aa, C);

        /*
	*Seteo de los bordes de la zona de gr�fico
         */
        g2.setPaint(fg3D);
        g2.draw3DRect(0, 0, d.width - 1, d.height - 1, true);
        g2.draw3DRect(3, 3, d.width - 7, d.height - 7, true);
        g2.setPaint(fg);
        int x = 5;
        int y = 7;
        int rectWidth = gridWidth - 2 * x;
        int stringY = gridHeight - 3 - fontMetrics.getDescent();
        int rectHeight = stringY - fontMetrics.getMaxAscent() - y - 2;

        /*
	* Centro de los Fasores
         */
        double set_Y = (gridHeight - 20) / 6;

        //double x00_0 = gridWidth/2;
        double x00_0 = 125;
        double y00_0 = set_Y + 10;
        //double x00_1 = gridWidth/2;
        double x00_1 = 125;
        double y00_1 = 3 * set_Y + 10;
        //double x00_2 = gridWidth/2;
        double x00_2 = 125;
        double y00_2 = 5 * set_Y + 10;

        double x00_ab = gridWidth / 2;
        double y00_ab = gridHeight / 2;
        double x00_c = gridWidth / 2;
        double y00_c = gridHeight / 2;

        double x_res = 475;
        double y_res = gridHeight / 2;

        //funcion que calcula los maximos de los fasores
        max_a = Math.sqrt((Math.abs(x11_a)) * (Math.abs(x11_a)) + (Math.abs(y11_a)) * (Math.abs(y11_a)));
        max_b = Math.sqrt((Math.abs(x11_b)) * (Math.abs(x11_b)) + (Math.abs(y11_b)) * (Math.abs(y11_b)));
        max_c = Math.sqrt((Math.abs(x11_c)) * (Math.abs(x11_c)) + (Math.abs(y11_c)) * (Math.abs(y11_c)));
        max_ab = Math.max(max_a, max_b);
        max_global = Math.max(max_c, max_ab);

        /*
	* Escalamiento de los fasores a el pixelado de la pantalla
         */
        double cota = (double) ((gridHeight - 100) / 6);
        double escala = cota / max_global;
        double X11_a = escala * x11_a;
        double Y11_a = escala * y11_a;
        double X11_b = escala * x11_b;
        double Y11_b = escala * y11_b;
        double X11_c = escala * x11_c;
        double Y11_c = escala * y11_c;
        double XB_a = escala * a_B.re;
        double YB_a = escala * a_B.im;
        double XB_aa = escala * aa_B.re;
        double YB_aa = escala * aa_B.im;
        double XC_a = escala * a_C.re;
        double YC_a = escala * a_C.im;
        double XC_aa = escala * aa_C.re;
        double YC_aa = escala * aa_C.im;

        /*
	*Dibujo de los Fasores
         */
 /*
	* Lineas de los FAsores A, B, C.
         */
 /*
	*Si los fasores son de largo cero, se dibujan las lineas de los ejes solamente
         */
        if (Complex.abs(A) <= presicion && Complex.abs(B) <= presicion && Complex.abs(C) <= presicion) {

            g2.draw(new Line2D.Double(-200 + x00_ab, y00_ab, 200 + x00_ab, y00_ab));
            g2.draw(new Line2D.Double(x00_ab, -200 + y00_ab, x00_ab, 200 + y00_ab));
            String logo = "logo.gif";
            //g2.draw(new Image(logo,90,0,300,62,this));

            /*
	*Salida de los complejos en forma num�rica en pantalla
             */
 /*
	*Salida A
             */
            g2.setColor(Color.magenta);
            double Real_A = Math.round(10000 * A.re);
            double Real_a = Real_A / 10000;
            String label_A_re = String.valueOf(Real_a);

            double Imag_A = Math.round(10000 * A.im);
            double Imag_a = Imag_A / 10000;
            String label_A_im = String.valueOf(Imag_a);
            g2.drawString("a\u2080 = " + label_A_re + "+j" + label_A_im, 10, 420);

            /*
	*Salida B
             */
            double Real_B = Math.round(10000 * B.re);
            double Real_b = Real_B / 10000;
            String label_B_re = String.valueOf(Real_b);

            double Imag_B = Math.round(10000 * B.im);
            double Imag_b = Imag_B / 10000;
            String label_B_im = String.valueOf(Imag_b);
            g2.drawString("a\2081 = " + label_B_re + "+j" + label_B_im, 10, 440);

            /*
	*Salida C
             */
            double Real_C = Math.round(10000 * C.re);
            double Real_c = Real_C / 10000;
            String label_C_re = String.valueOf(Real_c);

            double Imag_C = Math.round(10000 * C.im);
            double Imag_c = Imag_C / 10000;
            String label_C_im = String.valueOf(Imag_c);
            g2.drawString("a\2082 = " + label_C_re + "+j" + label_C_im, 10, 460);

        } /*
	*Si los fasores no son cero se realizan los dibujos sobre ellos
         */ else {
            //definicion de constantes
            double L = 10;
            double grados = 30;
            double alpha = grados * Math.PI / 180;

            //fasor 0
            //Linea del fasor
            g2.setColor(Color.blue);
            g2.draw(new Line2D.Double(x00_0, y00_0, X11_a + x00_0, -Y11_a + y00_0));
            g2.setColor(Color.green);
            g2.draw(new Line2D.Double(x00_0, y00_0 + 15, X11_a + x00_0, -Y11_a + y00_0 + 15));
            g2.setColor(Color.red);
            g2.draw(new Line2D.Double(x00_0, y00_0 + 30, X11_a + x00_0, -Y11_a + y00_0 + 30));

            //Circle
            //g2.setColor(Color.blue);
            //double alt_0 = Math.sqrt((X11_a*X11_a)+(Y11_a*Y11_a));
            //g2.draw(new Ellipse2D.Double(x00_0-alt_0,-alt_0+y00_0,2*alt_0,2*alt_0));
            //Punta de flecha
            g2.setColor(Color.blue);
            double beta_a0 = Complex.argument(A);
            double flecha_x1a0 = L * (Math.sin(Math.PI / 2 - beta_a0 - alpha));
            double flecha_y1a0 = L * (Math.cos(Math.PI / 2 - beta_a0 - alpha));

            double flecha_x2a0 = L * (Math.cos(beta_a0 - alpha));
            double flecha_y2a0 = L * (Math.sin(beta_a0 - alpha));

            g2.draw(new Line2D.Double(X11_a + x00_0, -Y11_a + y00_0, -flecha_x1a0 + x00_0 + X11_a, flecha_y1a0 + y00_0 - Y11_a));
            g2.draw(new Line2D.Double(X11_a + x00_0, -Y11_a + y00_0, -flecha_x2a0 + x00_0 + X11_a, flecha_y2a0 + y00_0 - Y11_a));

            //Punta de flecha
            g2.setColor(Color.green);
            double beta_a1 = Complex.argument(A);
            double flecha_x1a1 = L * (Math.sin(Math.PI / 2 - beta_a1 - alpha));
            double flecha_y1a1 = L * (Math.cos(Math.PI / 2 - beta_a1 - alpha));

            double flecha_x2a1 = L * (Math.cos(beta_a1 - alpha));
            double flecha_y2a1 = L * (Math.sin(beta_a1 - alpha));

            g2.draw(new Line2D.Double(X11_a + x00_0, -Y11_a + y00_0 + 15, -flecha_x1a1 + x00_0 + X11_a, flecha_y1a1 + y00_0 - Y11_a + 15));
            g2.draw(new Line2D.Double(X11_a + x00_0, -Y11_a + y00_0 + 15, -flecha_x2a1 + x00_0 + X11_a, flecha_y2a1 + y00_0 - Y11_a + 15));

            //Punta de flecha
            g2.setColor(Color.red);
            double beta_a2 = Complex.argument(A);
            double flecha_x1a2 = L * (Math.sin(Math.PI / 2 - beta_a2 - alpha));
            double flecha_y1a2 = L * (Math.cos(Math.PI / 2 - beta_a2 - alpha));

            double flecha_x2a2 = L * (Math.cos(beta_a2 - alpha));
            double flecha_y2a2 = L * (Math.sin(beta_a2 - alpha));

            g2.draw(new Line2D.Double(X11_a + x00_0, -Y11_a + y00_0 + 30, -flecha_x1a2 + x00_0 + X11_a, flecha_y1a2 + y00_0 - Y11_a + 30));
            g2.draw(new Line2D.Double(X11_a + x00_0, -Y11_a + y00_0 + 30, -flecha_x2a2 + x00_0 + X11_a, flecha_y2a2 + y00_0 - Y11_a + 30));

            //---------------------------------------------
            //fasor 1
            //Linea del Fasor I1
            g2.setColor(Color.blue);
            g2.draw(new Line2D.Double(x00_1, y00_1, X11_b + x00_1, -Y11_b + y00_1));
            //Circle

            g2.setColor(Color.darkGray);
            double alt_1 = Math.sqrt((X11_b * X11_b) + (Y11_b * Y11_b));
            g2.draw(new Ellipse2D.Double(x00_1 - alt_1, -alt_1 + y00_1, 2 * alt_1, 2 * alt_1));

            //Punta de Flecha
            g2.setColor(Color.blue);
            double beta_b = Complex.argument(B);
            double flecha_x1b = L * (Math.sin(Math.PI / 2 - beta_b - alpha));
            double flecha_y1b = L * (Math.cos(Math.PI / 2 - beta_b - alpha));
            double flecha_x2b = L * (Math.cos(beta_b - alpha));
            double flecha_y2b = L * (Math.sin(beta_b - alpha));
            g2.draw(new Line2D.Double(X11_b + x00_1, -Y11_b + y00_1, -flecha_x1b + x00_1 + X11_b, flecha_y1b + y00_1 - Y11_b));
            g2.draw(new Line2D.Double(X11_b + x00_1, -Y11_b + y00_1, -flecha_x2b + x00_1 + X11_b, flecha_y2b + y00_1 - Y11_b));

            //Linea fasor aaI1
            g2.setColor(Color.green);
            g2.draw(new Line2D.Double(x00_1, y00_1, XB_aa + x00_1, -YB_aa + y00_1));

            //Punta de Flecha
            double beta_aa_B = Complex.argument(aa_B);
            double flecha_x1b_aa_B = L * (Math.sin(Math.PI / 2 - beta_aa_B - alpha));
            double flecha_y1b_aa_B = L * (Math.cos(Math.PI / 2 - beta_aa_B - alpha));

            double flecha_x2b_aa_B = L * (Math.cos(beta_aa_B - alpha));
            double flecha_y2b_aa_B = L * (Math.sin(beta_aa_B - alpha));
            g2.draw(new Line2D.Double(XB_aa + x00_1, -YB_aa + y00_1, -flecha_x1b_aa_B + x00_1 + XB_aa, flecha_y1b_aa_B + y00_1 - YB_aa));
            g2.draw(new Line2D.Double(XB_aa + x00_1, -YB_aa + y00_1, -flecha_x2b_aa_B + x00_1 + XB_aa, flecha_y2b_aa_B + y00_1 - YB_aa));

            //Linea fasor aI1
            g2.setColor(Color.red);
            g2.draw(new Line2D.Double(x00_1, y00_1, XB_a + x00_1, -YB_a + y00_1));

            //Punta de Flecha
            double beta_a_B = Complex.argument(a_B);
            double flecha_x1b_a_B = L * (Math.sin(Math.PI / 2 - beta_a_B - alpha));
            double flecha_y1b_a_B = L * (Math.cos(Math.PI / 2 - beta_a_B - alpha));
            double flecha_x2b_a_B = L * (Math.cos(beta_a_B - alpha));
            double flecha_y2b_a_B = L * (Math.sin(beta_a_B - alpha));
            g2.draw(new Line2D.Double(XB_a + x00_1, -YB_a + y00_1, -flecha_x1b_a_B + x00_1 + XB_a, flecha_y1b_a_B + y00_1 - YB_a));
            g2.draw(new Line2D.Double(XB_a + x00_1, -YB_a + y00_1, -flecha_x2b_a_B + x00_1 + XB_a, flecha_y2b_a_B + y00_1 - YB_a));

            //---------------------------------------------
            //Fasor 2
            //Linea del fasor
            g2.setColor(Color.blue);
            g2.draw(new Line2D.Double(x00_2, y00_2, X11_c + x00_2, -Y11_c + y00_2));

            //Circle
            g2.setColor(Color.darkGray);
            double alt_2 = Math.sqrt((X11_c * X11_c) + (Y11_c * Y11_c));
            g2.draw(new Ellipse2D.Double(x00_2 - alt_2, -alt_2 + y00_2, 2 * alt_2, 2 * alt_2));
            g2.setColor(Color.blue);
            //Punta de Flecha
            double beta_2 = Complex.argument(C);
            double flecha_x1c = L * (Math.sin(Math.PI / 2 - beta_2 - alpha));
            double flecha_y1c = L * (Math.cos(Math.PI / 2 - beta_2 - alpha));

            double flecha_x2c = L * (Math.cos(beta_2 - alpha));
            double flecha_y2c = L * (Math.sin(beta_2 - alpha));

            g2.draw(new Line2D.Double(X11_c + x00_2, -Y11_c + y00_2, -flecha_x1c + x00_2 + X11_c, flecha_y1c + y00_2 - Y11_c));
            g2.draw(new Line2D.Double(X11_c + x00_2, -Y11_c + y00_2, -flecha_x2c + x00_2 + X11_c, flecha_y2c + y00_2 - Y11_c));

            //Linea fasor aI2
            g2.setColor(Color.green);
            g2.draw(new Line2D.Double(x00_2, y00_2, XC_a + x00_2, -YC_a + y00_2));

            //Punta de Flecha
            double beta_a_C = Complex.argument(a_C);
            double flecha_x1b_a_C = L * (Math.sin(Math.PI / 2 - beta_a_C - alpha));
            double flecha_y1b_a_C = L * (Math.cos(Math.PI / 2 - beta_a_C - alpha));

            double flecha_x2b_a_C = L * (Math.cos(beta_a_C - alpha));
            double flecha_y2b_a_C = L * (Math.sin(beta_a_C - alpha));
            g2.draw(new Line2D.Double(XC_a + x00_2, -YC_a + y00_2, -flecha_x1b_a_C + x00_2 + XC_a, flecha_y1b_a_C + y00_2 - YC_a));
            g2.draw(new Line2D.Double(XC_a + x00_2, -YC_a + y00_2, -flecha_x2b_a_C + x00_2 + XC_a, flecha_y2b_a_C + y00_2 - YC_a));

            //Linea fasor aaI2
            g2.setColor(Color.red);
            g2.draw(new Line2D.Double(x00_2, y00_2, XC_aa + x00_2, -YC_aa + y00_2));

            //Punta de Flecha
            double beta_aa_C = Complex.argument(aa_C);
            double flecha_x1b_aa_C = L * (Math.sin(Math.PI / 2 - beta_aa_C - alpha));
            double flecha_y1b_aa_C = L * (Math.cos(Math.PI / 2 - beta_aa_C - alpha));

            double flecha_x2b_aa_C = L * (Math.cos(beta_aa_C - alpha));
            double flecha_y2b_aa_C = L * (Math.sin(beta_aa_C - alpha));
            g2.draw(new Line2D.Double(XC_aa + x00_2, -YC_aa + y00_2, -flecha_x1b_aa_C + x00_2 + XC_aa, flecha_y1b_aa_C + y00_2 - YC_aa));
            g2.draw(new Line2D.Double(XC_aa + x00_2, -YC_aa + y00_2, -flecha_x2b_aa_C + x00_2 + XC_aa, flecha_y2b_aa_C + y00_2 - YC_aa));

            /*
	*  Resultado de la suma de los tres elementos A0+A1+A2
	*	Centro x_res, y_res
             */
            //A0
            g2.setColor(Color.blue);
            g2.draw(new Line2D.Double(x_res, y_res, X11_a + x_res, -Y11_a + y_res));
            //A1
            g2.setColor(Color.blue);
            g2.draw(new Line2D.Double(X11_a + x_res, -Y11_a + y_res, X11_b + X11_a + x_res, -Y11_b - Y11_a + y_res));

            //A2
            g2.setColor(Color.blue);
            g2.draw(new Line2D.Double(X11_b + X11_a + x_res, -Y11_b - Y11_a + y_res, X11_c + X11_b + X11_a + x_res, -Y11_c - Y11_b - Y11_a + y_res));

            //A0+A1+A2
            g2.setColor(Color.cyan);
            g2.draw(new Line2D.Double(x_res, y_res, X11_c + X11_b + X11_a + x_res, -Y11_c - Y11_b - Y11_a + y_res));
            int X_aA = (int) (X11_c + X11_b + X11_a + x_res);
            int Y_aA = (int) (-Y11_c - Y11_b - Y11_a + y_res);
            g2.setColor(Color.black);
            g2.drawString("  A", X_aA, Y_aA);

            //Ejes para la visualizaci�n
            //eje 0
            g2.setColor(Color.black);
            g2.draw(new Line2D.Double(x_res - 200, y_res, x_res + 200, y_res));
            g2.draw(new Line2D.Double(x_res, y_res - 200, x_res, y_res + 200));

            /*
	*  Resultado de la suma de los tres elementos A0+aaA1+aA2
	*	Centro x_res, y_res
             */
            //A0
            g2.setColor(Color.green);
            g2.draw(new Line2D.Double(x_res, y_res, X11_a + x_res, -Y11_a + y_res));

            //aaA1
            g2.setColor(Color.green);
            g2.draw(new Line2D.Double(X11_a + x_res, -Y11_a + y_res, XB_aa + X11_a + x_res, -YB_aa - Y11_a + y_res));

            //aA2
            g2.setColor(Color.green);
            g2.draw(new Line2D.Double(XB_aa + X11_a + x_res, -YB_aa - Y11_a + y_res, XC_a + XB_aa + X11_a + x_res, -YC_a - YB_aa - Y11_a + y_res));
            //A0+aaA1+aA2

            g2.setColor(Color.yellow);
            g2.draw(new Line2D.Double(x_res, y_res, XC_a + XB_aa + X11_a + x_res, -YC_a - YB_aa - Y11_a + y_res));

            int X_aB = (int) (XC_a + XB_aa + X11_a + x_res);
            int Y_aB = (int) (-YC_a - YB_aa - Y11_a + y_res);
            g2.setColor(Color.black);
            g2.drawString("  B", X_aB, Y_aB);

            /*
	*  Resultado de la suma de los tres elementos A0+aA1+aaA2
	*	Centro x_res, y_res
             */
            //A0
            g2.setColor(Color.red);
            g2.draw(new Line2D.Double(x_res, y_res, X11_a + x_res, -Y11_a + y_res));

            //aA1
            g2.setColor(Color.red);
            g2.draw(new Line2D.Double(X11_a + x_res, -Y11_a + y_res, XB_a + X11_a + x_res, -YB_a - Y11_a + y_res));
            //aaA2
            g2.setColor(Color.red);
            g2.draw(new Line2D.Double(XB_a + X11_a + x_res, -YB_a - Y11_a + y_res, XC_aa + XB_a + X11_a + x_res, -YC_aa - YB_a - Y11_a + y_res));
            //A0+aA1+aaA2

            g2.setColor(Color.pink);
            g2.draw(new Line2D.Double(x_res, y_res, XC_aa + XB_a + X11_a + x_res, -YC_aa - YB_a - Y11_a + y_res));

            int X_aC = (int) (XC_aa + XB_a + X11_a + x_res);
            int Y_aC = (int) (-YC_aa - YB_a - Y11_a + y_res);
            g2.setColor(Color.black);
            g2.drawString("  C", X_aC, Y_aC);

            /*
	*  Titulo de los Ejes
	*
             */
            int X_aT = (int) (x_res - 200);
            int Y_aT = (int) (y_res - 250);
            g2.setColor(Color.black);
            g2.drawString("Fasores A, B y C (Resultado de la Suma de sus Componentes Sim�tricas)", X_aT, Y_aT);

            /*
	*Asignaci�n de Nombres
             */
            //Componente Cero
            int X_a0 = (int) (X11_a + x00_0);
            int Y_a0 = (int) (-Y11_a + y00_0);
            g2.setColor(Color.blue);
            g2.drawString("  A\u2080", X_a0, Y_a0);

            int X_a1 = (int) (X11_a + x00_0);
            int Y_a1 = (int) (-Y11_a + y00_0 + 15);
            g2.setColor(Color.green);
            g2.drawString("  A\u2080", X_a1, Y_a1);

            int X_a2 = (int) (X11_a + x00_0);
            int Y_a2 = (int) (-Y11_a + y00_0 + 30);
            g2.setColor(Color.red);
            g2.drawString("  A\u2080", X_a2, Y_a2);

            //Componente 1
            //A1
            int X_b = (int) (X11_b + x00_1);
            int Y_b = (int) (-Y11_b + y00_1);
            g2.setColor(Color.blue);
            g2.drawString("  A\u2081", X_b, Y_b);

            //aA1
            int X_b_aa = (int) (XB_aa + x00_1);
            int Y_b_aa = (int) (-YB_aa + y00_1);
            g2.setColor(Color.green);
            g2.drawString("  a\u00b2 A\u2081", X_b_aa, Y_b_aa);
            //aA1
            int X_b_a = (int) (XB_a + x00_1);
            int Y_b_a = (int) (-YB_a + y00_1);
            g2.setColor(Color.red);
            g2.drawString("  a A\u2081", X_b_a, Y_b_a);

            //Componente 2
            //A2
            int X_c = (int) (X11_c + x00_2);
            int Y_c = (int) (-Y11_c + y00_2);
            g2.setColor(Color.blue);
            g2.drawString("  A\u2082", X_c, Y_c);
            g2.setColor(Color.black);
            //aA2
            int X_c_a = (int) (XC_a + x00_2);
            int Y_c_a = (int) (-YC_a + y00_2);
            g2.setColor(Color.green);
            g2.drawString("  a A\u2082", X_c_a, Y_c_a);
            //aaA2
            int X_c_aa = (int) (XC_aa + x00_2);
            int Y_c_aa = (int) (-YC_aa + y00_2);
            g2.setColor(Color.red);
            g2.drawString("  a\u00b2 A\u2082", X_c_aa, Y_c_aa);

            /*
	*Creaci�n de los ejes
             */
 /*
	//eje 0
	g2.setColor(Color.black);
	g2.draw(new Line2D.Double(-cota+x00_0, y00_0,cota+x00_ab, y00_0));
	g2.draw(new Line2D.Double(x00_0, -cota+y00_0,x00_0,cota+ y00_0));
	//eje 1
	g2.draw(new Line2D.Double(-cota+x00_1, y00_1,cota+x00_1, y00_1));
	g2.draw(new Line2D.Double(x00_1, -cota+y00_1,x00_1,cota+ y00_1));
	//eje 2
	g2.draw(new Line2D.Double(-cota+x00_2, y00_2,cota+x00_2, y00_2));
	g2.draw(new Line2D.Double(x00_2, -cota+y00_2,x00_2,cota+ y00_2));
	
             */
 /*
	*FOR para la creaci�n de la rejilla
             */
 /*
	if (escala<= tolerancia){
	
	return;
	}
	else{
	for (int i=-5;i<=5; i++){
	
	double paso =  max_global*i/5;
	double inc = escala*paso;
	
	//Divisiones de los ejes
	
	//eje 0
	g2.draw(new Line2D.Double(-5+x00_0, y00_0+ inc,5+x00_0, y00_0+ inc ));
	g2.draw(new Line2D.Double(inc + x00_0,-5+y00_0,inc + x00_0,5+ y00_0));
	//eje 1
	g2.draw(new Line2D.Double(-5+x00_1, y00_1+ inc,5+x00_1, y00_1+ inc ));
	g2.draw(new Line2D.Double(inc + x00_1,-5+y00_1,inc + x00_1,5+ y00_1));
	//eje 2
	g2.draw(new Line2D.Double(-5+x00_2, y00_2+ inc,5+x00_2, y00_2+ inc ));
	g2.draw(new Line2D.Double(inc + x00_2,-5+y00_2,inc + x00_2,5+ y00_2));
	
	
	
	}
	}
             */
 /*
	*Label de los ejes
             */
 /*
	g2.setColor(red);
	for (int j = -3; j<3; j++){
	
	double paso_par =  max_global*(2*j+1)/5;
	double inc = escala*paso_par;
	double y_label =Math.round(1000*paso_par);
	double Y_label = y_label/1000;
	String label = String.valueOf(Y_label);
	
	//label fasor 0
	int label_x_y_0 = (int)(-5+x00_0);
	int label_y_y_0 = (int)(y00_0-inc);
	g2.setColor(Color.blue);
	g2.drawString(label,label_x_y_0+7,label_y_y_0);
	int label_x_x_0 = (int)(x00_0-inc);
	int label_y_x_0= (int)(y00_0-5);
	g2.drawString(label,label_x_x_0,label_y_x_0+17);
	
	//label fasor 1
	int label_x_y_1 = (int)(-5+x00_1);
	int label_y_y_1 = (int)(y00_1-inc);
	g2.setColor(Color.blue);
	g2.drawString(label,label_x_y_1+7,label_y_y_1);
	int label_x_x_1 = (int)(x00_1-inc);
	int label_y_x_1= (int)(y00_1-5);
	g2.drawString(label,label_x_x_1,label_y_x_1+17);
	
	//label fasor 2
	int label_x_y_2 = (int)(-5+x00_2);
	int label_y_y_2 = (int)(y00_2-inc);
	g2.setColor(Color.blue);
	g2.drawString(label,label_x_y_2+7,label_y_y_2);
	int label_x_x_2 = (int)(x00_2-inc);
	int label_y_x_2= (int)(y00_2-5);
	g2.drawString(label,label_x_x_2,label_y_x_2+17);
	
	
	
	}
	
             */
 /*
	*Salida de los complejos en forma num�rica en pantalla
             */
 /*
	*Salida A
             */
            g2.setColor(Color.black);
            double Real_A0 = Math.round(10000 * A.re);
            double Real_a0 = Real_A0 / 10000;
            String label_A_re0 = String.valueOf(Real_a0);

            double Imag_A0 = Math.round(10000 * A.im);
            double Imag_a0 = Imag_A0 / 10000;
            String label_A_im0 = String.valueOf(Imag_a0);
            g2.drawString("A\u2080 = " + label_A_re0 + "+j" + label_A_im0, 10, d.height / 10);

            /*
	*Salida B
             */
            double Real_B1 = Math.round(10000 * B.re);
            double Real_b1 = Real_B1 / 10000;
            String label_B_re1 = String.valueOf(Real_b1);

            double Imag_B1 = Math.round(10000 * B.im);
            double Imag_b1 = Imag_B1 / 10000;
            String label_B_im1 = String.valueOf(Imag_b1);
            g2.drawString("A\u2081  = " + label_B_re1 + "+j" + label_B_im1, 10, 10 + d.height / 3);

            /*
	*Salida C
             */
            double Real_C2 = Math.round(10000 * C.re);
            double Real_c2 = Real_C2 / 10000;
            String label_C_re2 = String.valueOf(Real_c2);

            double Imag_C2 = Math.round(10000 * C.im);
            double Imag_c2 = Imag_C2 / 10000;
            String label_C_im2 = String.valueOf(Imag_c2);
            g2.drawString("A\u2082 = " + label_C_re2 + "+j" + label_C_im2, 10, 10 + 2 * d.height / 3);

            /*
	* Salida en pantalla de los graficos de los fasores A, B, C . Componentes Reales
	*
             */
            String label_A_re = String.valueOf(x11_a);
            String label_A_im = String.valueOf(y11_a);
            String label_B_re = String.valueOf(x11_b);
            String label_B_im = String.valueOf(y11_b);
            String label_C_re = String.valueOf(x11_c);
            String label_C_im = String.valueOf(y11_c);

            g2.setColor(Color.blue);
            g2.drawString("A = " + label_A_re + "+j" + label_A_im, 450, 300 + 220);
            g2.setColor(Color.green);
            g2.drawString("B = " + label_B_re + "+j" + label_B_im, 450, 20 + 300 + 220);
            g2.setColor(Color.red);
            g2.drawString("C = " + label_C_re + "+j" + label_C_im, 450, 40 + 300 + 220);

        }
    }

}
