/*
 *  *****************************************************************************
 *  ---- version information ----
 *  
 *  File Name:         LFCFrame.java
 *  Version:           (Check for latest version in SVN repository)
 *  Written by:        Frank Leanez
 *  URL:               www.centroenergia.cl 
 *  Initial release:   August 2018
 *  
 *  
 *  ---- Description ---- 
 *   Display the Nom Lufke load frequency control (LFC) toolbox window
 *   
 *   Copyright 1997-2019
 *   
 *   Rodrigo Palma Behnke 
 *   Universidad de Chile 
 *   Last Modified: (Check for latest version in SVN repository)
 * 
 *   ****************************************************************************
 */

package org.deepedit.addons.nomlufketool.frequencycontrol;

/**
 * Display the Nom Lufke load frequency control (LFC) toolbox window
 * @author Frank Leanez
 */
public class LFCFrame extends javax.swing.JDialog {

    private Op1 op1;
    private Op2 op2;
    
    /**
     * Creates a new Nom Lufke load frequency control (LFC) toolbox window
     *
     * @param parent Owner of this dialog. Use of null is accepted
     * @param modal window modal flag
     */
    public LFCFrame(javax.swing.JDialog parent, boolean modal) {
        super(parent, modal);
        setTitle("Frequency Control Calculator");
        initComponents();
    }

    public static void main(String[] arg) {
        LFCFrame f = new LFCFrame(null, false);
        f.setLocationByPlatform(true);
        f.setVisible(true);
    }

    private void initComponents() {
        op1 = new Op1();
        op2 = new Op2();
        op1.Metodos(op2);
        op2.Metodos(op1);
        getContentPane().setLayout(new java.awt.FlowLayout());
        getContentPane().add(op1);
        getContentPane().add(op2);
        op1.setVisible(true);
        op2.setVisible(false);
        pack();
    }
    
    /**
     * Makes the calculation panel visible and hides all others
     */
    void showCalculationPanel() {
        op1.setVisible(true);
        op2.setVisible(false);
    }
    
    /**
     * Makes the chart panel visible and hides all others
     */
    void showChartPanel() {
        op1.setVisible(false);
        op2.setVisible(true);
    }
    
}
