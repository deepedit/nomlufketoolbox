package org.deepedit.addons.nomlufketool.phasorcalculator;

import java.awt.*;
import java.awt.event.*;   
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JFrame;
import java.net.URL;
import javax.swing.JApplet;
import javax.swing.JComboBox;
import javax.swing.JRadioButton;
import javax.swing.border.TitledBorder;

public class appletInicio extends JApplet implements ActionListener  {
	protected JButton b1, b2;
	public JRadioButton b3,b4,b5;
	String label;
	public Calc2 myCalcFasor;
	private boolean inAnApplet = true;
	public int tipo_idioma, sel;
	protected Font TextDialogFont;   
	JRadioButton esp, eng, ger;
	JPanel pa,pb,pc,pd;
	JComboBox myChoice;
	boolean alter1, alter2; 
	JRadioButton alter1Checkbox;
	JRadioButton alter2Checkbox;
	DibFasor myfasor;
	TextHtml html;
	JPanel contn; //Container

	TitledBorder bordeOperaciones;
	TitledBorder bordeLenguaje;

	// Choice myChoice;
	URL codeBase; //used for applet version only
	
	URL dirURL;


	public appletInicio()  {
		this(true);
	}


	public appletInicio(boolean inAnApplet)  {
		this.inAnApplet = inAnApplet;

		if (inAnApplet)  {
			getRootPane().putClientProperty("defeatSystemEventQueueCheck", Boolean.TRUE);
		}
		init();
	}


	public void init()  {
		contn=makeContentPane();
		setContentPane(contn);
		//getContentPane().add(makeContentPane());
		//pack();

		dirURL=null;
		
		try{	
			System.out.println(dirURL = getCodeBase());
		}
		catch(Exception e)
		{
			java.io.File classFile = new java.io.File("");	// get current directory

			try{
				dirURL = new URL("file:" + classFile.getAbsolutePath());
			}
		
			catch(Exception f){
				System.out.println("Error 1:"+e);
				System.out.println("Error 2:"+f);
			}
		}			
	}


	public JPanel makeContentPane()  {
		BorderLayout border1 = new BorderLayout();	
		JPanel p = new JPanel();
		p.setLayout(border1);
		pa = botones();
		pb = idiomas();
		pc = relleno1();
		pd = relleno2();
		p.add("North",pa);
		p.add("Center",pb);
		p.add("West",pc);
		p.add("East",pd);	
		return p;
	}


	public JPanel relleno1() {
		JPanel p = new JPanel();
		p.setPreferredSize(new Dimension(60,70));
		return p;
	}


	public JPanel relleno2() {
		JPanel p = new JPanel();
		p.setPreferredSize(new Dimension(60,70));
		return p;
	}


	public JPanel botones()  {
		BorderLayout border1 = new BorderLayout();	
		JPanel p = new JPanel();
		GridBagLayout gridbag = new GridBagLayout();  
		GridBagConstraints c = new GridBagConstraints();  
		bordeOperaciones = new TitledBorder(null, "Operaciones",  TitledBorder.LEFT, TitledBorder.TOP, TextDialogFont);    
		
		p.setBorder(bordeOperaciones);
		
		p.setPreferredSize(new Dimension(200,70));
		BorderLayout borderl = new BorderLayout();
		getContentPane().setLayout(borderl);        
		c.fill = GridBagConstraints.VERTICAL;
		p.setLayout(new GridLayout(1,2));
		b1 = new JButton("Calculadora Fasorial");
		b1.setMnemonic(KeyEvent.VK_D);
		b1.setActionCommand("Fasor");
		b1.addActionListener(this);   
		p.add(b1);
		b1.setToolTipText("Presione el boton para activar la calculadora en el lenguaje seleccionado.");
		return p;
	}


	public JPanel idiomas()  {
		JPanel p = new JPanel();
		GridBagLayout gridbag = new GridBagLayout();  
		GridBagConstraints c = new GridBagConstraints();
		bordeLenguaje = new TitledBorder(null, "Lenguaje",  TitledBorder.LEFT, TitledBorder.TOP, TextDialogFont);    
		
		p.setBorder(bordeLenguaje);
		
		p.setPreferredSize(new Dimension(120,70));
		BorderLayout borderl = new BorderLayout();
		getContentPane().setLayout(borderl);        
		c.fill = GridBagConstraints.VERTICAL;
		p.setLayout(new GridLayout(1,1));
		// Create combo box 

		String[] idioma =  {
			"Espa�ol", "English", "Deutsche" 
		};

		myChoice = new JComboBox(idioma);
		myChoice.setPreferredSize(new Dimension(10,10));
		myChoice.addActionListener(this);
		p.add(myChoice);
		return p;
	}


	public void actionPerformed(ActionEvent ev)  {
		Object obj = ev.getSource();  
		String label = ev.getActionCommand();
		sel = myChoice.getSelectedIndex();

		b1.setText(gettitulo());
		//bordeLenguaje:"Idioma" bordeOperaciones:"Operaciones"

		bordeOperaciones=new TitledBorder(null, "",  TitledBorder.LEFT, TitledBorder.TOP, TextDialogFont);
		bordeLenguaje=new TitledBorder(null, "",  TitledBorder.LEFT, TitledBorder.TOP, TextDialogFont);

		switch(sel)
		{
			case 0: //Espa�ol
				bordeOperaciones.setTitle("Operaciones:");
				bordeLenguaje.setTitle("Lenguaje");
				break;
			case 1: //Ingl�s
				bordeOperaciones.setTitle("Operations:");
				bordeLenguaje.setTitle("Language");
				break;
			case 2: //Alem�n
				bordeOperaciones.setTitle("Betriebe:");
				bordeLenguaje.setTitle("Sprache");
				break;
		}


		pa.setBorder(bordeOperaciones);
		pb.setBorder(bordeLenguaje);

		//--------------
		//Si el frame fasor no ha sido creado, se crea.
		//--------------

		if (myCalcFasor == null) {

			if (label.equals("Fasor"))  {
				myCalcFasor= new Calc2(sel, gettitulo(),dirURL);
				myCalcFasor.setVisible(true);
			}

			return;
		}

		//--------------
		//Si el frame fasor existe, y esta en el mismo idioma, se activa, si no se crea uno nuevo.
		//--------------	

		else// if (myCalcFasor != null) 
                {
			int opcion =  myCalcFasor.clave;

			if (label.equals("Fasor")) {

				if(opcion == sel) {
					myCalcFasor.setVisible(true);
				}


				else //if (opcion != sel) 
                                {
					myCalcFasor.dispose();
					myCalcFasor = new Calc2(sel, gettitulo(),dirURL);
					myCalcFasor.setVisible(true);
				}

			}
			if (label.equals("cerrar")) {
				myCalcFasor.setVisible(false);
				myfasor.setVisible(false);
				html.setVisible(false);
			}

		}

	}


	protected URL getURL(String filename)  {
		URL url = null;

		if (codeBase == null)  {
			codeBase = getCodeBase();
		}


		try  {
			url = new URL(codeBase, filename);
		}


		catch (java.net.MalformedURLException e)  {
			return null;
		}

		return url;
	}


	public static void main(String[] args)  {
		JFrame frame = new JFrame("Calculadora Fasorial");

		frame.addWindowListener(new WindowAdapter()  {
			public void windowClosing(WindowEvent e)  {
				System.exit(0);
			}
		}
		);
		appletInicio applet = new appletInicio(false);
		frame.setContentPane(applet.makeContentPane());
		frame.pack();
		frame.setVisible(true);
	}


	public String gettitulo()  {

		if(sel == 0) {
			String titulo = "Calculadora Fasorial";
			return titulo;
		}

		else if (sel == 1) {
			String titulo = "Fasor Calculator";
			return titulo;	
		}

		else  {
			String titulo = "Fasor Rechner";
			return titulo;	
		}
	}
}