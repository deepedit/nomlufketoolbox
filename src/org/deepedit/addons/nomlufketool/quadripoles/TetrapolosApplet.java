package org.deepedit.addons.nomlufketool.quadripoles;

public class TetrapolosApplet extends javax.swing.JApplet {
    
    public TetrapolosApplet() {
        TetrapolosCalc TC = new TetrapolosCalc();
        Ayuda33 ayuda = new Ayuda33(TC);
        
        TC.Metodos(ayuda);
//        ayuda.PanelTop.Metodos(TC,ayuda);
        
        getContentPane().setLayout(new java.awt.FlowLayout());
        getContentPane().add(TC);
        getContentPane().add(ayuda);
        
        TC.setVisible(true);
        ayuda.setVisible(false);
        repaint();
        
    }
}