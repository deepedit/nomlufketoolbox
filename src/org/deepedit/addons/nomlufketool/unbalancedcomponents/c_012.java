package org.deepedit.addons.nomlufketool.unbalancedcomponents;

// @(#)CalcFasor.java
// @author 	Nolberto Emilio Oyarce Seguin
// @version 	1.0 final a, 5 de Noviembre 2001


import javax.swing.border.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;   
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JFrame;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import org.deepedit.addons.nomlufketool.utils.Complex;

//
//  012 class
//
	
public class c_012  extends JFrame implements KeyListener, ActionListener{
    BufferedReader  din = new BufferedReader(new InputStreamReader(System.in));
    protected Font TextDialogFont;   
    protected FontMetrics TextDialogFontMetrics;  
    Color   lightBlue = new Color(99,162,247);
    PrintStream myPrintStream;
    TextHtml html;

    //VARIABLES DE:     Paneles
  
    public JTextField TempTextField;
    public JTextField MyTextField1 = null;   
    public JTextField MyTextField2 = null;   
    public JTextField MyTextField3 = null;   
    public JTextField MyTextField4 = null;  
    public JTextField MyTextField5 = null;   
    public JTextField MyTextField6 = null;   
    public JTextField MyTextField7 = null;   
    public JTextField MyTextField8 = null; 
    public JTextField MyTextField9 = null;   
    public JTextField MyTextField10 = null; 
    public JTextField MyTextField11 = null;   
    public JTextField MyTextField12 = null; 
    public JTextField MyTextField13 = null;   
    public JTextField MyTextField14 = null; 
    public JTextField MyTextField15 = null; 
    public JTextField MyTextField16 = null;   
    public JTextField MyTextField17 = null; 
    public JTextField MyTextField18 = null;   
    public JTextField MyTextField19 = null; 
    public JTextField MyTextField20 = null;   
    public JTextField MyTextField21 = null; 
    public JTextField MyTextField22 = null; 
    public JTextField MyTextField23 = null;   
    public JTextField MyTextField24 = null; 

    String index;
    
    //
    //Botones para la acci�n sobre los fasores
    //
    //
    
    String label;
    double abs_a, abs_b, arg_a, arg_b, real_a, real_b, img_a, img_b, absolute_A,argument_A,absolute_B,argument_B, p_real_A,p_img_A,p_real_B,p_img_B ;
    

    //Seteo de la cantidad de decimales
    //
    //decimal indica la contidad de decimales
    //
    
    double presicion = 0.0000001;
    double decimal = Math.round(1/presicion);

    
    //
    //Texto de los paneles en m�ltiples idiomas (Espa�ol, Ing��s y Alem�n)

    String text[];  
    String espanol[] = {
	//0-1
	"Componentes Sim�tricas","Datos de las Componentes A0, A1, A2",
	//2-7
	"Modulo A0: ", "Modulo A1: ", "Modulo A2: ","Angulo A0: ","Angulo A1: ","Angulo A2: ",
	//8-13
	"Real A0: ","Real A1: ","Real A2: ","Imag. A0: ","Imag. A1: ","Imag. A2: ",
	//14-19
	"Modulo A: ","Modulo B: ","Modulo C: ","Angulo A: ","Angulo B: ","Angulo C: ",
	//20-25
	"Real A: ","Real B: ","Real C: ","Imag. A: ","Imag. B: ","Imag. C: ",
	//26
	"Componentes ABC",
	//27-32
	"Acciones Generales","Calcular","Graficar", "Reset", "Salir","Ayuda",
	//33
	"http://146.83.6.6/bdmc/software_mm/comp_sim/index_esp.html"};
    String english[] = {
	//0-1
	"Symmetrical Components","Data Components A0, A1, A2",
	//2-7
	"Module A0: ", "Module A1: ", "Module A2: ","Angle A0: ","Angle A1: ","Angle A2: ",
	//8-13
	"Real A0: ","Real A1: ","Real A2: ","Imag. A0: ","Imag. A1: ","Imag. A2: ",
	//14-19
	"Module A: ","Module B: ","Module C: ","Angle A: ","Angle B: ","Angle C: ",
	//20-25
	"Real A: ","Real B: ","Real C: ","Imag. A: ","Imag. B: ","Imag. C: ",
	//26
	"Components ABC",
	//27-32
	"General Actions","Calculate","Graphic", "Reset", "Exit","Help",
	//33
	"http://146.83.6.6/bdmc/software_mm/comp_sim/index_esp.html"};
    String german[] = {
//0-1
	"Componentes Sim�tricas","Datos de las Componentes A0, A1, A2",
	//2-7
	"Modulo A0: ", "Modulo A1: ", "Modulo A2: ","Angulo A0: ","Angulo A1: ","Angulo A2: ",
	//8-13
	"Real A0: ","Real A1: ","Real A2: ","Imag. A0: ","Imag. A1: ","Imag. A2: ",
	//14-19
	"Modulo A: ","Modulo B: ","Modulo C: ","Angulo A: ","Angulo B: ","Angulo C: ",
	//20-25
	"Real A: ","Real B: ","Real C: ","Imag. A: ","Imag. B: ","Imag. C: ",
	//26
	"Componentes ABC",
	//27-32
	"Acciones Generales","Calcular","Graficar", "Reset", "Salir","Ayuda",
	//33
	"http://146.83.6.6/bdmc/software_mm/comp_sim/index_esp.html"};



    JLabel MyJLabel1 = null;
    JComboBox Metodo = null;  
    JPanel    p1, p1a, p2, p1aa, p1ab, p1ac,p1ad,p1aba,p1abb, p1abc,p1ccc, opvar, mono, ops, p1abba, p1abbb;
    LAFPanel  p3;
    public JButton Suma=null;
    public JRadioButton atrib_var[];
    public String nameJRadioButton[];
    JButton correr=null;
    JButton graficar=null;
    JButton guardar=null;
    JButton estadistica=null;
    JButton salir=null;
    JButton crearBD=null;
    JButton ayuda=null;
    JButton graf=null;
    JButton reset=null;
    JButton sal=null;
    JButton cal=null;
    JRadioButton cero = null;
    JRadioButton abc = null;

    public int selec;
    
    public JButton suma2,mult,division,grafic,salir2, transa_b, transc_a, transc_b;

    //JRadioButton Io,I1,I2,b_menos;
    int ancho=330;
    int largo;
    public int varConAt = 0; // N�mero de variables con N� de atributos distinto de cero

    // VARIABLES DE     Proceso Matem�tico


    Complex A_a =  new Complex((double)0,(double)0);
    Complex B_b =  new Complex((double)0,(double)0);
    Complex C_c =  new Complex((double)0,(double)0);
    Complex A =  new Complex((double)0,(double)0);
    Complex B =  new Complex((double)0,(double)0);
    Complex C =  new Complex((double)0,(double)0);
    Complex un =  new Complex((double)0,(double)0);
    Complex a =  new Complex((double)0,(double)0);
    Complex aa =  new Complex((double)0,(double)0);
    Complex fact =  new Complex((double)0,(double)0);
    Complex A_B =  new Complex((double)0,(double)0);
    Complex A_B_C =  new Complex((double)0,(double)0);
    Complex a_B =  new Complex((double)0,(double)0);
    Complex aa_C =  new Complex((double)0,(double)0);
    Complex a_B_aa_C =  new Complex((double)0,(double)0);
    Complex A_a_B_aa_C =  new Complex((double)0,(double)0);
    Complex aa_B =  new Complex((double)0,(double)0);
    Complex a_C =  new Complex((double)0,(double)0);
    Complex aa_B_a_C =  new Complex((double)0,(double)0);
    Complex A_aa_B_a_C =  new Complex((double)0,(double)0);



    Complex I0 =  new Complex((double)0,(double)0);
    Complex I1 =  new Complex((double)0,(double)0);
    Complex I2 =  new Complex((double)0,(double)0);


    //Seteo del sistema grafico
    
    public FasorABC myfasorABC;
    public int clave;

    //
    //    Constructor
    //



    public c_012(int idioma, String Titulo) {
	super(Titulo);
        setModalExclusionType(Dialog.ModalExclusionType.APPLICATION_EXCLUDE);
	clave = idioma; 
	if (idioma == 0){
	    text = espanol;
	}
	else if (idioma == 1){
	    text = english;
	}
	else {
	    text = german;
	}
	setBackground(Color.lightGray);
	TextDialogFont = new Font("TimesRoman",Font.PLAIN, 12);
	TextDialogFontMetrics = getFontMetrics(TextDialogFont);
	setFont(TextDialogFont);
	setLocation(150,100);
		
	//
	// Crea el LAFPanel         (S)
	// Panel principal panel1() (C)
	//

	BorderLayout borderl = new BorderLayout();
	getContentPane().setLayout(borderl);
	p1 = panel1();
	p2 = new LAFPanel(this, false);
	getContentPane().add("Center",p1);
	getContentPane().add("South",p2);// Panel "Metal" "Motif" "Windows"
	show();
	pack();
	repaint();
	
 
    }

    //
    // Principal
    //
/*
    public static void main(String s[]) {
	CalcFasor mycalcfasor = new CalcFasor(1, "");
	mycalcfasor.addWindowListener(new WindowAdapter() {
		public void windowClosing(WindowEvent e) {System.exit(0);}
	    });
	mycalcfasor.setVisible(true);
    }
*/
    //
    // Panel de seteo() (O)
    //

    public JPanel panel1() {
	BorderLayout borderl = new BorderLayout();
	JPanel p = new JPanel();
	p.setLayout(borderl);
	p1a = seteo();
	p.add("West",p1a);
	return p;
    }

    
    public JPanel seteo() {
	BorderLayout border1 = new BorderLayout();	
	JPanel p = new JPanel();
	p.setLayout(border1);
	p1ab = opvar();
	p1ad = runPane();
	p.add("Center",p1ab);
	p.add("South",p1ad);
	return p;
    }


    
    
    





    //
    // Panel de opciones() 
    //
    public JPanel opvar() {
	BorderLayout border1 = new BorderLayout();	
	JPanel p = new JPanel();
	p.setLayout(border1);
	p1aba = opciones();
	//p1aba.setPreferredSize(new Dimension(500,150));
	p1abc = Resultados();
	//p1ccc = Operaciones();
	//p1ccc.setPreferredSize(new Dimension(350,170));
	p.add("North",p1aba);
	//p.add("Center",p1ccc);
	p.add("South",p1abc);
	return p;
    }
    
    //
    // Panel de Resultados() de la operaciones suma o multiplicaci�n  
    //
    
    public JPanel Operaciones(){
	JPanel p = new JPanel();
	GridBagLayout gridbag = new GridBagLayout();  
	GridBagConstraints c = new GridBagConstraints();  
	Border buttonBorder = new TitledBorder(null, text[10], 
					       TitledBorder.LEFT, TitledBorder.TOP,
					       TextDialogFont);    
	Border emptyBorder = new EmptyBorder(5,5,5,5);
	Border compoundBorder = new CompoundBorder( buttonBorder, emptyBorder);
	p.setBorder(compoundBorder);
	BorderLayout borderl = new BorderLayout();
	getContentPane().setLayout(borderl);        
	c.fill = GridBagConstraints.VERTICAL;
	p.setLayout(new GridLayout(5,1));
	

	abc = Utils.makeJRadioButton(p,"abc", gridbag, c);
	abc.setActionCommand("abc"); 
	abc.addActionListener(this);   


	cero = Utils.makeJRadioButton(p,"012", gridbag, c);
	cero.setActionCommand("cero"); 
	cero.addActionListener(this);   


	return p;
    }
    
    //
    // Panel para setear  
    //

    public JPanel opciones(){
	JPanel p = new JPanel();
	GridBagLayout gridbag = new GridBagLayout();  
	GridBagConstraints c = new GridBagConstraints();  
	Border buttonBorder = new TitledBorder(null, text[1], 
					       TitledBorder.LEFT, TitledBorder.TOP,
					       TextDialogFont);    
	Border emptyBorder = new EmptyBorder(5,5,5,5);
	Border compoundBorder = new CompoundBorder( buttonBorder, emptyBorder);	
	p.setBorder(compoundBorder);
	p.setPreferredSize(new Dimension(500,150));
	BorderLayout borderl = new BorderLayout();
	getContentPane().setLayout(borderl);        
	//	p.setLayout(gridbag);  
	c.fill = GridBagConstraints.VERTICAL;
	p.setLayout(new GridLayout(4,1));

	//----	
	//c.gridwidth = 2; 
	//c.gridheight = 1;
        Utils.makeJLabel(p,text[2], gridbag, c, Color.blue); 	
        c.gridwidth = GridBagConstraints.REMAINDER;
	MyTextField1 = Utils.makeJTField(p,"1", gridbag, c, 7 );
	MyTextField1.addActionListener(this);   
	MyTextField1.setActionCommand("moduloA"); 
	MyTextField1.addMouseListener(new MyMouseListener()); 
	MyTextField1.setEnabled(true);

	//----
	
	double auxa = 180*Complex.argument(A)/Math.PI;
	Utils.makeJLabel(p,text[3], gridbag, c, Color.blue); 
	c.gridwidth = GridBagConstraints.REMAINDER;
	MyTextField2 = Utils.makeJTField(p,"45", gridbag, c, 7 );
	MyTextField2.addActionListener(this);   
	MyTextField1.setActionCommand("anguloA"); 
	MyTextField2.addMouseListener(new MyMouseListener()); 
	MyTextField2.setEnabled(true);
	
//---
        Utils.makeJLabel(p,text[4], gridbag, c, Color.blue); 	
        c.gridwidth = GridBagConstraints.REMAINDER;
	MyTextField3 = Utils.makeJTField(p,"1", gridbag, c, 7 );
	MyTextField3.addActionListener(this);   
	MyTextField3.setActionCommand("moduloB"); 
	MyTextField3.addMouseListener(new MyMouseListener()); 
	MyTextField3.setEnabled(true);

	//----
	
	double auxb = 180*Complex.argument(B)/Math.PI;
	Utils.makeJLabel(p,text[5], gridbag, c, Color.blue); 
	c.gridwidth = GridBagConstraints.REMAINDER;
	MyTextField4 = Utils.makeJTField(p,"95", gridbag, c, 7 );
	MyTextField4.addActionListener(this);   
	MyTextField4.setActionCommand("anguloB"); 
	MyTextField4.addMouseListener(new MyMouseListener()); 
	MyTextField4.setEnabled(true);
	
//----	
	
        Utils.makeJLabel(p,text[6], gridbag, c, Color.blue); 	
        c.gridwidth = GridBagConstraints.REMAINDER;
	MyTextField5 = Utils.makeJTField(p,"0.5", gridbag, c, 7 );
	MyTextField5.addActionListener(this);   
	MyTextField5.setActionCommand("moduloC"); 
	MyTextField5.addMouseListener(new MyMouseListener()); 
	MyTextField5.setEnabled(true);

	//----
	
	double auxc = 180*Complex.argument(C)/Math.PI;
	Utils.makeJLabel(p,text[7], gridbag, c, Color.blue); 
	c.gridwidth = GridBagConstraints.REMAINDER;
	MyTextField6 = Utils.makeJTField(p,"-20", gridbag, c, 7 );
	MyTextField6.addActionListener(this);   
	MyTextField6.setActionCommand("anguloC"); 
	MyTextField6.addMouseListener(new MyMouseListener()); 
	MyTextField6.setEnabled(true);
	
	//----

        Utils.makeJLabel(p,text[8], gridbag, c, Color.blue); 	
        c.gridwidth = GridBagConstraints.REMAINDER;
	MyTextField7 = Utils.makeJTField(p,"0.7071067", gridbag, c, 7 );
	MyTextField7.addActionListener(this);   
	MyTextField7.setActionCommand("moduloA"); 
	MyTextField7.addMouseListener(new MyMouseListener()); 
	MyTextField7.setEnabled(true);

	//----
	
	double auxd = 180*Complex.argument(A)/Math.PI;
	Utils.makeJLabel(p,text[9], gridbag, c, Color.blue); 
	c.gridwidth = GridBagConstraints.REMAINDER;
	MyTextField8 = Utils.makeJTField(p,"0.7071067", gridbag, c, 7 );
	MyTextField8.addActionListener(this);   
	MyTextField8.setActionCommand("anguloA"); 
	MyTextField8.addMouseListener(new MyMouseListener()); 
	MyTextField8.setEnabled(true);
	
//----	
	
        Utils.makeJLabel(p,text[10], gridbag, c, Color.blue); 	
        c.gridwidth = GridBagConstraints.REMAINDER;
	MyTextField9 = Utils.makeJTField(p,"-0.00871", gridbag, c, 7 );
	MyTextField9.addActionListener(this);   
	MyTextField9.setActionCommand("moduloB"); 
	MyTextField9.addMouseListener(new MyMouseListener()); 
	MyTextField9.setEnabled(true);

	//----
	
	double auxe = 180*Complex.argument(B)/Math.PI;
	Utils.makeJLabel(p,text[11], gridbag, c, Color.blue); 
	c.gridwidth = GridBagConstraints.REMAINDER;
	MyTextField10 = Utils.makeJTField(p,"0.996194", gridbag, c, 7 );
	MyTextField10.addActionListener(this);   
	MyTextField10.setActionCommand("anguloB"); 
	MyTextField10.addMouseListener(new MyMouseListener()); 
	MyTextField10.setEnabled(true);
	
//----	
	
        Utils.makeJLabel(p,text[12], gridbag, c, Color.blue); 	
        c.gridwidth = GridBagConstraints.REMAINDER;
	MyTextField11 = Utils.makeJTField(p,"0.469846", gridbag, c, 7 );
	MyTextField11.addActionListener(this);   
	MyTextField11.setActionCommand("moduloC"); 
	MyTextField11.addMouseListener(new MyMouseListener()); 
	MyTextField11.setEnabled(true);

	//----
	
	double auxf = 180*Complex.argument(C)/Math.PI;
	Utils.makeJLabel(p,text[13], gridbag, c, Color.blue); 
	c.gridwidth = GridBagConstraints.REMAINDER;
	MyTextField12 = Utils.makeJTField(p,"-0.1710", gridbag, c, 7 );
	MyTextField12.addActionListener(this);   
	MyTextField12.setActionCommand("anguloC"); 
	MyTextField12.addMouseListener(new MyMouseListener()); 
	MyTextField12.setEnabled(true);
	
	//----

	
	return p;
	
    }

    //
    //Panel de Resultados, Componentes Sim�tricas
    //
    
    public JPanel Resultados(){
	JPanel p = new JPanel();
	GridBagLayout gridbag = new GridBagLayout();  
	GridBagConstraints c = new GridBagConstraints();  
	Border buttonBorder = new TitledBorder(null,text[26], 
					       TitledBorder.LEFT, TitledBorder.TOP,
					       TextDialogFont);    
	Border emptyBorder = new EmptyBorder(5,5,5,5);
	Border compoundBorder = new CompoundBorder( buttonBorder, emptyBorder);
	p.setBorder(compoundBorder);
	BorderLayout borderl = new BorderLayout();
	getContentPane().setLayout(borderl);        
	//	p.setLayout(gridbag);  
	c.fill = GridBagConstraints.VERTICAL;

	p.setLayout(new GridLayout(4,2));

	//----	
	c.gridwidth = 2; 
	c.gridheight = 1;
        Utils.makeJLabel(p,text[14], gridbag, c, Color.blue); 	
        c.gridwidth = GridBagConstraints.REMAINDER;
	MyTextField13 = Utils.makeJTField(p,"0", gridbag, c, 1);
	MyTextField13.setEnabled(false);


	//----	
	c.gridwidth = 2; 
	c.gridheight = 1;
        Utils.makeJLabel(p,text[15], gridbag, c, Color.blue); 	
        c.gridwidth = GridBagConstraints.REMAINDER;
	MyTextField14= Utils.makeJTField(p,"0", gridbag, c, 1 );
	MyTextField14.setEnabled(false);


	//----	
	c.gridwidth = 2; 
	c.gridheight = 1;
        Utils.makeJLabel(p,text[16], gridbag, c, Color.blue); 	
        c.gridwidth = GridBagConstraints.REMAINDER;
	MyTextField15= Utils.makeJTField(p,"0", gridbag, c, 1);
	MyTextField15.setEnabled(false);


	//----	
	c.gridwidth = 2; 
	c.gridheight = 1;
        Utils.makeJLabel(p,text[17], gridbag, c, Color.blue); 	
        c.gridwidth = GridBagConstraints.REMAINDER;
	MyTextField16 = Utils.makeJTField(p,"0", gridbag, c, 1 );
	MyTextField16.setEnabled(false);

	//----1
	c.gridwidth = 2;
	c.gridheight = 1;  
	Utils.makeJLabel(p,text[18], gridbag, c, Color.blue); 
	c.gridwidth = GridBagConstraints.REMAINDER;
	MyTextField17 = Utils.makeJTField(p,"0", gridbag, c, 1);
	MyTextField17.setEnabled(false);

	//----
	c.gridwidth = 2; 
	c.gridheight = 1;
        Utils.makeJLabel(p,text[19], gridbag, c, Color.blue); 	
        c.gridwidth = GridBagConstraints.REMAINDER;
	MyTextField18 = Utils.makeJTField(p,"0", gridbag, c, 1 );
	MyTextField18.setEnabled(false);


//----	
	c.gridwidth = 2; 
	c.gridheight = 1;
        Utils.makeJLabel(p,text[20], gridbag, c, Color.blue); 	
        c.gridwidth = GridBagConstraints.REMAINDER;
	MyTextField19 = Utils.makeJTField(p,"0", gridbag, c, 1);
	MyTextField19.setEnabled(false);


	//----	
	c.gridwidth = 2; 
	c.gridheight = 1;
        Utils.makeJLabel(p,text[21], gridbag, c, Color.blue); 	
        c.gridwidth = GridBagConstraints.REMAINDER;
	MyTextField20 = Utils.makeJTField(p,"0", gridbag, c, 1 );
	MyTextField20.setEnabled(false);


	//----	
	c.gridwidth = 2; 
	c.gridheight = 1;
        Utils.makeJLabel(p,text[22], gridbag, c, Color.blue); 	
        c.gridwidth = GridBagConstraints.REMAINDER;
	MyTextField21 = Utils.makeJTField(p,"0", gridbag, c, 1);
	MyTextField21.setEnabled(false);


	//----	
	c.gridwidth = 2; 
	c.gridheight = 1;
        Utils.makeJLabel(p,text[23], gridbag, c, Color.blue); 	
        c.gridwidth = GridBagConstraints.REMAINDER;
	MyTextField22 = Utils.makeJTField(p,"0", gridbag, c, 1 );
	MyTextField22.setEnabled(false);

	//----
	c.gridwidth = 2;
	c.gridheight = 1;  
	Utils.makeJLabel(p,text[24], gridbag, c, Color.blue); 
	c.gridwidth = GridBagConstraints.REMAINDER;
	MyTextField23 = Utils.makeJTField(p,"0", gridbag, c, 1);
	MyTextField23.setEnabled(false);

	//----
	c.gridwidth = 2; 
	c.gridheight = 1;
        Utils.makeJLabel(p,text[25], gridbag, c, Color.blue); 	
        c.gridwidth = GridBagConstraints.REMAINDER;
	MyTextField24 = Utils.makeJTField(p,"0", gridbag, c, 1 );
	MyTextField24.setEnabled(false);



	return p;
    }

    //	
    // Opciones para la calculadora de fasores
    //
    
    public  JPanel runPane() {
	JPanel p = new JPanel();
	GridBagLayout gridbag = new GridBagLayout();  
	GridBagConstraints c = new GridBagConstraints();  
	Border buttonBorder = new TitledBorder(null, text[27], 
					       TitledBorder.LEFT, TitledBorder.TOP,
					       TextDialogFont);    
	Border emptyBorder = new EmptyBorder(5,5,5,5);
	Border compoundBorder = new CompoundBorder( buttonBorder, emptyBorder);
	p.setBorder(compoundBorder);
	BorderLayout borderl = new BorderLayout();
	getContentPane().setLayout(borderl);        
	p.setLayout(gridbag);  
	c.fill = GridBagConstraints.VERTICAL;
	
	c.gridwidth = 3; 
	c.gridheight = 1;

	cal = Utils.makeJButton(p,text[28], gridbag, c);
	cal.setActionCommand("cal"); 
	cal.addActionListener(this);   


	graf = Utils.makeJButton(p,text[29], gridbag, c);
	graf.setActionCommand("Graficar"); 
	graf.addActionListener(this);   

	reset = Utils.makeJButton(p,text[30], gridbag, c);
	reset.setActionCommand("reset"); 
	reset.addActionListener(this); 
	
	sal = Utils.makeJButton(p,text[31], gridbag, c);
	sal.setActionCommand("salir"); 
	sal.addActionListener(this); 
	c.gridwidth = GridBagConstraints.REMAINDER;
	
	ayuda = Utils.makeJButton(p,text[32], gridbag, c);
	ayuda.addActionListener(this); 
	ayuda.setActionCommand("Ayuda"); 
	
	return p;
    } 


    //
    // Repinta los paneles
    //

    public void repaint() {
	p1.repaint();
	p2.repaint();
	p1a.repaint();
    } 

    //
    // Handle events of this text dialog   
    //

    public void keyPressed(KeyEvent event){   
	if (event.getKeyCode() == KeyEvent.VK_ESCAPE) {   
	    setVisible(false);   
	    System.exit(0);
	}   
    }   
    public void keyReleased(KeyEvent event){   
    }   
    public void keyTyped(KeyEvent event){   
	
    }   

    //
    // Action Performed
    //


    public void actionPerformed(ActionEvent ev){  
	Object obj = ev.getSource();  
	String label = ev.getActionCommand();
	int sel = 0;

	if (label.equals("salir")) {
	  
	    setVisible(false);
	    if (html != null){

	    html.setVisible(false);
	    }
	    if (myfasorABC != null){

	    myfasorABC.setVisible(false);
	    }
	}

	if (label.equals("Ayuda")) {
            html = new TextHtml(this, text[33]); 
	    html.pack();
	    html.setVisible(true);
	}

	if(label.equals("reset")) {
    
	    double aux1 = 0.0;
	    MyTextField1.setText(String.valueOf(aux1));
	    MyTextField2.setText(String.valueOf(aux1));
	    MyTextField3.setText(String.valueOf(aux1));
	    MyTextField4.setText(String.valueOf(aux1));
	    MyTextField5.setText(String.valueOf(aux1));
	    MyTextField6.setText(String.valueOf(aux1));
	    MyTextField7.setText(String.valueOf(aux1));
	    MyTextField8.setText(String.valueOf(aux1));
	    MyTextField9.setText(String.valueOf(aux1));
	    MyTextField10.setText(String.valueOf(aux1));
	    MyTextField11.setText(String.valueOf(aux1));
	    MyTextField12.setText(String.valueOf(aux1));
	    MyTextField13.setText(String.valueOf(aux1));
	    MyTextField14.setText(String.valueOf(aux1));
	    MyTextField15.setText(String.valueOf(aux1));
	    MyTextField16.setText(String.valueOf(aux1));
	    
	    MyTextField17.setText(String.valueOf(aux1));
	    MyTextField18.setText(String.valueOf(aux1));
	    MyTextField19.setText(String.valueOf(aux1));
	    MyTextField20.setText(String.valueOf(aux1));
	    MyTextField21.setText(String.valueOf(aux1));
	    MyTextField22.setText(String.valueOf(aux1));
	    MyTextField23.setText(String.valueOf(aux1));
	    MyTextField24.setText(String.valueOf(aux1));

   	}




	//Seteo del boton Graficar

	

	if (label.equals("Graficar")) {
	    if (myfasorABC != null){
		myfasorABC.dispose();
	  
	    //Creaci�n de los complejos para hacer el grafico de cada uno
	    //Creaci�n complejo A

	    double aux1=Double.valueOf(MyTextField19.getText()).doubleValue();
	    double aux2=Double.valueOf(MyTextField22.getText()).doubleValue();
	   
	    A =  new Complex(aux1,aux2);
	    //Creacion complejo B

	    double aux3=Double.valueOf(MyTextField20.getText()).doubleValue();
	    double aux4=Double.valueOf(MyTextField23.getText()).doubleValue();
	   
	    B =  new Complex(aux3,aux4);

	    double aux5=Double.valueOf(MyTextField21.getText()).doubleValue();
	    double aux6=Double.valueOf(MyTextField24.getText()).doubleValue();
	
	    C =  new Complex(aux5,aux6);
	     
	    int fasores =3;

	    myfasorABC= new FasorABC(this, fasores, A.re, A.im, B.re, B.im, C.re, C.im);
	    }
	    else {
 //Creaci�n de los complejos para hacer el grafico de cada uno
	    //Creaci�n complejo A

	    double aux1=Double.valueOf(MyTextField19.getText()).doubleValue();
	    double aux2=Double.valueOf(MyTextField22.getText()).doubleValue();
	   
	    A =  new Complex(aux1,aux2);
	    //Creacion complejo B

	    double aux3=Double.valueOf(MyTextField20.getText()).doubleValue();
	    double aux4=Double.valueOf(MyTextField23.getText()).doubleValue();
	   
	    B =  new Complex(aux3,aux4);

	    double aux5=Double.valueOf(MyTextField21.getText()).doubleValue();
	    double aux6=Double.valueOf(MyTextField24.getText()).doubleValue();
	
	    C =  new Complex(aux5,aux6);
	     
	    int fasores =3;

	    myfasorABC= new FasorABC(this, fasores, A.re, A.im, B.re, B.im, C.re, C.im);
	    }
	}
	
	if(label.equals("cal")) {
	    
	    A_B = null;
	    A_B_C = null; 
	    a_B = null; 
	    aa_C = null; 
	    a_B_aa_C =  null;
	    A_a_B_aa_C = null;
	    aa_B =  null;
	    a_C =  null;
	    aa_B_a_C =  null;
	    A_aa_B_a_C =  null;
	    I0 =  null;
	    I1 =null;
	    I2 = null;

	    //Componente A
	    double aux1=Double.valueOf(MyTextField1.getText()).doubleValue();
	    double aux2=Double.valueOf(MyTextField4.getText()).doubleValue();
	    double aux22 = aux2*Math.PI/180;

	    //Componente B
	    double aux3=Double.valueOf(MyTextField2.getText()).doubleValue();
	    double aux4=Double.valueOf(MyTextField5.getText()).doubleValue();
	    double aux44 = aux4*Math.PI/180;
	    	    
	    //Componente C
	    double aux5=Double.valueOf(MyTextField3.getText()).doubleValue();
	    double aux6=Double.valueOf(MyTextField6.getText()).doubleValue();
	    double aux66 = aux6*Math.PI/180;


	    // Construccion de las componentes
	    A_a = null;
	    A_a =  new Complex(aux1*Math.cos(aux22),aux1*Math.sin(aux22));
	    B_b = null;
	    B_b =  new Complex(aux3*Math.cos(aux44),aux3*Math.sin(aux44));
	    C_c = null;	    
	    C_c =  new Complex(aux5*Math.cos(aux66),aux5*Math.sin(aux66));
	    
	    //Variables a y aa
	    un = null;
	    un =  new Complex((double)1,(double)0);

	    double a_im = (Math.sqrt(3))/2;
	    a = null;
	    a =  new Complex((double)(-0.5),a_im);

	    aa = null; 
	    aa =  new Complex((double)(-0.5),-a_im);
double uno = 1;
double tres = 3;
	    double fac = uno/tres;

	    fact = null;
	    fact = new Complex (fac,(double)0);


	//Creacion de las componentes
	
	//Cero
	
	A_B = Complex.add(A_a,B_b);
	A_B_C = Complex.add(A_B,C_c);
	I0 = A_B_C;

	//1
	
	aa_B = Complex.multiply(aa, B_b);
	a_C = Complex.multiply(a,C_c);
	aa_B_a_C = Complex.add(aa_B, a_C);
	A_aa_B_a_C = Complex.add(A_a,aa_B_a_C);
	I1 = A_aa_B_a_C;
	
	//2
	
	a_B = Complex.multiply(a, B_b);
	aa_C = Complex.multiply(aa,C_c);
	a_B_aa_C = Complex.add(a_B, aa_C);
	A_a_B_aa_C = Complex.add(A_a,a_B_aa_C);
	I2 = A_a_B_aa_C;
	
	//Impresion en los texfield de los resultados de las componentes simetricas
	double aux77 = 180*(Complex.argument(I0))/Math.PI; 
	double aux7 = (double)(Math.round(decimal*Complex.abs(I0)))/decimal;
	double aux8 = (double)(Math.round(decimal*aux77))/decimal;
		
	MyTextField13.setText(String.valueOf(aux7));
	MyTextField16.setText(String.valueOf(aux8));

	double aux99 = 180*(Complex.argument(I1))/Math.PI; 
	double aux9 = (double)(Math.round(decimal*Complex.abs(I1)))/decimal;
	double aux10 = (double)(Math.round(decimal*aux99))/decimal;
		
	MyTextField14.setText(String.valueOf(aux9));
	MyTextField17.setText(String.valueOf(aux10));

	double aux1111 = 180*(Complex.argument(I2))/Math.PI; 
	double aux11 = (double)(Math.round(decimal*Complex.abs(I2)))/decimal;
	double aux12 = (double)(Math.round(decimal*aux1111))/decimal;
		
	MyTextField15.setText(String.valueOf(aux11));
	MyTextField18.setText(String.valueOf(aux12));

	double aux13 =(double)(Math.round(decimal*I0.re))/decimal;
	double aux14 =(double)(Math.round(decimal*I0.im))/decimal;

	MyTextField19.setText(String.valueOf(aux13));
	MyTextField22.setText(String.valueOf(aux14));

	double aux15 =(double)(Math.round(decimal*I1.re))/decimal;
	double aux16 =(double)(Math.round(decimal*I1.im))/decimal;

	MyTextField20.setText(String.valueOf(aux15));
	MyTextField23.setText(String.valueOf(aux16));

	double aux17 =(double)(Math.round(decimal*I2.re))/decimal;
	double aux18 =(double)(Math.round(decimal*I2.im))/decimal;

	MyTextField21.setText(String.valueOf(aux17));
	MyTextField24.setText(String.valueOf(aux18));



	repaint();
	}


	if(obj instanceof JTextField ) {
	
	}
    }
    // end actionPerformed	
    
    //
    // Para que el programa se quede esperando
    //

    public void waits(String mess) {
	int aux=0;
	if(!mess.equals(" ")) 
	    try {
		aux = (int)Integer.parseInt(din.readLine());
	    } catch (Exception e) { 
	    }
	return;
    }

    class MyMouseListener extends MouseAdapter {
	int cnt =0;
	public void mousePressed (MouseEvent evt) { 
	
	    // variables para modulo de A
	    double auxA_mod_1 = Double.valueOf(MyTextField1.getText()).doubleValue();
	    double auxA_mod_2 = Complex.abs(A);
	    double auxA_mod = Math.sqrt(( auxA_mod_1-auxA_mod_2)*( auxA_mod_1-auxA_mod_2));

	    //variables para modulo de B
	    double auxB_mod_1 = Double.valueOf(MyTextField2.getText()).doubleValue();
	    double auxB_mod_2 = Complex.abs(B);
	    double auxB_mod = Math.sqrt(( auxB_mod_1-auxB_mod_2)*( auxB_mod_1-auxB_mod_2));

	    //variables para modulo de C
	    double auxC_mod_1 = Double.valueOf(MyTextField3.getText()).doubleValue();
	    double auxC_mod_2 = Complex.abs(C);
	    double auxC_mod = Math.sqrt(( auxC_mod_1-auxC_mod_2)*( auxC_mod_1-auxC_mod_2));


	    //Variables para argumento de A
	    double auxA_ang_1 = Double.valueOf(MyTextField4.getText()).doubleValue();
	    double auxA_ang_2 = 180*(Complex.argument(A))/Math.PI;
	    double auxA_ang = Math.sqrt(( auxA_ang_1-auxA_ang_2)*( auxA_ang_1-auxA_ang_2));

	   
	    //Variables para argumento de B
	    double auxB_ang_1 = Double.valueOf(MyTextField5.getText()).doubleValue();
	    double auxB_ang_2 = 180*(Complex.argument(B))/Math.PI;
	    double auxB_ang = Math.sqrt(( auxB_ang_1-auxB_ang_2)*( auxB_ang_1-auxB_ang_2));


	    //Variables para argumento de C
	    double auxC_ang_1 = Double.valueOf(MyTextField6.getText()).doubleValue();
	    double auxC_ang_2 = 180*(Complex.argument(C))/Math.PI;
	    double auxC_ang = Math.sqrt(( auxC_ang_1-auxC_ang_2)*( auxC_ang_1-auxC_ang_2));


	    //Variables para real A
	    double auxA_re_1 =  Double.valueOf(MyTextField7.getText()).doubleValue();
	    double auxA_re_2 = A.re;
	    double auxA_re = Math.sqrt(( auxA_re_1-auxA_re_2)*( auxA_re_1-auxA_re_2));

	    //Variables para real B
	    double auxB_re_1 =  Double.valueOf(MyTextField8.getText()).doubleValue();
	    double auxB_re_2 = B.re;
	    double auxB_re = Math.sqrt(( auxB_re_1-auxB_re_2)*( auxB_re_1-auxB_re_2));

	    //Variables para real C
	    double auxC_re_1 =  Double.valueOf(MyTextField9.getText()).doubleValue();
	    double auxC_re_2 = C.re;
	    double auxC_re = Math.sqrt(( auxC_re_1-auxC_re_2)*( auxC_re_1-auxC_re_2));


	    //Variables para imaginario A
	    double auxA_im_1 =  Double.valueOf(MyTextField10.getText()).doubleValue();
	    double auxA_im_2 = A.im;
	    double auxA_im = Math.sqrt(( auxA_im_1-auxA_im_2)*( auxA_im_1-auxA_im_2));

	   //Variables para imaginario B
	    double auxB_im_1 =  Double.valueOf(MyTextField11.getText()).doubleValue();
	    double auxB_im_2 = B.im;
	    double auxB_im = Math.sqrt(( auxB_im_1-auxB_im_2)*( auxB_im_1-auxB_im_2));

	    //Variables para imaginario C
	    double auxC_im_1 =  Double.valueOf(MyTextField12.getText()).doubleValue();
	    double auxC_im_2 = C.im;
	    double auxC_im = Math.sqrt(( auxC_im_1-auxC_im_2)*( auxC_im_1-auxC_im_2));

	 
	    if(auxA_mod >= presicion || auxA_ang >= presicion){
		updatevaluesPolarA();
		repaint();
	    }
	    
	   else if(auxB_mod >= presicion || auxB_ang >= presicion){
	       updatevaluesPolarB();
	       repaint();
	   }
	   else if(auxC_mod >= presicion || auxC_ang >= presicion){
	       updatevaluesPolarC();
	       repaint();
	   }
	   else if(auxA_re >= presicion || auxA_im >= presicion){
		updatevaluesCartesianaA();
		repaint();
	    }

	   else if(auxB_re >= presicion || auxB_im >= presicion){
		updatevaluesCartesianaB();
		repaint();
	    }
	   else if(auxC_re >= presicion || auxC_im >= presicion){
		updatevaluesCartesianaC();
		repaint();

	    }
	    
	}
    }




    public void updatevaluesPolarA() {

	A = null;
	double aux1=Double.valueOf(MyTextField1.getText()).doubleValue();
	double aux3=Double.valueOf(MyTextField4.getText()).doubleValue();
	double aux4 = (Math.PI*aux3)/180;

	A =  new Complex(aux1*Math.cos(aux4),aux1*Math.sin(aux4));
	double aux7 = decimal*A.re;
	double aux8 = decimal*A.im;
	double aux5 = (Math.round(aux7))/decimal;
	double aux6 = (Math.round(aux8))/decimal;

	MyTextField7.setText(String.valueOf(aux5));
	MyTextField10.setText(String.valueOf(aux6));
	return;
    }
    
    public void updatevaluesPolarB() {

	B = null;
	double aux2=Double.valueOf(MyTextField2.getText()).doubleValue();
	double aux4=Double.valueOf(MyTextField5.getText()).doubleValue();
	double aux1= (Math.PI*aux4)/180;

	B =  new Complex(aux2*Math.cos(aux1),aux2*Math.sin(aux1));

	double aux7 =decimal*B.re;
	double aux8 = decimal*B.im;
	double aux5 = (Math.round(aux7))/decimal;
	double aux6 = (Math.round(aux8))/decimal;

	MyTextField8.setText(String.valueOf(aux5));
	MyTextField11.setText(String.valueOf(aux6));
	
	return;
    }
    public void updatevaluesPolarC() {

	C = null;
	double aux2=Double.valueOf(MyTextField3.getText()).doubleValue();
	double aux4=Double.valueOf(MyTextField6.getText()).doubleValue();
	double aux1= (Math.PI*aux4)/180;

	C =  new Complex(aux2*Math.cos(aux1),aux2*Math.sin(aux1));

	double aux7 =decimal*C.re;
	double aux8 = decimal*C.im;
	double aux5 = (Math.round(aux7))/decimal;
	double aux6 = (Math.round(aux8))/decimal;

	MyTextField9.setText(String.valueOf(aux5));
	MyTextField12.setText(String.valueOf(aux6));
	
	return;
    }


    
    public void updatevaluesCartesianaA() {

	A = null;
	double aux1=Double.valueOf(MyTextField7.getText()).doubleValue();
	double aux2=Double.valueOf(MyTextField10.getText()).doubleValue();

	A =  new Complex(aux1,aux2);
	double aux3 = 180*(Complex.argument(A))/Math.PI;
	double aux4 = (Math.round(decimal*Complex.abs(A)))/decimal;
	double aux5 = (Math.round(decimal*aux3))/decimal;

	MyTextField1.setText(String.valueOf(aux4));
	MyTextField4.setText(String.valueOf(aux5));
	
	return;
    }

    public void updatevaluesCartesianaB() {


	B = null;

	double aux1=Double.valueOf(MyTextField8.getText()).doubleValue();
	double aux2=Double.valueOf(MyTextField11.getText()).doubleValue();

	    
	B =  new Complex(aux1,aux2);
	double aux3 = 180*(Complex.argument(B))/Math.PI;
	double aux4 = (Math.round(decimal*Complex.abs(B)))/decimal;
	double aux5 = (Math.round(decimal*aux3))/decimal;


	MyTextField2.setText(String.valueOf(aux4));
	MyTextField5.setText(String.valueOf(aux5));
		
	return;
   }
   public void updatevaluesCartesianaC() {


	C = null;

	double aux1=Double.valueOf(MyTextField9.getText()).doubleValue();
	double aux2=Double.valueOf(MyTextField12.getText()).doubleValue();

	    
	C =  new Complex(aux1,aux2);
	double aux3 = 180*(Complex.argument(C))/Math.PI;
	double aux4 = (Math.round(decimal*Complex.abs(C)))/decimal;
	double aux5 = (Math.round(decimal*aux3))/decimal;


	MyTextField3.setText(String.valueOf(aux4));
	MyTextField6.setText(String.valueOf(aux5));
		
	return;
   }


}
  


