package org.deepedit.addons.nomlufketool.unbalancedcomponents;

/* PANEL INFERIOR CON LOS TEMAS DE JAVA
 * @(#)LAFPanel.java	 
 *
 * @author 	Fernando Eduardo Flatow Garrido
 * @version 	1.0, Septiembre de 2000
 */
import javax.swing.*;
import javax.swing.plaf.metal.MetalLookAndFeel;
import com.sun.java.swing.plaf.motif.MotifLookAndFeel;
import com.sun.java.swing.plaf.windows.WindowsLookAndFeel;
import java.awt.*;
import java.awt.event.*;

/*
 * Clase LAFPanel
 */
public class LAFPanel extends JPanel implements ActionListener {

    public final static MetalLookAndFeel metalLAF = new MetalLookAndFeel();
    public final static MotifLookAndFeel motifLAF = new MotifLookAndFeel();
    public final static WindowsLookAndFeel winLAF = new WindowsLookAndFeel();
    private JRadioButton metal, mac, motif, win;
    private JFrame applet;
    private boolean withPack;

    public LAFPanel(JFrame applet) {
        this(applet, true);
    }

    /*
     * Crea los tipos de botones
     */
    public LAFPanel(JFrame applet, boolean pack) {
        this.applet = applet;
        withPack = pack;
        setLayout(new GridLayout(1, 3));
        ButtonGroup lafGrp = new ButtonGroup();
        metal = new JRadioButton("Metal", true);
        motif = new JRadioButton("Motif");
        win = new JRadioButton("Windows");
        lafGrp.add(metal);
        add(metal);
        lafGrp.add(motif);
        add(motif);
        lafGrp.add(win);
        add(win);
        metal.addActionListener(this);
        motif.addActionListener(this);
        win.addActionListener(this);
    }

    public void actionPerformed(ActionEvent e) {
        Object obj = e.getSource();
        if (obj == metal) {
            setLAF(metalLAF);
        } else if (obj == motif) {
            setLAF(motifLAF);
        } else if (obj == win) {
            setLAF(winLAF);
        }
    }

    private void setLAF(LookAndFeel laf) {
        try {
            UIManager.setLookAndFeel(laf);
            SwingUtilities.updateComponentTreeUI(applet);
            if (withPack) {
                applet.repaint();
            }
        } catch (Exception e) {
            System.err.println("Could not switch to special look and feel: " + laf + "\n" + e);
        }
    }
}
