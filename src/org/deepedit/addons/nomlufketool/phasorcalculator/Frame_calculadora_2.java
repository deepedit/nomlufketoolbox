package org.deepedit.addons.nomlufketool.phasorcalculator;

/*
 * NewJFrame.java
 *
 * Created on 28 de junio de 2005, 11:58 PM
 */

/**
 *
 * @author  Maurito
 */
public class Frame_calculadora_2 extends javax.swing.JFrame {
    
    private int posCursor=0;
	Calc2 p;
    String idiom[];
    String[] espanol={"Borrar",         //0
    "Cursor a la Izquierda",                        //1
    "Cursor a la Derecha",                          //2
    "Aceptar",                          //3
    "Cancelar",                         //4
    "seno",                             //5
    "coseno",                           //6
    "seno hiperb�lico",                 //7
    "coseno hiperb�lico",               //8
    "potencia",                         //9
    "exponencial",                      //10
    "logaritmo natural",                //11
    "conjugado",                        //12
    "Acepta la ecuaci�n",//13
    "Editor de Ecuaciones",             //14
    "Unidad imaginaria",                  //15
    "�ngulo [grados]",                  //16
    "Notaci�n Cient�fica (2E3 = 2x10^3)",//17
    };
    
    String english[]={"Delete",         //0
    "Cursor Left",                        //1
    "Cursor Rigth",                          //2
    "Accept",                          //3
    "Cancel",                         //4
    "sine",                             //5
    "cosine",                           //6
    "hiperbolic sine",                 //7
    "hiperbolic cosine",               //8
    "pow",                         //9
    "exponential",                      //10
    "natural logarithmic",                //11
    "conjugated",                        //12
    "Accept the equation",              //13
    "Equations' Editor",                 //14
    "Imaginary unit",                        //15
    "Angle [grad]",                    //16
    "Scientific Notation (2E3 = 2x10^3)",//17
    };
    
    String german[]={"Borrar",         //0
    "Izquierda",                        //1
    "Derecha",                          //2
    "Aceptar",                          //3
    "Cancelar",                         //4
    "seno",                             //5
    "coseno",                           //6
    "seno hiperb�lico",                 //7
    "coseno hiperb�lico",               //8
    "potencia",                         //9
    "exponencial",                      //10
    "logaritmo natural",                //11
    "conjugado",                        //12
    "Acepta la f�rmula para calcularla",//13
    "Der Redakteur von Gleichungen",     //14
    "imagin�re Einheit",                 //15
    "Winkel",                           //16
    "Wissenschaftliche Darstellung (2E3 = 2x10^3)",//17
    };
    
    /** Creates new form Frame_calculadora_2 */
    public Frame_calculadora_2(Calc2 p, int idioma) {
		this.p=p;
        if(idioma == 0)
            idiom=espanol;
        else if(idioma == 1)
            idiom=english;
        else
            idiom=german;
        initComponents();
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code.
     */
    private void initComponents() {
        Panel_formula = new javax.swing.JPanel();
        TextField_formula = new javax.swing.JTextField();
        Panel_central = new javax.swing.JPanel();
        Panel_centralAuxiliar = new javax.swing.JPanel();
        Button_izq = new javax.swing.JButton();
        Button_delete = new javax.swing.JButton();
        Button_der = new javax.swing.JButton();
        Panel_centralPrincipal = new javax.swing.JPanel();
        Panel_funciones = new javax.swing.JPanel();
        Button_Re = new javax.swing.JButton();
        Button_Im = new javax.swing.JButton();
        Button_sin = new javax.swing.JButton();
        Button_cos = new javax.swing.JButton();
        Button_sinh = new javax.swing.JButton();
        Button_cosh = new javax.swing.JButton();
        Button_pow = new javax.swing.JButton();
        Button_exp = new javax.swing.JButton();
        Button_ln = new javax.swing.JButton();
        Button_conj = new javax.swing.JButton();
        Panel_numerosBk = new javax.swing.JPanel();
        Panel_num1a3 = new javax.swing.JPanel();
        Button_1 = new javax.swing.JButton();
        Button_2 = new javax.swing.JButton();
        Button_3 = new javax.swing.JButton();
        Panel_num4a6 = new javax.swing.JPanel();
        Button_4 = new javax.swing.JButton();
        Button_5 = new javax.swing.JButton();
        Button_6 = new javax.swing.JButton();
        Panel_num7a9 = new javax.swing.JPanel();
        Button_7 = new javax.swing.JButton();
        Button_8 = new javax.swing.JButton();
        Button_9 = new javax.swing.JButton();
        Panel_numOtros = new javax.swing.JPanel();
        Button_Cient = new javax.swing.JButton();
        Button_0 = new javax.swing.JButton();
        Button_decimal = new javax.swing.JButton();
        Panel_operadores = new javax.swing.JPanel();
        Button_div = new javax.swing.JButton();
        Button_mult = new javax.swing.JButton();
        Button_sustr = new javax.swing.JButton();
        Button_adic = new javax.swing.JButton();
        Button_equal = new javax.swing.JButton();
        Panel_var = new javax.swing.JPanel();
        Button_A = new javax.swing.JButton();
        Button_B = new javax.swing.JButton();
        Button_C = new javax.swing.JButton();
        Button_D = new javax.swing.JButton();
        Button_E = new javax.swing.JButton();
        Panel_acciones = new javax.swing.JPanel();
        Button_Aceptar = new javax.swing.JButton();
        Button_cancelar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle(idiom[14]);
//        setAlwaysOnTop(true);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setLocation(150,100);
        Panel_formula.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 0, 5));

        /*Campo de Texto para escribir F�rmula*
         **************************************
         **************************************
         */
        TextField_formula.setColumns(50);
        TextField_formula.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        TextField_formula.setAutoscrolls(false);
        TextField_formula.setPreferredSize(new java.awt.Dimension(350, 25));
        TextField_formula.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                TextField_formula_focusGained(evt);
            }
        });
        TextField_formula.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TextField_formula_mouseClicked(evt);
            }
        });
        TextField_formula.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                Frame_calculadora_2.this.caretUpdate(evt);
            }
        });

        Panel_formula.add(TextField_formula);

        getContentPane().add(Panel_formula, java.awt.BorderLayout.NORTH);

        Panel_central.setLayout(new java.awt.BorderLayout());

        Panel_central.setPreferredSize(new java.awt.Dimension(515, 200));
        Panel_centralAuxiliar.setLayout(new java.awt.GridLayout(1, 3));

        Panel_centralAuxiliar.setMaximumSize(new java.awt.Dimension(150, 50));
        Panel_centralAuxiliar.setMinimumSize(new java.awt.Dimension(150, 50));
        /*Bot�n izquierda <--*
         **************************************
         **************************************
         */
        Button_izq.setFont(new java.awt.Font("Tahoma", 0, 11));
        Button_izq.setText("<--");
        Button_izq.setName("izq");
        Button_izq.setToolTipText(idiom[1]);
        Button_izq.setPreferredSize(new java.awt.Dimension(50, 50));
        Button_izq.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ButtonMov_mouseClicked(evt);
            }
        });

        Panel_centralAuxiliar.add(Button_izq);
        /*Bot�n Borrar*
         **************************************
         **************************************
         */
        Button_delete.setFont(new java.awt.Font("Tahoma", 0, 10));
        Button_delete.setText(idiom[0]);
        Button_delete.setName("del");
        Button_delete.setPreferredSize(new java.awt.Dimension(50, 50));
        Button_delete.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ButtonMov_mouseClicked(evt);
            }
        });

        Panel_centralAuxiliar.add(Button_delete);
        /*Bot�n Derecha -->*
         **************************************
         **************************************
         */
        Button_der.setFont(new java.awt.Font("Tahoma", 0, 11));
        Button_der.setText("-->");
        Button_der.setName("der");
        Button_der.setToolTipText(idiom[2]);
        Button_der.setPreferredSize(new java.awt.Dimension(50, 50));
        Button_der.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ButtonMov_mouseClicked(evt);
            }
        });

        Panel_centralAuxiliar.add(Button_der);

        Panel_central.add(Panel_centralAuxiliar, java.awt.BorderLayout.NORTH);

        Panel_centralPrincipal.setLayout(new java.awt.GridLayout(1, 4));

        Panel_funciones.setLayout(new java.awt.GridLayout(5, 2));
        
        /*Botones de Funciones*
         ********************************************************************
         ********************************************************************
         */
        Button_Re.setFont(new java.awt.Font("Tahoma", 0, 10));
        Button_Re.setText("i");
        Button_Re.setName("i");
        Button_Re.setToolTipText(idiom[15]);
        Button_Re.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ButtonCentral_mouseClicked(evt);
            }
        });

        Panel_funciones.add(Button_Re);

        Button_Im.setFont(new java.awt.Font("Tahoma", 0, 10));
        Button_Im.setText("ang [\u00ba]");
        Button_Im.setName("<");
        Button_Im.setToolTipText(idiom[16]);
        Button_Im.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ButtonCentral_mouseClicked(evt);
            }
        });

        Panel_funciones.add(Button_Im);

        Button_sin.setFont(new java.awt.Font("Tahoma", 0, 10));
        Button_sin.setText("sin(x)");
        Button_sin.setName("sin()");
        Button_sin.setToolTipText(idiom[5]);
        Button_sin.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ButtonFunctions_mouseClicked(evt);
            }
        });

        Panel_funciones.add(Button_sin);

        Button_cos.setFont(new java.awt.Font("Tahoma", 0, 10));
        Button_cos.setText("cos(x)");
        Button_cos.setMaximumSize(new java.awt.Dimension(59, 23));
        Button_cos.setMinimumSize(new java.awt.Dimension(59, 23));
        Button_cos.setName("cos()");
        Button_cos.setToolTipText(idiom[6]);
        Button_cos.setPreferredSize(new java.awt.Dimension(59, 23));
        Button_cos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ButtonFunctions_mouseClicked(evt);
            }
        });

        Panel_funciones.add(Button_cos);

        Button_sinh.setFont(new java.awt.Font("Tahoma", 0, 10));
        Button_sinh.setText("sinh(x)");
        Button_sinh.setMaximumSize(new java.awt.Dimension(59, 23));
        Button_sinh.setMinimumSize(new java.awt.Dimension(59, 23));
        Button_sinh.setName("sinh()");
        Button_sinh.setToolTipText(idiom[7]);
        Button_sinh.setPreferredSize(new java.awt.Dimension(59, 23));
        Button_sinh.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ButtonFunctions_mouseClicked(evt);
            }
        });

        Panel_funciones.add(Button_sinh);

        Button_cosh.setFont(new java.awt.Font("Tahoma", 0, 10));
        Button_cosh.setText("cosh(x)");
        Button_cosh.setMaximumSize(new java.awt.Dimension(59, 23));
        Button_cosh.setMinimumSize(new java.awt.Dimension(59, 23));
        Button_cosh.setName("cosh()");
        Button_cosh.setToolTipText(idiom[8]);
        Button_cosh.setPreferredSize(new java.awt.Dimension(59, 23));
        Button_cosh.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ButtonFunctions_mouseClicked(evt);
            }
        });

        Panel_funciones.add(Button_cosh);

        Button_pow.setFont(new java.awt.Font("Tahoma", 0, 10));
        Button_pow.setText("x^y");
        Button_pow.setMaximumSize(new java.awt.Dimension(59, 23));
        Button_pow.setMinimumSize(new java.awt.Dimension(59, 23));
        Button_pow.setName("^");
        Button_pow.setToolTipText(idiom[9]);
        Button_pow.setPreferredSize(new java.awt.Dimension(59, 23));
        Button_pow.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ButtonCentral_mouseClicked(evt);
            }
        });

        Panel_funciones.add(Button_pow);

        Button_exp.setFont(new java.awt.Font("Tahoma", 0, 10));
        Button_exp.setText("exp(x)");
        Button_exp.setMaximumSize(new java.awt.Dimension(59, 23));
        Button_exp.setMinimumSize(new java.awt.Dimension(59, 23));
        Button_exp.setName("exp()");
        Button_exp.setToolTipText(idiom[10]);
        Button_exp.setPreferredSize(new java.awt.Dimension(59, 23));
        Button_exp.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ButtonFunctions_mouseClicked(evt);
            }
        });

        Panel_funciones.add(Button_exp);

        Button_ln.setFont(new java.awt.Font("Tahoma", 0, 10));
        Button_ln.setText("ln(x)");
        Button_ln.setMaximumSize(new java.awt.Dimension(59, 23));
        Button_ln.setMinimumSize(new java.awt.Dimension(59, 23));
        Button_ln.setName("ln()");
        Button_ln.setToolTipText(idiom[11]);
        Button_ln.setPreferredSize(new java.awt.Dimension(59, 23));
        Button_ln.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ButtonFunctions_mouseClicked(evt);
            }
        });

        Panel_funciones.add(Button_ln);

        Button_conj.setFont(new java.awt.Font("Tahoma", 0, 10));
        Button_conj.setText("conj(x)");
        Button_conj.setMaximumSize(new java.awt.Dimension(59, 23));
        Button_conj.setMinimumSize(new java.awt.Dimension(59, 23));
        Button_conj.setName("'");
        Button_conj.setToolTipText(idiom[12]);
        Button_conj.setPreferredSize(new java.awt.Dimension(59, 23));
        Button_conj.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ButtonCentral_mouseClicked(evt);
            }
        });

        Panel_funciones.add(Button_conj);

        Panel_centralPrincipal.add(Panel_funciones);

        Panel_numerosBk.setLayout(new java.awt.GridLayout(4, 1));

        Panel_numerosBk.setMinimumSize(new java.awt.Dimension(120, 198));
        Panel_numerosBk.setPreferredSize(new java.awt.Dimension(124, 120));
        Panel_num1a3.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 1, 0));

        Panel_num1a3.setMinimumSize(new java.awt.Dimension(124, 30));
        Panel_num1a3.setPreferredSize(new java.awt.Dimension(124, 30));
        /*Botones num�ricos
         ********************************************************************
         ********************************************************************
         */
        Button_1.setFont(new java.awt.Font("Tahoma", 0, 10));
        Button_1.setText("1");
        Button_1.setMaximumSize(new java.awt.Dimension(40, 30));
        Button_1.setMinimumSize(new java.awt.Dimension(40, 30));
        Button_1.setName("1");
        Button_1.setPreferredSize(new java.awt.Dimension(40, 30));
        Button_1.setRequestFocusEnabled(false);
        Button_1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ButtonCentral_mouseClicked(evt);
            }
        });

        Panel_num1a3.add(Button_1);

        Button_2.setFont(new java.awt.Font("Tahoma", 0, 10));
        Button_2.setText("2");
        Button_2.setMaximumSize(new java.awt.Dimension(40, 30));
        Button_2.setMinimumSize(new java.awt.Dimension(40, 30));
        Button_2.setName("2");
        Button_2.setPreferredSize(new java.awt.Dimension(40, 30));
        Button_2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ButtonCentral_mouseClicked(evt);
            }
        });

        Panel_num1a3.add(Button_2);

        Button_3.setFont(new java.awt.Font("Tahoma", 0, 10));
        Button_3.setText("3");
        Button_3.setMaximumSize(new java.awt.Dimension(40, 30));
        Button_3.setMinimumSize(new java.awt.Dimension(40, 30));
        Button_3.setName("3");
        Button_3.setPreferredSize(new java.awt.Dimension(40, 30));
        Button_3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ButtonCentral_mouseClicked(evt);
            }
        });

        Panel_num1a3.add(Button_3);

        Panel_numerosBk.add(Panel_num1a3);

        Panel_num4a6.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 1, 0));

        Panel_num4a6.setMinimumSize(new java.awt.Dimension(124, 30));
        Panel_num4a6.setPreferredSize(new java.awt.Dimension(124, 30));
        Button_4.setFont(new java.awt.Font("Tahoma", 0, 10));
        Button_4.setText("4");
        Button_4.setMaximumSize(new java.awt.Dimension(40, 30));
        Button_4.setMinimumSize(new java.awt.Dimension(40, 30));
        Button_4.setName("4");
        Button_4.setPreferredSize(new java.awt.Dimension(40, 30));
        Button_4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ButtonCentral_mouseClicked(evt);
            }
        });

        Panel_num4a6.add(Button_4);

        Button_5.setFont(new java.awt.Font("Tahoma", 0, 10));
        Button_5.setText("5");
        Button_5.setMaximumSize(new java.awt.Dimension(40, 30));
        Button_5.setMinimumSize(new java.awt.Dimension(40, 30));
        Button_5.setName("5");
        Button_5.setPreferredSize(new java.awt.Dimension(40, 30));
        Button_5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ButtonCentral_mouseClicked(evt);
            }
        });

        Panel_num4a6.add(Button_5);

        Button_6.setFont(new java.awt.Font("Tahoma", 0, 10));
        Button_6.setText("6");
        Button_6.setMaximumSize(new java.awt.Dimension(40, 30));
        Button_6.setMinimumSize(new java.awt.Dimension(40, 30));
        Button_6.setName("6");
        Button_6.setPreferredSize(new java.awt.Dimension(40, 30));
        Button_6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ButtonCentral_mouseClicked(evt);
            }
        });

        Panel_num4a6.add(Button_6);

        Panel_numerosBk.add(Panel_num4a6);

        Panel_num7a9.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 1, 0));

        Panel_num7a9.setMinimumSize(new java.awt.Dimension(124, 30));
        Panel_num7a9.setPreferredSize(new java.awt.Dimension(124, 30));
        Button_7.setFont(new java.awt.Font("Tahoma", 0, 10));
        Button_7.setText("7");
        Button_7.setMaximumSize(new java.awt.Dimension(40, 30));
        Button_7.setMinimumSize(new java.awt.Dimension(40, 30));
        Button_7.setName("7");
        Button_7.setPreferredSize(new java.awt.Dimension(40, 30));
        Button_7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ButtonCentral_mouseClicked(evt);
            }
        });

        Panel_num7a9.add(Button_7);

        Button_8.setFont(new java.awt.Font("Tahoma", 0, 10));
        Button_8.setText("8");
        Button_8.setMaximumSize(new java.awt.Dimension(40, 30));
        Button_8.setMinimumSize(new java.awt.Dimension(40, 30));
        Button_8.setName("8");
        Button_8.setPreferredSize(new java.awt.Dimension(40, 30));
        Button_8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ButtonCentral_mouseClicked(evt);
            }
        });

        Panel_num7a9.add(Button_8);

        Button_9.setFont(new java.awt.Font("Tahoma", 0, 10));
        Button_9.setText("9");
        Button_9.setMaximumSize(new java.awt.Dimension(40, 30));
        Button_9.setMinimumSize(new java.awt.Dimension(40, 30));
        Button_9.setName("9");
        Button_9.setPreferredSize(new java.awt.Dimension(40, 30));
        Button_9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ButtonCentral_mouseClicked(evt);
            }
        });

        Panel_num7a9.add(Button_9);

        Panel_numerosBk.add(Panel_num7a9);

        Panel_numOtros.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 1, 0));

        Panel_numOtros.setMinimumSize(new java.awt.Dimension(124, 30));
        Panel_numOtros.setPreferredSize(new java.awt.Dimension(124, 30));
        Button_Cient.setFont(new java.awt.Font("Tahoma", 0, 10));
        Button_Cient.setText("E");
        Button_Cient.setMaximumSize(new java.awt.Dimension(40, 30));
        Button_Cient.setMinimumSize(new java.awt.Dimension(40, 30));
        Button_Cient.setName("E");
        Button_Cient.setToolTipText(idiom[17]);
        Button_Cient.setPreferredSize(new java.awt.Dimension(40, 30));
        Button_Cient.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ButtonCentral_mouseClicked(evt);
            }
        });

        Panel_numOtros.add(Button_Cient);

        Button_0.setFont(new java.awt.Font("Tahoma", 0, 10));
        Button_0.setText("0");
        Button_0.setMaximumSize(new java.awt.Dimension(40, 30));
        Button_0.setMinimumSize(new java.awt.Dimension(40, 30));
        Button_0.setName("0");
        Button_0.setPreferredSize(new java.awt.Dimension(40, 30));
        Button_0.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ButtonCentral_mouseClicked(evt);
            }
        });

        Panel_numOtros.add(Button_0);

        Button_decimal.setFont(new java.awt.Font("Tahoma", 0, 10));
        Button_decimal.setText(",");
        Button_decimal.setMaximumSize(new java.awt.Dimension(40, 30));
        Button_decimal.setMinimumSize(new java.awt.Dimension(40, 30));
        Button_decimal.setName(".");
        Button_decimal.setPreferredSize(new java.awt.Dimension(40, 30));
        Button_decimal.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ButtonCentral_mouseClicked(evt);
            }
        });

        Panel_numOtros.add(Button_decimal);

        Panel_numerosBk.add(Panel_numOtros);

        Panel_centralPrincipal.add(Panel_numerosBk);

        Panel_operadores.setLayout(new java.awt.GridLayout(5, 0));

        Panel_operadores.setMaximumSize(new java.awt.Dimension(40, 125));
        Panel_operadores.setMinimumSize(new java.awt.Dimension(40, 125));
        Panel_operadores.setPreferredSize(new java.awt.Dimension(40, 125));
        Panel_operadores.setRequestFocusEnabled(false);
        /*Botones de Operaciones
          ********************************************************************
         ********************************************************************
         */
        Button_div.setFont(new java.awt.Font("Tahoma", 0, 10));
        Button_div.setText("/");
        Button_div.setMaximumSize(new java.awt.Dimension(40, 30));
        Button_div.setMinimumSize(new java.awt.Dimension(40, 30));
        Button_div.setName("/");
        Button_div.setPreferredSize(new java.awt.Dimension(40, 30));
        Button_div.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ButtonCentral_mouseClicked(evt);
            }
        });

        Panel_operadores.add(Button_div);

        Button_mult.setFont(new java.awt.Font("Tahoma", 0, 10));
        Button_mult.setText("*");
        Button_mult.setName("*");
        Button_mult.setPreferredSize(new java.awt.Dimension(40, 30));
        Button_mult.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ButtonCentral_mouseClicked(evt);
            }
        });

        Panel_operadores.add(Button_mult);

        Button_sustr.setFont(new java.awt.Font("Tahoma", 0, 10));
        Button_sustr.setText("-");
        Button_sustr.setName("-");
        Button_sustr.setPreferredSize(new java.awt.Dimension(40, 30));
        Button_sustr.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ButtonCentral_mouseClicked(evt);
            }
        });

        Panel_operadores.add(Button_sustr);

        Button_adic.setFont(new java.awt.Font("Tahoma", 0, 10));
        Button_adic.setText("+");
        Button_adic.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        Button_adic.setName("+");
        Button_adic.setPreferredSize(new java.awt.Dimension(40, 30));
        Button_adic.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ButtonCentral_mouseClicked(evt);
            }
        });

        Panel_operadores.add(Button_adic);

        Button_equal.setFont(new java.awt.Font("Arial", 1, 14));
        Button_equal.setText("=");
        Button_equal.setName("=");
        Button_equal.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ButtonCentral_mouseClicked(evt);
            }
        });

        Panel_operadores.add(Button_equal);

        Panel_centralPrincipal.add(Panel_operadores);

        Panel_var.setLayout(new java.awt.GridLayout(5, 1));

        Panel_var.setMaximumSize(null);
        Panel_var.setPreferredSize(new java.awt.Dimension(40, 125));
        /*Botones de Variables
         ********************************************************************
         ********************************************************************
         */
        Button_A.setFont(new java.awt.Font("Tahoma", 0, 10));
        Button_A.setText("A");
        Button_A.setMaximumSize(new java.awt.Dimension(40, 40));
        Button_A.setMinimumSize(new java.awt.Dimension(40, 40));
        Button_A.setName("A");
        Button_A.setPreferredSize(new java.awt.Dimension(40, 40));
        Button_A.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ButtonCentral_mouseClicked(evt);
            }
        });

        Panel_var.add(Button_A);

        Button_B.setFont(new java.awt.Font("Tahoma", 0, 10));
        Button_B.setText("B");
        Button_B.setMaximumSize(new java.awt.Dimension(40, 40));
        Button_B.setMinimumSize(new java.awt.Dimension(40, 40));
        Button_B.setName("B");
        Button_B.setPreferredSize(new java.awt.Dimension(40, 40));
        Button_B.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ButtonCentral_mouseClicked(evt);
            }
        });

        Panel_var.add(Button_B);

        Button_C.setFont(new java.awt.Font("Tahoma", 0, 10));
        Button_C.setText("C");
        Button_C.setMaximumSize(new java.awt.Dimension(40, 40));
        Button_C.setMinimumSize(new java.awt.Dimension(40, 40));
        Button_C.setName("C");
        Button_C.setPreferredSize(new java.awt.Dimension(40, 40));
        Button_C.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ButtonCentral_mouseClicked(evt);
            }
        });

        Panel_var.add(Button_C);

        Button_D.setFont(new java.awt.Font("Tahoma", 0, 10));
        Button_D.setText("D");
        Button_D.setMaximumSize(new java.awt.Dimension(40, 40));
        Button_D.setMinimumSize(new java.awt.Dimension(40, 40));
        Button_D.setName("D");
        Button_D.setPreferredSize(new java.awt.Dimension(40, 40));
        Button_D.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ButtonCentral_mouseClicked(evt);
            }
        });

        Panel_var.add(Button_D);

        Button_E.setFont(new java.awt.Font("Tahoma", 0, 10));
        Button_E.setText("E");
        Button_E.setMaximumSize(new java.awt.Dimension(40, 40));
        Button_E.setMinimumSize(new java.awt.Dimension(40, 40));
        Button_E.setName("E");
        Button_E.setPreferredSize(new java.awt.Dimension(40, 40));
        Button_E.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ButtonCentral_mouseClicked(evt);
            }
        });

        Panel_var.add(Button_E);

        Panel_centralPrincipal.add(Panel_var);

        Panel_central.add(Panel_centralPrincipal, java.awt.BorderLayout.CENTER);

        getContentPane().add(Panel_central, java.awt.BorderLayout.CENTER);

        Panel_acciones.setPreferredSize(new java.awt.Dimension(350, 33));
        Button_Aceptar.setFont(new java.awt.Font("Tahoma", 1, 11));
        Button_Aceptar.setMnemonic('a');
        Button_Aceptar.setText(idiom[3]);
        Button_Aceptar.setToolTipText(idiom[13]);
        Button_Aceptar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                Button_Aceptar_mouseClicked(evt);
            }
        });

        Panel_acciones.add(Button_Aceptar);

        Button_cancelar.setMnemonic('c');
        Button_cancelar.setText(idiom[4]);
        Button_cancelar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                Button_cancelar_mouseClicked(evt);
            }
        });

        Panel_acciones.add(Button_cancelar);

        getContentPane().add(Panel_acciones, java.awt.BorderLayout.SOUTH);

        pack();
    }
//////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////
    
    private void caretUpdate(javax.swing.event.CaretEvent evt) {
        //TextField_formula.setCaretPosition(TextField_formula.getCaretPosition());
    }

    private void Button_Aceptar_mouseClicked(java.awt.event.MouseEvent evt) {                                             
        this.p.setText(TextField_formula.getText());
        this.setVisible(false);
    }                                            

    /*public String getText(){
        return TextField_formula.getText();
    }*/
    
    private void TextField_formula_focusGained(java.awt.event.FocusEvent evt) {                                               
        TextField_formula_seleccionada();
    }                                              

    private void TextField_formula_mouseClicked(java.awt.event.MouseEvent evt) {                                                
       TextField_formula_seleccionada();
    }                                               
    
    private void TextField_formula_seleccionada(){
         posCursor=TextField_formula.getCaretPosition();
    }
    
    private void ButtonFunctions_mouseClicked(java.awt.event.MouseEvent evt) {                                              
        String texto=TextField_formula.getText();
        String texto_antes, texto_desp;
        texto_antes=texto.substring(0, posCursor);
        texto_desp=texto.substring(posCursor); 
        TextField_formula.setText(texto_antes + evt.getComponent().getName() + texto_desp);
        posCursor=posCursor+evt.getComponent().getName().length()-1;
        TextField_formula.setCaretPosition(posCursor);
    }                                             

    private void ButtonMov_mouseClicked(java.awt.event.MouseEvent evt) {                                        
        if(evt.getComponent().getName() == "der"){
           if(posCursor<TextField_formula.getText().length()) posCursor+=1;
        }
        else if(evt.getComponent().getName() == "izq"){
            if(posCursor>0) posCursor-=1;
        }
        else if(posCursor>0){
            String texto=TextField_formula.getText();
            String texto_antes, texto_desp;
            texto_antes=texto.substring(0, posCursor-1);
            texto_desp=texto.substring(posCursor);
            TextField_formula.setText(texto_antes + texto_desp);
            posCursor-=1;
        }
        TextField_formula.setCaretPosition(posCursor);
    }                                       

    private void ButtonCentral_mouseClicked(java.awt.event.MouseEvent evt) {                                            
        String texto=TextField_formula.getText();
        String texto_antes, texto_desp;
        texto_antes=texto.substring(0, posCursor);
        texto_desp=texto.substring(posCursor); 
        TextField_formula.setText(texto_antes + evt.getComponent().getName() + texto_desp);
        posCursor=posCursor+evt.getComponent().getName().length();
        TextField_formula.setCaretPosition(posCursor);
}                                           
    
    private void Button_cancelar_mouseClicked(java.awt.event.MouseEvent evt) {                                              
        this.setVisible(false);
    }                                             
        
    /**
     * @param args the command line arguments
     */
   /* public static void main(String args[]) {
        Paso pa = new Paso();
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Frame_calculadora(pa).setVisible(true);
            }
        });
    }*/
    
    // Variables declaration - do not modify
    private javax.swing.JButton Button_0;
    private javax.swing.JButton Button_1;
    private javax.swing.JButton Button_2;
    private javax.swing.JButton Button_3;
    private javax.swing.JButton Button_4;
    private javax.swing.JButton Button_5;
    private javax.swing.JButton Button_6;
    private javax.swing.JButton Button_7;
    private javax.swing.JButton Button_8;
    private javax.swing.JButton Button_9;
    private javax.swing.JButton Button_A;
    private javax.swing.JButton Button_Aceptar;
    private javax.swing.JButton Button_B;
    private javax.swing.JButton Button_C;
    private javax.swing.JButton Button_Cient;
    private javax.swing.JButton Button_D;
    private javax.swing.JButton Button_E;
    private javax.swing.JButton Button_Im;
    private javax.swing.JButton Button_Re;
    private javax.swing.JButton Button_adic;
    private javax.swing.JButton Button_cancelar;
    private javax.swing.JButton Button_conj;
    private javax.swing.JButton Button_cos;
    private javax.swing.JButton Button_cosh;
    private javax.swing.JButton Button_decimal;
    private javax.swing.JButton Button_delete;
    private javax.swing.JButton Button_der;
    private javax.swing.JButton Button_div;
    private javax.swing.JButton Button_equal;
    private javax.swing.JButton Button_exp;
    private javax.swing.JButton Button_izq;
    private javax.swing.JButton Button_ln;
    private javax.swing.JButton Button_mult;
    private javax.swing.JButton Button_pow;
    private javax.swing.JButton Button_sin;
    private javax.swing.JButton Button_sinh;
    private javax.swing.JButton Button_sustr;
    private javax.swing.JPanel Panel_acciones;
    private javax.swing.JPanel Panel_central;
    private javax.swing.JPanel Panel_centralAuxiliar;
    private javax.swing.JPanel Panel_centralPrincipal;
    private javax.swing.JPanel Panel_formula;
    private javax.swing.JPanel Panel_funciones;
    private javax.swing.JPanel Panel_num1a3;
    private javax.swing.JPanel Panel_num4a6;
    private javax.swing.JPanel Panel_num7a9;
    private javax.swing.JPanel Panel_numOtros;
    private javax.swing.JPanel Panel_numerosBk;
    private javax.swing.JPanel Panel_operadores;
    private javax.swing.JPanel Panel_var;
    private javax.swing.JTextField TextField_formula;
    // End of variables declaration
    
}

