package org.deepedit.addons.nomlufketool.transmissionmodels;

import java.awt.*;
import javax.swing.*;

public class Ayuda33 extends JPanel{

	JPanel PanelAyuda;

	//***Botones****
	JButton ButtonAyudaTematica;
	JButton ButtonAyudaInterfaz;
	JButton ButtonAnterior;
	JButton ButtonSiguiente;
	JPanel Panel;
	Top2 PanelTop;
	JLabel[] Foto1 = new JLabel[13];
	JLabel[] Foto2 = new JLabel[10];

	String Estado="";
	String Foto_1="F10";
	String Foto_2="F20";
	
	Op3 op3;
	Op2 op2;
	Op1 op1;

	public Ayuda33() {
        initComponents();
    }

	public void Metodos(Op1 o1, Op2 o2,Op3 o3)
    {
    	op1=o1;
    	op2=o2;
		op3=o3;
    }

	private void initComponents() {

		setLayout(new BorderLayout());

		PanelTop=new Top2(4);
		
		for(int i=0; i<13; i++){
			Foto1[i] = new JLabel();
		    Foto1[i].setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
    	    Foto1[i].setMinimumSize(new java.awt.Dimension(768, 400));
        	Foto1[i].setPreferredSize(new java.awt.Dimension(768, 400));
        }
		for(int i=0; i<10; i++){
			Foto2[i] = new JLabel();
		    Foto2[i].setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
    	    Foto2[i].setMinimumSize(new java.awt.Dimension(768, 400));
        	Foto2[i].setPreferredSize(new java.awt.Dimension(768, 400));
        }

		Foto1[0].setIcon(new javax.swing.ImageIcon(getClass().getResource("images/teo1.jpg")));
		Foto1[1].setIcon(new javax.swing.ImageIcon(getClass().getResource("images/teo2.jpg")));
		Foto1[2].setIcon(new javax.swing.ImageIcon(getClass().getResource("images/teo3.jpg")));
		Foto1[3].setIcon(new javax.swing.ImageIcon(getClass().getResource("images/teo4.jpg")));
		Foto1[4].setIcon(new javax.swing.ImageIcon(getClass().getResource("images/teo5.jpg")));
		Foto1[5].setIcon(new javax.swing.ImageIcon(getClass().getResource("images/teo6.jpg")));
		Foto1[6].setIcon(new javax.swing.ImageIcon(getClass().getResource("images/teo7.jpg")));
		Foto1[7].setIcon(new javax.swing.ImageIcon(getClass().getResource("images/teo8.jpg")));
		Foto1[8].setIcon(new javax.swing.ImageIcon(getClass().getResource("images/teo9.jpg")));
		Foto1[9].setIcon(new javax.swing.ImageIcon(getClass().getResource("images/teo10.jpg")));
		Foto1[10].setIcon(new javax.swing.ImageIcon(getClass().getResource("images/teo11.jpg")));
		Foto1[11].setIcon(new javax.swing.ImageIcon(getClass().getResource("images/teo12.jpg")));
		Foto1[12].setIcon(new javax.swing.ImageIcon(getClass().getResource("images/teo13.jpg")));
		Foto2[0].setIcon(new javax.swing.ImageIcon(getClass().getResource("images/interfaz1.jpg")));
		Foto2[1].setIcon(new javax.swing.ImageIcon(getClass().getResource("images/interfaz2.jpg")));
		Foto2[2].setIcon(new javax.swing.ImageIcon(getClass().getResource("images/interfaz3.jpg")));
		Foto2[3].setIcon(new javax.swing.ImageIcon(getClass().getResource("images/interfaz4.jpg")));
		Foto2[4].setIcon(new javax.swing.ImageIcon(getClass().getResource("images/interfaz5.jpg")));
		Foto2[5].setIcon(new javax.swing.ImageIcon(getClass().getResource("images/interfaz6.jpg")));
		Foto2[6].setIcon(new javax.swing.ImageIcon(getClass().getResource("images/interfaz7.jpg")));
		Foto2[7].setIcon(new javax.swing.ImageIcon(getClass().getResource("images/interfaz8.jpg")));
		Foto2[8].setIcon(new javax.swing.ImageIcon(getClass().getResource("images/interfaz9.jpg")));
		Foto2[9].setIcon(new javax.swing.ImageIcon(getClass().getResource("images/interfaz10.jpg")));


		PanelAyuda=new JPanel();
		ButtonAyudaTematica = new JButton();
		ButtonAyudaInterfaz = new JButton();
		ButtonAnterior=new JButton();
		ButtonSiguiente=new JButton();

		PanelAyuda.setLayout(new GridBagLayout());
		PanelAyuda.setMaximumSize(new java.awt.Dimension(768, 30));
		PanelAyuda.setMinimumSize(new java.awt.Dimension(768, 30));
		PanelAyuda.setPreferredSize(new java.awt.Dimension(768, 30));
		ButtonAnterior.setText("Anterior");
		PanelAyuda.add(ButtonAnterior);
		ButtonAnterior.setEnabled(false);

		ButtonAyudaTematica.setText("Ayuda Tematica");
		PanelAyuda.add(ButtonAyudaTematica);

       	ButtonAyudaInterfaz.setText("Ayuda Interfaz");
		PanelAyuda.add(ButtonAyudaInterfaz);

		ButtonSiguiente.setText("Siguiente");
		PanelAyuda.add(ButtonSiguiente);
		ButtonSiguiente.setEnabled(false);

		GridBagConstraints gc=new GridBagConstraints();

		Panel=new JPanel();
		Panel.setLayout(new java.awt.FlowLayout());
		Panel.setMaximumSize(new java.awt.Dimension(768, 550));
		Panel.setMinimumSize(new java.awt.Dimension(768, 550));
		Panel.setPreferredSize(new java.awt.Dimension(768, 550));

			gc.gridx=0;
			gc.gridy=0;
			gc.gridwidth=4;
			gc.gridheight=1;
			//Panel.add(PanelAyuda,gc);
			Panel.add(PanelAyuda);
		
			gc.gridx=0;
			gc.gridy=1;
			
			for(int i=0; i<13; i++){
				Panel.add(Foto1[i]);
				Foto1[i].setVisible(false);
	        }
			for(int i=0; i<10; i++){
				Panel.add(Foto2[i]);
				Foto2[i].setVisible(false);
	        }
		
		add(PanelTop,"North");
		add(Panel,"Center");
		

//***************************ESCUCHADORES********************************
//***********************************************************************

		ButtonAyudaTematica.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ButtonAyudaTematicaActionPerformed(evt);}});
		
		ButtonAyudaInterfaz.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
			ButtonAyudaInterfazActionPerformed(evt);}});
		
		ButtonAnterior.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
			ButtonAnteriorPerformed(evt);}});
		
		ButtonSiguiente.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
			ButtonSiguientePerformed(evt);}});
	
	
	}//init components

	private void ButtonAnteriorPerformed(java.awt.event.ActionEvent evt){
		
		int apretado_ant=0;
		if(Estado=="AT"){
			
			for(int i=0;i<13;i++){
				
				if(Foto_1.equals("F1"+i)){
					apretado_ant = i;					
				}
				Foto1[i].setVisible(false);
			}
			
			if(apretado_ant!=0){
				Foto1[apretado_ant-1].setVisible(true);
				Foto_1 = "F1"+(apretado_ant-1);
				ButtonAnterior.setEnabled(true);
			}
			
			if(apretado_ant==1){
				ButtonAnterior.setEnabled(false);
			}
			
			if(apretado_ant==12){
				ButtonSiguiente.setEnabled(true);
			}
		}
		
		else{
		
			for(int i=0;i<10;i++){
				
				if(Foto_2.equals("F2"+i)){
					apretado_ant = i;					
				}
				Foto2[i].setVisible(false);
			}
			
			if(apretado_ant!=0){
				Foto2[apretado_ant-1].setVisible(true);
				Foto_2 = "F2"+(apretado_ant-1);
				ButtonAnterior.setEnabled(true);
			}
			
			if(apretado_ant==1){
				
				ButtonAnterior.setEnabled(false);
			}
			
			if(apretado_ant==9){
				ButtonSiguiente.setEnabled(true);
			}
			
		}
	}

	private void ButtonSiguientePerformed(java.awt.event.ActionEvent evt){
		ButtonAnterior.setEnabled(true);
		int apretado_sig=0;
		if(Estado=="AT"){
			
			for(int i=0;i<13;i++){
				if(Foto_1.equals("F1"+i)){
					apretado_sig = i;
				}
				Foto1[i].setVisible(false);
			}
			
			if(apretado_sig!=12){
				Foto1[apretado_sig+1].setVisible(true);
				Foto_1 = "F1"+(apretado_sig+1);
				ButtonSiguiente.setEnabled(true);
			}
			
			if(apretado_sig==11){
				ButtonSiguiente.setEnabled(false);
			}
		}
		
		else{
			for(int i=0;i<10;i++){
				if(Foto_2.equals("F2"+i)){
					apretado_sig = i;					
				}
				Foto2[i].setVisible(false);
			}
			
			if(apretado_sig!=9){
				Foto2[apretado_sig+1].setVisible(true);
				Foto_2 = "F2"+(apretado_sig+1);
				ButtonSiguiente.setEnabled(true);
			}
			
			if(apretado_sig==8){
				ButtonSiguiente.setEnabled(false);
			}
		}
	}
		
	private void ButtonAyudaTematicaActionPerformed(java.awt.event.ActionEvent evt){
    	
		for(int i=1;i<13;i++){
			Foto1[i].setVisible(false);
		}
		
		for(int i=0;i<10;i++){
			Foto2[i].setVisible(false);
		}
    	
    	Foto1[0].setVisible(true);
    	
		ButtonSiguiente.setEnabled(true);
		ButtonAnterior.setEnabled(false);
		
		Foto_1="F10";
		Estado="AT";
   }

	private void ButtonAyudaInterfazActionPerformed(java.awt.event.ActionEvent evt){
		
		for(int i=0;i<13;i++){
			Foto1[i].setVisible(false);
		}
		
		for(int i=1;i<10;i++){
			Foto2[i].setVisible(false);
		}
		
		Foto2[0].setVisible(true);
		
		ButtonSiguiente.setEnabled(true);
		ButtonAnterior.setEnabled(false);
		
		Foto_2="F20";
		Estado="AI";
	}
}