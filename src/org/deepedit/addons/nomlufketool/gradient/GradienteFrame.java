/*
 *  *****************************************************************************
 *  ---- version information ----
 *  
 *  File Name:         GradienteFrame.java
 *  Version:           (Check for latest version in SVN repository)
 *  Written by:        Frank Leanez
 *  URL:               www.centroenergia.cl 
 *  Initial release:   August 2018
 *  
 *  
 *  ---- Description ---- 
 *   Display the Gradient toolbox app
 *   
 *   Copyright 1997-2019
 *   
 *   Rodrigo Palma Behnke 
 *   Universidad de Chile 
 *   Last Modified: (Check for latest version in SVN repository)
 * 
 *   ****************************************************************************
 */

package org.deepedit.addons.nomlufketool.gradient;

/**
 * Display the Gradient toolbox app
 * 
 * @author Frank Leanez
 */
public class GradienteFrame extends javax.swing.JDialog {

    /**
     * Display the Gradient toolbox app
     *
     * @param parent Owner of this dialog. Use of null is accepted
     * @param modal window modal flag
     */
    public GradienteFrame(javax.swing.JDialog parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }

    private void initComponents() {
        Grad Gradiente = new Grad();
        Tension Tens = new Tension();
        Parametros Param = new Parametros();
        Ayuda Ayu = new Ayuda();

        Gradiente.Metodos(Tens, Param, Ayu);
        Tens.Metodos(Gradiente, Param, Ayu);
        Param.Metodos(Gradiente, Tens, Ayu);
        Ayu.Metodos(Gradiente, Tens, Param);

        getContentPane().setLayout(new java.awt.FlowLayout());

        getContentPane().add(Gradiente);
        getContentPane().add(Tens);
        getContentPane().add(Param);
        getContentPane().add(Ayu);

        Gradiente.setVisible(true);
        Gradiente.primeraAccion();
        Tens.setVisible(false);
        Param.setVisible(false);
        Ayu.setVisible(false);
        pack();
    }
}
